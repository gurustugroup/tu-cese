<?php 

	function recursive_sanitize( $array ){

		$new_array = array( );

		foreach( $array as $key => $value ){
			
			$type = gettype( $value );

			if( $type == 'string' ){
				
				$new_array[$key] = strip_tags($value);

			} elseif ( $type == 'array' ) {

				$new_array[$key] = array();

				foreach( $value as $sub_key => $sub_value ){
					$new_array[$key][$sub_key] = strip_tags($sub_value);
				}

			}

		}

		$new_array['eker'] = $array['eker'];

		return $new_array;

	}

?>