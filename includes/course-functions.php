<?php 

	function guru_sort_class_dates( $class_array ){

		for($i=0;$i<count($class_array);$i++){

			if( isset( $class_array[$i]['class']['classes'] ) ){

				$inner_class_array = $class_array[$i]['class']['classes'];
				$temp_classes = array();

				for($c=0;$c<count($inner_class_array);$c++){
					$date = strtotime($inner_class_array[$c]['start_date']);
					$temp_classes[$date] = $inner_class_array[$c];
				}
				ksort($temp_classes);
				$class_array[$i]['class']['classes'] = $temp_classes;
			}
			
		}

		return $class_array;
		
	}

	function sort_single_class_dates( $class_array ){

		if( isset( $class_array['class']['classes'] ) ){
			
			$inner_class_array = $class_array['class']['classes'];
			$temp_classes = array();

			for($c=0;$c<count($inner_class_array);$c++){
				$date = strtotime($inner_class_array[$c]['start_date']);
				$temp_classes[$date] = $inner_class_array[$c];
			}

			ksort($temp_classes);
			$class_array['class']['classes'] = $temp_classes;
			return $class_array;

		}

	

	}

?>