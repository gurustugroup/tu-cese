<?php

    # GET CONTENTS VIA CURL
    function contents_curl($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    function META($url = null) {
        
        # Start
        $website = contents_curl($url);
        $doc = new DOMDocument();
        @$doc->loadHTML($website);
        
        # Title
        $THE = $doc->getElementsByTagName('title');
        $title = $THE->item(0)->nodeValue; 
        
        # META TAGS
        $metas = $doc->getElementsByTagName('meta');
        for ($i = 0; $i < $metas->length; $i++) {
            $meta = $metas->item($i);
            
            # DESCRIPTION 
            if($meta->getAttribute('name') == 'description') {
                $description = $meta->getAttribute('content');
            }
            
            # KEYWORDS
            if($meta->getAttribute('name') == 'keywords') {
                $keywords = $meta->getAttribute('content');
            }
        }
        
        ## RETURN ARRAY
        return array(
            'title' => @$title,
            'description' => @$description,
            'keywords' => @$keywords,
        );
    }
?>