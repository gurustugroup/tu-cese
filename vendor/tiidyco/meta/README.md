# META

## What is META?
A simple way to get the Title, Meta Description, and Meta Keywords for any website.
Licensed under the MIT license: [http://www.opensource.org/licenses/mit-license.php](http://www.opensource.org/licenses/mit-license.php)

## Requirements
    - PHP 5.3.0+
    
## Installing
```json
{
	"require": {
		"tiidyco/meta": "dev-master"
	}
}
```

## Sample Usage
```php
META($url);
/*
array(
    'title' => '[TITLE]',
    'description' => '[DESCRIPTION]',
    'keywords' => '[KEYWORDS]',
);
*/
```