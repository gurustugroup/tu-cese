<?php
	
	die('Server temporarily down for maintenance. Please contact GuRuStu. 918-582-1881');
	
	include('../config.php');
	ob_start('ob_gzhandler');
	define('DIR_WEB', dirname(__FILE__).$adminurl);
	define('DIR_MODUP', dirname(__FILE__).'/');
	define('DIR_SYS', DIR_MODUP.'/_system');
	define('DIR_CTRL', DIR_MODUP.'/controller');
	define('DIR_TMPL', DIR_MODUP.'/template');
	define('DIR_VIEW', DIR_MODUP.'/view');
	define('DIR_TOOL', DIR_MODUP.'/tools');
	
	$ru = &$_SERVER['REQUEST_URI'];
	$qmp = strpos($ru, '?');
	list($path, $params) = $qmp === FALSE
	    ? array($ru, NULL)
	    : array(substr($ru, 0, $qmp), substr($ru, $qmp + 1));
	$parts = explode('/', $path);
	$i = 0;
	foreach ($parts as $part)
	{
	    if (strlen($part) && $part !== '..' && $part !== '.') {
	        define('URI_PART_'.$i++, $part);
	    }
	}
	define('URI_PARAM', isset($params) ? '' : $params);
	define('URI_PARTS', $i);
	define('URI_PATH', $path);
	define('URI_REQUEST', $_SERVER['REQUEST_URI']);
	
	session_start();
	include DIR_SYS.'/router.php';
	include DIR_SYS.'/config.routes.php';
	
	if ($ctrl = Router::controller()) {
	    include $ctrl;
	}
	else {
	    header('HTTP/1.1 404 Not Found');
	    include DIR_VIEW.'/404.php';
	}
	


?>
