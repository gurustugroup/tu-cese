<?php include DIR_TMPL.'/header.php'; ?>
<div class="container">
	<div class="row">
		<div class="col-sm-9">
		<?php status(); ?>
		<a href="<?php echo base(); ?>admin/blogcategories/" class="btn btn-primary back-button"><i class="fa fa-angle-left"></i>&nbsp;&nbsp;Back</a>
			<form class="form-horizontal" role="form" action="?id=<?php echo $id; ?>&st=update" method="post">
				<?php echo ( !$id ) ? '<input type="hidden" name="new">' : '<input type="hidden" name="id" value="'.$id.'"/>'; ?>
				<h1><?php echo $results['name']; ?></h1>
				<?php include DIR_TOOL.'/alerts.php'; ?>
				<div class="panel panel-default">
					<div class="panel-heading">Name</div>
					<div class="panel-body">
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Title</label>
							<div class="col-sm-9">
								<input type="text" name="name" autofocus class="form-control" value="<?php echo $results['name']; ?>">
							</div>
						</div>						
					</div>
				</div>
				<button type="submit" class="btn btn-md btn-success"><i class="fa fa-check"></i> Save Page</button>
				<a href="<?php base(); ?>admin/blogcategories" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Cancel</a>
			</div>
		</div>
	</form>
</div>
<?php include DIR_TMPL.'/footer.php'; ?>