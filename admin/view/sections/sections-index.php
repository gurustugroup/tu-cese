<?php include DIR_TMPL.'/header.php'; ?>

	<div class="container">
			
		<!-- Sections -->
		<h1>Sections </h1> 
		<a href="new/">
			<button type="button" class="btn btn-primary btn-md col-md-2">New Section</button>
		</a>
		<br clear="all"/><br>
		
		<?php status(); ?>
		
		<table class="table table-hover panel" id="mytable">
			<thead>
				<tr>
					<th width="50px">#</th>
					<th>Name <i class="icon ion-android-sort"></i></th>
					<th>Public</th>
					<th width="200px">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					$cursor = $db->sections->find();
					$i = 1;
					foreach ($cursor as $doc) {
					    echo '<tr>';
					    	echo '<td>'.$i++.'</td>';
					    	echo '<td>'.$doc["name"].'</td>';
					    	if ($doc["public"] === "on") { 
					    		echo '<td><i class="fa fa-check"></i></td>';
					    	}
					    	else {
					    		echo '<td></td>';
					    	}
					    	echo '<td>';
					    		echo '<a href="edit/?id='.$doc["_id"].'&view"><button class="btn btn-primary btn-sm">Edit</button></a> <a href="?delete='.$doc["_id"].'" onclick="return ask();"><button class="btn btn-danger btn-sm">Remove</button></a>';
					    	echo '</td>';
					    echo '</tr>';
					}
				?>
			</tbody>
		</table>
			
	</div>


<?php include DIR_TMPL.'/footer.php'; ?>
