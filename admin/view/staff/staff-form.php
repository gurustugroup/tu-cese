<?php include DIR_TMPL.'/header.php'; ?>

	<div class="container">
		<div class="row">
			
		
			<div class="col-sm-9">
				
				<?php //if(isset($_POST['updated']) ) { header('Location: '.$adminurl.'pages/?st=update'); } ?>
				<form class="form-horizontal" role="form" action="?id=<?php echo $id; ?>&st=update" method="post">

					<?php echo ( !$id ) ? '<input type="hidden" name="new">' : '<input type="hidden" name="id" value="'.$id.'"/>'; ?>
					
					<!-- THE BASIC -->
					<h1><?php echo $results['fname']; ?> <?php echo $results['lname']; ?></h1>
					<?php include DIR_TOOL.'/alerts.php'; ?>

					<div class="panel panel-default">
						<div class="panel-heading">Title & URL</div>
						<div class="panel-body">
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-2 control-label">First Name</label>
								<div class="col-sm-9">
									<input type="text" name="fname" autofocus class="form-control" value="<?php echo $results['fname']; ?>">
								</div>
							</div>				
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-2 control-label">Last Name</label>
								<div class="col-sm-9">
									<input type="text" name="lname" autofocus class="form-control" value="<?php echo $results['lname']; ?>">
								</div>
							</div>				
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-2 control-label">Title</label>
								<div class="col-sm-9">
									<input type="text" name="title" autofocus class="form-control" value="<?php echo $results['title']; ?>">
								</div>
							</div>			
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-2 control-label">Phone Number</label>
								<div class="col-sm-9">
									<input type="text" name="phone" autofocus class="form-control" value="<?php echo @$results['phone']; ?>">
								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-2 control-label">Email Address</label>
								<div class="col-sm-9">
									<input type="text" name="email" autofocus class="form-control" value="<?php echo @$results['email']; ?>">
								</div>
							</div>			
						</div>
					</div>
					
					<div class="panel panel-default">
						<div class="panel-heading">Page Content</div>
						<div class="panel-body">
							<div class="form-group">
								<div class="col-sm-12">
									<textarea class="form-control editor" name="profile" rows="20"><?php echo $results["profile"] ?></textarea>
								</div>
							</div>
						</div>			
					</div>
					
					<br/>
					<?php include DIR_TOOL.'/alerts.php'; ?>

					<button type="submit" class="btn btn-md btn-success"><i class="fa fa-check"></i> Save Page</button>
					<a href="<?php base(); ?>admin/staff" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Cancel</a>
				
				</div>
				
				<div class="col-sm-3" style="padding-top: 70px;">
					<!-- Sidebar Checkboxes -->
					<div class="panel panel-default">
						<div class="panel-heading">Status</div>
						<div class="panel-body">
							<div class="checkbox">
								<label>
									<input type="checkbox" class="public" name="public" 
									
								    	<?php if ($results["public"] === "on") { 
								    		echo 'checked';
								    	}
								    	if( !$id ){
								    		echo 'checked';
								    	}  ?> > Public
								</label>
							</div>		
						</div>
					</div>
					<?php include DIR_TMPL.'/image_upload.php'; ?>
				</div>

			</div>
					
		</form>
	</div>
	
<?php include DIR_TMPL.'/footer.php'; ?>