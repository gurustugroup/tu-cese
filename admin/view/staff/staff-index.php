<?php include DIR_TMPL.'/header.php'; ?>

	<div class="container">
			
		<!-- staff -->
		<h1>Staff </h1> 
		<a href="new/">
			<button type="button" class="btn btn-primary btn-md col-md-2">New Staff Member</button>
		</a>
		<br clear="all"/><br>
		
		<div class="input-group"> <span class="input-group-addon">Search</span>
			<input id="filter" type="text" class="form-control" placeholder="Type here...">
		</div>
		
		
		<table class="table table-hover panel" id="mytable">
			<thead>
				<tr>
					<th width="50px">#</th>
					<th>Name <i class="icon ion-android-sort"></i></th>
					<th>Title</th>
					<th>Public</th>
					<th width="200px">Action</th>
				</tr>
			</thead>
			<tbody class="searchable">
				<?php 
					$cursor = $db->staff->find();
					$i = 1;
					foreach ($cursor as $doc) {
					    echo '<tr>';
					    	echo '<td>'.$i++.'</td>';
					    	echo '<td>'.$doc["fname"].' '.$doc["lname"].'</td>';
					    	echo '<td>'.$doc["title"].'</td>';
					    	if ($doc["public"] === "on") { 
					    		echo '<td><i class="fa fa-check"></i></td>';
					    	}
					    	else {
					    		echo '<td></td>';
					    	}
					    	echo '<td>';
					    		echo '<a href="edit/?id='.$doc["_id"].'&view"><button class="btn btn-primary btn-sm">Edit</button></a> <a href="?delete='.$doc["_id"].'" onclick="return ask();"><button class="btn btn-danger btn-sm">Remove</button></a>';
					    	echo '</td>';
					    echo '</tr>';
					}
				?>
			</tbody>
		</table>
			
	</div>


<?php include DIR_TMPL.'/footer.php'; ?>
