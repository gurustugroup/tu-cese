<?php include DIR_TMPL.'/header.php'; ?>

<div class="container">
	<div class="row">
		<div class="col-sm-9">
			<form class="form-horizontal" role="form" action="?id=<?php echo $id; ?>&st=update" method="post">
				<?php echo ( !$id ) ? '<input type="hidden" name="new">' : '<input type="hidden" name="id" value="'.$id.'"/>'; ?>
				<h1>Editing: <?php echo @$results['content'][0]; ?></h1>
				<div class="panel panel-default">
					<div class="panel-heading">Title</div>
					<div class="panel-body">
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Title</label>
							<div class="col-sm-9">
								<input type="hidden" name="slug" value="<?php echo @$results['slug']; ?>">
								<input type="text" id="makeslug" name="content[]" autofocus class="form-control" value="<?php echo @$results['content'][0]; ?>">
							</div>
						</div>	
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Booth Number</label>
							<div class="col-sm-9">
								<input type="text" name="content[]" autofocus class="form-control" value="<?php echo @$results['content'][1]; ?>">
							</div>
						</div>	
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Date</label>
							<div class="col-sm-9">
								<input type="text" name="content[]" autofocus class="form-control" value="<?php echo @$results['content'][2]; ?>">
							</div>
						</div>	
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Location</label>
							<div class="col-sm-9">
								<input type="text" name="content[]" autofocus class="form-control" value="<?php echo @$results['content'][3]; ?>">
							</div>
						</div>	
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Venue</label>
							<div class="col-sm-9">
								<input type="text" name="content[]" autofocus class="form-control" value="<?php echo @$results['content'][4]; ?>">
							</div>
						</div>	
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">Description</div>
					<div class="panel-body">
						<div class="form-group">
							<div class="col-sm-12">
								<textarea class="form-control editor" name="content[]" rows="20"><?php echo @$results['content'][5]; ?></textarea>
							</div>
						</div>
					</div>			
				</div>
				<br/>
				<button type="submit" class="btn btn-md btn-success"><i class="fa fa-check"></i> Save Page</button>
				<a href="<?php base(); ?>admin/glossary" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Cancel</a>
			</div>
			<div class="col-sm-3" style="padding-top: 70px">
				<?php include DIR_TMPL.'/image_upload.php'; ?>
			</div>
		</div>

	</form>
</div>

<?php include DIR_TMPL.'/footer.php'; ?>