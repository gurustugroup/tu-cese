<?php include DIR_TMPL.'/header.php'; ?>

	<div class="container">

		<h1>Exhibit Events</h1>
		<a href="new/">
			<button type="button" class="btn btn-primary btn-md col-md-2">New Exhibit Event</button>
		</a>
		<br clear="all"/><br>

		<div class="input-group"> <span class="input-group-addon">Search</span>
			<input id="filter" type="text" class="form-control" placeholder="Type here...">
		</div>

		<table class="table table-hover panel" id="mytable">
			<thead>
				<tr>
					<th width="50px">#</th>
					<th>Title <i class="icon ion-android-sort"></i></th>
					<th width="200px">Action</th>
				</tr>
			</thead>
			<tbody class="searchable">
				<?php
					$i = 1;
					$terms = $db->featuredconferences->find();
					foreach ($terms as $term) {
						echo '<tr>';
							echo '<td>'.$i++.'</td>';
					    	echo '<td>'.$term["content"][0].'</td>';
							echo '<td>';
					    		echo '<a href="edit/?id='.$term["_id"].'&view"><button class="btn btn-primary btn-sm">Edit</button></a>';
					    		echo ' <a href="?delete='.$term["_id"].'" onclick="return ask();"><button class="btn btn-danger btn-sm">Remove</button></a>';
					    	echo '</td>';
					    echo '</tr>';
					}
				?>
			</tbody>
		</table>

	</div>


<?php include DIR_TMPL.'/footer.php'; ?>
