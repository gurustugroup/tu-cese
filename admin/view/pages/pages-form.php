<?php include DIR_TMPL.'/header.php'; ?>

<div class="container">
	<div class="row">
		<div class="col-sm-9">
			<form class="form-horizontal" role="form" action="?id=<?php echo $id; ?>&st=update" method="post">
				<?php echo ( !$id ) ? '<input type="hidden" name="new">' : '<input type="hidden" name="id" value="'.$id.'"/>'; ?>
				<h1><?php echo $results['title']; ?></h1>
				<div class="panel panel-default">
					<div class="panel-heading">Title & URL</div>
					<div class="panel-body">
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-3 control-label">Title</label>
							<div class="col-sm-8">
								<input type="text" name="title" autofocus class="form-control" value="<?php echo $results['title']; ?>" required>
							</div>
						</div>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-3 control-label">Pick a top Page</label>
							<div class="col-sm-8">
								<select class="selectpicker form-control" name="base">
									<option value=""></option>
									<?php 
										$bases = array('about', 'courses', 'conferences', 'in-company-employee-development', 'blog');
										$names = array('CESE', 'Courses', 'Conferences', 'In-Company Employee Development', 'Blog');
										foreach( array_combine($bases, $names) as $base => $name ) { 
											if($results['base'] == $base) { ?>
												<option value="<?php echo $base ?>" selected><?php echo $name ?></option>
											<?php } else { ?>
												<option value="<?php echo $base ?>"><?php echo $name ?></option>
									<?php } }  ?>
								</select>
							<?php $base = ( !empty($results['base']) ) ? $results['base'] . '/' : ''; ?>
								<input type="hidden" name="slug" value="<?php echo $results['slug']; ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-3 control-label">Link to another page</label>
							<div class="col-sm-8">
								<div class="input-group">
								  <span class="input-group-addon" id="basic-addon1"><?php echo BASE_URL; ?></span>
								  <input type="text" name="pagelink" class="form-control" value="<?php echo $results['pagelink']; ?>">
								</div>
							</div>
						</div>
					</div>
				</div>
					<?php if($results['slug'] == 'faqs') { ?>
					<div class="panel panel-default">
						<div class="panel-heading">FAQs</div>
						<div class="panel-body">
							<div class="form-group">
								<div class="col-sm-12">
									<textarea class="form-control editor" name="content" rows="20"><?php echo $results["content"] ?></textarea>
								</div>
							</div>
						</div>			
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">Policies</div>
						<div class="panel-body">
							<div class="form-group">
								<div class="col-sm-12">
									<textarea class="form-control editor" name="policies" rows="20"><?php echo @$results["policies"] ?></textarea>
								</div>
							</div>
						</div>			
					</div>
					<?php } else { ?>
					<div class="panel panel-default">
						<div class="panel-heading">Page Content</div>
						<div class="panel-body">
							<div class="form-group">
								<div class="col-sm-12">
									<textarea class="form-control editor" name="content" rows="20"><?php echo $results["content"] ?></textarea>
								</div>
							</div>
						</div>			
					</div>
					<?php } ?>
				<br/>
				<?php include DIR_TOOL.'/alerts.php'; ?>
				<input type="hidden" name="link" autofocus class="form-control" value="<?php echo @$results['order']; ?>">
				<button type="submit" class="btn btn-md btn-success"><i class="fa fa-check"></i> Save Page</button>
				<a href="<?php base(); ?>admin/pages" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Cancel</a>
			</div>
			<div class="col-sm-3" style="padding-top: 70px">
				<div class="panel panel-default">
					<div class="panel-heading">Status</div>
					<div class="panel-body">
						<div class="checkbox">
							<label>
								<input type="checkbox" class="public" name="public" 
								
							    	<?php 
								    	if ($results["public"] === "on") { 
								    		echo 'checked';
								    	}
								    	if( !$id ){
								    		echo 'checked';
								    	}
							    	?> > Public
							</label>
						</div>		
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">Call-to-action</div>
					<div class="panel-body">
						<label>Title</label>
						<input type="text" name="ctitle" class="col-sm-12" value="<?php echo $results["ctitle"]; ?>">
						<br clear="all"/><br/>
						<label>Message</label>
						<textarea class="form-control" name="cmessage" rows="4"><?php echo $results["cmessage"]; ?></textarea>
						<br clear="all"/>
						<label>Link to a Page</label>
						<select class="selectpicker form-control" name="cpage">
							<option class="test">---</option>
							<?php
								$all_pages = $db->pages->find(array("public" => "on"))->sort(array('title' => 1));
								foreach( $all_pages as $p ) {
									if ($p['pagelink'] == '') {
										echo '<option ';
										if($results['cpage'] == $p['_id']) { 
											echo 'selected '; 
										}	
										echo 'value="'.$p['_id'].'">'.$p['title'].'</option>';
									}
								}
								?>
						</select>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
	
<?php include DIR_TMPL.'/footer.php'; ?>