<?php include DIR_TMPL.'/header.php'; ?>

	<div class="container">
			
		<h1>CESE Pages </h1> 
		<a href="new/">
			<button type="button" class="btn btn-primary btn-md col-md-2">Add New Page</button>
		</a>
		
		<br clear="all"/><br/>
		
		<div class="input-group"> <span class="input-group-addon">Search</span>
			<input id="filter" type="text" class="form-control" placeholder="Type here...">
		</div>
		
		
		<table class="table table-hover panel" id="mytable">
			<thead>
				<tr>
					<th width="50px">#</th>
					<th>Name <i class="icon ion-android-sort"></i></th>
					<th>Base URL</th>
					<th>Last Edit By</th>
					<th>Public</th>
					<th width="200px">Action</th>
				</tr>
			</thead>
			<tbody class="searchable">
				<?php 
					$cursor = $db->pages->find();
					$i = 1;
					foreach ($cursor as $doc) {
					    echo '<tr>';
					    	echo '<td>'.$i++.'</td>';
					    	echo '<td>'.$doc["title"];
					    	echo '</td>';
					    	$base = ( !empty($doc['base']) ) ? $doc['base'] . '/' : '';
					    	echo '<td>'. $doc["base"].' </td>';
					    	echo '<td>'.$doc["last_edit"].'</td>';
					    	if ($doc["public"] === "on") { 
					    		echo '<td><i class="fa fa-check"></i></td>';
					    	}
					    	else {
					    		echo '<td></td>';
					    	}
					    	echo '<td>';
					    		echo '<a target="_blank" class="btn btn-default btn-sm" href="'.BASE_URL .$base . $doc["slug"].'">View</a>&nbsp;';
					    		echo '<a href="edit/?id='.$doc["_id"].'&view"><button class="btn btn-primary btn-sm">Edit</button></a> <a href="?delete='.$doc["_id"].'" onclick="return ask();"><button class="btn btn-danger btn-sm">Remove</button></a>';
					    	echo '</td>';
					    echo '</tr>';
					}
				?>
			</tbody>
		</table>
	</div>


<?php include DIR_TMPL.'/footer.php'; ?>
