<?php include DIR_TMPL.'/header.php'; ?>

	<div class="container">
			
		<h1>Default Sidebar Call-to-Action</h1>
		<form class="panel panel-default" method="post" action="?">
			<div class="panel-body">
				<div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">Sidebar Message</label>
				    <div class="col-sm-10">
						<textarea name="sidebarcta" class="form-control"><?php echo $sidebar->cta->content; ?></textarea><br>
					</div>
				</div>
				<div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">Call-to-Action</label>
				    <div class="col-sm-10">
						<input name="sidebartitle" value="<?php echo $sidebar->cta->title; ?>" class="form-control"><br>
				    </div>
				</div>
				<div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">Link</label>
				    <div class="col-sm-10">
					    <div class="input-group">
							<div class="input-group-addon">http://</div>
							<input name="sidebarctaurl" value="<?php echo $sidebar->cta->slug; ?>" class="form-control">
					    </div>
						<br>
				    </div>
				</div>
				<br>
				<button class="btn btn-md btn-success" type="submit" name="cta">Save Call-to-Action Default</button>
			</div>
		</form>
		
	</div>


<?php include DIR_TMPL.'/footer.php'; ?>
