<?php include DIR_TMPL.'/header.php'; ?>

	<div class="container">
		
		<?php if(isset($_GET['st'])) {
			echo '<div class="alert alert-success" role="alert" style="margin-top: 10px;">The "MENU" was updated</div>';	
		}
		?>
		
		<h1>CESE Page order </h1> 
	
		<div class="alert alert-warning">Click and hold to drag around pages</div>
		
		<script>
			function saveorder() {
				$( ".list-group > li.list-group-item" ).each(function( index ) {
					var index = $(this).parent().children().index(this);
					index++;
					var nname = $(this).text();
					var id = $(this).attr('id');
					var item = $(this);
					$(this).children('#order').val(index);
				});
			}
		</script>
		<form action="?" method="post">
			<button type="submit" class="btn btn-md btn-success"> SAVE Page Order</button>
			<?php 
				$i = 0;
				$bases = $db->base->find(); 
				foreach($bases as $b) {
					if($b['name'] == 'Blog' || $b['name'] == 'Conferences') {
						break;
					}
					echo '<h3>'.$b['name'].'</h3>';	
					echo '<script>';
						echo '$(function() {
							$( "#sortable-'.$i.'" ).sortable();
							$( "#sortable-'.$i.'" ).disableSelection();
						});';
					echo '</script>';
					echo '<ul class="list-group" id="sortable-'.$i.'" onmousedown="saveorder()"  onmouseover="saveorder()">';
					$cursor = $pages->find(array("base" => $b['slug']))->sort(array("order" => 1));
					$i++;
					$v = 0;
					foreach ($cursor as $doc) { 
						$v++;	
						echo '<li class="list-group-item" style="cursor: move;">'.$doc['title'];
						echo '<input type="hidden" name="menu['.$i.'][menu]['.$v.'][base]" value="'.$b['slug'].'">';
						echo '<input type="hidden" name="menu['.$i.'][menu]['.$v.'][id]" value="'.$doc['_id'].'">';
						echo '<input type="hidden" name="menu['.$i.'][menu]['.$v.'][order]" id="order" value="'.$doc['order'].'">';
						echo '</li>';
					} 			
					echo '</ul>';
				}
			?>
			<button type="submit" class="btn btn-md btn-success"> SAVE Page Order</button>
		</form>
		
		<?php 
			if(isset($_POST['menu'])) {
				foreach($_POST['menu'] as $menu) {
					foreach($menu['menu'] as $item) {
						$pages->update(array("base" => $item['base'], "_id" => new MongoID($item['id'])), array('$set' => array("order" => $item['order'])));
					}
				} 
				header("Location: ?st=update");
			}
		?>
		
<?php include DIR_TMPL.'/footer.php'; ?>