<?php
	
	if( !defined('web_root') ) : die(); endif;

	include DIR_TMPL.'/header.php';
	
?>

	<!--	(11/20/14)	
	===================================================================
	======= Settings Page
	===================================================================
	-->
	<div class="container">
		
		<h1>Settings</h1>
		<?php status(); ?>

		<?php 
			if( isset( $_GET['st'] ) && $_GET['st'] == 'adminadded' ){
				echo '<div class="alert alert-success" role="alert">Administrator added.</div>';
			}

		?>
		
		<!--
		===================================================================
		======= User Accounts
		===================================================================
		-->
		<div class="row">
			<div class="col-sm-12">
		
				<div class="panel panel-default">
					<div class="panel-heading">Manage Users</div>
					<div class="panel-body">
						<table class="table table-hover panel" id="mytable">
							<thead>
								<tr>
									<th width="50px">#</th>
									<th>Name </th>
									<th>Email</th>
									<th>Change Password</th>
									<th width="200px">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									$cursor = $users->find();
									$i = 1;
									foreach ($cursor as $doc) {
									    echo '<tr>';
									    	echo '<td>'.$i++.'</td>';
									    	echo '<td>'.$doc["name"].'</td>';
									    	echo '<td>'.$doc["email"].'</td>';
									    	echo '<td><input class="form-control" id="password_'.$doc["_id"].'" type="password" placeholder="New Password"></td>';
									    	echo '<td>';
									    	echo '<button class="btn btn-primary" id="update_p_'.$doc["_id"].'">Update</button>&nbsp;';
									    	echo '<button class="btn btn-danger" id="delete_admin_'.$doc["_id"].'">Delete</button>';
									    	echo '</td>';
									    echo '</tr>';
									}
								?>
							</tbody>
						</table>
						<button type="submit" class="btn btn-md btn-success" data-toggle="modal" data-target="#user">
							<i class="fa fa-plus"></i> Create New User
						</button>
					</div>
				</div>
			</div
			
		</div>

		<!--
		===================================================================
		======= Add Admin Modal
		===================================================================
		-->
		<form action="?" method="post">
			<div class="modal fade in" id="user" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">
								<span aria-hidden="true">&times;</span>
								<span class="sr-only">Close</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">Add New Admin</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-2 control-label">Name</label>
								<div class="col-sm-9">
									<input type="text" name="adminname" class="form-control" placeholder="Name">
								</div> 
							</div><br><br>  
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-2 control-label">Email</label>
								<div class="col-sm-9">
									<input type="email" name="adminemail" class="form-control" placeholder="Email Address">
								</div> 
							</div><br><br>  
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-2 control-label">Password</label>
								<div class="col-sm-9">
									<input type="password" name="adminpassword" class="form-control" placeholder="Password">
								</div> 
							</div>    
							<br clear="all"/>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="submit" name="createadmin" class="btn btn-success">Create</button>
						</div>
					</div>
				</div>
			</div>
		</form>	
		

<?php include DIR_TMPL.'/footer.php'; ?>
