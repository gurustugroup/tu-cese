<?php include DIR_TMPL.'/header.php'; ?>

	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<?php status(); ?>
					<form class="form-horizontal" role="form" action="?" method="post">
						<h1><?php echo $tt; ?></h1>
						<div class="panel panel-default">
							<div class="panel-heading">Tab</div>
							<div class="panel-body">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label">Name</label>
									<div class="col-sm-9">
										<input type="hidden" name="order" class="form-control" value="<?php echo $results['order']; ?>">
										<input type="text" name="one_title" autofocus class="form-control" value="<?php echo $results['one_title']; ?>" required>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-12">
										<textarea class="form-control editor" name="one" rows="20"><?php echo $results["one"] ?></textarea>
									</div>
								</div>
							</div>
						</div>
						<h3>Tabs</h3>
						<div class="panel panel-default">
							<div class="panel-heading">Tab</div>
							<div class="panel-body">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label">Name</label>
									<div class="col-sm-9">
										<input type="text" name="two_title" autofocus class="form-control" value="<?php echo $results['two_title']; ?>" required>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-12">
										<textarea class="form-control editor" name="two" rows="20"><?php echo $results["two"] ?></textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">Tab</div>
							<div class="panel-body">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label">Name</label>
									<div class="col-sm-9">
										<input type="text" name="three_title" autofocus class="form-control" value="<?php echo $results['three_title']; ?>" required>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-12">
										<textarea class="form-control editor" name="three" rows="20"><?php echo $results["three"] ?></textarea>
									</div>
								</div>
							</div>
						</div>
						<h3>Promo Text</h3>
						<div class="panel panel-default">
							<div class="panel-heading">Promo Text</div>
							<div class="panel-body">
								<div class="form-group">
									<div class="col-sm-12">
										<textarea class="form-control editor" name="promo" rows="20"><?php echo @$results["promo"] ?></textarea>
									</div>
								</div>
							</div>
						</div>
						<h3>Buttons</h3>
						<?php for ($i = 0; $i < 4; $i++) {?>
							<div class="panel panel-default">
								<div class="panel-heading">Button</div>
								<div class="panel-body">
									<div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label">Title</label>
										<div class="col-sm-9">
											<div class="col-sm-12">
												<input type="text" name="title[]" autofocus class="form-control" value="<?php echo $results['title'][$i]; ?>">
											</div>
										</div>
									</div>
									<div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label">Link</label>
										<div class="col-sm-9">
											<div class="col-sm-9">
												<input type="text" name="link[]" autofocus class="form-control" value="<?php echo $results['link'][$i]; ?>">
											</div>
											<button type="button" onclick="$('.pdf_insert').show();" class="col-sm-3 btn btn-info open-PDFs">Files</button>
										</div>
									</div>
								</div>
							</div>
						<?php } ?>
						<br/>
						<button type="submit" class="btn btn-md btn-success" name="save"><i class="fa fa-check"></i> Save Page</button>
					</div>
				</div>
			</form>
		</div>
	</div>

<?php include DIR_TMPL.'/footer.php'; ?>
