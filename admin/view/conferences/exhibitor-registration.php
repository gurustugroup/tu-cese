<?php include DIR_TMPL.'/header.php'; ?>
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<form class="form-horizontal" role="form" action="?" method="post">
				<h1>Exhibitor Registration</h1>
				<h4 style="margin-top: 40px">Exhibitor Conference Representative:</h4>
				<table class="table table-hover panel" id="mytable">
					<thead>
						<tr>
							<th>Price</th>
							<th>Title</th>
						</tr>
					</thead>
					<tbody>
						<?php for ($x = 0; $x < 10; $x++) { ?>
						<tr>
							<td width="200px">
								<div class="input-group">
									<div class="input-group-addon">$</div>
									<input class="form-control" placeholder="$0.00" name="fee1[]" value="<?php echo @$results['fee1'][$x]; ?>"/>
								</div>
							</td>
							<td>
								<input class="form-control" placeholder="Title" name="title1[]" value="<?php echo @$results['title1'][$x]; ?>"/>
							</td>
						</tr>
						</tr>
						<?php } ?>
					</tbody>
				</table>
				<h4 style="margin-top: 40px">Optional Events:</h4>
				<table class="table table-hover panel" id="mytable">
					<thead>
						<tr>
							<th>Price</th>
							<th>Title</th>
						</tr>
					</thead>
					<tbody>
						<?php for ($x = 0; $x < 10; $x++) { ?>
						<tr>
							<td width="200px">
								<div class="input-group">
									<div class="input-group-addon">$</div>
									<input class="form-control" placeholder="$0.00" name="fee2[]" value="<?php echo @$results['fee2'][$x]; ?>"/>
								</div>
							</td>
							<td>
								<input class="form-control" placeholder="Title" name="title2[]" value="<?php echo @$results['title2'][$x]; ?>"/>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
				<h4 style="margin-top: 40px">Complimentary Booth Representatives (maximum of two):</h4>
				<table class="table table-hover panel" id="mytable">
					<thead>
						<tr>
							<th>Price</th>
							<th>Title</th>
						</tr>
					</thead>
					<tbody>
						<?php for ($x = 0; $x < 10; $x++) { ?>
						<tr>
							<td width="200px">
								<div class="input-group">
									<div class="input-group-addon">$</div>
									<input class="form-control" placeholder="$0.00" name="fee3[]" value="<?php echo @$results['fee3'][$x]; ?>"/>
								</div>
							</td>
							<td>
								<input class="form-control" placeholder="Title" name="title3[]" value="<?php echo @$results['title3'][$x]; ?>"/>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
				<h4 style="margin-top: 40px">Optional Events:</h4>
				<table class="table table-hover panel" id="mytable">
					<thead>
						<tr>
							<th>Price</th>
							<th>Title</th>
						</tr>
					</thead>
					<tbody>
						<?php for ($x = 0; $x < 10; $x++) { ?>
						<tr>
							<td width="200px">
								<div class="input-group">
									<div class="input-group-addon">$</div>
									<input class="form-control" placeholder="$0.00" name="fee4[]" value="<?php echo @$results['fee4'][$x]; ?>"/>
								</div>
							</td>
							<td>
								<input class="form-control" placeholder="Title" name="title4[]" value="<?php echo @$results['title4'][$x]; ?>"/>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
				<br/>
				<button type="submit" class="btn btn-md btn-success" name="save"><i class="fa fa-check"></i> Save Page</button>
			</form>
		</div>
	</div>
</div>

<?php include DIR_TMPL.'/footer.php'; ?>
