<?php include DIR_TMPL.'/header.php'; ?>

	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<?php status(); ?>			
					<form class="form-horizontal" role="form" action="?" method="post">
						<h1>Venue, Travel & Accommodations</h1>
						<div class="panel panel-default">
							<div class="panel-heading">Venue Information</div>
							<div class="panel-body">
								<div class="form-group">
									<div class="col-sm-12">
										<input type="text" name="order" value="<?php echo $results['order']; ?>" hidden required>
										<textarea class="form-control editor" name="info" rows="20"><?php echo $results["info"] ?></textarea>
									</div>
								</div>
							</div>			
						</div>
			
						<h3>Map</h3>
						<div class="panel panel-default">
							<div class="panel-heading">Map Embed</div>
							<div class="panel-body">
								<div class="form-group">
									<div class="col-sm-12">
										<textarea class="form-control" name="map" rows="20"><?php echo $results["map"] ?></textarea>
									</div>
								</div>
							</div>		
						</div>
						
						<h3>Etc.</h3>
						<div class="panel panel-default">
							<div class="panel-heading">Other Info</div>
							<div class="panel-body">
								<div class="form-group">
									<div class="col-sm-12">
										<textarea class="form-control editor" name="other" rows="20"><?php echo $results["other"] ?></textarea>
									</div>
								</div>
							</div>			
						</div>
						<br/>
						<button type="submit" class="btn btn-md btn-success" name="save"><i class="fa fa-check"></i> Save Page</button>
					</div>
				</div>	
			</form>
		</div>
	</div>
	
<?php include DIR_TMPL.'/footer.php'; ?>
