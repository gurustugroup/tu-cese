<?php include DIR_TMPL.'/header.php'; ?>

	<div class="container">
		<?php status(); ?>
		<form class="form-horizontal" role="form" action="?" method="post">
			<h2>IPEC Landing</h2>
			<div class="panel panel-default">
				<div class="panel-heading">Content</div>
				<div class="panel-body">
					<div class="form-group">
						<div class="col-sm-12">
							<textarea class="form-control editor" name="content[]" rows="20"><?php echo @$results["content"][0] ?></textarea>
						</div>
					</div>
				</div>
			</div>

			<h3>Sections</h3>
			<?php $x = -1; while($x <= 2) { $x++; ?>

				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-lg-3">
							<div  style="height: 250px">
								<a href="#" class="thumbnail" data-toggle="modal" data-target="#uploads" id="multiup" onclick="$('#myclass').val('<?php echo $x ?>')">
									<?php if($results["image"][$x]) { ?>
										<img src="<?php echo replaceHttp($results["image"][$x]); ?>" id="img_<?php echo $x ?>">
									<?php } else { ?>
										<img src="https://placehold.it/250x200" id="img_<?php echo $x ?>">
									<?php } ?>
									<input type="hidden" id="box_<?php echo $x ?>" name="image[]" value="<?php echo $results["image"][$x]; ?>"/>
								</a>
							</div>
						</div>
						<div class="col-lg-9">
							<div class="form-group">
								<div class="col-sm-12">
									<label>Title</label>
									<input class="form-control" name="title[]" value="<?php echo @$results["title"][$x] ?>">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<label>Title Link</label>
									<input class="form-control" name="link[]" value="<?php echo @$results["link"][$x] ?>">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<label>Summary</label>
									<textarea class="form-control" name="summary[]" rows="2"><?php echo @$results["summary"][$x] ?></textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<label>Button One Title</label>
									<input class="form-control" name="one_title[]" value="<?php echo @$results["one_title"][$x] ?>">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<label>Button One Link</label><br clear="all"/>
									<div class="col-sm-9" style="padding-left: 0px; margin-top: 4px;">
										<input type="text" name="one_link[]" autofocus class="form-control" value="<?php echo $results['one_link'][$x]; ?>">
									</div>
									<button type="button" onclick="$('.pdf_insert').show();" class="col-sm-3 btn btn-info open-PDFs">Files</button>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<label>Button Two Title</label>
									<input class="form-control" name="two_title[]" value="<?php echo @$results["two_title"][$x] ?>">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<label>Button Two Link</label><br clear="all"/>
									<div class="col-sm-9" style="padding-left: 0px; margin-top: 4px;">
										<input type="text" name="two_link[]" autofocus class="form-control" value="<?php echo $results['two_link'][$x]; ?>">
									</div>
									<button type="button" onclick="$('.pdf_insert').show();" class="col-sm-3 btn btn-info open-PDFs">Files</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>

			<h3>Promo Text</h3>
			<div class="panel panel-default">
				<div class="panel-heading">Promo Text</div>
				<div class="panel-body">
					<div class="form-group">
						<div class="col-sm-12">
							<textarea class="form-control editor" name="content[]" rows="20"><?php echo @$results["content"][1] ?></textarea>
						</div>
					</div>
				</div>
			</div>

			<h3>Buttons</h3>
			<?php for ($i = 0; $i < 4; $i++) {?>
				<div class="panel panel-default">
					<div class="panel-heading">Button</div>
					<div class="panel-body">
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Title</label>
							<div class="col-sm-9">
								<div class="col-sm-12">
									<input type="text" name="btitle[]" autofocus class="form-control" value="<?php echo $results['btitle'][$i]; ?>">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Link</label>
							<div class="col-sm-9">
								<div class="col-sm-9">
									<input type="text" name="blink[]" autofocus class="form-control" value="<?php echo $results['blink'][$i]; ?>">
								</div>
								<button type="button" onclick="$('.pdf_insert').show();" class="col-sm-3 btn btn-info open-PDFs">Files</button>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>

			<button type="submit" class="btn btn-md btn-success" name="save"><i class="fa fa-check"></i> Save Page</button>
		</form>
	</div>

<?php include DIR_TMPL.'/footer.php'; ?>
