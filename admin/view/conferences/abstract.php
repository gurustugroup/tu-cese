<?php include DIR_TMPL.'/header.php'; ?>
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<form class="form-horizontal" role="form" action="?" method="post">
				<h1>Abstract Submission</h1>
				<br>
				<div class="panel panel-default">
					<div class="panel-body">

							<h2>Abstract Subjects</h2>
							<table class="table table-hover panel" id="mytable">
								<thead>
									<tr>
										<th>Name</th>
									</tr>
								</thead>
								<tbody>
									<?php for ($x = 0; $x < 100; $x++) { ?>
									<tr>
										<td>
											<input class="form-control" placeholder="Title" name="subject[]" value="<?php echo @$results['subject'][$x]; ?>"/>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>

				<br/>
				<button type="submit" class="btn btn-md btn-success" name="save"><i class="fa fa-check"></i> Save Page</button>
			</form>
		</div>
	</div>
</div>

<?php include DIR_TMPL.'/footer.php'; ?>
