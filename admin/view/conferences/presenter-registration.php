<?php include DIR_TMPL.'/header.php'; ?>
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<form class="form-horizontal" role="form" action="?" method="post">
				<h1>Presenter Registration</h1>
				<table class="table table-hover panel" id="mytable">
					<thead>
						<tr>
							<th>Price</th>
							<th>Title</th>
						</tr>
					</thead>
					<tbody>
						<?php for ($x = 0; $x < 10; $x++) { ?>
						<tr>
							<td width="200px">
								<div class="input-group">
									<div class="input-group-addon">$</div>
									<input class="form-control" placeholder="$0.00" name="fee[]" value="<?php echo @$results['fee'][$x]; ?>"/>
								</div>
							</td>
							<td>
								<input class="form-control" placeholder="Title" name="title[]" value="<?php echo @$results['title'][$x]; ?>"/>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
				<br/>
				<button type="submit" class="btn btn-md btn-success" name="save"><i class="fa fa-check"></i> Save Page</button>
			</form>
		</div>
	</div>
</div>

<?php include DIR_TMPL.'/footer.php'; ?>
