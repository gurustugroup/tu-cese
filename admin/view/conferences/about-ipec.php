<?php include DIR_TMPL.'/header.php'; ?>

	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<?php status(); ?>
					<form class="form-horizontal" role="form" action="?" method="post">
						<h1>About IPEC</h1>
						<div class="panel panel-default">
							<div class="panel-heading">Title</div>
							<div class="panel-body">
								<div class="form-group">
									<div class="col-sm-12">
										<label>Title</label>
										<input type="hidden" name="order" value="<?php echo $results['order']; ?>" required>
										<input class="form-control" name="content[]" value="<?php echo @$results["content"][0] ?>">
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-12">
										<label>Button Title</label>
										<input class="form-control" name="content[]" value="<?php echo @$results["content"][1] ?>">
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-12">
										<label>Button Link</label><br clear="all"/>
										<div class="col-sm-9" style="padding-left: 0px; margin-top: 4px;">
											<input type="text" name="content[]" autofocus class="form-control" value="<?php echo $results['content'][2]; ?>">
										</div>
										<button type="button" onclick="$('.pdf_insert').show();" class="col-sm-3 btn btn-info open-PDFs">Files</button>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">Description</div>
							<div class="panel-body">
								<div class="form-group">
									<div class="col-sm-12">
										<textarea class="form-control editor" name="content[]" rows="20"><?php echo $results["content"][3] ?></textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">Video Embed</div>
							<div class="panel-body">
								<div class="form-group">
									<div class="col-sm-12">
										<textarea class="form-control" name="content[]" rows="10"><?php echo @$results["content"][4] ?></textarea>
									</div>
								</div>
							</div>
						</div>
						<br/>
						<button type="submit" class="btn btn-md btn-success" name="save"><i class="fa fa-check"></i> Save Page</button>
					</div>
				</div>
			</form>
		</div>
	</div>

<?php include DIR_TMPL.'/footer.php'; ?>
