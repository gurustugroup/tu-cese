<?php include DIR_TMPL.'/header.php'; ?>

	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<?php status(); ?>
					<form class="form-horizontal" role="form" action="?" method="post">
						<h1>Conferences Highlights</h1>
						<div class="panel panel-default">
							<div class="panel-heading">Highlights</div>
							<div class="panel-body">
								<div class="form-group">
									<div class="col-sm-12">
										<input type="text" name="order" value="<?php echo $results['order']; ?>" required hidden>
										<textarea class="form-control editor" name="highlights" rows="20"><?php echo $results["highlights"] ?></textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">Events</div>
							<div class="panel-body">
								<div class="form-group">
									<div class="col-sm-12">
										<textarea class="form-control editor" name="events" rows="20"><?php echo $results["events"] ?></textarea>
									</div>
								</div>
							</div>
						</div>
						<br/>
						<button type="submit" class="btn btn-md btn-success" name="save"><i class="fa fa-check"></i> Save Page</button>
					</div>
				</div>
			</form>
		</div>
	</div>

<?php include DIR_TMPL.'/footer.php'; ?>
