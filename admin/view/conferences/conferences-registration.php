<?php include DIR_TMPL.'/header.php'; ?>

	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<?php status(); ?>
					<form class="form-horizontal" role="form" action="?" method="post">
						<h1>Registration</h1>
						<div class="panel panel-default">
							<div class="panel-heading">Title</div>
							<div class="panel-body">
								<div class="form-group">
									<div class="col-sm-12">
										<input type="text" name="order" value="<?php echo @$results['order']; ?>" required hidden>
										<input class="form-control" name="content[]" value="<?php echo @$results["content"][0]; ?>">
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">Sub Title</div>
							<div class="panel-body">
								<div class="form-group">
									<div class="col-sm-12">
										<input class="form-control" name="content[]" value="<?php echo @$results["content"][1]; ?>">
									</div>
								</div>
							</div>
						</div>
						<h3>Buttons</h3>
						<?php for ($i = 0; $i < 3; $i++) {?>
							<div class="panel panel-default">
								<div class="panel-heading">Button</div>
								<div class="panel-body">
									<div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label">Title</label>
										<div class="col-sm-9">
											<div class="col-sm-12">
												<input type="text" name="title[]" autofocus class="form-control" value="<?php echo $results['title'][$i]; ?>">
											</div>
										</div>
									</div>
									<div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label">Link</label>
										<div class="col-sm-9">
											<div class="col-sm-9">
												<input type="text" name="link[]" autofocus class="form-control" value="<?php echo $results['link'][$i]; ?>">
											</div>
											<button type="button" onclick="$('.pdf_insert').show();" class="col-sm-3 btn btn-info open-PDFs">Files</button>
										</div>
									</div>
								</div>
							</div>
						<?php } ?>
						<h3>Registration Policies</h3>
						<div class="panel panel-default">
							<div class="panel-heading">Registration Policies</div>
							<div class="panel-body">
								<div class="form-group">
									<div class="col-sm-12">
										<textarea class="form-control editor" name="content[]" rows="20"><?php echo @$results["content"][2] ?></textarea>
									</div>
								</div>
							</div>
						</div>
						<h3>Files</h3>
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="form-group col-lg-4">
									<a href="#" class="btn btn-success form-control" data-toggle="modal" data-target="#uploads" onclick="$('#brochures').trigger('click');">
										Select / Upload File
									</a>
								</div>
								<br clear="all"/>
								<ul class="list-group file-box">
									<?php
										$z = -1;
										if(@$results["files"]) {
											foreach(@$results["files"] as $file) {
												$z++;
												echo '<li class="list-group-item pdfs">';
													echo '<span class="remove-file badge" onclick="$(this).parent().remove();">Remove</span>'.$file['file'];
													echo '<input name="files['.$z.'][file]" type="hidden" value="'.$file['file'].'"><br>';
													echo '<br><label>File Name</label>';
													echo '<input name="files['.$z.'][name]" class="form-control" type="text" value="'.$file['name'].'">';
												echo '</li>';
											}
										}
									?>
								</ul>
							</div>
						</div>
						<br/>
						<button type="submit" class="btn btn-md btn-success" name="save"><i class="fa fa-check"></i> Save Page</button>
					</div>
				</div>
			</form>
		</div>
	</div>

<?php include DIR_TMPL.'/footer.php'; ?>
