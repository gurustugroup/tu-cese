<?php include DIR_TMPL.'/header.php'; ?>

	<div class="container" id="technical-sessions" style="display: none">
		<div class="row">
			<div class="col-sm-12">
				<form class="form-horizontal" role="form" action="?" method="post">
					<input type="hidden" name="order" autofocus class="form-control" value="<?php echo @$results['order']; ?>">
					<input type="hidden" name="what" autofocus class="form-control" value="technical-sessions">
					<input type="hidden" name="page_title" autofocus class="form-control" value="Technical Sessions">
					<h1>Technical Sessions</h1>
					
					<style>
						.panel {
							margin-top: 20px;
							margin-bottom:0;
						}
						.sessions-wrapper {
							display: none;
						}
						.group .panel {
							/*display: none;*/
						}
						.panel-body {
							position: relative;
						}
						.panel-body h3 {
							margin-top:0;
							margin-bottom:0;
							position: relative;
							background-color: #eee;
							padding: 10px;
						}
						.panel-toggle, .panel-toggle-block {
							position: absolute;
							right: 10px;
							top: 6px;
						}
						.session-block {
							display: none;
						}

					</style>


					<?php for ($i = 0; $i < 4; $i++) { ?>

						<br>
						<div class="panel panel-default panel-<?php echo $i; ?>">
							<div class="panel-body">
								<div class="group">
									<h3>
										<span><?php echo @$results['title'][$i]; ?></span>
										<a class="panel-toggle-block btn btn-primary" data-class=".panel-block-<?php echo $i; ?>" href="#"><i class="fa fa-plus-square" style="font-size:16px"></i></a>
									</h3>
									<div class="panel panel-default">
										<div class="panel-body">
											<div class="form-group">
												<label for="inputEmail3" class="col-sm-1 control-label">Tab Title</label>
												<div class="col-sm-11">
													<input type="text" name="title[<?php echo $i; ?>]" autofocus class="form-control" value="<?php echo @$results['title'][$i]; ?>">
												</div>
											</div>
										</div>
									</div>
									<div class="sessions-wrapper panel-block-<?php echo $i; ?>">
									<?php for ($x = 0; $x < 18; $x++) { // sessions ?>
										<h3>
											<span>Session <?php echo $x + 1; ?></span>
											<a class="panel-toggle btn btn-primary" data-class=".session-block-<?php echo $x; ?>" href="#"><i class="fa fa-plus-square" style="font-size:16px"></i></a>
										</h3>
										<div class="panel panel-default session-block session-block-<?php echo $x; ?>">
											<div class="panel-body">
												
												<div class="form-group">
													<label for="inputEmail3" class="col-sm-2 control-label">Session Name</label>
													<div class="col-sm-10">
														<input type="text" name="session[<?php echo $i; ?>][]" autofocus class="form-control" value="<?php echo @$results['session'][$i][$x]; ?>" >
													</div><br/><br/>
													<label for="inputEmail3" class="col-sm-2 control-label">Session Content</label>
													<div class="col-sm-10">
														<textarea name="message[<?php echo $i; ?>][]" class="form-control editor"><?php echo @$results['message'][$i][$x]; ?></textarea>
													</div>
												</div>
												<label for="">Event listings</label>
												<table class="table">
													<thead>
														<tr>
															<th>Time</th>
															<th>Event</th>
															<th>Location</th>
															<th>Order</th>
														</tr>
													</thead>
													<tbody>
														<?php for ($y = 0; $y < 10; $y++) { ?>
															<tr>
																<td>
																	<div class="row">
																		<div class="col-md-4">
																			<input type="text" name="start[<?php echo $i; ?>][<?php echo $x; ?>][]" class="form-control" value="<?php echo @$results['start'][$i][$x][$y]; ?>">
																		</div>
																		<div class="col-md-1">
																			<p style="padding-top: 7px;">to</p>
																		</div>
																		<div class="col-md-4">
																			<input type="text" name="end[<?php echo $i; ?>][<?php echo $x; ?>][]" class="form-control" value="<?php echo @$results['end'][$i][$x][$y]; ?>">
																		</div>
																	</div>
																</td>
																<td><input type="text" name="event[<?php echo $i; ?>][<?php echo $x; ?>][]" class="form-control" value="<?php echo @$results['event'][$i][$x][$y]; ?>"></td>
																<td><input type="text" name="location[<?php echo $i; ?>][<?php echo $x; ?>][]" class="form-control" value="<?php echo @$results['location'][$i][$x][$y]; ?>"></td>
																<td><input type="number" name="sort_order[<?php echo $i; ?>][<?php echo $x; ?>][]" class="form-control" value="<?php echo @$results['sort_order'][$i][$x][$y]; ?>"></td>
															</tr>
														<?php } ?>
													</tbody>
												</table>

											</div>
										</div>
									<?php } ?>
									</div> <!-- end sessions-wrapper -->
								</div>

							</div>
						</div>

					<?php } ?>

					<br/>
					<button type="submit" class="btn btn-md btn-success" name="save"><i class="fa fa-check"></i> Save Page</button>
				</form>
			</div>
		</div>
	</div>
	
	<script>
		$(document).ready(function() {

			$(document).on('click', '.panel-toggle', function(e){
				e.preventDefault();
				$(this).find('.fa').toggleClass('fa-plus-square').toggleClass('fa-minus-square');
				var session = $(this).attr('data-class');
				$(session).slideToggle();
			});
			$(document).on('click', '.panel-toggle-block', function(e){
				e.preventDefault();
				$(this).find('.fa').toggleClass('fa-plus-square').toggleClass('fa-minus-square');
				var block = $(this).attr('data-class');
				$(block).slideToggle();
			});

			
			$('#loading').show();
		});
		$(window).load(function () {
			$('#technical-sessions').show();
			$('#loading').hide();
		});
	</script>

<?php include DIR_TMPL.'/footer.php'; ?>
