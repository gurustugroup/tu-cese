<?php include DIR_TMPL.'/header.php'; ?>

	<div class="container">
		<div class="row">

			<form class="form-horizontal" role="form" action="?" method="post">

				<div class="col-sm-9">
					<h1>IPEC Options</h1>
					<div class="panel panel-default">
						<div class="panel-heading">Headline</div>
						<div class="panel-body">
							<div class="form-group">
								<div class="col-sm-12">
									<input class="form-control" name="headline" value="<?php echo @$results["headline"] ?>">
								</div>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">Conferences Payment tag (Speaker Registration, Attendee Registration, Exhibitor Registration, Exhibitor OnLine Booking Form, Corporate Sponsor Online Agreement Form) </div>
						<div class="panel-body">
							<div class="form-group">
								<div class="col-sm-12">
									<input class="form-control" name="tag" value="<?php echo @$results["tag"] ?>">
								</div>
							</div>
						</div>
					</div>
					<br/>

					<h2>Sponsor Logos</h2>
					<?php $x = -1; while($x <= 14) { $x++; ?>
						<div class="col-lg-3 panel panel-default">
						<br/>
							<div  style="height: 100px; overflow: hidden;" class="logobox-<?php echo $x ?>">
								<a href="#" class="thumbnail" data-toggle="modal" data-target="#uploads" id="multiup" onclick="$('#myclass').val('<?php echo $x ?>')">
									<?php if(@@$results["logos"][$x]) { ?>
										<img src="<?php echo @@$results["logos"][$x]; ?>" id="img_<?php echo $x ?>">
									<?php } else { ?>
										<img src="http://placehold.it/250x100" id="img_<?php echo $x ?>">
									<?php } ?>
									<input type="hidden" id="box_<?php echo $x ?>" name="logos[]" value="<?php echo @@$results["logos"][$x]; ?>"/>
								</a>
							</div>
							<br/>
							<div class="form-group">
								<div class="col-sm-12">
									<label>Link</label>
									<input class="form-control" name="link[]" id="link_<?php echo $x ?>" value="<?php echo @@$results["link"][$x] ?>">
									<br>
									<label>Order</label>
									<input class="form-control" name="logo_order[]" value="<?php echo @@$results["logo_order"][$x] ?>">
									<br/>
									<?php if($results['logos'][$x]): ?>
										<button class="btn btn-danger btn-block btn-sm" type="button" onclick="deletelogo(<?php echo $x; ?>);">Delete Logo</button>
									<?php else: ?>
										<a class="btn btn-primary btn-block btn-sm" data-toggle="modal" data-target="#uploads" id="multiup" onclick="$('#myclass').val('<?php echo $x ?>')">Upload Logo</a>
									<?php endif; ?>
									<br/>
								</div>
							</div>
							<br/>
						</div>
					<?php } ?>

					<br clear="all"/><br/>
					<button type="submit" class="btn btn-md btn-success" name="save" id="end"><i class="fa fa-check"></i> Save Page</button>
				</div>

				<div class="col-sm-3">
					<div id="fixed">
						<?php include DIR_TMPL.'/image_upload.php'; ?>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">Call-to-action</div>
						<div class="panel-body">
							<label>Title</label>
							<input type="text" name="ctitle" class="col-sm-12" value="<?php echo @$results["ctitle"]; ?>">
							<br clear="all"/><br/>
							<label>Message</label>
							<textarea class="form-control" name="cmessage" rows="4"><?php echo @$results["cmessage"]; ?></textarea>
							<br clear="all"/>
							<label>Link</label>
							<input type="text" name="cpage" class="col-sm-12" value="<?php echo @$results["cpage"]; ?>">
						</div>
					</div>
				</div>

			</form>
		</div>
	</div>

	<script>
		function deletelogo(num) {
			$('#img_'+num).attr('src', '');
			$('#box_'+num).attr('value', '');
			$('#link_'+num).attr('value', '#');
			$('#end').trigger( "click" );
		}
	</script>


<?php include DIR_TMPL.'/footer.php'; ?>
