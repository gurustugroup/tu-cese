<?php include DIR_TMPL.'/header.php'; ?>

	<div class="container">
		
		<?php if(isset($_GET['st'])) {
			echo '<div class="alert alert-success" role="alert" style="margin-top: 10px;">The "MENU" was updated</div>';	
		}
		?>
		
		<h1>Conferences order </h1> 
	
		<div class="alert alert-warning">Click and hold to drag around pages</div>
		
		<script>
			function saveorder() {
				$( ".list-group > li.list-group-item" ).each(function( index ) {
					var index = $(this).parent().children().index(this);
					index++;
					var nname = $(this).text();
					var id = $(this).attr('id');
					var item = $(this);
					$(this).children('#order').val(index);
				});
				
				$(function() {
					$( "#sortable" ).sortable();
					$( "#sortable" ).disableSelection();
				});
			}
		</script>
		<form action="?" method="post">
			<button type="submit" class="btn btn-md btn-success"> SAVE Order</button>
			<br clear="all"/><br/>
			<ul class="list-group" id="sortable" onmousedown="saveorder()"  onmouseover="saveorder()">
			<?php 
				$sub = $conferences->find()->sort(array('order' => 1));
				$v = 0;
				foreach($sub as $doc) {
					$v++;
					if(!empty($doc["page_title"])) {
						echo '<li class="list-group-item" style="cursor: move;">'.$doc['page_title'];
						echo '<input type="hidden" name="menu[0][menu]['.$v.'][page_title]" value="'.$doc['page_title'].'">';
						echo '<input type="hidden" name="menu[0][menu]['.$v.'][order]" id="order" value="'.$doc['order'].'">';
						echo '</li>';
					}
				}	
			?>
			</ul>
			<button type="submit" class="btn btn-md btn-success"> SAVE Order</button>
		</form>
		
		<?php 
			if(isset($_POST['menu'])) {
				foreach($_POST['menu'] as $menu) {
					foreach($menu['menu'] as $item) {
						$conferences->update(array("page_title" => $item['page_title']), array('$set' => array("order" => $item['order'])));
					}
				} 
				header("Location: ?st=update");
			}
		?>
		
		<input type="checkbox" class="public online" hidden>
		
<?php include DIR_TMPL.'/footer.php'; ?>