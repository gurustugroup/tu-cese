<?php include DIR_TMPL.'/header.php'; ?>

	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<?php status(); ?>
					<form class="form-horizontal" role="form" action="?" method="post">
						<h1>Conference-at-a-Glance</h1>

						<?php for ($i = 0; $i < 4; $i++) { ?>
						<br/><br/>
						<h3><?php echo @$results['title'][$i]; ?></h3>
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label">Tab Title</label>
									<div class="col-sm-9">
										<input type="hidden" name="order" autofocus class="form-control" value="<?php echo $results['order']; ?>">
										<input type="text" name="title[]" autofocus class="form-control" value="<?php echo @$results['title'][$i]; ?>">
									</div>
								</div>
								<table class="table">
									<tr>
										<th>#</th>
										<th>Time</th>
										<th>Event</th>
										<th>Location</th>
										<th>Order</th>
									</tr>
									<tbody class="machine-<?php echo $i; ?>">
										<?php $c = @count($results['start'][$i]); for ($x = 0; $x < $c; $x++) { ?>
											<tr>
												<td><button type="button" class="btn btn-danger remove-clone">X</button></td>
												<td>
													<div class="row">
														<div class="col-md-4">
															<input type="text" name="start[<?php echo $i; ?>][]" class="form-control" value="<?php echo @$results['start'][$i][$x]; ?>">
														</div>
														<div class="col-md-1">
															<p style="padding-top: 7px;">to</p>
														</div>
														<div class="col-md-4">
															<input type="text" name="end[<?php echo $i; ?>][]" class="form-control" value="<?php echo @$results['end'][$i][$x]; ?>">
														</div>
													</div>
												</td>
												<td><input type="text" name="event[<?php echo $i; ?>][]" class="form-control" value="<?php echo @$results['event'][$i][$x]; ?>"></td>
												<td><input type="text" name="location[<?php echo $i; ?>][]" class="form-control" value="<?php echo @$results['location'][$i][$x]; ?>"></td>
												<td><input type="number" name="sort_order[<?php echo $i; ?>][]" class="form-control" value="<?php echo @$results['sort_order'][$i][$x]; ?>"></td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
								<button type="button" class="btn btn-md btn-success init-clone-<?php echo $i; ?>" onclick="clone(<?php echo $i; ?>)"><i class="fa fa-plus"></i> Add Row</button>
							</div>
						</div>
						<?php } ?>

						<br/>
						<button type="submit" class="btn btn-md btn-success" name="save"><i class="fa fa-check"></i> Save Page</button>
					</form>
				</div>
			</div>
		</div>
	</div>

<?php for ($i = 0; $i < 4; $i++) { ?>
	<template id="clone-<?php echo $i; ?>">
		<tr>
			<td><button type="button" class="btn btn-danger remove-clone">X</button></td>
			<td>
				<div class="row">
					<div class="col-md-4">
						<input type="text" name="start[<?php echo $i; ?>][]" class="form-control" value="">
					</div>
					<div class="col-md-1">
						<p style="padding-top: 7px;">to</p>
					</div>
					<div class="col-md-4">
						<input type="text" name="end[<?php echo $i; ?>][]" class="form-control" value="">
					</div>
				</div>
			</td>
			<td><input type="text" name="event[<?php echo $i; ?>][]" class="form-control" value=""></td>
			<td><input type="text" name="location[<?php echo $i; ?>][]" class="form-control" value=""></td>
			<td><input type="number" name="sort_order[<?php echo $i; ?>][]" class="form-control" value=""></td>
		</tr>
	</template>
<?php } ?>

<?php include DIR_TMPL.'/footer.php'; ?>
