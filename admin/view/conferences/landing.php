<?php include DIR_TMPL.'/header.php'; ?>

	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<?php status(); ?>			
					<form class="form-horizontal" role="form" action="?" method="post">
						<h1>Conferences Landing Page</h1>
						<div class="panel panel-default">
							<div class="panel-heading">Title</div>
							<div class="panel-body">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label">Title</label>
									<div class="col-sm-9">
										<input type="text" name="title" autofocus class="form-control" value="<?php echo $results['title']; ?>" required>
									</div>
								</div>	
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label">Sub Title</label>
									<div class="col-sm-9">
										<input type="text" name="subtitle" autofocus class="form-control" value="<?php echo $results['subtitle']; ?>" required>
									</div>
								</div>	
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">Description</div>
							<div class="panel-body">
								<div class="form-group">
									<div class="col-sm-12">
										<textarea class="form-control editor" name="content" rows="20"><?php echo $results["content"] ?></textarea>
									</div>
								</div>
							</div>			
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">Promo Text</div>
							<div class="panel-body">
								<div class="form-group">
									<div class="col-sm-12">
										<textarea class="form-control editor" name="promo" rows="20"><?php echo $results["promo"] ?></textarea>
									</div>
								</div>
							</div>			
						</div>
						<br/>
						<button type="submit" class="btn btn-md btn-success" name="save"><i class="fa fa-check"></i> Save Page</button>
					</div>
					
					<div class="col-sm-3">
						<div id="fixed">
							<?php include DIR_TMPL.'/image_upload.php'; ?>
						</div>
					</div>
				</div>	
			</form>
		</div>
	</div>

<?php include DIR_TMPL.'/footer.php'; ?>