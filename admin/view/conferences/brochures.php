<?php include DIR_TMPL.'/header.php'; ?>

	<div class="container">
		<?php status(); ?>			
		<form class="form-horizontal" role="form" action="?" method="post">
			<h2>Brochures</h2>
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="form-group col-lg-4">
						<a href="#" class="btn btn-success form-control" data-toggle="modal" data-target="#uploads" onclick="$('#brochures').trigger('click');">
							Select / Upload File
						</a>
					</div>
					<br clear="all"/>
					<ul class="list-group file-box">
						<?php 
							$z = -1;
							if($results["files"]) {
								foreach($results["files"] as $file) {
									$z++;
									echo '<li class="list-group-item pdfs">';
										echo '<span class="remove-file badge" onclick="$(this).parent().remove();">Remove</span>'.$file['file'];
										echo '<input name="files['.$z.'][file]" type="hidden" value="'.$file['file'].'"><br>';
										echo '<br><label>File Name</label>';
										echo '<input name="files['.$z.'][name]" class="form-control" type="text" value="'.$file['name'].'">';
									echo '</li>';
								}
							}
						?>
					</ul>	
				</div>
			</div>
			<button type="submit" class="btn btn-md btn-success" name="save"><i class="fa fa-check"></i> Save Page</button>
		</form>
	</div>
	
<?php include DIR_TMPL.'/footer.php'; ?>
