<?php include DIR_TMPL.'/header.php'; ?>

	<div class="container">
			
		<!--	(12/2/14)	
		===================================================================
		======= Courses
		===================================================================
		-->
		<h1>Public Courses</h1> 
		<a href="courses/new">
			<button type="button" class="btn btn-primary btn-md col-md-2">Add New Public Course</button>
		</a>
		
		<br clear="all"/><br/>
		
		<table class="table table-hover panel" id="mytable">
			<thead>
				<tr>
					<th width="50px">#</th>
					<th>Name <i class="icon ion-android-sort"></i></th>
					<th width="200px">Action</th>
				</tr>
			</thead>
			<tbody class="searchable">
				<?php 
					$cursor = $classes->find()->sort(array('title' => 1));
					$i = 1;
					foreach ($cursor as $doc) {
					    echo '<tr>';
					    	echo '<td>'.$i++.'</td>';
					    	echo '<td>'.$doc["title"].'</td>';									    	
					    	echo '<td>';
					    		echo '<a target="_blank" class="btn btn-default btn-sm" href="'.$baseurl.'courses/'. $doc["slug"].'">View</a>&nbsp;';
					    		echo '<a href="courses/edit/?id='.$doc["_id"].'&view"><button class="btn btn-primary btn-sm">Edit</button></a> <a href="courses/?delete='.$doc["_id"].'" onclick="return ask();"><button class="btn btn-danger btn-sm">Remove</button></a>';
					    	echo '</td>';
					    echo '</tr>';
					}
				?>
			</tbody>
		</table>
		<br/>
		
		
		<!--	(12/2/14)	
		===================================================================
		======= Courses
		===================================================================
		-->
		<h1>In-Company Courses</h1> 
		<a href="incompany/new">
			<button type="button" class="btn btn-primary btn-md col-md-2">Create New Course</button>
		</a>
		
		<br clear="all"/><br/>
		
		<table class="table">
			<thead>
				<tr>
					<th width="50px">#</th>
					<th>Name <i class="icon ion-android-sort"></i></th>
					<th>Last Edit By</th>
					<th>Public</th>
					<th width="200px">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					$cursor = $company->find()->limit(10);
					$i = 1;
					foreach ($cursor as $doc) {
					    echo '<tr>';
						    echo '<td>'.$i++.'</td>';
					    	echo '<td>'.$doc["title"].'</td>';
					    	echo '<td>'.$doc["last_edit"].'</td>';
					    	if ($doc["public"] === "on") { 
					    		echo '<td><i class="fa fa-check"></i></td>';
					    	}
					    	else {
					    		echo '<td></td>';
					    	}
					    	echo '<td>';
					    		echo '<a target="_blank" class="btn btn-default btn-sm" href="'.$baseurl.'in-company-training/'. $doc["slug"].'">View</a>&nbsp;';
					    		echo '<a href="incompany/edit/?id='.$doc["_id"].'&view"><button class="btn btn-primary btn-sm">Edit</button></a> <a href="incompany/?delete='.$doc["_id"].'" onclick="return ask();"><button class="btn btn-danger btn-sm">Remove</button></a>';
					    	echo '</td>';
					    echo '</tr>';
					}
				?>
			</tbody>
		</table>
		<br/>
		
		
		
		<!--	(11/21/14)	
		===================================================================
		======= Pages
		===================================================================
		-->
		<h1>Pages</h1> 
		<a href="pages/new/">
			<button type="button" class="btn btn-primary btn-md col-md-2">Create New Page</button>
		</a>
		<br clear="all"/><br>
		
		<table class="table table-hover panel" id="mytable">
			<thead>
				<tr>
					<th width="50px">#</th>
					<th>Name <i class="icon ion-android-sort"></i></th>
					<th>URL</th>
					<th>Last Edit By</th>
					<th>Public</th>
					<th width="200px">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					$cursor = $pages->find();
					$i = 1;
					if($cursor->count()) { 
						foreach ($cursor as $doc) {
							if($doc['content'] == '') {
								
							}
							else {
							    echo '<tr>';
							    	echo '<td>'.$i++.'</td>';
							    	echo '<td>'.$doc["title"].'</td>';
							    	$base = ( !empty($doc['base']) ) ? $doc['base'] . '/' : '';
							    	echo '<td>http://www.cese.utulsa.edu/'.$base . $doc["slug"].' </td>';
							    	echo '<td>'.$doc["last_edit"].'</td>';
							    	if ($doc["public"] === "on") { 
							    		echo '<td><i class="fa fa-check"></i></td>';
							    	}
							    	else {
							    		echo '<td></td>';
							    	}
							    	echo '<td>';
						    		echo '<a target="_blank" class="btn btn-default btn-sm" href="'.$baseurl .$base . $doc["slug"].'">View</a>&nbsp;';
						    		echo '<a href="pages/edit/?id='.$doc["_id"].'&view"><button class="btn btn-primary btn-sm">Edit</button></a> <a href="pages/?delete='.$doc["_id"].'" onclick="return ask();"><button class="btn btn-danger btn-sm">Remove</button></a>';
							    	echo '</td>';
							    echo '</tr>';
							}
						}
					}
					else {
						echo "<tr><td colspan='5'> No pages by ".$_SESSION["whois"]."</td></tr>";
					}
				?>
			</tbody>
		</table>

		
	</div>


<?php 
	include DIR_TMPL.'/footer.php'; 
?>
