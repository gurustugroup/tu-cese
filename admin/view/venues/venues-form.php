<?php include DIR_TMPL.'/header.php'; ?> 
	<div class="container">
		<div class="row">

			<div class="col-sm-9">
				<?php status(); ?>
				<form class="form-horizontal" role="form" id="new-venue-form" action="?id=<?php echo $id; ?><?php echo ($id) ? '&st=update' : ''; ?>" method="post">

					<?php echo ( !$id ) ? '<input type="hidden" name="new">' : '<input type="hidden" name="id" value="'.$id.'"/>'; ?>
					
					<!-- THE BASIC -->
					<h1><?php echo $results['name']; ?></h1>

					<div class="panel panel-default">
						<div class="panel-heading">Venue Information</div>
						<div class="panel-body">
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-2 control-label">Name</label>
								<div class="col-sm-9">
									<input type="text" name="name" autofocus class="form-control" value="<?php echo $results['name']; ?>">
								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-2 control-label">Address</label>
								<div class="col-sm-9">
									<input type="text" name="address" autofocus class="form-control" value="<?php echo $results['address']; ?>">
								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-2 control-label">City</label>
								<div class="col-sm-9">
									<input type="text" name="city" autofocus class="form-control" value="<?php echo $results['city']; ?>">
								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-2 control-label">State</label>
								<div class="col-sm-9">
									<select name="state" id="state" class="form-control">
										<?php
											$states = statesArray();
											foreach( $states as $key => $value ){
												if( $results['state'] == $value ){
													echo '<option state-code="'.$key.'" selected>' . $value . '</option>';
												} else {
													echo '<option state-code="'.$key.'">' . $value . '</option>';
												}
											}
										?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-2 control-label">ZIP</label>
								<div class="col-sm-9">
									<input type="text" name="zip" autofocus class="form-control" value="<?php echo $results['zip']; ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Country</label>
								<div class="col-sm-9">
									<select name="country" class="form-control" id="country">
										<option country-code="US">United States</option>
										<?php
											$countries = countryArray();
											foreach( $countries as $key => $value ){
												if( $results['country'] == $value ){
													echo '<option country-code="'.$key.'" selected>' . $value . '</option>';
												} else {
													echo '<option country-code="'.$key.'">' . $value . '</option>';
												}
											}

										?>
									</select>
									<input type="hidden" id="country_code" name="country_code" value="<?php echo $results['country_code']; ?>">
								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-2 control-label">Phone</label>
								<div class="col-sm-9">
									<input type="text" name="phone" autofocus class="form-control" value="<?php echo $results['phone']; ?>">
								</div>
							</div>
						</div>
					</div>

					<button type="submit" class="btn btn-md btn-success"><i class="fa fa-check"></i> Save Venue</button>
					<a href="<?php base(); ?>admin/venues" class="btn btn-danger"><i class="fa fa-times"></i> Back</a>
					<button type="button" class="btn btn-md btn-info pull-right" data-toggle="modal" data-target=".venue"><i class="fa fa-pencil"></i> Add New Venue</button>
					
				</form>
			</div>
			
		</div>
	</div>
<?php include DIR_TMPL.'/footer.php'; ?>