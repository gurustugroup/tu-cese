<?php include DIR_TMPL.'/header.php'; ?>

	<div class="container">
			
		<!-- Pages -->
		<h1>Course Venues</h1> 
		<button type="button" class="btn btn-primary btn-md col-md-2" data-toggle="modal" data-target=".venue">Add new Venue</button>
		<br clear="all"/><br>
		
		<div class="input-group"> <span class="input-group-addon">Search</span>
			<input id="filter" type="text" class="form-control" placeholder="Type here...">
		</div>
		
		
		<table class="table table-hover panel" id="mytable">
			<thead>
				<tr>
					<th width="50px">#</th>
					<th>Name <i class="icon ion-android-sort"></i></th>
					<th>Address</th>
					<th>City State Zip</th>
					<th>Phone</th>
					<th width="150px">Action</th>
				</tr>
			</thead>
			<tbody class="searchable">
				<?php 
					$cursor = $db->venues->find();
					$i = 1;
					foreach ($cursor as $doc) {
					    echo '<tr>';
					    	echo '<td>'.$i++.'</td>';
					    	echo '<td>'.$doc["name"].'</td>';
					    	echo '<td>'.$doc["address"].'</td>';
					    	echo '<td>'.$doc["city"].', '.$doc["state"].' '.$doc["zip"].'</td>';
					    	echo '<td>'.$doc["phone"].'</td>';
					    	echo '<td>';
					    		echo '<a href="edit/?id='.$doc["_id"].'&view"><button class="btn btn-primary">Edit</button></a>';
					    		venuecheck($doc["_id"]);
					    	echo '</td>';
					    echo '</tr>';
					}
				?>
			</tbody>
		</table>
			
	</div>


<?php include DIR_TMPL.'/footer.php'; ?>
