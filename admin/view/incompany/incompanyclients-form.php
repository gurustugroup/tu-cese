<?php include DIR_TMPL.'/header.php'; ?>

	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<?php status(); ?>
				<h1>In-Company Clients</h1>
				<br/>
				<form class="form-horizontal" role="form" action="?st=update" method="post">
					
					<div class="panel panel-default">
						<div class="panel-heading">CESE Around the World</div>
						<div class="panel-body">
							<div class="form-group">
								<div class="col-sm-12">
									<textarea class="form-control editor" name="world" rows="7"><?php echo $results["world"] ?></textarea>
								</div>
							</div>
						</div>			
					</div>
					
					<div class="panel panel-default">
						<div class="panel-heading">Testimonials</div>
						<div class="panel-body">
							<div class="form-group">
								<div class="col-sm-12">
									<textarea class="form-control editor" name="testimonials" rows="7"><?php echo $results["testimonials"] ?></textarea>
								</div>
							</div>
						</div>			
					</div>
					
					<input type="submit" value="Save In-Company Landing Page" class="btn btn-success" name="incompany" />
				
			</div>
			
			<div class="col-sm-3">
				
				<div style="margin-top: 100px;">
						
					<?php include DIR_TMPL.'/image_upload.php'; ?>
				
					<!-- Call to Action -->
					<div class="panel panel-default">
						<div class="panel-heading">Call-to-action</div>
						<div class="panel-body">
							<label>Title</label>
							<input type="text" name="ctitle" class="col-sm-12" value="<?php echo @$results["ctitle"] ?>">
							<br clear="all"/><br/>
							<label>Message</label>
							<textarea class="form-control" name="cmessage" rows="4"><?php echo @$results["cmessage"] ?></textarea>
							<br clear="all"/>
							<label>Link to a Page</label>
							<select class="selectpicker form-control" name="cpage">
								<option class="test">---</option>
								<?php
									$all_pages = $db->pages->find(array("public" => "on"))->sort(array('title' => 1));
									foreach( $all_pages as $p ) {
										if ($p['pagelink'] == '') {
											echo '<option ';
											if($results['cpage'] == $p['_id']) { 
												echo 'selected '; 
											}	
											echo 'value="'.$p['_id'].'">'.$p['title'].'</option>';
										}
									}
									?>
							</select>
						</div>
					</div>

				</div>

			
					
				</form>
		</div>
	</div>
	
<?php include DIR_TMPL.'/footer.php'; ?>