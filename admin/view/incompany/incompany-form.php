<?php include DIR_TMPL.'/header.php'; ?>

<div class="container">
	<div class="row">
		<div class="col-sm-9">
		<?php status(); ?>
			<form class="form-horizontal" role="form" action="?id=<?php echo $id; ?>&st=update" method="post">

				<?php echo ( !$id ) ? '<input type="hidden" name="new">' : '<input type="hidden" name="id" value="'.$id.'"/>'; ?>
				<h1><?php echo $results['title']; ?></h1>
				<div class="panel panel-default">
					<div class="panel-heading">Title</div>
					<div class="panel-body">
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Title</label>
							<div class="col-sm-9">
								<input type="text" name="slug" class="form-control" value="<?php echo $results['slug']; ?>" style="display: none">
								<input type="text" name="title" autofocus class="form-control" value="<?php echo $results['title']; ?>" required>
							</div>
						</div>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">How many days?</label>
							<div class="col-sm-9">
								<input type="text" name="length" autofocus class="form-control" value="<?php echo $results['length']; ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Public Course</label>
							<div class="col-sm-9">
							 	<select class="form-control" name="public_c">
							 		<option value="">--</option>
								 	<?php $all_courses = $db->classes->find(); foreach ($all_courses as $course) { ?>
								 	<option value="<?php echo $course['slug']; ?>" <?php if(@$results['public_c'] == @$course['slug']) { echo 'selected'; }?>><?php echo $course['title']; ?></option>
								 	<?php } ?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<?php include (DIR_TOOL.'/ajax/categories.php'); ?>
				<?php include (DIR_TOOL.'/ajax/instructors.php'); ?>
				<div class="panel panel-default">
					<div class="panel-heading">Description</div>
					<div class="panel-body">
						<div class="form-group">
							<div class="col-sm-12">
								<textarea class="form-control editor" name="content" rows="7"><?php echo $results["content"] ?></textarea>
							</div>
						</div>
					</div>			
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">Short Description</div>
					<div class="panel-body">
						<div class="form-group">
							<div class="col-sm-12">
								<textarea class="form-control editor" name="short_description" rows="7"><?php echo @$results["short_description"] ?></textarea>
							</div>
						</div>
					</div>			
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">SEO Description (Max 160 characters)</div>
					<div class="panel-body">
						<div class="form-group">
							<div class="col-sm-12">
								<textarea class="form-control" name="seo" rows="4"><?php echo @$results["seo"] ?></textarea>
							</div>
						</div>
					</div>			
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">Accreditation</div>
					<div class="panel-body">
						<div class="form-group">
							<div class="col-sm-12">
								<textarea class="form-control editor" name="accreditation" rows="7"><?php echo $results["accreditation"] ?></textarea>
							</div>
						</div>
					</div>			
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">Who Should Attend?</div>
					<div class="panel-body">
						<div class="form-group">
							<div class="col-sm-12">
								<textarea class="form-control editor" name="audience" rows="7"><?php echo @$results["audience"] ?></textarea>
							</div>
						</div>
					</div>			
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">Objectives / Benefits </div>
					<div class="panel-body">
						<div class="form-group">
							<div class="col-sm-12">
								<textarea class="form-control editor" name="objectives" rows="7"><?php echo $results["objectives"] ?></textarea>
							</div>
						</div>
					</div>			
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">Special Features</div>
					<div class="panel-body">
						<div class="form-group">
							<div class="col-sm-12">
								<textarea class="form-control editor" name="special" rows="7"><?php echo $results["special"] ?></textarea>
							</div>
						</div>
					</div>			
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">Testimonies</div>
					<div class="panel-body">
						<div class="form-group">
							<div class="col-sm-12">
								<textarea class="form-control editor" name="comments" rows="7"><?php echo @$results["testimonies"] ?></textarea>
							</div>
						</div>
					</div>			
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">Participants Comments</div>
					<div class="panel-body">
						<div class="form-group">
							<div class="col-sm-12">
								<textarea class="form-control editor" name="comments" rows="7"><?php echo $results["comments"] ?></textarea>
							</div>
						</div>
					</div>			
				</div>
				<br/>
				<?php include DIR_TOOL.'/alerts.php'; ?>
				<button type="submit" class="btn btn-md btn-success"><i class="fa fa-check"></i> Save Page</button>
				<a href="<?php base(); ?>admin/incompany" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Cancel</a>
			</div>
			<div class="col-sm-3">
				<div style="margin-top: 100px;">
					<div class="panel panel-default">
						<div class="panel-heading">Status</div>
						<div class="panel-body">
							<div class="checkbox">
								<label>
									<input type="checkbox" class="public" name="public" 
									
								    	<?php 
									    	if ($results["public"] === "on") { 
									    		echo 'checked';
									    	}
									    	if( !$id ){
									    		echo 'checked';
									    	}
								    	?> > Public
								</label>
							</div>		
						</div>
					</div>
					<?php include DIR_TMPL.'/image_upload.php'; ?>
					<div class="panel panel-default">
						<div class="panel-heading">Call-to-action</div>
						<div class="panel-body">
							<label>Title</label>
							<input type="text" name="ctitle" class="col-sm-12" value="<?php echo @$results["ctitle"] ?>">
							<br clear="all"/><br/>
							<label>Message</label>
							<textarea class="form-control" name="cmessage" rows="4"><?php echo @$results["cmessage"] ?></textarea>
							<br clear="all"/>
							<label>Link to a Page</label>
							<select class="selectpicker form-control" name="cpage">
								<option>---</option>
								<?php
									$all_pages = $db->pages->find(array("public" => "on"))->sort(array('title' => 1));
									foreach( $all_pages as $p ) {
										if ($p['pagelink'] == '') {
											echo '<option ';
											if(@$results["cpage"] == $p['_id']) { 
												echo 'selected '; 
											}	
											echo 'value="'.$p['_id'].'">'.$p['title'].'</option>';
										}
									}
									?>
							</select>
						</div>
					</div>
				</div>
		</div>
				
	</form>
</div>
	
<?php include DIR_TMPL.'/footer.php'; ?>