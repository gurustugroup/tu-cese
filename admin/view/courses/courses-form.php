<?php include DIR_TMPL.'/header.php'; ?>

<form action="?id=<?php echo $id; ?>&st=update" method="post">
	
	<?php echo ( !$id ) ? '<input type="hidden" name="new">' : '<input type="hidden" name="id" value="'.$id.'"/>'; ?>
	<div class="container">
		<div class="col-md-8">
			<br clear="all"/>
			<a href="<?php base(); ?>admin/courses">
				<button type="button" class="btn btn-primary btn-md col-md-2">Go Back</button>
			</a>
			<br clear="all"/>
			
			<?php include DIR_TOOL.'/alerts.php'; ?>	
			
			<?php
				$public = $results["public"];
				$online = $results["online"];
				$image = $results["image"];
			?>
			
			<h2><?php echo $results['title']; ?></h2>
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-2 control-label">Title</label>
							<div class="col-sm-10">
								<input type="text" name="title" class="form-control" value="<?php echo $results['title']; ?>" placeholder="Title" required>
								<input type="hidden" name="slug" class="form-control" value="<?php echo $results['slug']; ?>" placeholder="Slug">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Number of Days</label>
							<div class="col-sm-10">
								<input type="text" name="days" class="form-control" value="<?php echo $results['days']; ?>" placeholder="Number of Days">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Day of the Week</label>
							<div class="col-sm-10">
							<?php 

								$weekdays = array(
									'Sunday',
									'Monday',
									'Tuesday',
									'Wednesday',
									'Thursday',
									'Friday',
									'Saturday'
								);
							?>
								<select name="dow" id="dow" class="form-control">
									<option></option>
									<?php
										foreach( $weekdays as $day ){
											if( $results['dow'] == $day ){
												$selected = 'selected';
											} else {
												$selected = '';
											}
											echo '<option '.$selected.'>'.$day.'</option>';
										}
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Time of Class</label>
							<div class="col-sm-10">
								<input type="text" name="time" class="form-control" value="<?php echo $results['time']; ?>" placeholder="Time of Class">
							</div>
						</div>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">In-Company Course</label>
							<div class="col-sm-10">
							 	<select class="form-control" name="incompany">
								 	<option value="">--</option>
								 	<?php 
									 	$all_incompany = $db->company->find();
									 	foreach ($all_incompany as $course) { ?>
								 	<option value="<?php echo $course['slug']; ?>" 
								 	<?php if(@$results['incompany'] == $course['slug']) { echo 'selected'; }?>>
								 		<?php echo $course['title']; ?>
								 	</option>
								 	<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Accounts</label>
							<div class="col-sm-10">
							 	<select class="form-control tags_select" name="tag">
									<option>--</option>
					        		<?php 
					            		$ins = $db->tags->find();
										foreach( $ins as $in ) {
											if($in['_id'] == @$results['tag']) {
												echo '<option selected id="'.$in['_id'].'" value="'.$in['_id'].'">'.$in['name'].'</option>';
											} else {
												echo '<option id="'.$in['_id'].'" value="'.$in['_id'].'">'.$in['name'].'</option>';
											}
										}
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">SEO Description (Max 160 characters)</label>
							<div class="col-sm-10">
								<textarea class="form-control" name="seo" maxlength="160" rows="4"><?php echo trim($results['seo']); ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Search keywords</label>
							<div class="col-sm-10">
								<textarea class="form-control" name="keywords" maxlength="160" rows="4"><?php echo trim(@$results['keywords']); ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Course Description</label>
							<div class="col-sm-10">
								<textarea class="form-control editor" name="description" rows="7"><?php echo trim($results['description']); ?></textarea>
							</div>
						</div>	
					</div>
				</div>
			</div>
			
			<?php $array = array('instructors', 'categories'); foreach($array as $item) { include DIR_TOOL.'/ajax/'.$item.'.php'; } ?>
			
			
			<br/>
			<h2>Other Course Information</h2>
			<div class="panel panel-default">
				<div class="panel-body">	
					
					<?php 
						$loop =	array('one', 'two', 'three', 'four', 'five'); 
						foreach($loop as $item) {
							echo '<div class="form-group">';
								echo '<div class="col-sm-12">';
									echo '<input type="text" style="margin-bottom: 10px;" name="'.$item.'[name]" class="form-control" placeholder="Number of Days" value="'.trim($results[$item]['name']).'">';
								echo '</div>';
							echo '</div>';
							echo '<div class="form-group" style="margin-top: 10px; margin-bottom: 20px;">';
								echo '<div class="col-sm-12">';
									echo '<textarea class="form-control editor" name="'.$item.'[content]" rows="7">'.trim($results[$item]['content']).'</textarea>';
								echo '</div>';
							echo '</div><br clear="all"/><br/><br/>';
						}
					?>
											
				</div>			
			</div>
			
			<br/>
			<h2>Brochures</h2>
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="form-group col-lg-4">
						<a href="#" class="btn btn-success form-control" data-toggle="modal" data-target="#uploads" onclick="$('#brochures').trigger('click');">
							Select / Upload File
						</a>
					</div>
					<br clear="all"/>
					<ul class="list-group file-box">
						<?php 
							$z = -1;
							if($results["files"]) {
								foreach($results["files"] as $file) {
									$z++;
									echo '<li class="list-group-item pdfs">';
										echo '<span class="remove-file badge" onclick="$(this).parent().remove();">Remove</span>'.$file['file'];
										echo '<input name="files['.$z.'][file]" type="hidden" value="'.$file['file'].'"><br>';
										echo '<br><label>File Name</label>';
										echo '<input name="files['.$z.'][name]" class="form-control" type="text" value="'.$file['name'].'">';
									echo '</li>';
								}
							}
						?>
					</ul>	
				</div>
			</div>
				
			
			<br/>
			<h2>Classes / Venues</h2>
			<?php include('classes/templates.php'); ?>
			<?php 
				$ctitle = @$results["ctitle"];
				$cmessage = @$results["cmessage"];
				$cpage = @$results["cpage"];
				if(isset($_GET['id'])) {
					include('classes/exist.php');
				} 
				else {
					include('classes/form.php'); 
				}
			?>
			<div id="class_box"></div>	
			<button type="button" class="btn btn-success hideonline" id="add_class">Add Another Class / Venue</button>
			
			
			<br/><br/><br/>
			
			<?php include DIR_TOOL.'/alerts.php'; ?>	
			<button type="submit" class="btn btn-lg btn-success"><i class="fa fa-check"></i> Save Course</button>
 			<a href="<?php base(); ?>admin/courses" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Cancel</a>
		
			
		</div>
		
		<div class="col-md-3 col-md-offset-1">
		
			<div id="fixed">
				<div class="panel panel-default">
					<div class="panel-heading">Course Options</div>
					<div class="panel-body">
						<div class="checkbox">
							<label>
								<input type="checkbox" class="public" name="public" <?php 
							    	if ($public == "on") { 
							    		echo 'checked';
							    	}
							    	if( !$id ){
							    		echo 'checked';
							    	}
						    	?> > Public
							</label>
						</div>
						<hr>
						<div class="checkbox">
							<label>
								<input type="checkbox" class="online" name="online" <?php 
							    	if ($online == "on") { 
							    		echo 'checked';
							    	}
						    	?> > Online Course
							</label>
						</div>
	
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">Image</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="thumbnail">
									<input type="hidden" name="image" id="gallery-url" class="form-control" value="<?php echo $image?>">
									<?php if ($image) {  ?>
										<img src="<?php echo replaceHttp($image); ?>" id="gallery-preview" width="250px"/>
									<?php } else { ?>
										<img src="https://placehold.it/200x200" id="gallery-preview" width="250px"/>
									<?php } ?>
									<div class="caption">
										<p> 
											<a href="#" class="btn btn-success form-control" data-toggle="modal" data-target="#uploads" onclick="$('#ufile').trigger('click');">
												Set Feature Image
											</a>
											<?php if ($image) {  ?>
												<button onclick="remove_featured()" style="margin-top: 5px;" class="btn btn-danger form-control">
													Remove Image
												</button>
											<?php } ?>
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">Call-to-action</div>
					<div class="panel-body">
						<label>Title</label>
						<input type="text" name="ctitle" class="col-sm-12" value="<?php echo $ctitle; ?>">
						<br clear="all"/><br/>
						<label>Message</label>
						<textarea class="form-control" name="cmessage" rows="4"><?php echo $cmessage; ?></textarea>
						<br clear="all"/>
						<label>Link to a Page</label>
						<select class="selectpicker form-control" name="cpage">
							<option>---</option>
							<?php
								$all_pages = $db->pages->find(array("public" => "on"))->sort(array('title' => 1));
								foreach( $all_pages as $p ) {
									if ($p['pagelink'] == '') {
										echo '<option ';
										if($cpage == $p['_id']) { 
											echo 'selected '; 
										}	
										echo 'value="'.$p['_id'].'">'.$p['title'].'</option>';
									}
								}
								?>
						</select>
					</div>
				</div>	
			</div>
		</div>
		
	</div>
</form>	

<?php include DIR_TMPL.'/footer.php'; ?>	