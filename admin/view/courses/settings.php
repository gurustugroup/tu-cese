<?php include DIR_TMPL.'/header.php'; ?>

<div class="container">
	<div class="row">
		<div class="col-sm-9">
			<form class="form-horizontal" role="form" action="?" method="post">
				<h1>Course Settings</h1>
				<div class="panel panel-default">
					<div class="panel-heading">No Classes Message</div>
					<div class="panel-body">
						<div class="form-group">
							<div class="col-sm-12">
								<textarea name="expiremessage" class="form-control editor" rows="5"><?php echo $results['expiremessage']; ?></textarea>
							</div>
						</div>	
					</div>
				</div>
				<button type="submit" class="btn btn-md btn-success" name="update"><i class="fa fa-check"></i> Save</button>
			</div>
		</div>
	</form>
</div>

<?php include DIR_TMPL.'/footer.php'; ?>