<?php include DIR_TMPL.'/header.php'; ?>

	<div class="container">
			
		<!-- Pages -->
		<h1>Public Courses </h1> 
		<a href="new/">
			<button type="button" class="btn btn-primary btn-md col-md-2">Add new Public Course</button>
		</a>
		<br clear="all"/><br/>
		
		<div class="input-group"> <span class="input-group-addon">Search</span>
			<input id="filter" type="text" class="form-control" placeholder="Type here...">
		</div>
		
		<table class="table table-hover panel" id="mytable">
			<thead>
				<tr>
					<th width="50px"><center>#</center></th>
					<th>Name</th>
					<th><center>Status</center></th>
					<th>Available In-Company </th>
					<th width="200px">Action</th>
				</tr>
			</thead>
			<tbody class="searchable">
				<?php 
					$cursor = $db->classes->find()->sort(array('title' => 1));
					$i = 1;
					foreach ($cursor as $doc) {
					    echo '<tr>';	
					    
				   		 # Status
				    	$status = '<td></td>';
				    	if($doc["public"] == 'on') {
				    		$status = '<td><center><i class="fa fa-check"></i></center></td>';	
				    	}	
				    	
				    	# Incompany
				    	if($doc["incompany"]) {
				    		$incompany = '<td><center><i class="fa fa-check"></i></center></td>';	
				    	}		
				    	else {
				    		$incompany = '<td></td>';
				    	}	
					    	
					    	
				    	echo '<td><center>'.$i++.'</center></td>';
				    	echo '<td>'.$doc["title"].'</td>';
				    	echo $status;		
				    	echo $incompany;	    	
				    	echo '<td>';
				    		echo '<a target="_blank" class="btn btn-default btn-sm" href="'.BASE_URL.'courses/'. $doc["slug"].'">View</a>&nbsp;';
				    		echo '<a href="edit/?id='.$doc["_id"].'&view"><button class="btn btn-primary btn-sm">Edit</button></a> <a href="?delete='.$doc["_id"].'" onclick="return ask();"><button class="btn btn-danger btn-sm">Remove</button></a>';
				    	echo '</td>';
					    echo '</tr>';
					}
				?>
			</tbody>
		</table>
	</div>

<? include DIR_TMPL.'/footer.php'; ?>
