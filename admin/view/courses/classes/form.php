<div class="panel panel-default class-templ">
	<div class="panel-body">
		<div class="form-group">
			<div class="col-sm-12">
				<label for="inputEmail3">Start Date</label>
				<div class="row">
					<div class="col-sm-12">
						<input 
							class="form-control date" 
							name="class[classes][0][start_date]"  
							type="text" 
							placeholder="MM/DD/YYYY" 
						>
					</div>
				</div>
				<br>
				<label for="inputEmail3" class="hideonline">Ending Date</label>
				<div class="row hideonlines">
					<div class="col-sm-12 hideonline">
						<input 
							class="form-control date hideonline" 
							name="class[classes][0][end_date]"  
							type="text" 
							placeholder="MM/DD/YYYY" 
						>
					</div>
				</div>
				<br>
				<label for="inputEmail3">Course Code</label>
				<div class="row">
					<div class="col-sm-12">
						<input 
							type="text" 
							name="class[classes][0][code]" 
							class="form-control">
					</div>
				</div>
				<br/>
				<label for="inputEmail3" class="hideonline">Venue</label>
				<select class="form-control venue-select hideonline" name="class[classes][0][venue]">
					<?php 
						$all_venues = $db->venues->find();
						foreach( $all_venues as $venue ){
							echo '<option value="'.$venue['_id'].'">'.$venue['name'].' ('.$venue['address'].' '.$venue['city'].', '.$venue['state'].' '.$venue['zip'].')</option>';
						}
					?>
				</select>
				<br/>
				<button type="button" class="btn btn-danger hideonline" data-toggle="modal" data-target=".venue">Add New Venue</button>
				<br/>
				<hr>
				<label>Fees</label>
				
				<?php for ($x = 0; $x <= 8; $x++) { ?>
				<div class="row">
					<div class="col-sm-6">
						<input 
							class="form-control" 
							type="text" 
							name="class[classes][0][fee][<?php echo $x; ?>][title]"
						>
					</div>
					<div class="col-sm-4">
						
						<div class="input-group">
							<div class="input-group-addon">$</div>
							<input 
								class="form-control" 
								type="number" 
								name="class[classes][0][fee][<?php echo $x; ?>][rate]"
								placeholder="0.00"
							>
						</div>
					</div>
					<div class="col-sm-2">
						<button type="button"class="btn btn-info" data-toggle="modal" data-target="#ext_<?php echo $x ?>">Options</button>
					</div>
				</div>
				<div class="modal fade in" id="ext_<?php echo $x ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
					<div class="modal-dialog">
				    	<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">
									<span aria-hidden="true">×</span>
									<span class="sr-only">Close</span>
								</button>
								<h4 class="modal-title" id="myModalLabel">More Fee Options</h4>
				      		</div>
					  		<div class="modal-body">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-4 control-label">Multiple Registrants</label>
									<div class="col-sm-4">
										<input 
											type="checkbox" 
											name="class[classes][0][fee][<?php echo $x; ?>][m]" 
										>
									</div>
									<br clear="all">
								</div>
								<hr>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-3 control-label">Min Number of Registrants</label>
									<div class="col-sm-9">
										<input 
											type="number" 
											name="class[classes][0][fee][<?php echo $x; ?>][registrants]" 
											class="form-control" id="inputEmail3"
											placeholder="0" 
											value="0"
										>
									</div>
									<br clear="all">
								</div>
								<hr>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-3 control-label">Expires</label>
									<div class="col-sm-9">
										<input 
											class="form-control date" 
											name="class[classes][0][fee][<?php echo $x; ?>][expire]" 
											type="text" 
											placeholder="MM/DD/YYYY" 
										>
										<p style="padding: 5px 0px;">Leave Empty for no expiration date!</p>
									</div>
									<br clear="all"/>
								</div>
								<br>						
					    	</div>
							<div class="modal-footer">
					        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								<button type="button" class="btn btn-primary" data-dismiss="modal">Save Options</button>
					      	</div>
					    </div>
					</div>
				</div>
				<br/>
				<?php } ?>
			</div>
		</div>
		<div class="row">
			<div class="form-group">
				<label class="col-sm-12 control-label">Additional Information</label>
				<br/>
				<div class="col-sm-12">
					<textarea class="form-control editor" name="class[classes][0][notes]" rows="7"></textarea>
				</div>
			</div>
		</div>
		<br/>
		<button type="button" class="btn btn-danger" id="remove_class" data-dismiss="modal">Remove Class</button>
		<br/>
		<br/>
	</div>
</div>
