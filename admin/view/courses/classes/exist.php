<?php $a = -1;  if(@$results['class']['classes']) {  foreach($results['class']['classes'] as $results) { $a++; ?>
	<div class="panel panel-default class-templ">
		<div class="panel-body">
			<div class="form-group">
				<div class="col-sm-12">
					<label for="inputEmail3">Start Date</label>
					<div class="row">
						<div class="col-sm-12">
							<input 
								class="form-control date" 
								name="class[classes][<?php echo $a; ?>][start_date]"  
								type="text" 
								placeholder="MM/DD/YYYY" 
								value="<?php echo $results['start_date']; ?>"
							>
						</div>
					</div>
					<br>
					<label for="inputEmail3" class="hideonline">Ending Date</label>
					<div class="row hideonline">
						<div class="col-sm-12 hideonline">
							<input 
								class="form-control date hideonline" 
								name="class[classes][<?php echo $a; ?>][end_date]"  
								type="text" 
								placeholder="MM/DD/YYYY" 
								value="<?php echo $results['end_date']; ?>"
							>
						</div>
					</div>
					<br>
					<label for="inputEmail3">Course Code</label>
					<div class="row">
						<div class="col-sm-12">
							<input 
								type="text" 
								name="class[classes][<?php echo $a; ?>][code]" 
								class="form-control"
								value="<?php echo $results['code']; ?>">
						</div>
					</div>
					<br/>
					<label for="inputEmail3" class="hideonline">Venue</label>
					<select class="form-control venue-select hideonline" name="class[classes][<?php echo $a; ?>][venue]">
						<?php 
							$all_venues = $db->venues->find();
							foreach( $all_venues as $venue ){
								if($venue['_id'] == $results['venue']) {
									echo '<option selected value="'.$venue['_id'].'">'.$venue['name'].' ('.$venue['address'].' '.$venue['city'].', '.$venue['state'].' '.$venue['zip'].')</option>';
								}
								else {
									echo '<option value="'.$venue['_id'].'">'.$venue['name'].' ('.$venue['address'].' '.$venue['city'].', '.$venue['state'].' '.$venue['zip'].')</option>';
								}
							}
						?>
					</select>
					<br/>
					<button type="button" class="btn btn-danger hideonline" data-toggle="modal" data-target=".venue">Add New Venue</button>
					<br/>
					<hr>
					<label>Fees</label>
					
					<?php for ($x = 0; $x <= 8; $x++) { ?>
					<div class="row">
						<div class="col-sm-6">
							<input 
								class="form-control" 
								type="text" 
								name="class[classes][<?php echo $a; ?>][fee][<?php echo $x; ?>][title]"
								value="<?php echo $results['fee'][$x]['title']; ?>"
							>
						</div>
						<div class="col-sm-4">
							
							<div class="input-group">
								<div class="input-group-addon">$</div>
								<input 
									class="form-control" 
									type="number" 
									name="class[classes][<?php echo $a; ?>][fee][<?php echo $x; ?>][rate]"
									placeholder="0.00"
									value="<?php echo $results['fee'][$x]['rate']; ?>"
								>
							</div>
						</div>
						<div class="col-sm-2">
							<button type="button"class="btn btn-info" data-toggle="modal" data-target="#ext_<?php echo $a.$x ?>">Options</button>
						</div>
					</div>
					<div class="modal fade in" id="ext_<?php echo $a.$x ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" >
						<div class="modal-dialog">
					    	<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
										<span aria-hidden="true">×</span>
										<span class="sr-only">Close</span>
									</button>
									<h4 class="modal-title" id="myModalLabel">More Fee Options</h4>
					      		</div>
						  		<div class="modal-body">
									<div class="form-group">
										<label for="inputEmail3" class="col-sm-3 control-label">Multiple Registrants</label>
										<div class="col-sm-9">
											<input 
												type="checkbox" 
												name="class[classes][<?php echo $a; ?>][fee][<?php echo $x; ?>][m]" 
												<?php if(isset($results['fee'][$x]['m']) && @$results['fee'][$x]['m'] == 'on') { echo 'checked'; } ?>
											>
										</div>
										<br clear="all">
									</div>
									<hr>
									<div class="form-group">
										<label for="inputEmail3" class="col-sm-3 control-label">Min Number of Registrants</label>
										<div class="col-sm-9">
											<input 
												type="number" 
												name="class[classes][<?php echo $a; ?>][fee][<?php echo $x; ?>][registrants]" 
												class="form-control" id="inputEmail3" 
												value="<?php echo $results['fee'][$x]['registrants']; ?>"
											>
										</div>
										<br clear="all">
									</div>
									<hr>
									<div class="form-group">
										<label for="inputEmail3" class="col-sm-3 control-label">Expires</label>
										<div class="col-sm-9">
											<input 
												class="form-control date" 
												name="class[classes][<?php echo $a; ?>][fee][<?php echo $x; ?>][expire]" 
												type="text" 
												placeholder="MM/DD/YYYY" 
												value="<?php echo $results['fee'][$x]['expire']; ?>"
											>
											<p style="padding: 5px 0px;">Leave Empty for no expiration date!</p>
										</div>
										<br clear="all"/>
									</div>
									<br>						
						    	</div>
								<div class="modal-footer">
						        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									<button type="button" class="btn btn-primary" data-dismiss="modal">Save Options</button>
						      	</div>
						    </div>
						</div>
					</div>
					<br/>
					<?php } ?>
				</div>
			</div>
			<div class="row">
				<div class="form-group">
					<label class="col-sm-12 control-label">Additional Information</label>
					<br/>
					<div class="col-sm-12">
						<textarea class="form-control editor" name="class[classes][<?php echo $a; ?>][notes]" rows="7"><?php echo @$results['notes']; ?></textarea>
					</div>
				</div>
			</div>
			<br/>
			<button type="button" class="btn btn-danger" id="remove_class" data-dismiss="modal">Remove Class</button>
			<br/>
			<br/>
		</div>
	</div>

<?php } }?>
