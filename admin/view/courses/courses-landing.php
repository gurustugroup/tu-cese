<?php include DIR_TMPL.'/header.php'; ?>

	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<?php status(); ?>
				<h1>CESE Courses landing Page</h1>
				<br/>
				<form class="form-horizontal" role="form" action="?st=update" method="post">
					
					<div class="panel panel-default">
						<div class="panel-heading">Slider Message</div>
						<div class="panel-body">
							<div class="form-group">
								<div class="col-sm-12">
									<textarea class="form-control editor" name="slider_message" rows="7"><?php echo $results["slider_message"] ?></textarea>
								</div>
							</div>
						</div>			
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">The Mission</div>
						<div class="panel-body">
							<div class="panel panel-default">
								<div class="panel-heading">Mission Statement</div>
								<div class="panel-body">
									<div class="form-group">
										<div class="col-sm-12">
											<textarea class="form-control editor" name="mission_statement" rows="7"><?php echo $results["mission_statement"] ?></textarea>
										</div>
									</div>
								</div>			
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">Mission Statement Explained</div>
								<div class="panel-body">
									<div class="form-group">
										<div class="col-sm-12">
											<textarea class="form-control editor" name="mission_statement_explained" rows="7"><?php echo $results["mission_statement_explained"] ?></textarea>
										</div>
									</div>
								</div>			
							</div>
						</div>			
					</div>
										
					<input type="submit" value="Save Courses Landing Page" class="btn btn-success" name="incompany" />
				
			</div>
			
			<div class="col-sm-3" style="padding-top: 70px">
						
				<?php include DIR_TMPL.'/image_upload.php'; ?>
			
				<!-- Call to Action -->
				<div class="panel panel-default">
					<div class="panel-heading">Call-to-action</div>
					<div class="panel-body">
						<label>Title</label>
						<input type="text" name="ctitle" class="col-sm-12" value="<?php echo @$results["ctitle"] ?>">
						<br clear="all"/><br/>
						<label>Message</label>
						<textarea class="form-control" name="cmessage" rows="4"><?php echo @$results["cmessage"] ?></textarea>
						<br clear="all"/>
						<label>Link to a Page</label>
						<select class="selectpicker form-control" name="cpage">
							<option>---</option>
							<?php
								$all_pages = $db->pages->find(array("public" => "on"))->sort(array('title' => 1));
								foreach( $all_pages as $p ) {
									if ($p['pagelink'] == '') {
										echo '<option ';
										if(@$results["cpage"] == $p['_id']) { 
											echo 'selected '; 
										}	
										echo 'value="'.$p['_id'].'">'.$p['title'].'</option>';
									}
								}
								?>
						</select>
					</div>
				</div>
				</form>
		</div>
	</div>
	
<?php include DIR_TMPL.'/footer.php'; ?>