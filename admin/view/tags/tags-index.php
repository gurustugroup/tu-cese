<?php include DIR_TMPL.'/header.php'; ?>

	<div class="container">
			
		<h1>Tags</h1> 
		<a href="new/">
			<button type="button" class="btn btn-primary btn-md col-md-2">New Tag</button>
		</a>
		<br clear="all"/><br>
		
		<?php status(); ?>
		
		<table class="table table-hover panel" id="mytable">
			<thead>
				<tr>
					<th width="50px">#</th>
					<th>Name <i class="icon ion-android-sort"></i></th>
					<th width="200px">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					$cursor = $db->tags->find();
					$i = 1;
					foreach ($cursor as $doc) {
					    echo '<tr>';
					    	echo '<td>'.$i++.'</td>';
					    	echo '<td>'.$doc["name"].'</td>';					    	
					    	echo '<td>';
					    		echo '<a href="?id='.$doc["_id"].'"><button class="btn btn-primary btn-sm">Edit</button></a> ';
					    		echo '<a href="?delete='.$doc["_id"].'" onclick="return ask();"><button class="btn btn-danger btn-sm">Remove</button></a> ';
					    	echo '</td>';
					    echo '</tr>';
					}
				?>
			</tbody>
		</table>
			
	</div>


<?php include DIR_TMPL.'/footer.php'; ?>
