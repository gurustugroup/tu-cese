<?php include DIR_TMPL.'/header.php'; ?>

	<div class="container">
		<div class="row">
			<div class="col-sm-9">
			<?php status(); ?>			
				<form class="form-horizontal" role="form" action="?id=<?php echo $id; ?>&st=update" method="post">
					<?php echo ( !$id ) ? '<input type="hidden" name="new">' : '<input type="hidden" name="id" value="'.$id.'"/>'; ?>
					<h1>Editing: <?php echo $results['title']; ?></h1>
					<div class="panel panel-default">
						<div class="panel-heading">Title & URL</div>
						<div class="panel-body">
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-2 control-label">Title</label>
								<div class="col-sm-9">
									<input type="text" name="title" autofocus class="form-control" value="<?php echo $results['title']; ?>" required>
									<input type="hidden" name="slug" autofocus class="form-control" value="<?php echo $results['slug']; ?>" required>
								</div>
							</div>	
						</div>
					</div>
					<?php include (DIR_TOOL.'/ajax/blogcategories.php'); ?>
					<div class="panel panel-default">
						<div class="panel-heading">Page Content</div>
						<div class="panel-body">
							<div class="form-group">
								<div class="col-sm-12">
									<textarea class="form-control editor" name="content" rows="20"><?php echo $results["content"] ?></textarea>
								</div>
							</div>
						</div>			
					</div>
					<br/>
					<button type="submit" class="btn btn-md btn-success"><i class="fa fa-check"></i> Save Page</button>
					<a href="<?php base(); ?>admin/blog" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Cancel</a>
				</div>
				<div class="col-sm-3">
					<div id="fixed">
						<div class="panel panel-default">
							<div class="panel-heading">Status</div>
							<div class="panel-body">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="public" 
										
									    	<?php if ($results["public"] === "on") { 
									    		echo 'checked';
									    		}
										    	if( !$id ){
										    		echo 'checked';
										    	} 
										    ?> > Public
									</label>
								</div>		
							</div>
						</div>
						<?php include DIR_TMPL.'/image_upload.php'; ?>
					</div>
				</div>
			</div>	
		</form>
	</div>
	
<?php include DIR_TMPL.'/footer.php'; ?>