<?php include DIR_TMPL.'/header.php'; ?>

	<h1></h1>
	
	<div class="container">
		<div class="row">		
			<div class="col-lg-10">
				<h1>Help (Cheatsheet)</h1>
				<div class="well" style="background: #fff" name="one">
					<h2 id="one">Text Editor</h2>
					<br>
					<h3>What todo when everything looks weird?</h3>
					<img src="http://gurustudev.com/~terrillo/tu/assets/_images/help/paragraph.png" width="100%"/>
					<br clear="all"><br/><br/>
					<img src="http://gurustudev.com/~terrillo/tu/assets/_images/help/text_align.png" width="100%"/>
					<br clear="all"><br/><br/>
					<img src="http://gurustudev.com/~terrillo/tu/assets/_images/help/fonts.png" width="100%"/>
				</div>
				<br/>
				<div class="well" style="background: #fff" name="two">
					<h2 id="two">Google Maps</h2>
					<ol>
						<li>Visit Google Maps.</li>
						<li>Type in any street address or geographic location and click the Search button.</li>
						<li>Once your map is fully loaded, click on the <b>Gear</b> icon found in the lower right corner of your browser window.</li>
						<li>Select Share and embed map and a popup will appear</li>
						<li>Select the Embed tab as above and copy the code from the box. The code will begin with <b>&lt;iframe</b>.</li>
						<li>Select the code in the text box and paste in the code. </li>
						<br>
						<img src="http://gurustudev.com/~terrillo/tu/assets/_images/help/code.png" width="100%"><br>
						<li>The Google Map should appear on the page</li>
					</ol>
				</div>
				<br/>
				<div class="well" style="background: #fff" name="three">
					<h2 id="two">Insert PDFs into forms</h2>
					<ol>
						<li>Click "PDF" button on toolbar.</li>
						<li>Click "Copy to Clipboard" for desired PDF</li>
						<img src="http://gurustudev.com/~terrillo/tu/assets/_images/help/pdf.png" width="100%"><br>
						<li>Create a new link a paste new link</li>
						<img src="http://gurustudev.com/~terrillo/tu/assets/_images/help/link.png" width="100%"><br>
					</ol>
				</div>
				<div class="well" style="background: #fff" name="three">
					<h2 id="two">Insert PDFs using "Files" butotn</h2>
					<ol>
						<li>Click "Files" button</li>
						<img src="http://gurustudev.com/~terrillo/tu/assets/_images/help/pdf1.png" width="100%"><br>
						<li>Click "Select" on the "PDF" you want</li>
						<img src="http://gurustudev.com/~terrillo/tu/assets/_images/help/pdf2.png" width="100%"><br>
						<li>"Copy" the link input field</li>
						<img src="http://gurustudev.com/~terrillo/tu/assets/_images/help/pdf3.png" width="100%"><br>
						<li>Or "Upload" a new PDF</li>
						<img src="http://gurustudev.com/~terrillo/tu/assets/_images/help/pdf4.png" width="100%"><br>
						<li>"Paste" link into "Button Link"</li>
						<img src="http://gurustudev.com/~terrillo/tu/assets/_images/help/pdf5.png" width="100%"><br>
					</ol>
				</div>
			</div>
		</div>
	</div>

<?php include DIR_TMPL.'/footer.php'; ?>
