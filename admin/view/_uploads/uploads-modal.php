<!-- Modal -->
<div class="modal fade" id="uploads" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel">CESE Uploads</h4>
			</div>
			<div class="modal-body">
			
				<div id="content">
				    <br clear="all"/>
				    <?php if(isset($single_course['title'])) { ?>
						<iframe frameborder="0" src="<?php base(); ?>admin/upload/?course=<?php echo @$single_course['title']; ?>" width="100%" height="130px" id="upload-frame"></iframe>
					<?php } else { ?>
						<iframe frameborder="0" src="<?php base(); ?>admin/upload/" width="100%" height="130px" id="upload-frame"></iframe>
					<?php } ?>
					<br clear="all"/>
				    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
				        <li class="active"><a href="#Images" data-toggle="tab" id="ufile">Images</a></li>
				        <li class="getfiles"><a href="#files" data-toggle="tab" id="brochures">Brochures</a></li>
				    </ul>
				    <div id="my-tab-content" class="tab-content">
				        <div class="tab-pane active" id="Images">
				            <div id="images-ajax"></div>
				        </div>
				        <div class="tab-pane" id="files">
					        <div class="tab-pane active" id="files">
					            <div id="files-ajax"></div>
					        </div>
				        </div>
				    </div>
				</div>    

			</div>
			<div class="modal-footer">
				<br clear="all"/>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close File Browser</button>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
   
   //(12/22/14)	
    //===================================================================
    //======= Ajax IN
    //===================================================================
    jQuery(document).ready(function ($) {
        $('#tabs').tab();
		$("#files-ajax").load("<?php base(); ?>admin/files/");
		$("#images-ajax").load("<?php base(); ?>admin/images/");
		
    });
    
    
    //(12/22/14)	
    //===================================================================
    //======= File Upload
    //===================================================================
	$('#upload-frame').load(function() {
		$("#files-ajax").load("<?php base(); ?>admin/files/");
		$("#images-ajax").load("<?php base(); ?>admin/images/");
	});
	
	$('#files-ajax').click(function() {
		//$("#files-ajax").load("<?php base(); ?>admin/files/");
	});
	
	$('.getfiles').click(function() {
		$("#files-ajax").load("<?php base(); ?>admin/files/");
	});
	
	$('#images-ajax').click(function() {
		setTimeout(function(){
			$("#images-ajax").load("<?php base(); ?>admin/images/");
	    }, 300);
	});
	
	
	//(12/22/14)	
	//===================================================================
	//======= Delete File / Image
	//===================================================================
	
</script>