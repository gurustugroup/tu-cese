<?php include DIR_TMPL.'/header.php'; ?>

<div class="container">
	<div class="row">
		<div class="col-sm-9">
			<form class="form-horizontal" role="form" action="?" method="post">
				<h1>Homepage Call-to-Action</h1>
				<div class="panel panel-default">
					<div class="panel-heading">Call-to-Action Title & URL</div>
					<div class="panel-body">
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Call-to-Action Message</label>
							<div class="col-sm-9">
								<input type="text" name="content" autofocus class="form-control" value="<?php echo $results['content']; ?>" required>
							</div>
						</div>	
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Button Label</label>
							<div class="col-sm-9">
								<input type="text" name="title" autofocus class="form-control" value="<?php echo $results['title']; ?>" required>
							</div>
						</div>	
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">URL</label>
							<div class="col-sm-9">
								<div class="input-group">
									<div class="input-group-addon">http://</div>
									<input type="text" name="url" autofocus class="form-control" value="<?php echo $results['url']; ?>" required>
								</div>
							</div>
						</div>	
					</div>
				</div>
				<br/>
				<button type="submit" class="btn btn-md btn-success" name="update"><i class="fa fa-check"></i> Save Page</button>
			</div>
		</div>
	</form>
</div>

<?php include DIR_TMPL.'/footer.php'; ?>