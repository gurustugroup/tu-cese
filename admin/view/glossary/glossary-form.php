<?php include DIR_TMPL.'/header.php'; ?>

<div class="container">
	<div class="row">
		<div class="col-sm-9">
			<form class="form-horizontal" role="form" action="?id=<?php echo $id; ?>&st=update" method="post">
				<?php echo ( !$id ) ? '<input type="hidden" name="new">' : '<input type="hidden" name="id" value="'.$id.'"/>'; ?>
				<h1>Editing: <?php echo $results['term']; ?></h1>
				<div class="panel panel-default">
					<div class="panel-heading">Title & URL</div>
					<div class="panel-body">
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Title</label>
							<div class="col-sm-9">
								<input type="text" name="term" autofocus class="form-control" value="<?php echo $results['term']; ?>" required>
							</div>
						</div>	
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">Definition</div>
					<div class="panel-body">
						<div class="form-group">
							<div class="col-sm-12">
								<textarea class="form-control" name="definition" rows="20"><?php echo $results["definition"] ?></textarea>
							</div>
						</div>
					</div>			
				</div>
				<br/>
				<button type="submit" class="btn btn-md btn-success"><i class="fa fa-check"></i> Save Page</button>
				<a href="<?php base(); ?>admin/glossary" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Cancel</a>
			</div>
		</div>
	</form>
</div>

<?php include DIR_TMPL.'/footer.php'; ?>