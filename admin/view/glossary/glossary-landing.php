<?php include DIR_TMPL.'/header.php'; ?>

	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<?php status(); ?>
				<h1>Glossary landing Page</h1>
				<br/>
				<form class="form-horizontal" role="form" action="?st=update" method="post">
					<div class="panel panel-default">
						<div class="panel-heading">Page Content</div>
						<div class="panel-body">
							<div class="form-group">
								<div class="col-sm-12">
									<textarea class="form-control editor" name="content" rows="7"><?php echo $results["content"] ?></textarea>
								</div>
							</div>
						</div>			
					</div>			
					<input type="submit" value="Save Landing Page" class="btn btn-success" name="glossary" />
			</div>
			<div class="col-sm-3" style="padding-top: 70px">					
				<?php include DIR_TMPL.'/image_upload.php'; ?>
				<div class="panel panel-default">
					<div class="panel-heading">Call-to-action</div>
					<div class="panel-body">
						<label>Title</label>
						<input type="text" name="ctitle" class="col-sm-12" value="<?php echo @$results["ctitle"] ?>">
						<br clear="all"/><br/>
						<label>Message</label>
						<textarea class="form-control" name="cmessage" rows="4"><?php echo @$results["cmessage"] ?></textarea>
						<br clear="all"/>
						<label>Link to a Page</label>
						<select class="selectpicker form-control" name="cpage">
							<option class="test"></option>
							<?php foreach( $all_pages as $p ) : ?>
								<option <?php echo ( @$results['cpage'] == $p['_id'] ) ? 'selected' : ''; ?> value="<?php echo $p['_id']; ?>"><?php echo $p['title']; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
			</form>
		</div>
	</div>
	
<?php include DIR_TMPL.'/footer.php'; ?>