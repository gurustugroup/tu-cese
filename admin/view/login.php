<?php include DIR_TMPL.'/header.php'; ?>

	<div class="container">

      <form class="form-signin col-md-offset-4 col-sm-4" role="form" action="" method="post">
		
		<center>
			<img src="<?php image('logo.png'); ?>" class="col-sm-12" />
		</center>
      	
      	<br clear="all"/>
      	
		<?php  
			
			#(11/21/14)	
			#===================================================================
			#======= Login Error
			#===================================================================		
			if(isset($_GET['login'])) { 
				echo '<div class="alert alert-danger" role="alert">Wrong Password / Email</div>';
			}  
			
		?>
		<input type="text" name="email" class="form-control space-it" placeholder="Email Address" required autofocus>
		<input type="password" name="password" class="form-control space-it" placeholder="Password" autocomplete="off" required>
		
		<br/>
		<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
		
		</form>

    </div>

<?php include DIR_TMPL.'/footer.php'; ?>
