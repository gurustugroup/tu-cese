<?php
	include DIR_TMPL.'/header.php'; 
	if(isset($_GET['archive'])) {
		$db->attendees->update(
		    array( '_id' => new MongoID($_GET['archive']) ),
		    array( '$set' => array('status' => 'archive') )
		);
		header('Location: ?');
	}
?>

<div class="container">
		
	<h1>Conference Registrations </h1> 
	
	<table class="table table-hover panel" id="mytable">
		<thead>
			<tr>
				<th width="50px">#</th>
				<th>Name <i class="icon ion-android-sort"></i></th>
				<th>Type</th>
				<th>Time</th>
				<th>Confirmation</th>
				<th>Total</th>
				<th width="250px">Action</th>
			</tr>
		</thead>
		<tbody class="searchable">
			<?php 
				$cursor = $db->attendees->find()->sort(array('date' => -1));
				$list = 0;
				foreach ($cursor as $doc) {
					if($doc['status'] == 'archive') {
					}
					else {
					$list++;
				    echo '<tr>';
				    	echo '<td>'.$list.'</td>';
				    	echo '<td>'.ucfirst($doc["FirstName"][0]).' '.ucfirst($doc["LastName"][0]).'</td>';
				    	echo '<td>'.ucfirst($doc["type"]).'</td>';	
				    	echo '<td>'.$doc["date"].'</td>';	
				    	echo '<td>#'.$doc["confirmation"].'</td>';	
				    	echo '<td>$'.number_format($doc['eker'], 2).'</td>';			    	
				    	echo '<td><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#open-'.$list.'">View Full Entry</button> ';
				    	echo '<a class="btn btn-danger btn-sm" href="?archive='.$doc["_id"].'">Archive</a> </td>';
				    	?>
				    	
				    	
				    	<!-- Modal -->
						<div class="modal fade" id="open-<?php echo $list; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel"><?php echo ucfirst($doc["FirstName"][0]).' '.ucfirst($doc["LastName"][0]); ?></h4>
									</div>
									<div class="modal-body">
										
										<!-- Company -->
										<h3 style="margin-left: 30px; color: #205180">Company Information</h3><br/>
										<dl class="dl-horizontal">
											<dt class="allcaps" style="opacity: 0.5;">Company</dt>
											<dd><?php echo $doc["company"]; ?></dd>
										</dl>
										<dl class="dl-horizontal">
											<dt class="allcaps" style="opacity: 0.5;">Company Address</dt>
											<dd><?php echo $doc["address"]; ?><br/><?php echo $doc["city"]; ?>, <?php echo $doc["state"]; ?> <?php echo $doc["zip"]; ?></dd>
										</dl>
										<hr>
										
										<!-- Registrations -->
										<h3 style="margin-left: 30px; color: #205180">Registrations</h3><br/>
										<?php $i = -1; foreach($doc['FirstName'] as $key) { $i++ ?>
											<dl class="dl-horizontal">
												<dt class="allcaps" style="opacity: 0.5;">Name</dt>
												<dd><?php echo ucfirst($doc["FirstName"][$i]); ?> <?php echo ucfirst($doc["LastName"][$i]); ?></dd>
											</dl>
											<dl class="dl-horizontal">
												<dt class="allcaps" style="opacity: 0.5;">Name on Badge</dt>
												<dd><?php echo ucfirst($doc["nameonbadge"][$i]); ?></dd>
											</dl>
											<dl class="dl-horizontal">
												<dt class="allcaps" style="opacity: 0.5;">Title</dt>
												<dd><?php echo $doc["title"][$i]; ?></dd>
											</dl>
											<dl class="dl-horizontal">
												<dt class="allcaps" style="opacity: 0.5;">Department</dt>
												<dd><?php echo $doc["department"][$i]; ?></dd>
											</dl>
											<dl class="dl-horizontal">
												<dt class="allcaps" style="opacity: 0.5;">Phone</dt>
												<dd><?php echo $doc["phone"][$i]; ?></dd>
											</dl>
											<dl class="dl-horizontal">
												<dt class="allcaps" style="opacity: 0.5;">Fax</dt>
												<dd><?php echo $doc["fax"][$i]; ?></dd>
											</dl>
											<dl class="dl-horizontal">
												<dt class="allcaps" style="opacity: 0.5;">Email</dt>
												<dd><a href="mailto:<?php echo $doc["email"][$i]; ?>"><?php echo $doc["email"][$i]; ?></a></dd>
											</dl>
											<?php if(isset($doc["online"])) { ?>
												<dl class="dl-horizontal">
													<dt class="allcaps" style="opacity: 0.5;">Start Date</dt>
													<dd><?php echo date('M j, Y', strtotime($doc["online"][$i])); ?></dd>
												</dl>
											<?php } ?>
											<hr>
										<br/>
										<h2 style="margin-left: 30px; color: #205180">Total: $<?php echo number_format($doc['eker'], 2); ?></h2>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal" disabled="disabled">$<?php echo number_format($doc['eker'], 2); ?></button>
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>	
											
					<?php echo '</td></tr>'; }
				} }
			?>
		</tbody>
	
	</table>
	
	<br/>
	<h4>Archived</h4> 
	<table class="table table-hover panel" id="mytable">
		<thead>
			<tr>
				<th width="50px">#</th>
				<th>Name <i class="icon ion-android-sort"></i></th>
				<th>Type</th>
				<th>Time</th>
				<th>Confirmation</th>
				<th>Total</th>
				<th width="250px">Action</th>
			</tr>
		</thead>
		<tbody class="searchable">
			<?php 
				$cursor = $db->attendees->find();
				$list = 0;
				foreach ($cursor as $doc) {
					if($doc['status'] == 'archive') {
					$list++;
				    echo '<tr>';
				    	echo '<td>'.$list.'</td>';
				    	echo '<td>'.ucfirst($doc["FirstName"][0]).' '.ucfirst($doc["LastName"][0]).'</td>';
				    	echo '<td>'.ucfirst($doc["type"]).'</td>';	
				    	echo '<td>'.$doc["date"].'</td>';	
				    	echo '<td>#'.$doc["confirmation"].'</td>';	
				    	echo '<td>$'.number_format($doc['eker'], 2).'</td>';			    	
				    	echo '<td><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#open-'.$list.'">View Full Entry</button></td>';
				    	?>
				    	
				    	
				    	<!-- Modal -->
						<div class="modal fade" id="open-<?php echo $list; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel"><?php echo ucfirst($doc["FirstName"][0]).' '.ucfirst($doc["LastName"][0]); ?></h4>
									</div>
									<div class="modal-body">
										
										<!-- Company -->
										<h3 style="margin-left: 30px; color: #205180">Company Information</h3><br/>
										<dl class="dl-horizontal">
											<dt class="allcaps" style="opacity: 0.5;">Company</dt>
											<dd><?php echo $doc["company"]; ?></dd>
										</dl>
										<dl class="dl-horizontal">
											<dt class="allcaps" style="opacity: 0.5;">Company Address</dt>
											<dd><?php echo $doc["address"]; ?><br/><?php echo $doc["city"]; ?>, <?php echo $doc["state"]; ?> <?php echo $doc["zip"]; ?></dd>
										</dl>
										<hr>
										
										<!-- Registrations -->
										<h3 style="margin-left: 30px; color: #205180">Registrations</h3><br/>
										<?php $i = -1; foreach($doc['FirstName'] as $key) { $i++ ?>
											<dl class="dl-horizontal">
												<dt class="allcaps" style="opacity: 0.5;">Name</dt>
												<dd><?php echo ucfirst($doc["FirstName"][$i]); ?> <?php echo ucfirst($doc["LastName"][$i]); ?></dd>
											</dl>
											<dl class="dl-horizontal">
												<dt class="allcaps" style="opacity: 0.5;">Name on Badge</dt>
												<dd><?php echo ucfirst($doc["nameonbadge"][$i]); ?></dd>
											</dl>
											<dl class="dl-horizontal">
												<dt class="allcaps" style="opacity: 0.5;">Title</dt>
												<dd><?php echo $doc["title"][$i]; ?></dd>
											</dl>
											<dl class="dl-horizontal">
												<dt class="allcaps" style="opacity: 0.5;">Department</dt>
												<dd><?php echo $doc["department"][$i]; ?></dd>
											</dl>
											<dl class="dl-horizontal">
												<dt class="allcaps" style="opacity: 0.5;">Phone</dt>
												<dd><?php echo $doc["phone"][$i]; ?></dd>
											</dl>
											<dl class="dl-horizontal">
												<dt class="allcaps" style="opacity: 0.5;">Fax</dt>
												<dd><?php echo $doc["fax"][$i]; ?></dd>
											</dl>
											<dl class="dl-horizontal">
												<dt class="allcaps" style="opacity: 0.5;">Email</dt>
												<dd><a href="mailto:<?php echo $doc["email"][$i]; ?>"><?php echo $doc["email"][$i]; ?></a></dd>
											</dl>
											<?php if(isset($doc["online"])) { ?>
												<dl class="dl-horizontal">
													<dt class="allcaps" style="opacity: 0.5;">Start Date</dt>
													<dd><?php echo date('M j, Y', strtotime($doc["online"][$i])); ?></dd>
												</dl>
											<?php } ?>
											<hr>
										<br/>
										<h2 style="margin-left: 30px; color: #205180">Total: $<?php echo number_format($doc['eker'], 2); ?></h2>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal" disabled="disabled">$<?php echo number_format($doc['eker'], 2); ?></button>
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>	
											
					<?php echo '</td></tr>'; }
				} }
			?>
		</tbody>
	
	</table>
	
</div>
<?php include DIR_TMPL.'/footer.php'; ?>