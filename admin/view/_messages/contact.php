<?php
	include DIR_TMPL.'/header.php'; 
	if(isset($_GET['archive'])) {
		$db->messages->update(
		    array( '_id' => new MongoID($_GET['archive']) ),
		    array( '$set' => array('status' => 'archive') )
		);
		header('Location: ?');
	}
?>

<div class="container">
		
	<h1>Contact Messages </h1> 
	
	<table class="table table-hover panel" id="mytable">
		<thead>
			<tr>
				<th width="50px">#</th>
				<th>Name <i class="icon ion-android-sort"></i></th>
				<th>To</th>
				<th>Time</th>
				<th width="250px">Action</th>
			</tr>
		</thead>
		<tbody class="searchable">
			<?php 
				$cursor = $db->messages->find(array('type' => 'contact'))->sort(array('date' => -1));
				$i = 1;
				foreach ($cursor as $doc) {
					if($doc['status'] != 'archive') {
				    echo '<tr>';
				    	echo '<td>'.$i++.'</td>';
				    	echo '<td>'.ucfirst($doc["name"]).'</td>';
				    	if ($doc["to"] == 'contact') {
				    		echo '<td>CESE Staff</td>';
				    	}
				    	else {
				    		echo '<td>'.$doc["to"].'</td>';
				    	}
				    	echo '<td>'.$doc["time"].'</td>';
				    	echo '<td>';
				    	echo '<button class="btn btn-primary btn-sm"  data-toggle="modal" data-target="#open-'.$i.'">View Full Entry</button> ';
				    	echo '<a href="?archive='.$doc["_id"].'" onclick="return ask();"><button class="btn btn-danger btn-sm">Archive</button></a>';
				    	?>
				    	
				    	<!-- Modal -->
						<div class="modal fade" id="open-<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel"><?php echo $doc["name"]; ?></h4>
									</div>
									<div class="modal-body">
										<?php 
											unset($doc['_id']);
											unset($doc['status']);
											unset($doc['type']);
											foreach ($doc as $key => $value) { 
												echo '<dl class="dl-horizontal">';
													if($key == 'email') {
														echo '<dt class="allcaps" style="opacity: 0.5;">'.$key.'</dt><dd><a href="mailto:'.$value.'">'.$value.'</a></dd>';
													}
													else {
														echo '<dt class="allcaps" style="opacity: 0.5;">'.$key.'</dt><dd>'.$value.'</dd>';
													}
												echo '</dl>';		
											} 
										?>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>	
											
					<?php echo '</td></tr>'; }
				}
			?>
		</tbody>
	
	</table>
	
	<br/>
	<h4>Archived</h4> 
	<table class="table table-hover panel" id="mytable">
		<thead>
			<tr>
				<th width="50px">#</th>
				<th>Name <i class="icon ion-android-sort"></i></th>
				<th>To</th>
				<th>Time</th>
				<th width="250px">Action</th>
			</tr>
		</thead>
		<tbody class="searchable">
			<?php 
				$cursor = $db->messages->find(array('type' => 'contact'));
				$i = 1;
				foreach ($cursor as $doc) {
					if($doc['status'] == 'archive') {
				    echo '<tr>';
				    	echo '<td>'.$i++.'</td>';
				    	echo '<td>'.ucfirst($doc["name"]).'</td>';
				    	if ($doc["to"] == 'contact') {
				    		echo '<td>CESE Staff</td>';
				    	}
				    	else {
				    		echo '<td>'.$doc["to"].'</td>';
				    	}
				    	echo '<td>'.$doc["time"].'</td>';
				    	echo '<td>';
				    	echo '<button class="btn btn-primary btn-sm"  data-toggle="modal" data-target="#open-'.$i.'">View Full Entry</button> ';
				    	echo '<a href="?read='.$doc["_id"].'" onclick="return ask();"><button class="btn btn-danger btn-sm">Delete</button></a>';
				    	?>
				    	
				    	<!-- Modal -->
						<div class="modal fade" id="open-<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel"><?php echo $doc["name"]; ?></h4>
									</div>
									<div class="modal-body">
										<?php 
											unset($doc['_id']);
											unset($doc['status']);
											unset($doc['type']);
											foreach ($doc as $key => $value) { 
												echo '<dl class="dl-horizontal">';
													if($key == 'email') {
														echo '<dt class="allcaps" style="opacity: 0.5;">'.$key.'</dt><dd><a href="mailto:'.$value.'">'.$value.'</a></dd>';
													}
													else {
														echo '<dt class="allcaps" style="opacity: 0.5;">'.$key.'</dt><dd>'.$value.'</dd>';
													}
												echo '</dl>';		
											} 
										?>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>	
											
					<?php echo '</td></tr>';
				} }
			?>
		</tbody>
	
	</table>
	
</div>
<?php include DIR_TMPL.'/footer.php'; ?>