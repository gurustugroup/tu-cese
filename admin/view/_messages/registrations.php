<?php
	include DIR_TMPL.'/header.php'; 
	if(isset($_GET['archive'])) {
		$db->registrations->update(
		    array( '_id' => new MongoID($_GET['archive']) ),
		    array( '$set' => array('status' => 'archive') )
		);
		header('Location: ?');
	}
	// TODO: All data we need here. We need to limit this eventually
	$cursor = $db->registrations->find()->sort(array('date' => -1));

?>
<div class="container">
		
	<h1>Course Registrations </h1> 
	
	<table class="table table-hover panel" id="mytable">
		<thead>
			<tr>
				<th width="50px">#</th>
				<th>Account Tag</th>
				<th>Name</th>
				<th>Company</th>
				<th>Date</th>
				<th>Confirmation Number</th>
				<th>Total</th>
				<th width="300px">Action</th>
			</tr>
		</thead>
		<tbody class="searchable">
			<?php 
				$list = 0;
				foreach ($cursor as $doc) {

					$course_id = $doc['course'];
					$this_course = query(array(
						'model' => 'classes',
						'_id' => new MongoID($doc['course'])
					));
					$tag_id = $this_course['tag'];
					$tag = query(array(
						'model' => 'tags',
						'_id' => new MongoID($tag_id)
					));
					$tag_name = $tag['name'];


					$list++;
					if($doc['status'] != 'archive') {
				    echo '<tr>';
				    	echo '<td>'.$list.'</td>';
				    	echo '<td>'.$tag_name.'</td>';
				    	echo '<td>'.ucfirst($doc["FirstName"][0]).' '.ucfirst($doc["LastName"][0]).'</td>';
				    	echo '<td>'.$doc["company"].'</td>';	
				    	echo '<td>'.$doc["date"].'</td>';	
				    	echo '<td>#'.$doc["confirmation"].'</td>';	
				    	echo '<td>$'.number_format($doc['eker'], 2).'</td>';			    	
				    	echo '<td>';
				    		echo '<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#open-'.$list.'">View Full Entry</button> ';
				    		echo ' <a href="?archive='.$doc["_id"].'" onclick="return ask();"><button class="btn btn-danger btn-sm">Archive</button></a> ';
				    		if($doc["status"] == 'Pending') {
				    			echo '<button class="btn btn-warning btn-sm" disabled="disabled">Pending</button>';
				    		} 
				    		if($doc["status"] == 'failed') {
				    			echo '<button class="btn btn-danger btn-sm" disabled="disabled">Payment Failed</button>';
				    		}
				    		if($doc["status"] == 'success') {
				    			echo '<button class="btn btn-success btn-sm" disabled="disabled">Successful</button>';
				    		}
				    	?>
				    	
				    	
				    	<!-- Modal -->
						<div class="modal fade" id="open-<?php echo $list; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel"><?php echo ucfirst($doc["FirstName"][0]).' '.ucfirst($doc["LastName"][0]); ?></h4>
									</div>
									<div class="modal-body">
										
										<!-- Company -->
										<h3 style="margin-left: 30px; color: #205180">Company Information</h3><br/>
										<dl class="dl-horizontal">
											<dt class="allcaps" style="opacity: 0.5;">Company</dt>
											<dd><?php echo $doc["company"]; ?></dd>
										</dl>
										<dl class="dl-horizontal">
											<dt class="allcaps" style="opacity: 0.5;">Company Address</dt>
											<dd><?php echo $doc["address"]; ?><br/><?php echo $doc["city"]; ?>, <?php echo $doc["state"]; ?> <?php echo $doc["zip"]; ?></dd>
										</dl>
										<hr>
										
										<!-- Registrations -->
										<h3 style="margin-left: 30px; color: #205180">Registrations</h3><br/>
										<?php $i = -1; foreach($doc['FirstName'] as $key) { $i++ ?>
											<dl class="dl-horizontal">
												<dt class="allcaps" style="opacity: 0.5;">Name</dt>
												<dd><?php echo ucfirst($doc["FirstName"][$i]); ?> <?php echo ucfirst($doc["LastName"][$i]); ?></dd>
											</dl>
											<dl class="dl-horizontal">
												<dt class="allcaps" style="opacity: 0.5;">Name on Badge</dt>
												<dd><?php echo ucfirst($doc["nameonbadge"][$i]); ?></dd>
											</dl>
											<dl class="dl-horizontal">
												<dt class="allcaps" style="opacity: 0.5;">Title</dt>
												<dd><?php echo $doc["title"][$i]; ?></dd>
											</dl>
											<dl class="dl-horizontal">
												<dt class="allcaps" style="opacity: 0.5;">Department</dt>
												<dd><?php echo $doc["department"][$i]; ?></dd>
											</dl>
											<dl class="dl-horizontal">
												<dt class="allcaps" style="opacity: 0.5;">Phone</dt>
												<dd><?php echo $doc["phone"][$i]; ?></dd>
											</dl>
											<dl class="dl-horizontal">
												<dt class="allcaps" style="opacity: 0.5;">Fax</dt>
												<dd><?php echo $doc["fax"][$i]; ?></dd>
											</dl>
											<dl class="dl-horizontal">
												<dt class="allcaps" style="opacity: 0.5;">Email</dt>
												<dd><a href="mailto:<?php echo $doc["email"][$i]; ?>"><?php echo $doc["email"][$i]; ?></a></dd>
											</dl>
											<?php if(isset($doc["online"])) { ?>
												<dl class="dl-horizontal">
													<dt class="allcaps" style="opacity: 0.5;">Start Date</dt>
													<dd><?php echo date('M j, Y', strtotime($doc["online"][$i])); ?></dd>
												</dl>
											<?php } ?>
											<hr>
										<?php } 
											echo '<h3 style="margin-left: 30px; color: #205180">Class & Venue</h3><br/>';
											$array = $db->classes->find(array('_id' => new MongoId($doc['course'])));
											foreach ($array as $item) {
												echo '<dl class="dl-horizontal">';
													echo '<dt class="allcaps" style="opacity: 0.5;">Title</dt>';
													echo '<dd>'.$item['title'].'</dd>';
												echo '</dl>';
												echo '<dl class="dl-horizontal">';
													echo '<dt class="allcaps" style="opacity: 0.5;">Length</dt>';
													echo '<dd>'.$item['days'].'</dd>';
												echo '</dl>';
												echo '<dl class="dl-horizontal">';
													echo '<dt class="allcaps" style="opacity: 0.5;">Time</dt>';
													echo '<dd>'.$item['time'].'</dd>';
												echo '</dl>';
												$venues = $item['class']['classes'];
												if(!isset($doc["online"])) {
													foreach($venues as $nitems) {
														if($nitems['venue'] == $doc['venue']) {
															$search = $db->venues->find(array('_id' => new MongoID($nitems['venue']))); 
															foreach($search as $nnitems) {
																echo '<dl class="dl-horizontal">';
																	echo '<dt class="allcaps" style="opacity: 0.5;">Venue</dt>';
																	echo '<dd>'.$nnitems['name'].'</dd>';
																echo '</dl>';
																echo '<dl class="dl-horizontal">';
																	echo '<dt class="allcaps" style="opacity: 0.5;">Address</dt>';
																	echo '<dd>'.$nnitems['address'].'<br/>'.$nnitems['city'].', '.$nnitems['state'].' '.$nnitems['zip'].'</dd>';
																echo '</dl>';
																echo '<dl class="dl-horizontal">';
																	echo '<dt class="allcaps" style="opacity: 0.5;">Website</dt>';
																	echo '<dd>'.@$nnitems['website'].'</dd>';
																echo '</dl>';
																echo '<dl class="dl-horizontal">';
																	echo '<dt class="allcaps" style="opacity: 0.5;">Start Date</dt>';
																	echo '<dd>'.date("F j, Y", strtotime($nitems['start_date'])).'</dd>';
																echo '</dl>';
																echo '<dl class="dl-horizontal">';
																	echo '<dt class="allcaps" style="opacity: 0.5;">Emd Date</dt>';
																	echo '<dd>'.date("F j, Y", strtotime($nitems['end_date'])).'</dd>';
																echo '</dl>';
															}
														}
													}
												}
											}	
										?>
										<br/>
										<h2 style="margin-left: 30px; color: #205180">Total: $<?php echo number_format($doc['eker'], 2); ?></h2>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal" disabled="disabled">$<?php echo number_format($doc['eker'], 2); ?></button>
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>	
											
					<?php echo '</td></tr>';
				} }
			?>
		</tbody>
	</table>
	
	<br/>
	<h4>Archived</h4> 
	<table class="table table-hover panel" id="mytable">
		<thead>
			<tr>
				<th width="50px">#</th>
				<th>Name</th>
				<th>Company</th>
				<th>Date</th>
				<th>Confirmation Number</th>
				<th>Total</th>
				<th width="300px">Action</th>
			</tr>
		</thead>
		<tbody class="searchable">
			<?php 
				$cursor = $db->registrations->find();
				$list = 0;
				foreach ($cursor as $doc) {
					$list++;
					if($doc['status'] == 'archive') {
				    echo '<tr>';
				    	echo '<td>'.$list.'</td>';
				    	echo '<td>'.ucfirst($doc["FirstName"][0]).' '.ucfirst($doc["LastName"][0]).'</td>';
				    	echo '<td>'.$doc["company"].'</td>';	
				    	echo '<td>'.$doc["date"].'</td>';	
				    	echo '<td>#'.$doc["confirmation"].'</td>';	
				    	echo '<td>$'.number_format($doc['eker'], 2).'</td>';			    	
				    	echo '<td>';
				    		echo '<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#open-'.$list.'">View Full Entry</button> ';
				    		echo ' <a href="?archive='.$doc["_id"].'" onclick="return ask();"><button class="btn btn-danger btn-sm">Archive</button></a> ';
				    		if($doc["status"] == 'Pending') {
				    			echo '<button class="btn btn-warning btn-sm" disabled="disabled">Pending</button>';
				    		} 
				    		if($doc["status"] == 'failed') {
				    			echo '<button class="btn btn-danger btn-sm" disabled="disabled">Payment Failed</button>';
				    		}
				    		if($doc["status"] == 'success') {
				    			echo '<button class="btn btn-success btn-sm" disabled="disabled">Successful</button>';
				    		}
				    	?>
				    	
				    	
				    	<!-- Modal -->
						<div class="modal fade" id="open-<?php echo $list; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel"><?php echo ucfirst($doc["FirstName"][0]).' '.ucfirst($doc["LastName"][0]); ?></h4>
									</div>
									<div class="modal-body">
										
										<!-- Company -->
										<h3 style="margin-left: 30px; color: #205180">Company Information</h3><br/>
										<dl class="dl-horizontal">
											<dt class="allcaps" style="opacity: 0.5;">Company</dt>
											<dd><?php echo $doc["company"]; ?></dd>
										</dl>
										<dl class="dl-horizontal">
											<dt class="allcaps" style="opacity: 0.5;">Company Address</dt>
											<dd><?php echo $doc["address"]; ?><br/><?php echo $doc["city"]; ?>, <?php echo $doc["state"]; ?> <?php echo $doc["zip"]; ?></dd>
										</dl>
										<hr>
										
										<!-- Registrations -->
										<h3 style="margin-left: 30px; color: #205180">Registrations</h3><br/>
										<?php $i = -1; foreach($doc['FirstName'] as $key) { $i++ ?>
											<dl class="dl-horizontal">
												<dt class="allcaps" style="opacity: 0.5;">Name</dt>
												<dd><?php echo ucfirst($doc["FirstName"][$i]); ?> <?php echo ucfirst($doc["LastName"][$i]); ?></dd>
											</dl>
											<dl class="dl-horizontal">
												<dt class="allcaps" style="opacity: 0.5;">Name on Badge</dt>
												<dd><?php echo ucfirst($doc["nameonbadge"][$i]); ?></dd>
											</dl>
											<dl class="dl-horizontal">
												<dt class="allcaps" style="opacity: 0.5;">Title</dt>
												<dd><?php echo $doc["title"][$i]; ?></dd>
											</dl>
											<dl class="dl-horizontal">
												<dt class="allcaps" style="opacity: 0.5;">Department</dt>
												<dd><?php echo $doc["department"][$i]; ?></dd>
											</dl>
											<dl class="dl-horizontal">
												<dt class="allcaps" style="opacity: 0.5;">Phone</dt>
												<dd><?php echo $doc["phone"][$i]; ?></dd>
											</dl>
											<dl class="dl-horizontal">
												<dt class="allcaps" style="opacity: 0.5;">Fax</dt>
												<dd><?php echo $doc["fax"][$i]; ?></dd>
											</dl>
											<dl class="dl-horizontal">
												<dt class="allcaps" style="opacity: 0.5;">Email</dt>
												<dd><a href="mailto:<?php echo $doc["email"][$i]; ?>"><?php echo $doc["email"][$i]; ?></a></dd>
											</dl>
											<?php if(isset($doc["online"])) { ?>
												<dl class="dl-horizontal">
													<dt class="allcaps" style="opacity: 0.5;">Start Date</dt>
													<dd><?php echo date('M j, Y', strtotime($doc["online"][$i])); ?></dd>
												</dl>
											<?php } ?>
											<hr>
										<?php } 
											echo '<h3 style="margin-left: 30px; color: #205180">Class & Venue</h3><br/>';
											$array = $db->classes->find(array('_id' => new MongoId($doc['course'])));
											foreach ($array as $item) {
												echo '<dl class="dl-horizontal">';
													echo '<dt class="allcaps" style="opacity: 0.5;">Title</dt>';
													echo '<dd>'.$item['title'].'</dd>';
												echo '</dl>';
												echo '<dl class="dl-horizontal">';
													echo '<dt class="allcaps" style="opacity: 0.5;">Length</dt>';
													echo '<dd>'.$item['days'].'</dd>';
												echo '</dl>';
												echo '<dl class="dl-horizontal">';
													echo '<dt class="allcaps" style="opacity: 0.5;">Time</dt>';
													echo '<dd>'.$item['time'].'</dd>';
												echo '</dl>';
												$venues = $item['class']['classes'];
												if(!isset($doc["online"])) {
													foreach($venues as $nitems) {
														if($nitems['venue'] == $doc['venue']) {
															$search = $db->venues->find(array('_id' => new MongoID($nitems['venue']))); 
															foreach($search as $nnitems) {
																echo '<dl class="dl-horizontal">';
																	echo '<dt class="allcaps" style="opacity: 0.5;">Venue</dt>';
																	echo '<dd>'.$nnitems['name'].'</dd>';
																echo '</dl>';
																echo '<dl class="dl-horizontal">';
																	echo '<dt class="allcaps" style="opacity: 0.5;">Address</dt>';
																	echo '<dd>'.$nnitems['address'].'<br/>'.$nnitems['city'].', '.$nnitems['state'].' '.$nnitems['zip'].'</dd>';
																echo '</dl>';
																echo '<dl class="dl-horizontal">';
																	echo '<dt class="allcaps" style="opacity: 0.5;">Website</dt>';
																	echo '<dd>'.@$nnitems['website'].'</dd>';
																echo '</dl>';
																echo '<dl class="dl-horizontal">';
																	echo '<dt class="allcaps" style="opacity: 0.5;">Start Date</dt>';
																	echo '<dd>'.date("F j, Y", strtotime($nitems['start_date'])).'</dd>';
																echo '</dl>';
																echo '<dl class="dl-horizontal">';
																	echo '<dt class="allcaps" style="opacity: 0.5;">Emd Date</dt>';
																	echo '<dd>'.date("F j, Y", strtotime($nitems['end_date'])).'</dd>';
																echo '</dl>';
															}
														}
													}
												}
											}	
										?>
										<br/>
										<h2 style="margin-left: 30px; color: #205180">Total: $<?php echo number_format($doc['eker'], 2); ?></h2>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal" disabled="disabled">$<?php echo number_format($doc['eker'], 2); ?></button>
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>	
											
					<?php echo '</td></tr>';
				} }
			?>
		</tbody>
	
	</table>

</div>
<?php include DIR_TMPL.'/footer.php'; ?>