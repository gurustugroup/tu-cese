<?php if(@$place === "Login"): ?>
	<!DOCTYPE html>
	<html lang="en-US">
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	    <meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	    <title>CESE Admin</title>
		<?php admin_assets('admin.min.css'); ?>
		<style>
			body, html {
				background: #fff;
			}
		</style>
	</head>
	<body>
<?php else: $_SESSION["whois"] = $_SESSION["whois"]; ?>
	<!DOCTYPE html>
	<html lang="en-US">
	<head>
		<title>CESE Admin</title>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

	    <!-- CSS -->
	    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<?php admin_assets('admin.min.css'); ?>

		<!-- JS -->
		<?php admin_assets('jquery.min.js'); ?>
		<?php admin_assets('jquery.tmpl.min.js'); ?>
		<?php admin_assets('jquery-ui.js'); ?>
		<?php admin_assets('bootstrap.min.js'); ?>
		<?php admin_assets('bootstrap-multiselect.js'); ?>
		<?php admin_assets('jquery.zclip.js'); ?>
		<?php admin_assets('tinymce.min.js'); ?>
		<?php admin_assets('admin.min.js'); ?>
		<script>
			var ajaxurl = '<?php echo ADMIN_URL; ?>/ajax/';
			var swf = '<?php admin_assets("ZeroClipboard.swf"); ?>';
		</script>

	</head>
	<body>

		<!-- NAV -->
		<nav class="navbar navbar-default navbar-fixed-top navbar-inverse" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo BASE_URL; ?>admin/">CESE</a>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown">Courses <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="<?php echo BASE_URL; ?>admin/ceselanding/">Landing Page</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/courses/">Public Courses</a></li>
								<li class="divider"></li>
								<li><a href="<?php echo BASE_URL; ?>admin/venues/">Course Venues</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/tags/">Course Tags</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/categories/">Course Categories</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/coursessettings">Course Settings</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown">People <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="<?php echo BASE_URL; ?>admin/instructors/">Instructors</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/staff/">Staff</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Homepage <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="<?php echo BASE_URL; ?>admin/sliders/">Slider Images</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/sections/">Sections</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/calltoaction/">Call-To-Action</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="<?php echo BASE_URL; ?>admin/incompanylanding/">In-Company <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="<?php echo BASE_URL; ?>admin/incompanyclients/">Clients</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/incompanylanding/">Landing Page</a></li>
								<li class="divider"></li>
								<li><a href="<?php echo BASE_URL; ?>admin/incompany/new">Add new In-Company Courses</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/incompany/">In-Company Courses</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/categories/">Course Categories</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown">Pages <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="<?php echo BASE_URL; ?>admin/pages/new">Create new Page</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/pages/">List of Pages</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/pageorder/">Page Order</a></li>
								<li class="divider"></li>
								<li><a href="<?php echo BASE_URL; ?>admin/blog/">Blog</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/blogcategories/">Blog Categories</a></li>
								<li class="divider"></li>
								<li><a href="<?php echo BASE_URL; ?>admin/sidebar/">Sidebar Settings</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">Conferences <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="<?php echo BASE_URL; ?>admin/conferences-landing">Landing Page</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/featuredconferences">Exhibit Events</a></li>
								<li class="divider"></li>
								<li><a href="<?php echo BASE_URL; ?>admin/ipec-landing">IPEC Landing</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/about-ipec">About IPEC</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/conferences-highlights">Conference Highlights</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/conferences-venue">Venue, Travel & Accommodations</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/glance">Conference-at-a-glance</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/technical-sessions">Technical Sessions</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/session-chairs">Session Chairs / Presenter Info</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/conferences-attend">Attend</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/conferences-exhibit">Exhibit</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/conferences-sponsors">Corporate Sponsors</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/brochures">Brochures</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/abstract">Abstract Submission</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/conferences-registration">Registration</a></li>
								<li class="divider"></li>
								<li><a href="<?php echo BASE_URL; ?>admin/attendee-registration">Attendee Conference Registration</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/presenter-registration">Presenter Conference Registration</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/exhibitor-registration">Exhibitor Conference Registration</a></li>
								<li class="divider"></li>
								<li><a href="<?php echo BASE_URL; ?>admin/conferencesorder">Conferences Section Order</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/conferences-settings">IPEC Options</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">Glossary <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="<?php echo BASE_URL; ?>admin/glossarylanding/">Landing Page</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/glossary/">Terms</a></li>
							</ul>
						</li>
						<li><a href="<?php echo BASE_URL; ?>admin/help">Help</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Messages <span class='badge badge-danger'><?php message_count('all'); ?></span></a>
							<ul class="dropdown-menu" role="menu">
								<li>
									<a href="<?php echo BASE_URL; ?>admin/registrations/"> Course Registrations <span class='badge badge-danger'><?php message_count('registrations'); ?></span></a>
								</li>
								<li>
									<a href="<?php echo BASE_URL; ?>admin/contactmessages/">Contact via form <span class='badge badge-danger'><?php message_count('contact'); ?></span></a>
								</li>
								<li>
									<a href="<?php echo BASE_URL; ?>admin/incompanymessages/">In-Company Quotes <span class='badge badge-danger'><?php message_count('incompany'); ?></span></a>
								</li>
								<li>
									<a href="<?php echo BASE_URL; ?>admin/conference-attendees/">Conference Attendees <span class='badge badge-danger'><?php message_count('attendees'); ?></span></a>
								</li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $_SESSION['whois'];?> <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="<?php echo BASE_URL; ?>" target="_blank">View Site</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/settings">Admin Accounts</a></li>
								<li><a href="<?php echo BASE_URL; ?>admin/logout/">Logout</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>

		<br clear="all"/><br/><br/>

<?php endif ?>
