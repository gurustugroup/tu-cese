<!--	(11/21/14)	
===================================================================
======= Featured Image
===================================================================
-->
<div class="panel panel-default">
	<div class="panel-heading">Featured Image</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
				<div class="thumbnail">
					<input type="hidden" name="image" id="gallery-url" class="form-control" value="<?php echo @$results["image"]?>">
					<?php if (@$results["image"]) {  ?>
						<img src="<?php echo @replaceHttp($results["image"]); ?>" id="gallery-preview" width="200px"/>
					<?php } else { ?>
						<img src="//placehold.it/200x200" id="gallery-preview" width="200px"/>
					<?php } ?>
					<div class="caption">
						<p> 
							<a href="#" class="btn btn-success form-control" data-toggle="modal" data-target="#uploads" onclick="$('#ufile').trigger('click');">
								Set Feature Image
							</a>
							<button onclick="remove_featured();" type="button" style="margin-top: 5px;" class="btn btn-danger form-control rm-gallery-preview">
								Remove Image
							</button>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>