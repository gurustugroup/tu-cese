<?php

	check();
	
	$title = '';
	$content = array();
	$image = array();
	$title = array();
	$link = array();
	$summary = array();
	$one_title = array();
	$one_link = array();
	$two_title = array();
	$two_link = array();
	$btitle = array();
	$blink = array();
	
	extract( $_POST, EXTR_IF_EXISTS );
	
	$results = array(  
		"what" => 'ipec-landing',
 		"content" => $content,
 		"image" => $image,
 		"title" => $title,
 		"link" => $link,
 		"summary" => $summary,
 		"one_title" => $one_title,
 		"one_link" => $one_link,
 		"two_title" => $two_title,
 		"two_link" => $two_link,
 		"btitle" => $btitle,
 		"blink" => $blink
	);
	
	# Update Document
	if(isset( $_POST['save'])) {
		$conferences->update(array('what' => 'ipec-landing'), $results);
		header('location: ?');
	}
	else {
	
		# Populate
		$cursor = $conferences->find(array('what' => 'ipec-landing'));
		$results = iterator_to_array($cursor, false);
		$results = $results[0];
	}

	
	include DIR_VIEW.'/conferences/ipec-landing.php';
?>
