<?php
	
	# The Modal
	$title='';
	$content='';
	$public='';
	$image='';
	$categories='';
	$slug='';

	extract( $_POST, EXTR_IF_EXISTS );

	# The Array
	$results = array ( 
		"title" => $title, 
		"content" => cleartxt($content), 
		"public" => $public, 
		"image" => $image, 
		"categories" => $categories,
		"slug" => $slug,
		"time" => date("F j, Y, g:i a"),
	);

	
	lumos($blog, 'blog', $results);

?>