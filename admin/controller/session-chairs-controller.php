<?php

	check();
	
	$one = '';
	$one_title = '';
	$two = '';
	$two_title = '';
	$three = '';
	$three_title = '';
	$title = array();
	$link = array();
	$order = '0';
	$promo = '';
	
	extract( $_POST, EXTR_IF_EXISTS );
	
	$results = array(  
		"what" => 'session-chairs',
		"page_title" => 'Session Chairs / Presenter Info',
 		"one" => $one,
 		"one_title" => $one_title,
 		"two" => $two,
 		"two_title" => $two_title,
 		"three" => $three,
 		"three_title" => $three_title,
 		"title" => $title,
 		"link" => $link,
 		"order" => $order,
 		"promo" => $promo,
	);
	
	# Update Document
	if(isset( $_POST['save'])) {
		$conferences->update(array('what' => 'session-chairs'), $results);
		header('location: ?');
	}
	else {
	
		# Populate
		$cursor = $conferences->find(array('what' => 'session-chairs'));
		$results = iterator_to_array($cursor, false);
		$results = $results[0];
	}
	
	$tt = 'Session Chairs / Presenter Info';
	include DIR_VIEW.'/conferences/tabs.php';

?>
