<?php

	check();
	
	$slider_message='';
	$mission_statement='';
	$mission_statement_explained='';
	$image='';
	$ctitle='';
	$cmessage='';
	$cpage='';
	
	extract( $_POST, EXTR_IF_EXISTS );

	$results = array(  
		"what" => 'cesecourses',
 		"slider_message" => $slider_message,
		"mission_statement" => $mission_statement,
		"mission_statement_explained" => $mission_statement_explained,
		"image" => $image, 
		"ctitle" => $ctitle, 
		"cmessage" => $cmessage, 
		"cpage" => $cpage, 
	);
	
	if( isset($_POST['incompany'])) {
		$meta->update(array("what" => "cesecourses"), $results);
	}
	
	$cursor = $meta->find(array("what" => "cesecourses"));
	$results = iterator_to_array($cursor, false);
	$results = $results[0];
	

	include DIR_VIEW.'/courses/courses-landing.php';

?>