<?php

	check();
	$order = '0';
	$title = array();
	$start = array();
	$end = array();
	$event = array();
	$location = array();
	$sort_order = array();
	$session = array();
	$message = array();

	extract( $_POST, EXTR_IF_EXISTS );

	$results = array(
		'what' => 'technical-sessions',
		'page_title' => 'Technical Sessions',
		'session' => $session,
		'message' => $message,
 		'order' => $order,
		'title' => $title,
		'start' => $start,
		'end' => $end,
		'event' => $event,
		'location' => $location,
		'sort_order' => $sort_order,
	);

	# Update Document
	if(isset( $_POST['save'])) {
		unset($_POST['save']);
		$conferences->update(array('what' => 'technical-sessions'), $results);
		header('location: ?');
	}
	else {

		# Populate
		$cursor = $conferences->find(array('what' => 'technical-sessions'));
		$results = iterator_to_array($cursor, false);
		$results = $results[0];
	}

	include DIR_VIEW.'/conferences/technical-sessions.php';

?>
