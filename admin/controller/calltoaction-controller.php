<?php

	# The Modal
	$title='';
	$url='';
	$content='';

	extract( $_POST, EXTR_IF_EXISTS );

	# The Array
	$results = array( 
		'name' => 'homepage',
		'title' => $title, 
		'url' => $url, 
		'content' => $content,
	);
	
	if(isset( $_POST['update'])) {
		$cta->update(array('name' => 'homepage'), $results);
	}
	else {
		$cursor = $cta->find(array('name' => 'homepage'));
		$results = iterator_to_array($cursor, false);
		$results = $results[0];
	}
	
	include DIR_VIEW.'/homepage/calltoaction.php';
?>