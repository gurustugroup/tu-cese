<?php

	check();
	
	$info = '';
	$map = '';
	$other = '';
	$order = '0';
	
	extract( $_POST, EXTR_IF_EXISTS );
	
	$results = array(  
		"what" => 'venue',
		"page_title" => 'Venue, Travel & Accommodations',
 		"info" => $info,
 		"map" => $map,
 		"other" => $other,
 		"order" => $order,
	);
	
	# Update Document
	if(isset( $_POST['save'])) {
		$conferences->update(array('what' => 'venue'), $results);
		header('location: ?');
	}
	else {
	
		# Populate
		$cursor = $conferences->find(array('what' => 'venue'));
		$results = iterator_to_array($cursor, false);
		$results = $results[0];
	}
	
	include DIR_VIEW.'/conferences/conferences-venue.php';

?>
