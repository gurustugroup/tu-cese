<?php

	check();
	
	$slider_message='';
	$mission_statement='';
	$mission_statement_explained='';
	$training_dollars='';
	$advantages='';;
	$image='';
	$ctitle='';
	$cmessage='';
	$cpage='';
	
	extract( $_POST, EXTR_IF_EXISTS );

	$results = array(  
		"what" => 'incompanylanding',
 		"slider_message" => cleartxt($slider_message),
		"mission_statement" => cleartxt($mission_statement),
		"mission_statement_explained" => cleartxt($mission_statement_explained),
		"training_dollars" => cleartxt($training_dollars),
		"advantages" => cleartxt($advantages),
		"image" => $image, 
		"ctitle" => $ctitle, 
		"cmessage" => $cmessage, 
		"cpage" => $cpage, 
	);
	
	if( isset($_POST['incompany'])) {
		$meta->update(array("what" => "incompanylanding"), $results);
	}
	
	$cursor = $meta->find(array("what" => "incompanylanding"));
	$results = iterator_to_array($cursor, false);
	$results = $results[0];
	

	include DIR_VIEW.'/incompany/incompanylanding-form.php';

?>