<?php 
	check();
	$sidebar = new stdClass;
	$home = $cta->find(array("name" => "sidebar"));
	foreach($home as $doc) {
		$sidebar->cta = new stdClass;
		$sidebar->cta->content = $doc['content'];
		$sidebar->cta->slug = $doc['url'];
		$sidebar->cta->title = $doc['title'];
	}
	
	include DIR_VIEW.'/pages/sidebar.php';
	
	if( isset( $_POST['sidebarcta'] ) ){
		extract($_POST);
		$db->cta->update(array("name" => "sidebar"), array( "name" => "sidebar",  "title" => $sidebartitle,  "url" => $sidebarctaurl, "content" => $sidebarcta)); 
		header('location: ?cta');
	}
	
?>