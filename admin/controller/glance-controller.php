<?php

	check();
	$order = '0';
	$title = array();
	$start = array();
	$end = array();
	$event = array();
	$location = array();
	$sort_order = array();

	extract( $_POST, EXTR_IF_EXISTS );

	$results = array(
		"what" => 'glance',
		"page_title" => 'Conference-at-a-Glance',
 		"order" => $order,
		"title" => $title,
		"start" => $start,
		"end" => $end,
		"event" => $event,
		"location" => $location,
		"sort_order" => $sort_order,
	);

	# Update Document
	if(isset( $_POST['save'])) {
		$conferences->update(array('what' => 'glance'), $results);
		header('location: ?');
	}
	else {

		# Populate
		$cursor = $conferences->find(array('what' => 'glance'));
		$results = iterator_to_array($cursor, false);
		$results = $results[0];
	}

	include DIR_VIEW.'/conferences/glance.php';

?>
