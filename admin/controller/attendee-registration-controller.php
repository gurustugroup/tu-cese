<?php

	check();
	$fee = array();
	$title = array();
	$date = array();
	
	extract( $_POST, EXTR_IF_EXISTS );
	
	$results = array( 
		'title' => $title,
		'fee' => $fee,
		'date' => $date,
		'what' => 'attendee-registration',
	);
	
	# Update Document
	if(isset( $_POST['save'])) {
		$conferences->update(array('what' => 'attendee-registration'), $results);
		header('location: ?');
	}
	else {
	
		# Populate
		$cursor = $conferences->find(array('what' => 'attendee-registration'));
		$results = iterator_to_array($cursor, false);
		$results = $results[0];
	}
	
	include DIR_VIEW.'/conferences/attendee-registration.php';

?>
