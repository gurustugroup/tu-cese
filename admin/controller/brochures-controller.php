<?php

	check();
	
	$files = array();
	
	extract( $_POST, EXTR_IF_EXISTS );
	
	$results = array(
		"what" => 'brochures',
		'files' => $files,
	);
	
	# Update Document
	if(isset( $_POST['save'])) {
		$conferences->update(array('what' => 'brochures'), $results);
		header('location: ?');
	}
	else {
	
		# Populate
		$cursor = $conferences->find(array('what' => 'brochures'));
		$results = iterator_to_array($cursor, false);
		$results = $results[0];
	}
	
	include DIR_VIEW.'/conferences/brochures.php';

?>
