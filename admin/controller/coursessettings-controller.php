<?php

	# The Modal
	$expiremessage = '';

	extract( $_POST, EXTR_IF_EXISTS );

	# The Array
	$results = array( 
		'what' => 'coursessettings',
		'expiremessage' => $expiremessage
	);
	
	if(isset( $_POST['update'])) {
		$db->meta->update(array('what' => 'coursessettings'), $results);
	}
	else {
		$cursor = $db->meta->find(array('what' => 'coursessettings'));
		$results = iterator_to_array($cursor, false);
		$results = $results[0];
	}
	
	include DIR_VIEW.'/courses/settings.php';
?>