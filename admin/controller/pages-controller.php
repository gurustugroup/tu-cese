<?php

	$title='';
	$base='';
	$slug='';
	$content='';
	$public='';
	$ctitle='';
	$cmessage='';
	$cpage='';
	$pagelink='';
	$dev='';
	$order='';
	$policies='';
	$last = $_SESSION['whois'];

	extract( $_POST, EXTR_IF_EXISTS );

	$results = array( 
		"title" => $title, 
		"base" => $base, 
		"slug" => $slug, 
		"content" => cleartxt($content), 
		"public" => $public, 
		"ctitle" => $ctitle, 
		"cmessage" => $cmessage, 
		"cpage" => $cpage, 
		"pagelink" => $pagelink, 
		"dev" => $dev, 
		"order" => $order,   
		"last_edit" => $last,
		"policies" => cleartxt($policies),
	);
	
	lumos($pages, 'pages', $results);
	

?>