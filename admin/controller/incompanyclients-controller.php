<?php

	check();
	
	$world='';	
	$testimonials='';
	$image='';
	$ctitle='';
	$cmessage='';
	$cpage='';
	
	extract( $_POST, EXTR_IF_EXISTS );

	$results = array(  
		"what" => 'incompanyclients',
 		"world" => cleartxt($world),
		"testimonials" => $testimonials,
		"image" => $image, 
		"ctitle" => $ctitle, 
		"cmessage" => $cmessage, 
		"cpage" => $cpage, 
	);
	
	if( isset($_POST['incompany'])) {
		$meta->update(array("what" => "incompanyclients"), $results);
	}
	
	$cursor = $meta->find(array("what" => "incompanyclients"));
	$results = iterator_to_array($cursor, false);
	$results = $results[0];
	

	include DIR_VIEW.'/incompany/incompanyclients-form.php';

?>