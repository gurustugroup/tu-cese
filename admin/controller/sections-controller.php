<?php
	
	# The Modal
	$name='';
	$profile='';
	$public='';
	$slug='';
	$image='';
	$content='';
	$last = $_SESSION['whois'];

	extract( $_POST, EXTR_IF_EXISTS );
	
	# The Array
	$results = array( 
		"name" => $name, 
		"profile" => $profile, 
		"public" => $public,
		"content" => cleartxt($content),
		"image" => $image, 
		"slug" => $slug,
		"last_edit" => $last
	);

	lumos($sections, 'sections', $results);

?>