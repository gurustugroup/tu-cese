<?php

	check();
	
	$content = array();
	$order = '0';
	$title = array();
	$link = array();
	$date = array();
	$order = '0';
	
	extract( $_POST, EXTR_IF_EXISTS );
	
	$results = array(
		'what' => 'registration',
		'page_title' => 'Registration',
		'content' => $content,
		'order' => $order,
 		'title' => $title,
 		'link' => $link,
		'date' => $date,
		'files' => $files,
 		"order" => $order,
	);
	
	# Update Document
	if(isset( $_POST['save'])) {
		$conferences->update(array('what' => 'registration'), $results);
		header('location: ?');
	}
	else {
	
		# Populate
		$cursor = $conferences->find(array('what' => 'registration'));
		$results = iterator_to_array($cursor, false);
		$results = $results[0];
	}

	
	include DIR_VIEW.'/conferences/conferences-registration.php';

?>
