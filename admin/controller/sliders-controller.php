<?php
	
	# The Model
	$name='';
	$public='';
	$image='';
	$profile='';
	$slug='';
	$last = $_SESSION['whois'];

	extract( $_POST, EXTR_IF_EXISTS );

	# The Array
	$results = array( 
		"name" => $name, 
		"public" => $public, 
		"image" => $image, 
		"slug" => $slug,
		"profile" => cleartxt($profile),
		"last_edit" => $last
	);
	
	lumos($sliders, 'sliders', $results);

?>