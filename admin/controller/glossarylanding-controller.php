<?php

	check();
	
	$content='';
	$image='';
	$ctitle='';
	$cmessage='';
	$cpage='';
	
	extract( $_POST, EXTR_IF_EXISTS );

	$results = array(  
		"what" => 'glossarylanding',
 		"content" => cleartxt($content),
		"image" => $image, 
		"ctitle" => $ctitle, 
		"cmessage" => $cmessage, 
		"cpage" => $cpage, 
	);
	
	if( isset($_POST['glossary'])) {
		$meta->update(array("what" => "glossarylanding"), $results);
	}
	
	$cursor = $meta->find(array("what" => "glossarylanding"));
	$results = iterator_to_array($cursor, false);
	$results = $results[0];
	
	include DIR_VIEW.'/glossary/glossary-landing.php';

?>