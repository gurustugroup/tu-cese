<?php

	check();
	$fee1 = array();
	$fee2 = array();
	$fee3 = array();
	$fee4 = array();
	$title1 = array();
	$title2 = array();
	$title3 = array();
	$title4 = array();
	$date1 = array();
	$date2 = array();
	$date3 = array();
	$date4 = array();
	
	extract( $_POST, EXTR_IF_EXISTS );
	
	$results = array( 
		'fee1' => $fee1,
		'fee2' => $fee2,
		'fee3' => $fee3,
		'fee4' => $fee4,
		'title1' => $title1,
		'title2' => $title2,
		'title3' => $title3,
		'title4' => $title4,
		'date1' => $date1,
		'date2' => $date2,
		'date3' => $date3,
		'date4' => $date4,
		'what' => 'exhibitor-registration',
	);
	
	# Update Document
	if(isset( $_POST['save'])) {
		$conferences->update(array('what' => 'exhibitor-registration'), $results);
		header('location: ?');
	}
	else {
	
		# Populate
		$cursor = $conferences->find(array('what' => 'exhibitor-registration'));
		$results = iterator_to_array($cursor, false);
		$results = $results[0];
	}

	
	include DIR_VIEW.'/conferences/exhibitor-registration.php';

?>
