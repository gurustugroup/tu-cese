<?php

	check();
	$subject = array();

	extract( $_POST, EXTR_IF_EXISTS );

	$results = array(
		'what' => 'abstract',
		'subject' => $subject
	);

	# Update Document
	if(isset( $_POST['save'])) {
		unset($_POST['save']);
		$conferences->update(array('what' => 'abstract'), $results);
		header('location: ?');
	}
	else {

		# Populate
		$cursor = $conferences->find(array('what' => 'abstract'));
		$results = iterator_to_array($cursor, false);
		$results = $results[0];
	}

	include DIR_VIEW.'/conferences/abstract.php';

?>
