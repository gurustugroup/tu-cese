<?php

	# The Modal
	$title='';
	$slug='';
	$days='';
	$time='';
	$incompany='';
	$instructors='';
	$categories='';
	$tag='';
	$seo='';
	$instructors='';
	$description='';
	$class='';
	$public='';
	$online='';
	$files='';
	$image='';
	$one = array( 'name' => 'Objectives / Benefits', 'content' => '');
	$two = array( 'name' => 'Who Should Attend', 'content' => '' );
	$three = array( 'name' => 'Special Features', 'content' => '' );
	$four = array( 'name' => 'Participant Comments', 'content' => '' );
	$five = array( 'name' => 'Accreditation', 'content' => '' );
	$ctitle='';
	$cmessage='';
	$cpage='';
	$keywords='';
	$dow='';

	extract( $_POST, EXTR_IF_EXISTS );

	# The Array
	$results = array(
		'title' => $title,
		'slug' => $slug,
		'one' => $one,
		'two' => $two,
		'three' => $three,
		'four' => $four,
		'five' => $five,
		'days' => $days,
		'time' => $time,
		'instructors' => $instructors,
		'incompany' => $incompany,
		'categories' => $categories,
		'tag' => $tag,
		'seo' => $seo,
		'description' => $description,
		'class' => $class,
		'public' => $public,
		'online' => $online,
		'files' => $files,
		'image' => $image,
		'ctitle' => $ctitle,
		'cmessage' => $cmessage,
		'cpage' => $cpage,
		'keywords' => $keywords,
		'dow' => $dow
	);

	lumos($classes, 'courses', $results);


?>
