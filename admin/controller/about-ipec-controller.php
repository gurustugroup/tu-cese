<?php

	check();
	
	$title = '';
	$content = array();
	$order = '0';
	
	extract( $_POST, EXTR_IF_EXISTS );
	
	$results = array(  
		"what" => 'about-ipec',
		"page_title" => 'About',
 		"content" => $content,
 		"order" => $order,
	);
	
	# Update Document
	if(isset( $_POST['save'])) {
		$conferences->update(array('what' => 'about-ipec'), $results);
		header('location: ?');
	}
	else {
	
		# Populate
		$cursor = $conferences->find(array('what' => 'about-ipec'));
		$results = iterator_to_array($cursor, false);
		$results = $results[0];
	}

	
	include DIR_VIEW.'/conferences/about-ipec.php';
?>
