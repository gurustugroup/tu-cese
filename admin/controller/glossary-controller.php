<?php
	
	# The Modal
	$term='';
	$definition='';
	
	extract( $_POST, EXTR_IF_EXISTS );
	
	# The Array
	$results = array( 
		"term" => $term, 
		"definition" => $definition,
		"sort" => str_replace(',', '', str_replace(' ', '', strtolower($term))),
	);
	
	lumos($glossary, 'glossary', $results);
	

?>