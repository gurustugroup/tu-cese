<?php

	check();
	
	$title = '';
	$content = '';
	$image = '';
	$subtitle = '';
	$promo = '';
	
	extract( $_POST, EXTR_IF_EXISTS );
	
	$results = array(  
		"what" => 'landing',
 		"title" => $title,
 		"content" => $content,
		"image" => $image, 
		"subtitle" => $subtitle,
		"promo" => $promo,
	);
	
	# Update Document
	if(isset( $_POST['save'])) {
		$conferences->update(array('what' => 'landing'), $results);
		header('location: ?');
	}
	else {
	
		# Populate
		$cursor = $conferences->find(array('what' => 'landing'));
		$results = iterator_to_array($cursor, false);
		$results = $results[0];
	}
	
	include DIR_VIEW.'/conferences/landing.php';

?>