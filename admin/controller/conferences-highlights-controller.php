<?php

	check();
	
	$highlights = '';
	$events = '';
	$order = '0';
	
	extract( $_POST, EXTR_IF_EXISTS );
	
	$results = array(  
		"what" => 'highlights',
		"page_title" => 'Conference Highlights & Events',
 		"highlights" => $highlights,
 		"events" => $events,
 		"order" => $order,
	);
	
	# Update Document
	if(isset( $_POST['save'])) {
		$conferences->update(array('what' => 'highlights'), $results);
		header('location: ?');
	}
	else {
	
		# Populate
		$cursor = $conferences->find(array('what' => 'highlights'));
		$results = iterator_to_array($cursor, false);
		$results = $results[0];
	}
	
	include DIR_VIEW.'/conferences/conferences-highlights.php';

?>
