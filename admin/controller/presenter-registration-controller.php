<?php

	check();
	$fee = array();
	$title = array();
	
	extract( $_POST, EXTR_IF_EXISTS );
	
	$results = array( 
		'title' => $title,
		'fee' => $fee,
		'what' => 'presenter-registration',
	);
	
	# Update Document
	if(isset( $_POST['save'])) {
		$conferences->update(array('what' => 'presenter-registration'), $results);
		header('location: ?');
	}
	else {
	
		# Populate
		$cursor = $conferences->find(array('what' => 'presenter-registration'));
		$results = iterator_to_array($cursor, false);
		$results = $results[0];
	}
	
	include DIR_VIEW.'/conferences/presenter-registration.php';

?>
