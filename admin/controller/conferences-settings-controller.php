<?php

	check();

	$headline = '';
	$tag = '';
	$image = '';
	$logos = array();
	$link = array();
	$logo_order = array();
	$ctitle = '';
	$cmessage = '';
	$cpage = '';

	extract( $_POST, EXTR_IF_EXISTS );

	$results = array(
		'what' => 'options',
 		'headline' => $headline,
 		'tag' => $tag,
		'logos' => $logos,
		'logo_order' => $logo_order,
		'image' => $image,
		'link' => $link,
		'ctitle' => $ctitle,
		'cmessage' => $cmessage,
		'cpage' => $cpage,
	);

	# Update Document
	if(isset( $_POST['save'])) {
		$conferences->update(array('what' => 'options'), $results);
		header('location: ?');
	}
	else {

		# Populate
		$cursor = $conferences->find(array('what' => 'options'));
		$results = iterator_to_array($cursor, false);
		$results = $results[0];
	}

	include DIR_VIEW.'/conferences/conferences-settings.php';

?>
