<?php

	check();
	
	$one = '';
	$one_title = '';
	$two = '';
	$two_title = '';
	$three = '';
	$three_title = '';
	$title = array();
	$link = array();
	$order = '0';
	$promo = '';
	
	extract( $_POST, EXTR_IF_EXISTS );
	
	$results = array(  
		"what" => 'exhibit',
		"page_title" => 'Exhibit',
 		"one" => $one,
 		"one_title" => $one_title,
 		"two" => $two,
 		"two_title" => $two_title,
 		"three" => $three,
 		"three_title" => $three_title,
 		"title" => $title,
 		"link" => $link,
 		"order" => $order,
 		"promo" => $promo,
	);
	
	# Update Document
	if(isset( $_POST['save'])) {
		$conferences->update(array('what' => 'exhibit'), $results);
		header('location: ?');
	}
	else {
	
		# Populate
		$cursor = $conferences->find(array('what' => 'exhibit'));
		$results = iterator_to_array($cursor, false);
		$results = $results[0];
	}
	
	$tt = 'Exhibit';
	include DIR_VIEW.'/conferences/tabs.php';

?>
