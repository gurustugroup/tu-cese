<?php

	# The Modal
	$fname='';
	$lname='';
	$profile='';
	$public='';
	$image='';
	$phone='';
	$email='';
	$last = $_SESSION['whois'];

	extract( $_POST, EXTR_IF_EXISTS );

	# The Array
	$results = array( 
		"fname" => $fname, 
		"lname" => $lname, 
		"profile" => cleartxt($profile), 
		"public" => $public, 
		"image" => $image, 
		"phone" => $phone,
		"email" => $email,
		"last_edit" => $last
	);

	lumos($instructors, 'instructors', $results);

?>