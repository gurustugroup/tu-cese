<?php
	
	# The Modal
	$content = array();
	$image = '';
	$slug = '';
	
	extract( $_POST, EXTR_IF_EXISTS );
	
	# The Array
	$results = array( 
		'slug' => $slug,
		'image' => $image,
		'content' => $content,
	);
	
	lumos($featuredconferences, 'featuredconferences', $results);
	
?>
