<?php 
	
	#(11/21/14)	
	#===================================================================
	#======= Gallery Files
	#===================================================================
		
	echo '<br/><ul class="list-group">';	
	$a = 0;
	foreach ($all_files as $file) {
		$a++;
		echo '<li class="list-group-item">';
			echo '<span id="file-'.$a.'" width="100%" style="height: 100%; width: auto !important;">'.$file["name"].'</span>';
			if(@$file['course']) { echo '<span class="pull-right" style="padding-right: 10px"> ('.$file['course'].') </span> '; }
		echo '</li> ';
	}
	echo '</ul>';


?>

<script>
	$('[id^=file-]').click(function() {
		var file = $(this).html();
		var id = event.target.id.slice(5);
		var numItems = $('.pdfs').length;
		$( ".file-box" ).append( '<li class="list-group-item pdfs"><span class="remove-file badge" onclick="$(this).parent().remove();">Remove</span>'+file+' <input name="files['+numItems+'][file]" type="hidden" value="'+file+'"><br><label>File Name</label><input name="files['+numItems+'][name]" class="form-control" type="text" value="'+file+'"></li>' );
		$('#file-'+id).parent().addClass('disabled');
	});
		
	$('[id^=delete-file-]').click( function(event) {
		var id = event.target.id.slice(12);
		console.log(id);
		$.post( "<?php base(); ?>admin/upload/delete/", { deletefile: id } );
	});
</script>