<?php
	
	check();

	extract($_POST, EXTR_SKIP);
	$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_SPECIAL_CHARS);
	$_GET = filter_input_array(INPUT_GET, FILTER_SANITIZE_SPECIAL_CHARS);


	//(2/12/15)	
	//===================================================================
	//======= Account Update
	//===================================================================
	if(isset($_POST['account'])) {
		$newdata = array('$set' => array("password" => md5($_POST['password'])));
		$users->update(array("_id" => new MongoId($_POST['account'])), $newdata);
	}
	
	
	

	//(2/12/15)	
	//===================================================================
	//======= Categories
	//===================================================================
	if(isset($_POST['categoryname'])) {
		$cat = array('name' => $categoryname);
		
		// Insert
		$category->insert($cat);
		
		// Find
		$cursor = $category->find(array('name' => $categoryname)); 
		foreach($cursor as $ins) {
			echo $ins['_id'];
		}	
	}
	
	//(2/12/15)	
	//===================================================================
	//======= Blog Categories
	//===================================================================
	if(isset($_POST['blogcat'])) {
		$cat = array('name' => $blogcat);
		
		// Insert
		$blogcats->insert($cat);
		
		// Find
		$cursor = $blogcats->find(array('name' => $blogcat)); 
		foreach($cursor as $ins) {
			echo $ins['_id'];
		}	
	}
	
	
	//(/3/15)	
	//===================================================================
	//======= Tags
	//===================================================================
	if(isset($_POST['tagname'])) {
		$cat = array('name' => $tagname);
		
		// Insert
		$db->tags->insert($cat);
		
		// Find
		$cursor = $db->tags->find(array('name' => $tagname)); 
		foreach($cursor as $ins) {
			echo $ins['_id'];
		}	
	}




	//(2/12/15)	
	//===================================================================
	//======= instructor
	//===================================================================
	if(isset($_POST['instructor'])) {
		$page = array( 
			"fname" => $instructor, 
			"lname" => $lname, 
			"profile" => $profile, 
			"public" => 'no', 
			"image" => '', 
			"last_edit" => '',
		);
		
		// Insert
		$instructors->insert($page);
		
		// Find
		$cursor = $instructors->find(array('fname' => $instructor, 'lname' => $lname)); 
		foreach($cursor as $ins) {
			echo $ins['_id'];
		}
	}



	//(2/12/15)	
	//===================================================================
	//======= Venues
	//===================================================================
	if(isset($_POST['venue'])) {
		
		$name='';
		$address='';
		$city='';
		$state='';
		$zip='';
		$phone='';
		$country='';
		$website='';
		$comments='';
		$rr='';
		$country_code='US';
		
		extract($_POST);
		
		$page = array( 
			"name" => $name, 
			"address" => $address, 
			"city" => $city, 
			"state" => $state, 
			"zip" => $zip,
			"website" => $website,  
			"phone" => $phone, 
			"country" => $country,
			"comments" => $comments,
			"rr" => $rr,
			"country_code" => $country_code,
		);
		
		// Insert
		$venues->insert($page);
		
		// Find
		$cursor = $venues->find(array('name' => $name)); 
		foreach($cursor as $ins) {
			echo $ins['_id'];
		}
	}
	
	
	//(2/13/15)	
	//===================================================================
	//======= Change Page Order
	//===================================================================
	if(isset($_POST['order'])) {
		$pages->update(array("name" => $_POST['name'], "base" => $_POST['base']), array('$set' => array("order" => $_POST['order'])));
	}

?>
