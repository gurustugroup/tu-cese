<?php 
	
	#(11/21/14)	
	#===================================================================
	#======= Gallery Images
	#===================================================================
	$cursor = $gallery->find()->sort(array('_id' => -1));
	$i = 1;
	echo '<br/>';
	foreach ($cursor as $doc) {
		$i++;
	    echo '<div class="thumbnail col-md-2">';
	    	echo '<a href="#" style="height: 100px; width: 100%; display: block; overflow: hidden; position: relative;" data-dismiss="modal">';
	    	echo '<div id="select-'.$i.'" style="width: 100%; height: 100%; background: url('.replaceHttp($doc["url"]).'); background-size: cover; "></div>';
	    	echo '<img id="image-'.$i.'" data-dismiss="modal" width="100%" style="height: 100%; width: auto !important;" src="'.replaceHttp($doc["url"]).'"></a>';
	    	echo '<button class="btn btn-xs btn-danger" style="position: absolute; bottom: 0; right: 0; margin: 5px;" id="delete-image-'.$doc["_id"].'">Delete</button>';
	    echo '</div> ';
	}
	echo '<br clear="all"/>';
?>
<input type="hidden" id="myclass"/>
<script>
	$('[id^=select-]').click(function(event) {
		var num = event.target.id.slice(7);
		var url = $('#image-'+num).attr('src');
		$('#gallery-url').val(url);
		$('img#gallery-preview').attr('src', url);
		console.log(url);
	
		if ($('#multiup').length) {
			var box = $('#myclass').val();
			$('#box_'+box).val(url);
			$('img#img_'+box).attr('src', url);
		}
	});
		
	$('[id^=delete-image-]').click( function(event) {
		var id = event.target.id.slice(13);
		$.post( "<?php echo BASE_URL; ?>admin/upload/delete/", { deleteimage: id } );
	});
	
	function remove_featured() {
		$('#gallery-url').val('');
		$('img#gallery-preview').attr('src', 'https://placehold.it/200x200');
	}
</script>