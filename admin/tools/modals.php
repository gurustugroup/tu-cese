<!--	(11/21/14)
===================================================================
======= Instructor(s)
===================================================================
-->
<div class="modal fade instructor" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="mySmallModalLabel">Add Instructor(s)</h4>
			</div>
			<div class="modal-body">
				<div class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-3 control-label">First Name</label>
						<div class="col-sm-8">
							<input type="text" id="in-first" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Last Name</label>
						<div class="col-sm-8">
							<input type="text" id="in-last" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Bio</label>
						<div class="col-sm-8">
							<textarea class="form-control" id="in-bio" rows="3"></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="form-group">
						<button class="btn btn-success pull-left" id="instructor" data-toggle="modal" data-target=".instructor" onclick="addinstructor()">Add New Instructor</button>
						<button type="button" class="btn btn-danger pull-left" data-toggle="modal" data-target=".instructor">Cancel</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--	(11/21/14)
===================================================================
======= tinyMCE
===================================================================
-->
<div class="modal pdf_insert" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="background: rgba(0, 0, 0, 0.56); position: absolute; height: 1000px;">
	<div class="modal-dialog modal-md" s>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" onclick="$('.pdf_insert').hide();"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="mySmallModalLabel">GET PDF LINK</h4>
			</div>
			<div class="modal-body" style="max-height: 370px">
				<?php
					echo '<br/><ul class="list-group">';
					$a = 0;
					$myfiles = $db->files->find();
					foreach ($myfiles as $file) {
						$a++;
						echo '<li class="list-group-item" id="pd-'.$file["_id"].'">';
							echo '<span id="pdf-'.$a.'" width="100%" style="height: 100%; width: auto !important;">'.$file["name"].'</span>';
							echo ' <button class="btn btn-danger btn-xs pull-right" onclick="self(\'rpdf\',\''.$file["_id"].'\')">Delete</button></a> ';
							echo '<button class="btn btn-warning btn-xs pull-right" onclick="copyToClipboard(\''.replaceHttp($file["url"]).'\')" style="margin-right: 10px">Select</button>';
						echo '</li> ';
					}
					echo '</ul>';
				?>
			</div>
				<div class="modal-footer">

						<iframe frameborder="0" src="<?php base(); ?>admin/upload/" width="100%" height="130px" id="upload-frame"></iframe>
					<div class="form-group">
						<div class="col-lg-12">
							<label style="float: left">Get PDF link</label>
						</div>
						<div class="col-lg-8">
							<input type="text" autofocus class="form-control col-sm-12 pdf-linking" value="">
						</div>
						<button type="button" class="btn btn-danger pull-left col-lg-4" data-toggle="modal" onclick="$('.pdf_insert').hide();">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	function copyToClipboard(element) {
		$('.pdf-linking').val(element);
	}

	function self(input,value) {
		$.post( "<?php echo BASE_URL; ?>ajax-helper.php", { rpdf: value } );
		$('#pd-'+value).hide();
		console.log('#pd-'+value);
	}
</script>

<!--	(11/21/14)
===================================================================
======= Categories
===================================================================
-->
<div class="modal fade category" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="mySmallModalLabel">Add a Category</h4>
			</div>
			<div class="modal-body">
				<div class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-3 control-label">Create Category</label>
						<div class="col-sm-8">
							<input type="text" id="category-n" class="form-control">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="form-group">
						<button class="btn btn-success pull-left" data-toggle="modal" data-target=".category" onclick="addcategory()">Add New Category</button>
						<button type="button" class="btn btn-danger pull-left" data-toggle="modal" data-target=".category">Cancel</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--	(11/21/14)
===================================================================
======= Blog Categories
===================================================================
-->
<div class="modal fade cats" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="mySmallModalLabel">Add a Category</h4>
			</div>
			<div class="modal-body">
				<div class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-3 control-label">Create Category</label>
						<div class="col-sm-8">
							<input type="text" id="blog-n" class="form-control">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="form-group">
						<button class="btn btn-success pull-left" data-toggle="modal" data-target=".cats" onclick="addblogcategory()">Add New Category</button>
						<button type="button" class="btn btn-danger pull-left" data-toggle="modal" data-target=".cats">Cancel</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--	(3/1/15)
===================================================================
======= Tags
===================================================================
-->
<div class="modal fade tag" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="mySmallModalLabel">Add a New Tag</h4>
			</div>
			<div class="modal-body">
				<div class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-3 control-label">Create Tag</label>
						<div class="col-sm-8">
							<input type="text" id="tag-n" class="form-control">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="form-group">
						<button class="btn btn-success pull-left" data-toggle="modal" data-target=".tag" onclick="addtags()">Add New Tag</button>
						<button type="button" class="btn btn-danger pull-left" data-toggle="modal" data-target=".tag">Cancel</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--	(6/11/15)
===================================================================
======= My Files
===================================================================
-->
<div class="modal fade myfiles" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="mySmallModalLabel">My Files</h4>
			</div>
			<div class="modal-body">
				<div id="content">
					<br clear="all"/>
					<?php
						$image = $db->gallery->find();
						foreach($image as $file) {
							echo '<div class="thumbnail col-md-3">';
								echo '<a href="#" style="height: 140px; width: 100%; display: block; overflow: hidden; position: relative;">';
									echo '<div id="select-2" style="width: 100%; height: 100%; background: url('.replaceHttp($file['url']).'); background-size: cover; "></div>';
									echo '<img id="image-2" data-dismiss="modal" width="100%" style="height: 100%; width: auto !important;" src="'.replaceHttp($file['url']).'">';
								echo '</a>';
								echo '<button class="btn btn-sm btn-warning" style="position: absolute; margin-top: -34px; margin-left: 5px;">Copy Link</button>';
							echo '</div>';
						}
					?>
				</div>
			</div>
			<div class="modal-footer">
				<br clear="all"/>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close File Browser</button>
			</div>
		</div>
	</div>
</div>



<!--	(11/21/14)
===================================================================
======= Venues
===================================================================
-->
<div class="modal fade venue" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="mySmallModalLabel">Add a Venue</h4>
			</div>
			<div class="modal-body">
				<div class="form-horizontal">
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Name</label>
						<div class="col-sm-9">
							<input type="text" name="name" autofocus class="form-control" value="">
						</div>
					</div>
				</div>
				<div class="form-horizontal">
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Address</label>
						<div class="col-sm-9">
							<input type="text" name="address" autofocus class="form-control" value="">
						</div>
					</div>
				</div>
				<div class="form-horizontal">
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">City</label>
						<div class="col-sm-9">
							<input type="text" name="city" autofocus class="form-control" value="">
						</div>
					</div>
				</div>
				<div class="form-horizontal">
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">State</label>
						<div class="col-sm-9">
							<select name="state" id="state" class="form-control">
								<?php
									$states = statesArray();
									foreach( $states as $key => $value ){
										echo '<option state-code="'.$key.'">' . $value . '</option>';
									}
								?>
							</select>
						</div>
					</div>
				</div>
				<div class="form-horizontal">
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">ZIP</label>
						<div class="col-sm-9">
							<input type="text" name="zip" autofocus class="form-control" value="">
						</div>
					</div>
				</div>
				<div class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-2 control-label">Country</label>
						<div class="col-sm-9">
							<select name="country" class="form-control" id="country">
								<option country-code="US">United States</option>
								<?php
									$countries = countryArray();
									foreach( $countries as $key => $value ){
										echo '<option country-code="'.$key.'">' . $value . '</option>';
									}

								?>
							</select>
							<input type="hidden" id="country_code" name="country_code" value="">
						</div>
					</div>
				</div>
				<div class="form-horizontal">
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Phone</label>
						<div class="col-sm-9">
							<input type="text" name="phone" autofocus class="form-control" value="">
						</div>
					</div>
				</div>
				<div class="form-horizontal">
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Room Reservation</label>
						<div class="col-sm-9">
							<input type="text" name="rr" autofocus class="form-control" value="">
						</div>
					</div>
				</div>
				<div class="form-horizontal">
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Website</label>
						<div class="input-group col-sm-9">
							<span class="input-group-addon" id="basic-addon1">http://</span>
							<input type="text" name="website" class="form-control" value="">
						</div>
					</div>
				</div>
				<div class="form-horizontal">
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Comments</label>
						<div class="col-sm-9">
							<textarea class="form-control" name="comments" rows="7"></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="form-group">
					<button class="btn btn-success pull-left" data-toggle="modal" data-target=".venue" onclick="addvenue()">Add New Venue</button>
					<button type="button" class="btn btn-danger pull-left" data-toggle="modal" data-target=".venue">Cancel</button>
				</div>
			</div>
		</div>
	</div>
</div>
