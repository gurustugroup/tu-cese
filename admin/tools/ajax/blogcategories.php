<h2>Blog Categories</h2>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="blogcategories_box">
			<?php if(@$results['categories']) { foreach(@$results['categories'] as $item) { ?>
				<div class="form-group blogcategories_select">
					<label for="inputEmail3" class="col-sm-2 control-label">Category</label>
					<div class="col-sm-7">
					 	<select class="form-control blogcategories_selection" name="categories[]">
			        		<?php 
			            		$ins = $db->blogcats->find();
								foreach( $ins as $in ) {
									if($in['_id'] == $item) {
										echo '<option selected id="'.$in['_id'].'" value="'.$in['_id'].'">'.$in['name'].'</option>';
									} else {
										echo '<option id="'.$in['_id'].'" value="'.$in['_id'].'">'.$in['name'].'</option>';
									}
								}
							?>
						</select>
					</div>
					<div class="col-sm-3">
						<button type="button" class="btn btn-danger delete_blogcategories">X</button>
					</div>
					<br clear="all"><br/>
				</div>
			<?php } }?>
		</div>
		<div class="col-sm-12">
			<button type="button" class="btn btn-primary" id="add_blogcategories">Add Category</button>
			<button type="button" class="btn btn-danger" data-toggle="modal" data-target=".cats">Create New Category</button>
		</div>	
	</div>		
</div>