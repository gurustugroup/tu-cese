<h2>Instructors</h2>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="instructor_box">
			<?php if(@$results['instructors']) { foreach(@$results['instructors'] as $item) { ?>
				<div class="form-group instructor_select">
					<label for="inputEmail3" class="col-sm-2 control-label">Instructor</label>
					<div class="col-sm-7">
					 	<select class="form-control ins_select" name="instructors[]">
			        		<?php 
			            		$ins = $db->instructors->find()->sort(array('fname' => 1));
								foreach( $ins as $in ) {
									if($in['_id'] == $item) {
										echo '<option selected id="'.$in['_id'].'" value="'.$in['_id'].'">'.$in['fname'].' '.$in['lname'].'</option>';
									} else {
										echo '<option id="'.$in['_id'].'" value="'.$in['_id'].'">'.$in['fname'].' '.$in['lname'].'</option>';
									}
								}
							?>
						</select>
					</div>
					<div class="col-sm-3">
						<button type="button" class="btn btn-danger delete_instructor">X</button>
					</div>
					<br clear="all"><br/>
				</div>
			<?php } } ?>
		</div>
		<div class="col-sm-12">
			<button type="button" class="btn btn-primary" id="add_instructor">Add Instructor</button>
			<button type="button" class="btn btn-danger" data-toggle="modal" data-target=".instructor">Create New Instructor</button>
		</div>	
	</div>		
</div>