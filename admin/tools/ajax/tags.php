<h2>Accounts</h2>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="tag_box">
			<?php if(@$results['tags']) { foreach(@$results['tags'] as $item) { ?>
				<div class="form-group tag_select">
					<label for="inputEmail3" class="col-sm-2 control-label">Accounts</label>
					<div class="col-sm-7">
					 	<select class="form-control tags_select" name="tags[]">
			        		<?php 
			            		$ins = $db->tags->find();
								foreach( $ins as $in ) {
									if($in['_id'] == $item) {
										echo '<option selected id="'.$in['_id'].'" value="'.$in['_id'].'">'.$in['name'].'</option>';
									} else {
										echo '<option id="'.$in['_id'].'" value="'.$in['_id'].'">'.$in['name'].'</option>';
									}
								}
							?>
						</select>
					</div>
					<div class="col-sm-3">
					</div>
					<br clear="all"><br/>
				</div>
			<?php } }?>
		</div>
		<div class="col-sm-12">
			<button type="button" class="btn btn-danger" data-toggle="modal" data-target=".tag">Create New Account</button>
		</div>	
	</div>		
</div>