<div style="display: none">
	
	<!-- Instructors -->
	<div class="form-group instructor_select ugh" id="instructor_clone">
		<label for="inputEmail3" class="col-sm-2 control-label">Instructor</label>
		<div class="col-sm-7">
		 	<select class="form-control ins_select" name="instructors[]">
	    		<?php 
	        		$ins = $db->instructors->find()->sort(array('fname' => 1));
					foreach( $ins as $in ) {
						echo '<option id="'.$in['_id'].'" value="'.$in['_id'].'">'.$in['fname'].' '.$in['lname'].'</option>';
					}
				?>
			</select>
		</div>
		<div class="col-sm-3">
			<button type="button" class="btn btn-danger delete_instructor">X</button>
		</div>
		<br clear="all"><br/>
	</div>
	
	<!-- Categories -->
	<div class="form-group category_select" id="cat_clone">
		<label for="inputEmail3" class="col-sm-2 control-label">Category</label>
		<div class="col-sm-7">
		 	<select class="form-control cat_select" name="categories[]">
        		<?php 
            		$ins = $db->category->find();
					foreach( $ins as $in ) {
						echo '<option id="'.$in['_id'].'" value="'.$in['_id'].'">'.$in['name'].'</option>';
					}
				?>
			</select>
		</div>
		<div class="col-sm-3">
			<button type="button" class="btn btn-danger delete_category">X</button>
		</div>
		<br clear="all"><br/>
	</div>
	
	<!-- Blog Categories -->
	<div class="form-group blogcategories_select" id="blogcategory_clone">
		<label for="inputEmail3" class="col-sm-2 control-label">Category</label>
		<div class="col-sm-7">
		 	<select class="form-control blogcategories_selection" name="categories[]">
        		<?php 
            		$ins = $db->blogcats->find();
					foreach( $ins as $in ) {
						echo '<option id="'.$in['_id'].'" value="'.$in['_id'].'">'.$in['name'].'</option>';
					}
				?>
			</select>
		</div>
		<div class="col-sm-3">
			<button type="button" class="btn btn-danger delete_blogcategories">X</button>
		</div>
		<br clear="all"><br/>
	</div>
	
	<!-- Tags -->
	<div class="form-group tag_select" id="tag_clone">
		<label for="inputEmail3" class="col-sm-2 control-label">Tag</label>
		<div class="col-sm-7">
		 	<select class="form-control tags_select" name="tags[]">
        		<?php 
            		$ins = $db->tags->find();
					foreach( $ins as $in ) {
						echo '<option selected id="'.$in['_id'].'" value="'.$in['_id'].'">'.$in['name'].'</option>';
					}
				?>
			</select>
		</div>
		<div class="col-sm-3">
			<button type="button" class="btn btn-danger delete_tag">X</button>
		</div>
		<br clear="all"><br/>
	</div>
			
</div>