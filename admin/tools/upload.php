<!-- <link rel="stylesheet" href="../../assets/_admin/css/bootstrap.min.css"/> -->
<span id="upload-file">
	<form method="post" action="?" id="upload-form" enctype="multipart/form-data">
		<?php if(isset($_GET['course'])) { ?>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-1 control-label">Course</label>
			<div class="col-sm-11">
				<input class="form-control" rows="7" name="coursef" value="<?php echo @$_GET['course']; ?>">
			</div>
		</div>
		<br/><br/>
		<?php } ?>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-1 control-label">File</label>
			<div class="col-sm-11">
				<input type="file" name="file" class="btn btn-default btn-file" style="float: left">
			</div>
		</div>
		<br/><br/>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-1 control-label"></label>
			<div class="col-sm-11">
				<input type="submit" class="btn btn-info btn-file" style="float: left;" value="Upload File (.JPG, .JPEG, .DOC, .DOCX, .PDF)">
			</div>
		</div>

	</form>
</span>
<br clear="all"/>
</div>

<style>
	body {
		overflow: hidden;
	}
</style>

<!-- (11/21/14)  Upload Image  -->
<?php

	# File Types
	$msg = "";
	$image_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");
	$file_formats = array("pdf", "PDF", "doc", "docx", "DOC", "DOCX");

	# Start Upload
	if($_SERVER["REQUEST_METHOD"] == "POST") {
		$name = $_FILES["file"]["name"];
		$size = $_FILES["file"]["size"];
		$tmp = $_FILES["file"]["tmp_name"];
		$ext = getExtension($name);

		if(strlen($name) > 0) {
			if(in_array($ext,$image_formats)) {
				$actual_image_name = time().".".$ext;
				if($s3->putObjectFile($tmp, $bucket , $actual_image_name, S3::ACL_PUBLIC_READ) ) {
					$s3file="https://".$bucket.".s3.amazonaws.com/".$actual_image_name;
					$date = date("M d, Y");

					$form = array( "url" => $s3file, "date" => $date, "uploade_by" => $_SESSION["whois"]);
					$gallery->insert($form);

					# Success Upload
					$msg = "Success Upload";
				}
			}
			elseif(in_array($ext,$file_formats)) {
				$actual_image_name = time().".".$ext;
				if($s3->putObjectFile($tmp, $bucket , $actual_image_name, S3::ACL_PUBLIC_READ) ) {
					$s3file="https://".$bucket.".s3.amazonaws.com/".$actual_image_name;
					$date = date("M d, Y");

					$form = array( "url" => $s3file, "date" => $date, "name" => $name, "course" => @$_POST['coursef'], "uploade_by" => $_SESSION["whois"]);
					$files->insert($form);

					# Success Upload
					$msg = "Success Upload";
				}
			}
			else {

				# Not an Image
				$msg = "Please upload image file.";
			}
		}
	}

	# Uplaod Done
	if($msg) {
		echo "<script> $('.media-gallery').modal('show'); $('.imags').load('<?php base(); ?>admin/gallery/');</script>";
	}
	
?>
