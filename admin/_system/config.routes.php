<?php

# Admin Dash
Router::add($adminurl, DIR_CTRL.'/_dash.php'); // Dash

# Ajax Tools
Router::add($adminurl.'files/', DIR_TOOL.'/files.php');
Router::add($adminurl.'images/', DIR_TOOL.'/images.php');
Router::add($adminurl.'ajax/', DIR_TOOL.'/ajax.php');
Router::add($adminurl.'logout/', DIR_TOOL.'/logout.php');
Router::add($adminurl.'upload/', DIR_TOOL.'/upload.php');
Router::add($adminurl.'upload/delete/', DIR_TOOL.'/upload-delete.php');
Router::add($adminurl.'ajax/instructors/', DIR_TOOL.'/instructors.php');

# Admin Pages
$router = array("settings");
foreach ($router as $value) {
	Router::add($adminurl.$value.'/', DIR_CTRL.'/'.$value.'-controller.php'); // Settings
}

# Admin Groups
routergroup('pages');
routergroup('sidebar');
routergroup('pageorder');
routergroup('incompany');
routergroup('courses');
routergroup('instructors');
routergroup('staff');
routergroup('sliders');
routergroup('sections');
routergroup('blog');
routergroup('venues');
routergroup('glossary');
routergroup('glossarylanding');
routergroup('incompanylanding');
routergroup('incompanyclients');
routergroup('ceselanding');
routergroup('incompanymessages');
routergroup('contactmessages');
routergroup('conferences-visited');
routergroup('tags');
routergroup('blogcategories');
routergroup('categories');
routergroup('calltoaction');
routergroup('coursessettings');
routergroup('registrations');
routergroup('conferences-landing');
routergroup('featuredconferences');
routergroup('about-ipec');
routergroup('conferences-highlights');
routergroup('brochures');
routergroup('session-chairs');
routergroup('conferences-attend');
routergroup('conferences-exhibit');
routergroup('conferences-sponsors');
routergroup('conferencesorder');
routergroup('glance');
routergroup('conferences-venue');
routergroup('conferences-settings');
routergroup('ipec-landing');
routergroup('conferences-registration');
routergroup('attendee-registration');
routergroup('presenter-registration');
routergroup('exhibitor-registration');
routergroup('technical-sessions');
routergroup('conference-attendees');
routergroup('abstract');
routergroup('help');

?>
