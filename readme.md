# The University of Tulsa CESE

```
   ______      ____        _____ __
  / ____/_  __/ __ \__  __/ ___// /___  __
 / / __/ / / / /_/ / / / /\__ \/ __/ / / /
/ /_/ / /_/ / _, _/ /_/ /___/ / /_/ /_/ /
\____/\__,_/_/ |_|\__,_//____/\__/\__,_/
```

## Live Environment
- MongoDB
- PHP5
- PHP MongoDB drivers

## Dev Environment  
- MongoDB
- PHP5
- PHP MongoDB drivers
- Node.JS
- NPM
- Composer

## Dependencies
- **S3 Bucket** (cese.s3.amazonaws.com)

### Key files for Payment
- **Payment Form for all payments** - public/view/courses/confirmation.php
- **Payment Error** - public/controller/payment-error.php
- **Payment Successful** - public/controller/payment-successful.php

### Admin Key files
- **File Uploads** - admin/tools/upload.php
- **AJAX Request** - admin/tools/ajax.php
- **EMAIL Templates** - public/mustache

### Weird Frontend Stuff
- **Training Calendar JS** - public/view/courses/training.php

### File Structure
- admin (/admin)
	- _system
		- `config.routes.php` (Add 'routergroups')
	- Controllers (server)
	- Views (public)
	- Tools
	- Templates (Header, Footer, Sidebar)
- public (/)
	- Controllers (server)
	- Views (public)
	- mustache (Email templates)
	- Templates (Header, Footer, Sidebar)
- Assets
	- Public
	- Admin
	- Images
- `fabrication.php` (A large list of PHP functions)
- `index.php` (Public pages router)

### Launch Process
- Clone Repo
- Look for the **#CHANGE ME**
- Add `.htacces` to root
``` .htaccess
	RewriteEngine on

	#CHANGE ME
	RewriteBase /~terrillo/tu/
	#/CHANGE ME

	RewriteCond %{REQUEST_FILENAME} !-f
	RewriteCond %{REQUEST_FILENAME} !-d
	RewriteRule ^([^/]+)$ index.php?p=$1 [L,QSA]
	RewriteRule ^([^/]+)/([^/]+)$ index.php?p=$1&sp=$2 [L]

	#exclude /assets/$ folder

	RewriteEngine On
	
	#SSL
	# RewriteCond %{HTTPS} !on
	# RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI}

	Rewriterule ^rss.xml$ rss [L]

	# Deny Access to Readme
	<Files ./readme.md>
		Order Allow,Deny
		Deny from all
	</Files>

	# Deny Access to Config.php
	<Files ./config.php>
		Order Allow,Deny
		Deny from all
	</Files>

```

- Add another `.htaccess` to /admin/
``` .htaccess
	RewriteEngine On
	
	# SSL
	# RewriteCond %{HTTPS} !on
	# RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI}
	
	RewriteCond %{REQUEST_FILENAME} !-f
	RewriteCond %{REQUEST_FILENAME} !-d


	#CHANGE ME
	RewriteRule ^(.*)$ /~terrillo/tu/admin/index.php [L,QSA]
	#/CHANGE ME
```

- Add `config.php` file
``` php
	// Base URL

	#CHANGE ME
	$baseurl = '/~terrillo/tu/';
	#/CHANGE ME
	$adminurl = $baseurl.'admin/';

	#CHANGE ME
	define('DOMAIN', 'http://gurustudev.com' );
	#/CHANGE ME

	define('BASE_URL', DOMAIN.$baseurl );
	define('ADMIN_URL', DOMAIN.$baseurl.’admin' );
	function base() {
		global $baseurl;
		echo $baseurl;
	}

	include('fabrication.php');
```

- VirtualHost Sample

```
<Virtualhost *:80>
DocumentRoot /var/www/tu-test/tu-cese/
ServerName tu.gurustudev.com

    <Directory "/var/www/">
        Options FollowSymLinks
        AllowOverride All

        Order allow,deny
        Allow from all
    </Directory>

</VirtualHost>
```

#### Start Dev Environment
- `sudo npm install`
- `grunt`


## Helpful file paths

./public/view/courses/confirmation.php - This is the page used to submit directly to TouchNet
./public/view/courses/enroll-view.php - After user selects a date/time and number of participants, All course information and fields to enroll visible 
./public/controller/enroll-controller.php - 


## TouchNet Test Card

test card:
Visa
4111111111111111

Exp Date:
6/17

3 Digit:
543

