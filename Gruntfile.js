module.exports = function(grunt) {

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		// Sass
		sass: {
			dist: {
				options: {
				  style: 'compressed'
				},
				files: {
					'assets/_public/css/screen.css' : 'assets/_public/sass/screen.scss',
					'assets/_public/builds/print.min.css' : 'assets/_public/sass/print.scss'
				}
			}
		},

		// CSS
		cssmin: {
			options: {
				shorthandCompacting: false,
				roundingPrecision: -1
			},
			target: {
				files: {
					'assets/_admin/builds/admin.min.css': [
						'assets/_admin/css/bootstrap.css',
						'assets/_admin/css/bootstrap-multiselect.css',
						'assets/_admin/css/bootstrap-theme.css',
						'assets/_admin/css/guru.css'
					],
					'assets/_public/builds/public.min.css': [
						'assets/_public/css/fullcalendar.css',
						'assets/_public/css/screen.css'
					]
				}
			}
		},

		// JS
		uglify: {
			options: {
				mangle: true,
				compress: {
					drop_console: true
				}
			},
            my_target: {
                options: {
                    beautify: false
                },
                files: {
                    'assets/_admin/js/admin.min.js': [
						'assets/_admin/js/CloneMachine/CloneMachine.js',
						'assets/_admin/js/_tu.js'
                    ]
                }
            }
		},

		// Watch END:
        watch: {
            css: { // Admin CSS
                files: 'assets/_admin/css/*.css',
                tasks: ['cssmin']
            },
	        css: { // Public CSS
                files: 'assets/_public/css/*.css',
                tasks: ['cssmin']
            },
            css: {
                files: 'assets/_public/sass/*.scss',
                tasks: ['sass', 'cssmin']
            },
            js: {
                files: ['assets/_admin/js/*.js'],
                tasks: ['uglify']
            }
        }
	});

	grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.registerTask('default', [ 'watch' ]);
};
