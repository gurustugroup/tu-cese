<?php

define('web_root', __DIR__ );

# Composer
include('vendor/autoload.php');

# Mustache
define('DIR_MOD', dirname(__FILE__).'/public');
$mustache = new Mustache_Engine(array(
    'loader' => new Mustache_Loader_FilesystemLoader(DIR_MOD . '/mustache'),
));

# Connect to Mongo Database
try {
	$m = new MongoClient();
	$db = $m->CESE;
} catch (Exception $e) {
	die('Error establishing connection to database.');
}

# Collections
$users = $db->users;
$pages = $db->pages;
$blog = $db->blog;
$blogcats = $db->blogcats;
$sections = $db->sections;
$meta = $db->meta;
$tags = $db->tags;
$category = $db->category;

$sliders = $db->sliders;
$gallery = $db->gallery;
$cta = $db->cta;
$settings = $db->settings;
$instructors = $db->instructors;
$venues = $db->venues;
$staff = $db->staff;
$company = $db->company;
$glossary = $db->glossary;
$files = $db->files;
$conferences = $db->conferences;
$featuredconferences = $db->featuredconferences; // Also know as Exhibit Event in the administration area of the application

$testing = $db->testing;

/* 

	Registration Specific
		* registration
		* attendees

*/
// Important
$messages = $db->messages;
$classes = $db->classes;

$corporate_sponsor_request = $db->corporate_messages;




# List
$all_pages = iterator_to_array($pages->find()->sort(array('title' => 1)), false);
$all_classes = iterator_to_array($classes->find()->sort(array('title' => 1)), false);
$all_instructors = $instructors->find(array("public" => "on"))->sort(array('lname' => 1));
$all_staff = $staff->find(array("public" => "on"))->sort(array('lname' => 1));
$all_gallery = $gallery->find(array("public" => "on"))->sort(array('_id' => -1));
$all_courses = $classes->find(array("public" => "on"))->sort(array('title' => 1));
$all_incompany = $company->find(array("public" => "on"))->sort(array('title' => 1));
$all_posts = $blog->find(array("public" => "on"))->sort(array('_id' => -1));
$all_categories = $category->find()->sort(array('name' => 1));
$all_tags = $tags->find()->sort(array('name' => 1));
$all_venues = $venues->find();
$all_files = $files->find();
$all_terms = $glossary->find()->sort(array('sort' => 1));
$all_blogcats = $blogcats->find();

# Vars
$thispage = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$place = "";

# S3 Set Up
$bucket="cese";
if (!class_exists('S3')) include('libs/S3.php');
if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJFFFOR4BZ4GP2FUA');
if (!defined('awsSecretKey')) define('awsSecretKey', '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');
$s3 = new S3(awsAccessKey, awsSecretKey);
$s3->putBucket($bucket, S3::ACL_PUBLIC_READ);

//
// Includes
//
require_once(__DIR__ . '/includes/confirmation-number.php');
require_once(__DIR__ . '/includes/form-functions.php');
require_once(__DIR__ . '/includes/sanitize-functions.php');
require_once(__DIR__ . '/includes/course-functions.php');



//(3/2/15)
//===================================================================
//======= Functions
//===================================================================

# Get Image
function image($file) {
	global $baseurl;
	echo $baseurl.'assets/_images/'.$file.'';
}

# Admin Assets
function admin_assets($file) {
	global $baseurl;

	if (strpos($file,'.css') !== false) {
		echo '<link rel="stylesheet" href="'.$baseurl.'assets/_admin/builds/'.$file.'" />';
	}
    elseif (strpos($file,'.js') !== false) {
    	echo '<script type="text/javascript" src="'.$baseurl.'assets/_admin/js/'.$file.'"></script>';
	}
}

# Public Assets
function public_assets($file) {
	global $baseurl;

	if( strpos( $file, '.png') || strpos( $file, '.jpg') ){
		echo '<img src="'.$baseurl.'assets/_images/'.$file.'" alt="" />';
	}
	if (strpos($file,'.css') !== false) {
		echo '<link rel="stylesheet" href="'.$baseurl.'assets/_public/builds/'.$file.'" />';
	}
    if (strpos($file,'.js') !== false) {
    	echo '<script type="text/javascript" src="'.$baseurl.'assets/_public/js/'.$file.'"></script>';
	}
}

# Get Link from ID
function get_link($slug) {
	global $pages, $baseurl;
	$sub = $pages->find(array("_id" => new MongoID($slug)));
	foreach($sub as $doc) {
		echo $baseurl.$doc['base'].'/'.$doc['slug'];
	}
}

# Get Title form ID
function get_title($slug) {
	global $pages, $baseurl;
	$sub = $pages->find(array("_id" => new MongoID($slug) ));
	foreach($sub as $doc) {
		echo $doc['title'];
	}
}

# Get File Extension
function getExtension($str)  {
	$i = strrpos($str,".");
	if (!$i) { return ""; }
	$l = strlen($str) - $i;
	$ext = substr($str,$i+1,$l);
	return $ext;
}

# Get Childern Pages
function get_subpages() {
	global $pages, $baseurl;
	$base = $_GET['p'];
	$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	$sub = $pages->find(array("base" => $base, "public" => "on"))->sort(array('order' => 1));
	foreach($sub as $doc) {
		if($doc['pagelink']) {
			echo '<li><a href="'.$baseurl.$doc["pagelink"].'">'.$doc["title"].'</a></li>';
		}
		else {
			echo '<li><a href="'.$baseurl.$base.'/'.$doc["slug"].'">'.$doc["title"].'</a></li>';
		}
	}
}

# Get Top Navigatiion
function get_nav($base = "about") {
	global $db, $baseurl;
	$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	$sub = $db->pages->find(array("base" => $base, "public" => "on"))->sort(array("order" => 1));
	foreach($sub as $doc) {
		if($doc['pagelink']) {
			echo '<li><a href="'.$baseurl.$doc["pagelink"].'">'.$doc["title"].'</a></li>';
		}
		else {
			echo '<li><a href="'.$baseurl.$base.'/'.$doc["slug"].'">'.$doc["title"].'</a></li>';
		}
	}
}

# Recent Blog Post
function recent_posts() {
	global $db, $baseurl;
	$sub = $db->blog->find()->limit(5)->sort(array('_id' => -1));
	foreach($sub as $doc) {
		echo '<li><a href="'.$baseurl.'blog/'.$doc["slug"].'">'.$doc["title"].'</a></li>';
	}
}

# Blog Categories
function blog_categories() {
	global $db, $baseurl;
	$sub = $db->blogcats->find()->limit(5);
	foreach($sub as $doc) {
		echo '<li><a href="'.$baseurl.'blog?cat='.$doc["_id"].'">'.$doc["name"].'</a></li>';
	}
}

# Get Parent Page
function get_parent() {
	echo str_replace("-"," ",$_GET['p']);
}

# Get Sub Page
function get_child() {
	echo str_replace("-"," ",$_GET['sp']);
}

# Login Check
function check() {

	if(isset($_SESSION['member'])) {
		$place = "";
	}
	else {
		global $baseurl;
		header('Location:'.$baseurl.'admin/');
	}
}

function cleartxt($content) {
	return preg_replace('/^\s+|\n|\r|\s+$/m', '', $content);
}

# Show Status
function status() {
	if(isset($_GET['st'])) {
		if($_GET['st'] === 'update') {
			echo '<div class="alert alert-success" role="alert">Successfully Updated</div>';
		}
		elseif($_GET['st'] === 'created') {
			echo '<div class="alert alert-success" role="alert">Successfully Created</div>';
		}
		elseif(@$_GET['error'] === 'error') {
			echo '<div class="alert alert-danger" role="alert">Error</div>';
		}
		elseif(@$_GET['login'] === 'error') {
			echo '<div class="alert alert-danger" role="alert">Wrong Password</div>';
		}
	}
}

# Pretty JSON Arary
function dump($data) {
    echo nl2br(str_replace(' ', '&nbsp;', print_r($data, true)));
}

function idToString( $array ){
	$i=0;
	foreach( $array as $a ){
		if( is_array($a) ){
			$new_id = (string)$a['_id'];
			$array[$i]['id'] = $new_id;
			unset($array[$i]['_id']);
		}
		$i++;
	}
	return $array;
}

# Query
function query( $array ){
	global $db;
	extract( $array );
	unset($array['model']);
	try {
		// if( array_key_exists('id', $array) ){
			// $array
		// }
		$return = $db->$model->find( $array );
		$return = idToString( iterator_to_array($return, false) );

		if( count($return) == 1 ){
			return $return[0];
		} else {
			return $return;
		}
	} catch (Exception $e) {
		return array('error' => 'nothing found');

	}
}

# Clean URLS
function cleanurl($var) {
	$string = strtolower(str_replace(' ', '-', $var));
	return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
}

# UNClean URLS
function uncleanurl($var) {
	$string = str_replace('-', ' ', $var);
	return preg_replace('/[^A-Za-z0-9\-]/', ' ', $string);
}

# Place?
function place($var) {
	$one = str_replace('<br><br>', '-', $var);
	echo nl2br($one);
}

# Message count
function message_count($type) {
	global $db;
	if($type == 'all') {
		echo $db->messages->find(array('status' => '1'))->count() + $db->registrations->find(array('status' => 'Pending'))->count();
	}
	if($type == 'incompany') {
		echo $db->messages->find(array('status' => '1', 'type' => $type))->count();
	}
	if($type == 'contact') {
		echo $db->messages->find(array('status' => '1', 'type' => $type))->count();
	}
	if($type == 'registrations') {
		echo $db->registrations->find(array('status' => 'Pending'))->count();
	}
	if($type == 'attendees') {
		echo $db->attendees->find()->count();
	}
}

# Get Instuctors
function get_instructors($ppl) {
	global $instructors, $baseurl;
	foreach($ppl as $person) {
		$query = $instructors->find(array('_id' => new MongoID($person)));
		foreach($query as $name) {
			echo '<li><a href="'.$baseurl.'about/instructors#'.$name['_id'].'">'.$name['fname'].' '.$name['lname'].'</a></li>';
		}
	}
}

# Send Email
function emailthis($email) {
	global $mustache, $baseurl;
	$template = $mustache->render('email_template', $email);
	$template = str_replace('&lt;', '<', $template);
	$template = str_replace('&gt;', '>', $template);
	$headers = "From: " . strip_tags($email['from']) . "\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
	mail($email['to'], $email['subject'], $template, $headers);
}

# Tinymce Weird Stuff
function tinymce($var) {
	$var = str_replace('<p>', '', $var);
	$var = str_replace('</p>', '', $var);
	$var = str_replace('<div>', '', $var);
	$var = str_replace('</div>', '', $var);
	$var = str_replace('&nbsp;', ' ', $var);
	return $var;
}

# Internal PHP Post
function httpPost($url,$params){
	$postData = '';
	foreach($params as $k => $v) {
		$postData .= $k . '='.$v.'&';
	}
	rtrim($postData, '&');
	$ch = curl_init();
	curl_setopt($ch,CURLOPT_URL,$url);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
	curl_setopt($ch,CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_POST, count($postData));
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
	$output=curl_exec($ch);
	curl_close($ch);
	return $output;
}

function lumos($collection, $name, $array) {

	# Start
	check();
	global $db, $place, $results;
	$id = ( isset($_GET['id']) && !empty($_GET['id']) ) ? $_GET['id'] : '';

	# Delete
	if(isset($_GET['delete'])) {
		$id = $_GET['delete'];
		$collection->remove(array('_id' => new MongoId($id)));
		header('location:?');
	}

	# Load Listing
	if(!strpos($_SERVER['REQUEST_URI'], $name.'/new') && !isset($_GET['id'])) {
		include DIR_VIEW.'/'.$name.'/'.$name.'-index.php';
		return;
	}

	# Edit Document
	if(isset($_GET['id']) && !empty($_GET['id'])) {
		$cursor = $collection->find(array('_id' => new MongoId($id)));
		$results = iterator_to_array($cursor, false);
		$results = $results[0];
	}

	# New Document
	if(isset( $_POST['new'])) {
		$collection->insert($array);
		$id = $results['_id'];
		header('location: '.ADMIN_URL.'/'.$name);
	}

	# Update Document
	if(isset( $_GET['st']) && $_GET['st'] == 'update') {
		$id = $_GET['id'];
		unset($results['_id']);
		$collection->update(array("_id" => new MongoId($id)), $array);
		header('location: '.ADMIN_URL.'/'.$name);
	}

	# Load Form
	include DIR_VIEW.'/'.$name.'/'.$name.'-form.php';
}


# Venue in USE
function venuecheck($id) {
	global $db;
	$count = 0;
	$cursor = $db->classes->find();
	foreach ($cursor as $doc) {
		if(isset($doc['class']['classes'])) {
			$docs = $doc['class']['classes'];
		    foreach($docs as $item) {
			    if($item['venue'] == $id) {
				    $count++;
			    }
		    }
		}
	}
	if($count > 0 ) {
		echo ' <a href="#"><button class="btn btn-danger" disabled="disabled">Remove</button></a> ';
	}
	else {
		echo ' <a href="?delete='.$id.'" onclick="return ask();"><button class="btn btn-danger">Remove</button></a> ';
	}
}

//(3/2/15)
//===================================================================
//======= The Router
//===================================================================

# Groups
function routergroup($value) {
	global $adminurl;
	Router::add($adminurl.$value.'/', DIR_CTRL.'/'.$value.'-controller.php'); // Landing
	Router::add($adminurl.$value.'/new/', DIR_CTRL.'/'.$value.'-controller.php'); // New
	Router::add($adminurl.$value.'/edit/', DIR_CTRL.'/'.$value.'-controller.php'); // Edit
}

function replaceHttp( $string ) {
	if( !preg_match('/https/', $string) ){
		return str_replace('http', 'https', $string);
	} else {
		return $string;
	}
}

function filter_input_array_with_default_flags($type, $filter, $flags, $add_empty = true) {
    $loopThrough = array();
    switch ($type) {
        case INPUT_GET : $loopThrough = $_GET; break;
        case INPUT_POST : $loopThrough = $_POST; break;
        case INPUT_COOKIE : $loopThrough = $_COOKIE; break;
        case INPUT_SERVER : $loopThrough = $_SERVER; break;
        case INPUT_ENV : $loopThrough = $_ENV; break;
    }
   
    $args = array();
    foreach ($loopThrough as $key=>$value) {
        $args[$key] = array('filter'=>$filter, 'flags'=>$flags);
    }
    
    return filter_input_array($type, $args, $add_empty);
}


?>
