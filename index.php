<?php

	die('Server temporarily down for maintenance. Please contact GuRuStu. 918-582-1881');


	# Config
	include('config.php');

	ob_start('ob_gzhandler');
	define('DIR_WEB', dirname(__FILE__).$baseurl);
	define('DIR_MODUP', dirname(__FILE__).'/public');
	define('DIR_TMPL', DIR_MODUP.'/template');
	define('DIR_CTRL', DIR_MODUP.'/controller');
	define('DIR_VIEW', DIR_MODUP.'/view');

	session_start();


	# Gobal Settings
	$settings = $db->settings->find();
	if($settings->count()) {
		foreach($settings as $doc) {
			$gobal = (object) array(
				'linkedin' => 'https://www.linkedin.com/pub/university-of-tulsa-cese/31/960/8a1',
				'facebook' => 'https://www.facebook.com/CESE.TU',
				'youtube' => $doc['youtube']
			);
		}
	}

	# 404
	$page = (object) array(
		'title' => 'Page not Found',
		'control' => '404'
	);


	# Enroll Process
	if( isset($_GET['p']) && $_GET['p'] == 'enroll' ){
		$page = (object) array(
			'title' => 'Course registration',
			'control' => 'enroll-controller',
			'meta' => ''
		);
	}

	# Courses Confirmation
	if( isset($_GET['p']) && $_GET['p'] == 'confirmation' ){
		$page = (object) array(
			'title' => 'Confirmation ',
			'control' => 'confirmation-controller',
			'meta' => ''
		);
	}

	# Conferences Confirmation
	if( isset($_GET['p']) && $_GET['p'] == 'conferences-confirmation' ){
		$page = (object) array(
			'title' => 'Confirmation ',
			'control' => 'confirmation',
			'meta' => ''
		);
	}
	if( isset($_GET['p']) && $_GET['p'] == 'payment' ){
		$page = (object) array(
			'title' => 'Payment',
			'control' => 'payment',
			'meta' => ''
		);
	}

	# Request a Quote
	if( isset($_GET['p']) && $_GET['p'] == 'request' ){
		$page = (object) array(
			'title' => 'Request a Quote',
			'control' => 'request-controller',
			'meta' => ''
		);
	}
	if( isset($_GET['p']) && $_GET['p'] == 'customize' ){
		$page = (object) array(
			'title' => 'Customize a Course',
			'control' => 'customize-controller',
			'meta' => ''
		);
	}

	# Contact
	if( isset($_GET['p']) && $_GET['p'] == 'contact' ){
		$page = (object) array(
			'title' => 'Contact',
			'control' => 'email-controller',
			'meta' => ''
		);
	}

	# Conferences
	if( isset($_GET['p']) && $_GET['p'] == 'conferences' ){
		$page = (object) array(
			'title' => 'Conferences',
			'control' => 'conferences-controller',
			'meta' => ''
		);
		$options = iterator_to_array($conferences->find(array('what' => 'options')), false);
		$options = $options[0];
		if(isset($_GET['sp'])) {
			if($_GET['sp'] == 'ipec') {
				$page = (object) array(
					'title' => 'IPEC Conference' ,
					'control' => 'ipec-controller',
					'meta' => ''
				);
			}
			if($_GET['sp'] == 'about-ipec') {
				$page = (object) array(
					'title' => 'IPEC: About IPEC' ,
					'control' => 'about-ipec-controller',
					'meta' => ''
				);
			}
			if($_GET['sp'] == 'session-chairs') {
				$page = (object) array(
					'title' => 'IPEC: Session Chairs / Presenter Info' ,
					'control' => 'tabs',
					'meta' => '',
					'what' => 'session-chairs'
				);
			}
			if($_GET['sp'] == 'sponsors') {
				$page = (object) array(
					'title' => 'IPEC: Corporate Sponsors' ,
					'control' => 'tabs',
					'meta' => '',
					'what' => 'sponsors'
				);
			}
			if($_GET['sp'] == 'attend') {
				$page = (object) array(
					'title' => 'IPEC: Attend' ,
					'control' => 'tabs',
					'meta' => '',
					'what' => 'attend'
				);
			}
			if($_GET['sp'] == 'exhibit') {
				$page = (object) array(
					'title' => 'IPEC: Exhibit' ,
					'control' => 'tabs',
					'meta' => '',
					'what' => 'exhibit'
				);
			}
			if($_GET['sp'] == 'highlights') {
				$page = (object) array(
					'title' => 'IPEC: Conference Highlights & Events',
					'control' => 'highlights-controller',
					'meta' => '',
				);
			}
			if($_GET['sp'] == 'technical-sessions') {
				$page = (object) array(
					'title' => 'IPEC: Technical Sessions',
					'control' => 'technical-sessions',
					'meta' => '',
				);
			}
			if($_GET['sp'] == 'venue') {
				$page = (object) array(
					'title' => 'IPEC: Venue, Travel & Accommodations',
					'control' => 'venue-controller',
					'meta' => '',
				);
			}
			if($_GET['sp'] == 'glance') {
				$page = (object) array(
					'title' => 'IPEC: Conference-at-a-Glance',
					'control' => 'glance-controller',
					'meta' => '',
				);
			}
			if($_GET['sp'] == 'registration') {
				$page = (object) array(
					'title' => 'IPEC: Registration' ,
					'control' => 'conference-registration-controller',
					'meta' => '',
				);
			}
			if($_GET['sp'] == 'attendee-registration') {
				$page = (object) array(
					'title' => 'IPEC: Conference Registration',
					'control' => 'attendee-registration',
					'meta' => '',
				);
			}
			if($_GET['sp'] == 'presenter-registration') {
				$page = (object) array(
					'title' => 'IPEC: Presenter Registration',
					'control' => 'presenter-registration',
					'meta' => '',
				);
			}
			if($_GET['sp'] == 'exhibitor-registration') {
				$page = (object) array(
					'title' => 'IPEC: Exhibitor Registration',
					'control' => 'exhibitor-registration',
					'meta' => '',
				);
			}

			# forms
			if($_GET['sp'] == 'presenter-abstract') {
				$page = (object) array(
					'title' => 'IPEC Abstract Submission Form',
					'control' => 'presenter-abstract',
					'meta' => '',
				);
			}
			if($_GET['sp'] == 'corporate-sponsorship-confirmation') {
				$page = (object) array(
					'title' => 'Corporate Sponsorship Reservation Form',
					'control' => 'corporate-sponsorship-confirmation',
					'meta' => '',
				);
			}
		}
	}

	if( isset($_GET['sp']) && $_GET['p'] == 'featured-conf' ){
			$course = idToString(iterator_to_array($db->featuredconferences->find( array('slug'=> $_GET['sp'] ) ), false));
			$course =  @$course[0];
			$options = iterator_to_array($conferences->find(array('what' => 'options')), false);
			$options = $options[0];

			$page = (object) array(
				'title' => $course['content'][0],
				'control' => 'featured-conf',
				'meta' => ''
			);
	}

	# Search
	if( isset($_GET['p']) && $_GET['p'] == 'search' ){
		$page = (object) array(
			'title' => 'Search',
			'control' => 'search',
			'meta' => ''
		);
	}

	#  courses
	$status = '';
	if( isset( $_GET['sp'] ) && isset($_GET['p']) && $_GET['p'] == 'courses' ){
		if($_GET['sp'] === 'instructors') {
			$page = (object) array(
				'title' => 'Instructors' ,
				'control' => 'instructors',
				'meta' => ''
			);
		}
		else {
			$course_slug = $_GET['sp'];
			$course = idToString(iterator_to_array($db->classes->find( array('slug'=> $course_slug ) ), false));
			$course =  @$course[0];
			if( $course ){
				if( $_GET['p'] === 'courses' ){
					$page = (object) array(
						'title' =>  $course['title'],
						'control' => 'course-controller',
						'single' => true,
						'meta' => $course['seo']
					);
				}
			}
		}
	}

	# Sub / Sub Page
	if(isset($_GET['sp'])) {
		$sub = $pages->find(array('slug' => $_GET['sp'], 'base' => $_GET['p']));
		if($sub->count()) {
			foreach($sub as $doc) {
				$page = (object) array(
					'title' => $doc['title'],
					'control' => 'default',
					'id' => $doc['_id'],
					'meta' => ''
				);
			}
		}
		$sub = $blog->find(array('slug' => $_GET['sp']));
		if($sub->count()) {
			foreach($sub as $doc) {
				$page = (object) array(
					'title' => $doc['title'],
					'control' => 'blog-posts',
					'id' => $doc['_id'],
					'meta' => ''
				);
			}
		}
		if($_GET['sp'] === 'staff') {
			$page = (object) array(
				'title' => 'Staff' ,
				'control' => 'staff',
				'meta' => ''
			);
		}

		if($_GET['sp'] === 'contact') {
			$page = (object) array(
				'title' => 'Contact',
				'control' => 'email-controller',
				'meta' => ''
			);
		}

		if($_GET['sp'] === 'training-calendar') {
			$page = (object) array(
				'title' => 'Training calendar',
				'control' => 'training-controller',
				'meta' => ''
			);
		}

		if($_GET['sp'] === 'in-company-clients') {
			$page = (object) array(
				'title' => 'In-Company Clients',
				'control' => 'incompany-clients',
				'meta' => ''
			);
		}

		if($_GET['sp'] === 'cese-courses' ){
			$page = (object) array(
				'title' => 'Courses',
				'control' => 'course-controller',
				'single' => false,
				'meta' => ''
			);
		}
		if($_GET['sp'] === 'instructors') {
			$page = (object) array(
				'title' => 'Instructors',
				'control' => 'instructors',
				'meta' => ''
			);
		}
		$abcs = range('A', 'Z');
		if (in_array($_GET['sp'], $abcs)) {
			$page = (object) array(
				'title' => 'Glossary Term '.$_GET['sp'],
				'control' => 'glossary-controller',
				'meta' => ''
			);
		}
		if($_GET['sp'] === 'faqs') {
			$page = (object) array(
				'title' => 'FAQs / Policies',
				'control' => 'faq-policies',
				'meta' => ''
			);
		}
		if($_GET['sp'] === 'courses-offered') {
			$page = (object) array(
				'title' => 'Courses Offered' ,
				'control' => 'incompany-controller',
				'meta' => '',
				'single' => false,
			);
		}

		$sub = $company->find(array('slug' => $_GET['sp']));
		if($sub->count()) {
			foreach($sub as $doc) {
				$page = (object) array(
					'title' => $doc['title'],
					'control' => 'incompany-controller',
					'id' => $doc['_id'],
					'meta' => '',
					'single' => true,
				);
			}
		}

	}

	elseif(isset($_GET['p'])) {
		if($_GET['p'] === 'about') {
			$parent = $pages->find(array('_id' => new MongoId('54adc6f7309a6eeb32fd7a79')));
			if($parent->count()) {
				foreach($parent as $doc) {
					$page = (object) array(
						'title' => $doc['title'],
						'control' => 'default',
						'id' => $doc['_id'],
						'meta' => ''
					);
				}
			}
		}
		# In-Company
		if($_GET['p'] == 'in-company-employee-development') {
			$page = (object) array(
				'title' => 'In-company Employee Development',
				'control' => 'in-company',
				'meta' => ''
			);
		}

		# Courses
		if($_GET['p'] === 'courses') {
			$page = (object) array(
				'title' => 'CESE Courses' ,
				'control' => 'cese-courses',
				'meta' => '',
				'single' => false,
			);
		}

		# Blog
		if($_GET['p'] === 'blog') {
			$page = (object) array(
				'title' => 'Blog',
				'control' => 'blog',
				'meta' => ''
			);
		}

		# Glossary
		if($_GET['p'] === 'glossary') {
			$page = (object) array(
				'title' => 'Glossary',
				'control' => 'glossary-default',
				'meta' => ''
			);
		}

		// Setup error page
		if($_GET['p'] === 'error') {
			$page = (object) array(
				'title' => 'There was an error processing your payment.',
				'control' => 'payment-error',
				'meta' => ''
			);
		}
		// Setup success page
		if($_GET['p'] === 'successful') {
			$page = (object) array(
				'title' => 'Your Payment was successful',
				'control' => 'payment-successful',
				'meta' => ''
			);
		}
		else {
			$parent = $pages->find(array('slug' => $_GET['p']));
			if($parent->count()) {
				foreach($parent as $doc) {
					$page = (object) array(
						'title' => $doc['title'],
						'control' => 'default',
						'id' => $doc['_id'],
						'meta' => ''
					);
				}
			}
		}

	}

	# Homepage
	else {
		$page = (object) array(
			'title' => 'The University of Tulsa',
			'control' => 'home',
			'meta' => ''
		);
	}

	// echo '<pre style="font-size: 12px;">';
	// print_r( $_POST );
	// echo '</pre>';
	
	// echo '<pre style="font-size: 12px;">';
	// echo 'DEBUG MODE ON<br>';
	// print_r( $_SESSION);
	// echo '</pre>';
	// echo '<pre style="font-size: 12px;">';
	// print_r( $_POST );
	// echo '<pre>';


	# Get Header / Control / Nav
	include DIR_TMPL.'/_header.php';
	include DIR_CTRL.'/'.$page->control.'.php';
	include DIR_TMPL.'/_footer.php';

?>
