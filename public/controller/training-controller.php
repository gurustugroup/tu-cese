<?php

	

	// Todo: We should get the data we need here ( the controller ), not in the "View"
	// 
	
	// Get all class information for list view
	$classes = $db->classes->find(array(
		'class.classes.start_date' => array('$ne' => '')
	))->sort(array(
		'class.classes.start_date' => 1
	));

	$class_array = iterator_to_array($classes, false);
	$classes = guru_sort_class_dates( $class_array );

	$week = 60*60*24*7;

	// Get all class information for Calendar view 
	// $all_classes variable made available by fabrication.php in root folder
	// 
	$json_string = '[';

	foreach($all_classes as $class) {
		
		$title = $class['title'];
		@$dow = $class['dow'];

		if(isset($class['class']['classes'])) {

			foreach($class['class']['classes'] as $list) {

				$curdate = strtotime(date("Y-m-d"));
				$mydate = strtotime($list['start_date']);
				
				if($mydate > $curdate) {

					$start_date = $list['start_date'];
					$end_date = $list['end_date'];

					// error_log( $end_date );

					// Convert to format calendar needs
					$start = date("Y-m-d", strtotime($start_date));
					$end = date("Y-m-d", strtotime($end_date . ' + 1 day'));

					if( !empty($dow) ){
						
						$end = date("Y-m-d", strtotime($end_date));

						$start_repeating_days = date('U', strtotime($start_date) );
						$end_repeating_days = date('U', strtotime($end_date) );

						if( $title == 'PE Mechanical Engineering Exam Review' ) {

							// error_log('--'.$title.'--');
							// error_log( 'START: ' .$start_repeating_days );
							// error_log( 'END: ' . $end_repeating_days);
							// error_log( 'MULTIPLIED BY 7 WEEKS: ' . ($start_repeating_days + ( $week * 6 )) );
							// error_log('----');

						}

						$current_week = $start_repeating_days - $week;
						$mycounter = 1;

						while( $current_week <= $end_repeating_days ){

							if( $title == 'PE Mechanical Engineering Exam Review' ) {
								// error_log('my counter');
								// error_log($mycounter++);
							}

							$current_week = $current_week + $week;
							$start = date('Y-m-d', $current_week);
							$end = date('Y-m-d', $current_week);
							$json_string .= "{ title: '".$title."', start: '".$start."', end: '".$end."', url: '".BASE_URL."courses/".$class['slug']."' },";
							
						}
						
					} else {
						$json_string .= "{ title: '".$title."', start: '".$start."', end: '".$end."', url: '".BASE_URL."courses/".$class['slug']."' },";
					}

				
				}

			}

		}

	}

	$json_string .= "{title: 'Click for Google',url: 'http://google.com/',start: '2014-11-28'}";
	$json_string .= ']';

	include DIR_VIEW.'/courses/training.php';
?>

