<?php
	
	$get = new stdClass;
	$incompany = $meta->find(array("what" => "incompanyclients"));
	foreach($incompany as $doc) {
		$get->image = replaceHttp($doc['image']);
		$get->world = $doc['world'];
		$get->testimonials = $doc['testimonials'];
		$get->sidebar = new stdClass;
		$get->sidebar->title = $doc['ctitle'];
		$get->sidebar->content = $doc['cmessage'];
		$get->sidebar->page = $doc['cpage'];
		$get->sidebar->slug = $doc['cpage'];
		
	}
	include DIR_VIEW.'/incompany/incompany-clients.php';
?>