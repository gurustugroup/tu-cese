<?php  
	$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_SPECIAL_CHARS);
	$_GET = filter_input_array(INPUT_GET, FILTER_SANITIZE_SPECIAL_CHARS);

	if(isset($_POST['name'])) {
		
		extract($_POST);
		
		$time = date("F j, Y, g:i a");
		
		$to = $people->public_contact;
		
		$arr = array (
			'name' => $name,
			'to' => $to,
			'phone' => $phone,
			'email' => $email,	
			'message' => $message,
			'status' => '1',
			'time' => $time,
			'type' => 'contact',
		);
		
		$email = array(
			'to' => $to,
			'from' => $email,
			'subject' => 'CESE Website Contact message',
			'message' => $message,
			'h1' => 'Hello from '.$name,
			'group' => array(
				'<tr><td width=300px;><b>Sent to</b></td><td>'.$to.'</td></tr>',
				'<tr><td width=300px;><b>Time / Date Submitted</b></td><td>'.$time.'</td></tr>',
				'<tr><td width=300px;><b>Email Address</b></td><td>'.$email.'</td></tr>',
				'<tr><td width=300px;><b>Phone Number</b></td><td>'.$phone.'</td></tr>',
			),
		);
		
		emailthis($email);
		$messages->insert($arr);
		header("Location: ?thanks");
	}

	include DIR_VIEW.'/email-view.php';


?>