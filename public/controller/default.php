<?php
	
	#(11/20/14)	
	#===================================================================
	#======= Getting Page Data
	#===================================================================
	$get = new stdClass;
	$sub = $pages->find(array("_id" => new MongoId($page->id), "public" => "on"));
	foreach($sub as $doc) {
		// Base
		$get->title = $doc['title'];
		$get->content = $doc['content'];
		$get->slug = $doc['slug'];
		
		// Sidebar
		$get->sidebar = new stdClass;
		$get->sidebar->title = $doc['ctitle'];
		$get->sidebar->content = $doc['cmessage'];
		$get->sidebar->page = $doc['cpage'];
		$get->sidebar->slug = $doc['cpage'];
	}
	
	include DIR_VIEW.'/default.php';
?>

