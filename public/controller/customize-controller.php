<?php  

	$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_SPECIAL_CHARS);
	$_GET = filter_input_array(INPUT_GET, FILTER_SANITIZE_SPECIAL_CHARS);

	include DIR_VIEW.'/incompany/customize-view.php';
	
	if(isset($_POST['contact'])) {
		extract($_POST);
		
		$time = date("F j, Y, g:i a");
		
		$arr = array (
			'company' => $company,
			'address' => $address,
			'city' => $city,
			'state' => $state,	
			'zip' => $zip,
			'country' => $country,
			'name' => $name,
			'title' => $title,
			'phone' => $phone,
			'email' => $email,
			'class' => $class,
			'participants' => $participants,
			'dates' => $dates,
			'type_class' => $type,
			'objective' => $objective,
			'comments' => $comments,
			'status' => '1',
			'time' => date("F j, Y, g:i a"),
			'type' => 'incompany',
		);
		
		$email = array(
			'to' => $people->customize_course,
			'from' => $email,
			'subject' => 'CESE Customize a Program',
			'h1' => 'Customize a Program Requested from '.$company,
			'group' => array(
				'<tr><td width=250px;><b>Company</b></td><td>'.$company.'</td></tr>',
				'<tr><td width=250px;><b>Address</b></td><td>'.$address.'</td></tr>',
				'<tr><td width=250px;><b>City</b></td><td>'.$city.'</td></tr>',
				'<tr><td width=250px;><b>State</b></td><td>'.$state.'</td></tr>',
				'<tr><td width=250px;><b>Zip</b></td><td>'.$zip.'</td></tr>',
				'<tr><td width=250px;><b>Country</b></td><td>'.$country.'</td></tr>',
			),
			'2' => true,
			'group2' => array(
				'<tr><td width=250px;><b>Name</b></td><td>'.$name.'</td></tr>',
				'<tr><td width=250px;><b>Job Title</b></td><td>'.$title.'</td></tr>',
				'<tr><td width=250px;><b>Phone Number</b></td><td>'.$phone.'</td></tr>',
				'<tr><td width=250px;><b>Email</b></td><td>'.$email.'</td></tr>',
			),
			'3' => true,
			'group3' => array(
				'<tr><td width=250px;><b>Name of Class</b></td><td>'.$class.'</td></tr>',
				'<tr><td width=250px;><b># of Participants</b></td><td>'.$participants.'</td></tr>',
				'<tr><td width=250px;><b>Preferred Dates</b></td><td>'.$dates.'</td></tr>',
				'<tr><td width=250px;><b>Type of class</b></td><td>'.$type_class.'<br/></td></tr>',
				'<tr><td width=250px;><b>Class Objective / Overall Goal</b></td><td>'.$objective.'<br/></td></tr>',
				'<tr><td width=250px;><b>Comments</b></td><td>'.$comments.'<br/></td></tr>',
			),
		);

		
		emailthis($email);
		$messages->insert($arr);
		header("Location: ?thanks");
	}




?>