<?php
	
	#(11/20/14)	
	#===================================================================
	#======= Getting Call to Action
	#===================================================================
	$get = new stdClass;
	$home = $cta->find(array("name" => "homepage"));
	foreach($home as $doc) {
		$get->cta = new stdClass;
		$get->cta->content = $doc['content'];
		$get->cta->slug = $doc['url'];
		$get->cta->title = $doc['title'];
	}
	
	if(isset($_POST['newsletter'])) {

		$_POST['newsletter'] = strip_tags( $_POST['newsletter'] );
		$_POST['vEmail'] = strip_tags($_POST['vEmail']);
		if( filter_var( $_POST['vEmail'], FILTER_VALIDATE_EMAIL ) ) {

			$templale = @$mustache->render('newsletter', $_POST);

			$templale = str_replace('&lt;', '<', $templale);
			$templale = str_replace('&gt;', '>', $templale);

			$headers = "From: " . $people->public_contact . "\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

			mail($people->public_contact, 'Email List Signup', $templale, $headers);
			header("Location: ?signedup");

		} else {
			header("Location: ?invalid_email");
		}

	}
	
	include DIR_VIEW.'/home.php';
?>