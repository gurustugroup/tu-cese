<?php
	
	#(11/20/14)	
	#===================================================================
	#======= Getting Page Data
	#===================================================================
	$get = new stdClass;
	$sub = $pages->find(array("title" => "Payment Error"));
	foreach($sub as $doc) {
		
		// Base
		$get->title = $doc['title'];
		$get->content = $doc['content'];
		
		// Sidebar
		$get->sidebar = new stdClass;
		$get->sidebar->title = $doc['ctitle'];
		$get->sidebar->content = $doc['cmessage'];
		$get->sidebar->page = $doc['cpage'];
		$get->sidebar->slug = $doc['cpage'];
	}
	
	$cursor = $db->registrations->find(array("confirmation" => $_COOKIE['confirmation']));
	$array = iterator_to_array($cursor, false);
	$array = $array[0];
	
	$email = array(
		'to' => $array['email'][0],
		'from' =>  'cese@utulsa.edu',
		'message' => 'Your payment was not successful.  Please call 918-631-3088.',
		'subject' => 'Your CESE Payment wasn\'t Successful',
		'h1' => 'Hello '.$array['FirstName'][0].' '.$array['LastName'][0],
		'payment' => true,
		'total' => $array['eker'],
		'confirmation' => $array['confirmation'],
		'enroll' => true,
	);
	
	$email['group'] = array(
		'<tr><td width=200px;><b>Confirmation Number</b></td><td>'.$array['confirmation'].'</td></tr>',
		'<tr><td width=200px;><b>Total</b></td><td> $'.number_format($array['eker'], 2).'</td></tr>',
	);
	
	emailthis($email);
	
	$newdata = array('$set' => array("status" => "failed"));
	$db->registrations->update(array("confirmation" => $_COOKIE['confirmation']), $newdata);
	
	include DIR_VIEW.'/default.php';
?>

