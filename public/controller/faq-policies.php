<?php
	
	#(11/20/14)	
	#===================================================================
	#======= Getting Page Data
	#===================================================================
	$get = new stdClass;
	$sub = $pages->find(array("slug" => "faq-policies"));
	foreach($sub as $doc) {
		// Base
		$get->title = $doc['title'];
		$get->content = $doc['content'];
		
		// Sidebar
		$get->sidebar = new stdClass;
		$get->sidebar->title = $doc['ctitle'];
		$get->sidebar->content = $doc['cmessage'];
		$get->sidebar->page = $doc['cpage'];
		$get->sidebar->slug = $doc['cpage'];
	}
	
	$faqs = $pages->find(array("_id" => new MongoId('54adb9e6309a6e5f2dfd7a79')));
	foreach($faqs as $doc) {
		$get->faq = $doc['content'];
		$get->policies = $doc['policies'];
	}
	
	
	include DIR_VIEW.'/faq.php';
?>

