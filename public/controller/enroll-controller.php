<?php  


	// XSS vulnerable

	if(isset($_POST['fee'])) {
		
		# Get Request
		extract($_POST);
		$fees = explode( ' | ', $fee );
		$course_id = $id;
		$venue_id = $fees[0];
		$start_date = $fees[1];
		$rate = $fees[2];
		$qty = $fee_qty;
		
		# Get Course Colletion
		$course = iterator_to_array($classes->find(array('_id' => new MongoID($course_id))), false);
		$course = $course[0];
		
		# Get Class Colletion
		$query = $classes->find(array('_id' => new MongoID($course_id)));
		foreach($query as $item) {
			foreach($item['class']['classes'] as $itemm) {
				if(date("mdy", strtotime($itemm['start_date'])) == $start_date && $itemm['venue'] == $venue_id) {
					$class['start_date'] = $itemm['start_date'];
					$class['end_date'] = $itemm['end_date'];
					$class['code'] = $itemm['code'];
					foreach($itemm['fee'] as $itemmm) {
						if($itemmm['rate'] == $rate) {
							$class['title'] = $itemmm['title'];
							$class['rate'] = $itemmm['rate'];
							$class['registrants'] = $itemmm['registrants'];
							if($class['registrants'] < 1) {
								$class['registrants'] = 1;
								$qty = $class['registrants'];
							}
							else {
								$class['registrants'] = $itemmm['registrants'];
								if($qty > $class['registrants']) {
									$qty = $qty;
								}
								else {
									$qty = $class['registrants'];
								}

							}
							$class['expire'] = $itemmm['expire'];
						}	
					}
				}
			}
		}
		
		if(isset($_POST['online'])) {
			$online = 1;
		}
		else {
			$online = 0;
		}
		
		# Get Venue Collection
		$venue = iterator_to_array($venues->find(array('_id' => new MongoID($venue_id))), false);
		$venue = $venue[0];

		# Start View
		include DIR_VIEW.'/courses/enroll-view.php';
	}
	else {
		header("Location: ".$baseurl);
	}
	


?>