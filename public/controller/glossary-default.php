<?php
	
	#(11/20/14)	
	#===================================================================
	#======= Getting Page Data
	#===================================================================
	$get = new stdClass;
	$sub = $meta->find(array("what" => "glossarylanding"));
	foreach($sub as $doc) {
		// Base
		$get->title = 'Glossary';
		$get->content = $doc['content'];
		
		// Sidebar
		$get->sidebar = new stdClass;
		$get->sidebar->title = $doc['ctitle'];
		$get->sidebar->content = $doc['cmessage'];
		$get->sidebar->page = $doc['cpage'];
		$get->sidebar->slug = $doc['cpage'];
	}
	
	
	include DIR_VIEW.'/glossary/glossary-landing.php';
?>

