<?php
	
	$get = new stdClass;
	$sub = $blog->find(array("_id" => new MongoId($page->id), "public" => "on"));
	foreach($sub as $doc) {
		// Base
		$get->title = $doc['title'];
		$get->content = $doc['content'];
		$get->image = replaceHttp($doc['image']);
		$get->time = $doc['time'];
	}
	
	
	include DIR_VIEW.'/blog/single-blog.php';
?>

