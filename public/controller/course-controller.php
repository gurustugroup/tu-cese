<?php
	
	if( $page->single ){
		include DIR_VIEW.'/courses/single-course.php';
	} else {
		$courses = idToString(iterator_to_array($db->classes->find(), false));
		include DIR_VIEW.'/courses/courses-index.php';

	}

?>