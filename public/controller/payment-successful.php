<?php
	
	/**
	 *
	 *	Confirmation number
	 *		- valid format
	 *		- matched cookie to GET param
	 *		- is set
	 * 
	 */
	$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_SPECIAL_CHARS);
	$_GET = filter_input_array(INPUT_GET, FILTER_SANITIZE_SPECIAL_CHARS);

	if( !isset( $_GET['confirmation'] ) ){
		die();
	}

	#(11/20/14)	
	#===================================================================
	#======= Getting Page Data
	#===================================================================
	$get = new stdClass;
	$sub = $pages->find(array("title" => "Payment Success"));
	foreach($sub as $doc) {
		// Base
		$get->title = $doc['title'];
		$get->content = $doc['content'];
		
		// Sidebar
		$get->sidebar = new stdClass;
		$get->sidebar->title =   $doc['ctitle'];
		$get->sidebar->content = $doc['cmessage'];
		$get->sidebar->page =    $doc['cpage'];
		$get->sidebar->slug =    $doc['cpage'];
	}
	
	$newdata = array('$set' => array("status" => "success"));
	$db->registrations->update(array("confirmation" => strip_tags($_GET['confirmation'])), $newdata);
	
	
	$cursor = $db->registrations->find(array("confirmation" => $_GET['confirmation']));
	$array = iterator_to_array($cursor, false);


	$array = $array[0];
	
	$email = array(
		'to' => $array['email'][0],
		'from' =>  'cese@utulsa.edu',
		'subject' => 'Your CESE Payment was successful',
		'h1' => 'Hello '.$array['FirstName'][0].' '.$array['LastName'][0],
		'payment' => true,
		'total' => $array['eker'],
		'confirmation' => $array['confirmation'],
		'enroll' => true,
		'footer' => array(
			'If you wish to make overnight accommodations, contact the hotel directly for reservations. Specify you are with The University of Tulsa seminar for special group rates. To ensure space availability and group rate, reservations should be made one month prior to the seminar date.',
			'Please check the spelling of your name and other information above. This is how it will appear in our permanent records and on the information at the program. If any changes need to be made, notify us at 918-631-2347 no later than three days prior to the program.',
			'Seminar rooms are typically cool. To ensure your comfort during the course, you may want to bring a jacket or sweater.',
			'We are looking forward to having you participate in this excellent continuing education program. If for any reason you cannot attend, please notify our office as soon as possible. Substitutions are permitted at any time. ',
			'All paid seminar fees will be refunded in the unlikely event the course is canceled. Those who cancel ten working days or less prior to the seminar will receive a refund less an administrative fee of $95. Refunds will not be granted after class has begun or for nonattendance. ',
			'Enjoy the Class!',
		)
	);
	
	$email['group'] = array(
		'<tr><td width=200px;><b>Confirmation Number</b></td><td>'.$array['confirmation'].'</td></tr>',
		'<tr><td width=200px;><b>Total</b></td><td> $'.number_format($array['eker'], 2).'</td></tr>',
	);
	
	emailthis($email);
	
	include DIR_VIEW.'/default.php';
?>

