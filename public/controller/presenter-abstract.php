<?php

	include DIR_VIEW.'/conferences/presenter-abstract.php';
	
	$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_SPECIAL_CHARS);
	$_GET = filter_input_array(INPUT_GET, FILTER_SANITIZE_SPECIAL_CHARS);

	if(isset($_POST['title'])) {

		$templale = $mustache->render('abstract', $_POST);

		$headers = "From: " . strip_tags($_POST['Email_Address']) . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

		mail($people->abstract, 'IPEC Abstract Submission', $templale, $headers);
		mail(strip_tags($_POST['Email_Address']), 'IPEC Abstract Submission', $templale, $headers);

	}
?>
