<?php
	
	#(12/8/14)	
	#===================================================================
	#======= Getting Page Data
	#===================================================================
	$get = new stdClass;
	$incompany = $meta->find(array("what" => "incompanylanding"));
	foreach($incompany as $doc) {
		$get->image = replaceHttp($doc['image']);
		$get->mission_statement = $doc['mission_statement'];
		$get->mission_statement_explained = $doc['mission_statement_explained'];
		$get->training_dollars = $doc['training_dollars'];
		$get->advantages = $doc['advantages'];
		$get->slider_message = $doc['slider_message'];
		$get->sidebar = new stdClass;
		$get->sidebar->title = $doc['ctitle'];
		$get->sidebar->content = $doc['cmessage'];
		$get->sidebar->page = $doc['cpage'];
		$get->sidebar->slug = $doc['cpage'];
		
	}
	
	include DIR_VIEW.'/incompany/incompany-index.php';
?>
