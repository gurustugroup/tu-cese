<?php
	$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_SPECIAL_CHARS);
	$_GET = filter_input_array(INPUT_GET, FILTER_SANITIZE_SPECIAL_CHARS);
	
	//dump($_POST);
	$confirmation = rand(100000, 150000).rand(100000, 150000);
	$registrations = count($_POST['FirstName']);
	$total = $_POST['eker'] * $registrations;
	$_POST['status'] = 'Pending';
	$_POST['date'] = date("F j, Y, g:i a");
	$_POST['confirmation'] = $confirmation;
	$_POST['eker'] = $total;


	// Inserting Raw post, not good...
	$db->attendees->insert($_POST);

	if(isset($_POST['conferences'])) {
		setcookie('conferences', $confirmation, time()+960000);

		$_POST['group'] = array(
			'<b>First Name</b>: '.strip_tags($_POST['FirstName'][0]),
			'<b>Last Name</b>: '.strip_tags($_POST['LastName'][0]),
			'<b>Name on badge</b>: '.strip_tags($_POST['nameonbadge'][0]),
			'<b>Title</b>: '.strip_tags($_POST['title'][0]),
			'<b>Department</b>: '.strip_tags($_POST['department'][0]),
			'<b>Phone</b>: '.strip_tags($_POST['phone'][0]),
			'<b>Fax</b>: '.strip_tags($_POST['fax'][0]),
			'<b>Email</b>: '.strip_tags($_POST['email'][0]),
		);

		if(isset($_POST['FirstName'][1])) {
			$_POST['group1'] = array(
				'<tr><b>First Name</b>: '.strip_tags($_POST['FirstName'][1]).'</td></tr>',
				'<tr><b>Last Name</b>: '.strip_tags($_POST['LastName'][1]).'</td></tr>',
				'<tr><b>Name on badge</b>: '.$_POST['nameonbadge'][1].'</td></tr>',
				'<tr><b>Title</b>: '.strip_tags($_POST['title'][1]).'</td></tr>',
				'<tr><b>Department</b>: '.strip_tags($_POST['department'][1]).'</td></tr>',
				'<tr><b>Phone</b>: '.strip_tags($_POST['phone'][1]).'</td></tr>',
				'<tr><b>Fax</b>: '.strip_tags($_POST['fax'][1]).'</td></tr>',
				'<tr><b>Email</b>: '.strip_tags($_POST['email'][1]).'</td></tr>',
			);
		}

		if(isset($_POST['FirstName'][2])) {
			$_POST['group2'] = array(
				'<b>First Name</b>: '.strip_tags($_POST['FirstName'][2]),
				'<b>Last Name</b>: '.strip_tags($_POST['LastName'][2]),
				'<b>Name on badge</b>: '.strip_tags($_POST['nameonbadge'][2]),
				'<b>Title</b>: '.strip_tags($_POST['title'][2]),
				'<b>Department</b>: '.strip_tags($_POST['department'][2]),
				'<b>Phone</b>: '.strip_tags($_POST['phone'][2]),
				'<b>Fax</b>: '.strip_tags($_POST['fax'][2]),
				'<b>Email</b>: '.strip_tags($_POST['email'][2]),
			);
		}

		$templale = @$mustache->render('conference_email', $_POST);

		$templale = str_replace('&lt;', '<', $templale);
		$templale = str_replace('&gt;', '>', $templale);

		$headers = "From: " . strip_tags($_POST['myemail']) . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

		mail($people->public_contact, 'IPEC Abstract Submission', $templale, $headers);
		mail(strip_tags($_POST['myemail']), 'IPEC Abstract Submission', $templale, $headers);
	}

	elseif(isset($_POST['course'])) {

		$db->registrations->insert($_POST);
		
		if(!isset($_COOKIE['confirmation'])) {
			setcookie('confirmation', $confirmation, time()+960000);
		}

		$email = array(
			'to' => strip_tags($_POST['email'][0]),
			'from' => 'cese@utulsa.edu',
			'subject' => 'Course Registration Confirmation',
			'message' => 'Thank you for registering online with the Continuing Engineering & Science Education (CESE) of the University of Tulsa. We have received your registration information and payment for the following:',
			'h1' => 'Hello '.strip_tags($_POST['FirstName'][0]).' '.strip_tags($_POST['LastName'][0]),
			'enroll' => true,
			'questionscomments' => strip_tags($_POST['questionscomments']),
			'total' => strip_tags($_POST['eker']),
			'status' => strip_tags($_POST['status']),
			'confirmation' => strip_tags($_POST['confirmation']),
			'course_S' => true,
			'venue_S' => true,
			'footer' => array(
				'If you wish to make overnight accommodations, contact the hotel directly for reservations. Specify you are with The University of Tulsa seminar for special group rates. To ensure space availability and group rate, reservations should be made one month prior to the seminar date.',
				'Please check the spelling of your name and other information above. This is how it will appear in our permanent records and on the information at the program. If any changes need to be made, notify us at 918-631-2347 no later than three days prior to the program.',
				'Seminar rooms are typically cool. To ensure your comfort during the course, you may want to bring a jacket or sweater.',
				'We are looking forward to having you participate in this excellent continuing education program. If for any reason you cannot attend, please notify our office as soon as possible. Substitutions are permitted at any time. ',
				'All paid seminar fees will be refunded in the unlikely event the course is canceled. Those who cancel ten working days or less prior to the seminar will receive a refund less an administrative fee of $95. Refunds will not be granted after class has begun or for nonattendance. ',
				'Enjoy the Class!',
				'Thank you for your registration! You will recieve a registration confirmation leter with more information!',
			)
		);

		# Base
		$email['group'] = array(
			'<tr><td style="text-align: left"  width=200px;><b>Confirmation Number</b></td><td style="text-align: left" >'.$confirmation.'</td></tr>',
			'<tr><td style="text-align: left"  width=200px;><b>First Name</b></td><td style="text-align: left" >'.strip_tags($_POST['FirstName'][0]).'</td></tr>',
			'<tr><td style="text-align: left"  width=200px;><b>Last Name</b></td><td style="text-align: left" >'.strip_tags($_POST['LastName'][0]).'</td></tr>',
			'<tr><td style="text-align: left"  width=200px;><b>Name on badge</b></td><td style="text-align: left" >'.strip_tags($_POST['nameonbadge'][0]).'</td></tr>',
			'<tr><td style="text-align: left"  width=200px;><b>Title</b></td><td style="text-align: left" >'.strip_tags($_POST['title'][0]).'</td></tr>',
			'<tr><td style="text-align: left"  width=200px;><b>Department</b></td><td style="text-align: left" >'.strip_tags($_POST['department'][0]).'</td></tr>',
			'<tr><td style="text-align: left"  width=200px;><b>Phone</b></td><td style="text-align: left" >'.strip_tags($_POST['phone'][0]).'</td></tr>',
			'<tr><td style="text-align: left"  width=200px;><b>Fax</b></td><td style="text-align: left" >'.strip_tags($_POST['fax'][0]).'</td></tr>',
			'<tr><td style="text-align: left"  width=200px;><b>Email</b></td><td style="text-align: left" >'.strip_tags($_POST['email'][0]).'</td></tr>',
		);
		# Course
		$course_array = array();
		$cursor = $db->classes->find(array('_id' => new MongoId(strip_tags($_POST['course']))));
		$array = iterator_to_array($cursor, false);
		$array = $array[0];
		array_push($course_array, '<tr><td style="text-align: left"  width=200px;><b>Course Title</b></td><td style="text-align: left" >'.$array['title'].'</td></tr>');
		array_push($course_array, '<tr><td style="text-align: left"  width=200px;><b>Length</b></td><td style="text-align: left" >'.$array['days'].'</td></tr>');
		array_push($course_array, '<tr><td style="text-align: left"  width=200px;><b>Time</b></td><td style="text-align: left" >'.$array['time'].'</td></tr>');
		array_push($course_array, '<tr><td style="text-align: left"  width=200px;><b>Start Date</b></td><td style="text-align: left" ><i>'.$_POST['start_date'].'</i></td></tr>');
		array_push($course_array, '<tr><td style="text-align: left"  width=200px;><b>End Date</b></td><td style="text-align: left" ><i>'.$_POST['end_date'].'</i></td></tr>');
		array_push($course_array, '<tr><td style="text-align: left"  width=200px;><b>Total Cost</b></td><td style="text-align: left" >$ '.number_format($total, 2).'</td></tr>');
		$the_title = $array['title'];
		$the_days = $array['days'];
		$the_time = $array['time'];
		$email['course'] = $course_array;
		## Stop Course


		$venue_array = array();
		if(isset($_POST['online'])) {

		}
		else {

			# Venue
			$venues = $array['class']['classes'];
			array_push($venue_array, '<tr><td style="text-align: left"  width=200px;><b>Venue Name</b></td><td style="text-align: left" >'.$_POST['v_name'].'</td></tr>');
			array_push($venue_array, '<tr><td style="text-align: left"  width=200px;><b>Venue Address</b></td><td style="text-align: left" >'.$_POST['v_address'].'</td></tr>');
			array_push($venue_array, '<tr><td style="text-align: left"  width=200px;><b></b></td><td style="text-align: left" >'.$_POST['v_city'].'</td></tr>');
			array_push($venue_array, '<tr><td style="text-align: left"  width=200px;><b>Phone Number</b></td><td style="text-align: left" >'.$_POST['v_phone'].'</td></tr>');
			$email['venue'] = $venue_array;
			# Stop Venue
		}

		if(count($_POST['FirstName']) > 1) {
			$i = -1;
			foreach($_POST['FirstName'] as $key) {
				$i++;
				$loop = array(
					'<tr><td style="text-align: left"  width=200px;><b>First Name</b></td><td style="text-align: left" >'.$key.'</td></tr>',
					'<tr><td style="text-align: left"  width=200px;><b>Last Name</b></td><td style="text-align: left" >'.strip_tags($_POST['LastName'][$i]).'</td></tr>',
					'<tr><td style="text-align: left"  width=200px;><b>Name on badge</b></td><td style="text-align: left" >'.strip_tags($_POST['nameonbadge'][$i]).'</td></tr>',
					'<tr><td style="text-align: left"  width=200px;><b>Title</b></td><td style="text-align: left" >'.strip_tags($_POST['title'][$i]).'</td></tr>',
					'<tr><td style="text-align: left"  width=200px;><b>Department</b></td><td style="text-align: left" >'.strip_tags($_POST['department'][$i]).'</td></tr>',
					'<tr><td style="text-align: left"  width=200px;><b>Phone</b></td><td style="text-align: left" >'.strip_tags($_POST['phone'][$i]).'</td></tr>',
					'<tr><td style="text-align: left"  width=200px;><b>Fax</b></td><td style="text-align: left" >'.strip_tags($_POST['fax'][$i]).'</td></tr>',
					'<tr><td style="text-align: left"  width=200px;><b>Email</b></td><td style="text-align: left" >'.strip_tags($_POST['email'][$i]).'</td></tr>',
				);


				if(isset($_POST['online'])) {
					array_push($loop,'<tr><td style="text-align: left"  width=200px;><b>Start Date </b></td><td style="text-align: left" >'.strip_tags($_POST['online'][0]).'</td></tr>');
				}
				$email[$i]['group'.$i] = $loop;
			}

		}

		# Start Customer Email
		emailthis($email);
		## End Customer Email

		# Staff Email
		$email['to'] = 'cese@utulsa.edu';
		$email['from'] = $_POST['email'][0];
		$email['subject'] = 'New Course Registration';
		$email['h1'] = 'New Course Registration';
		$email['footer'] = array();
		$email['message'] = '';
		emailthis($email);
	## End Staff Email

	}


	if(strpos($thispage,'UPAY_SITE_ID') !== false) {
		header("Location:".BASE_URL."error");
	}
	include DIR_VIEW.'/courses/confirmation.php';
