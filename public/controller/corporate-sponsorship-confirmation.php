<?php

$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_SPECIAL_CHARS);
$_GET = filter_input_array(INPUT_GET, FILTER_SANITIZE_SPECIAL_CHARS);

if( isset( $_POST['sponsor_submission'] ) ){

	$corporate_sponsor_request->insert( $_POST );
	extract($_POST);
	
	$time = date("F j, Y, g:i a");
	
	$to = $people->public_contact;

	
	$arr = array (
		'name' => strip_tags($_POST['Company_Contact_Name']),
		'to' => $to,
		'phone' => strip_tags($_POST['Company_Phone']),
		'status' => '1',
		'time' => $time,
		'type' => 'sponsor-submission',
	);
	
	$email = array(
		'to' => $to,
		'from' => 'cese@utulsa.edu',
		'subject' => 'Corporate Sponsorship Reservation Submission',
		'h1' => 'Hello from '. strip_tags($_POST['Company_Contact_Name']),
		'group' => array(
			'<tr><td width=300px;><b>Sent to</b></td><td>'.$to.'</td></tr>',
			'<tr><td width=300px;><b>Company/Affiliation</b></td><td>'.strip_tags($_POST['Company_/_Affiliation']).'</td></tr>',
			'<tr><td width=300px;><b>Address</b></td><td>'.strip_tags($_POST['Address']).'</td></tr>',
			'<tr><td width=300px;><b>City</b></td><td>'.strip_tags($_POST['City']).'</td></tr>',
			'<tr><td width=300px;><b>State</b></td><td>'.strip_tags($_POST['State']).'</td></tr>',
			'<tr><td width=300px;><b>Zip</b></td><td>'.strip_tags($_POST['Zip']).'</td></tr>',
			'<tr><td width=300px;><b>Country</b></td><td>'.strip_tags($_POST['Country']).'</td></tr>',
			'<tr><td width=300px;><b>Company Phone</b></td><td>'.strip_tags($_POST['Company_Phone']).'</td></tr>',
			'<tr><td width=300px;><b>Company Website</b></td><td>'.strip_tags($_POST['Company_Website']).'</td></tr>',
			'<tr><td width=300px;><b>Company Contact Name</b></td><td>'.strip_tags($_POST['Company_Contact_Name']).'</td></tr>',
			'<tr><td width=300px;><b>Contact\'s Phone Number</b></td><td>'.strip_tags($_POST['Contact\'s_Phone_Number']).'</td></tr>',
			'<tr><td width=300px;><b>Contact\'s Email</b></td><td>'.strip_tags($_POST['Contact\'s_Email']).'</td></tr>'
		),
	);
	emailthis($email);
}


	#!! {ISSUE} Corporate Sponsorship Confirmation needs to be finished.
	include DIR_VIEW.'/conferences/corporate-sponsorship-confirmation.php';
?>