<?php
	
	#(12/8/14)	
	#===================================================================
	#======= Getting Page Data
	#===================================================================
	$get = new stdClass;
	
	
	#===================================================================
	#======= Getting Call to Action
	#===================================================================
	$home = $cta->find(array("name" => "sidebar"));
	foreach($home as $doc) {
		$get->cta = new stdClass;
		$get->cta->content = $doc['content'];
		$get->cta->slug = $doc['url'];
		$get->cta->title = $doc['title'];
	}
	
	
	if( $page->single ){
		
		$single = $company->find(array("slug" => $_GET['sp']));
		foreach($single as $doc) {
			$get->title = $doc['title'];
			$get->length = $doc['length'];
			$get->instructors = $doc['instructors'];
			$get->content = $doc['content'];
			$get->accreditation = $doc['accreditation'];
			$get->objectives = $doc['objectives'];
			$get->special = $doc['special'];
			$get->audience = @$doc['audience'];
			$get->comments = @$doc['comments'];
			$get->public_c = @$doc['public_c'];
		}
		include DIR_VIEW.'/incompany/incompany-single.php';
		
	} 
	else {
		
		
		$sub = $pages->find(array("slug" => "courses-offered"));
		foreach($sub as $doc) {
			// Base
			$get->title = $doc['title'];
			$get->content = $doc['content'];
			$get->slug = $doc['slug'];
			
			// Sidebar
			$get->sidebar = new stdClass;
			$get->sidebar->title = $doc['ctitle'];
			$get->sidebar->content = $doc['cmessage'];
			$get->sidebar->page = $doc['cpage'];
			$get->sidebar->slug = $doc['cpage'];
		}
		include DIR_VIEW.'/incompany/incompany-list.php';

	}
?>
