<div class="wrap page-p">
	<div class="row">
		
		<!-- Sidebar -->
		<div class="re3 sidebar">
			<?php public_assets('tu-cese-logo-enroll-page.png'); ?>
		</div>
	
		<!--- Content -->
		<div class="re9 mainbody">
			<div class="mainbody-inner">
				
				<?php if((strpos($thispage,'?thanks') == true)): ?>
					<div class="status">
						Your Message was sent
					</div>
				<?php endif; ?>
				
				<article>
					<?php 
						if(isset($_GET['sp'])) {
							$all_staff = $staff->find(array('email' => $_GET['sp']));
							foreach($all_staff as $staff) {
								$name = $staff['fname'].' '.$staff['lname'];
							}
						}
					?>
					<h2>Contact <?php echo @$name; ?></h2>
					<form action="?" method="post" id="registration_form">
						<fieldset>
							<legend>Your Information</legend>
							
							<div class="field">
								<label>Name</label>
								<input type="text" name="name" required>												
							</div>
							<div class="field">
								<label>Phone</label>
								<input type="text" name="phone" required>												
							</div>
							<div class="field">
								<label>Email</label>
								<input type="text" name="email" required>												
							</div>
							<?php if(isset($_GET['sp'])) {?>
								<input type="hidden" name="to" value="<?php echo strip_tags($_GET['sp']); ?>">
							<?php } ?>

						</fieldset>
						<fieldset>
							<legend>Message</legend>
							<div class="field">
								<textarea name="message" id="questions-comments" cols="30" rows="10" required></textarea>
							</div>
						</fieldset>
						<script src='https://www.google.com/recaptcha/api.js'></script>
						<div class="g-recaptcha" data-sitekey="6LeNBAETAAAAAJ3xKpsz6zXGU9xInqjRQcQ49yIh"></div>
						<br/>
						<button class="enroll-btn" type="submit">Submit</button>
					</form>
				</article>
			</div>
		</div>
		
	</div>
</div>