<div class="wrap">
	<div class="row">
		<?php include DIR_TMPL.'/_sidebar.php'; ?>
		<div class="re9 mainbody">
			<div class="mainbody-inner">
				<article>
					
					<!-- Sorting -->
					<div class="sort">
						<select id="categories">
							<option value="all">All Categories</option>
							<?php 
								$all_categories = $db->category->find();
								foreach($all_categories as $cat) { 
									echo '<option value="'.$cat['_id'].'">'.$cat['name'].'</option>';
								} 
							?>
						</select>
						<input id="courses-search" name="search" placeholder="Search Courses" type="text" data-list=".courses" autocomplete="off">
					</div>
					
					<!-- Courses -->
					<div class="courses re">
						<?php 
							$counter = 0;
							foreach( $all_classes as $course ) {
								$counter++;
								echo '<div id="course" class="re4 ccb ';
								if(@$course['categories']) {
									foreach($course['categories'] as $cat) { 
										echo $cat.' ';
									}
								}
								echo '">';
								echo '<a href="'.$baseurl.'courses/'.$course['slug'].'">';
								if($course['image'] == '') { 
									echo '<div class="image" style="background: url('.$baseurl.'assets/_images/thumb-u5436-fr.png); background-size: cover;"></div>';
								}
								else {
									echo '<div class="image" style="background: url('.replaceHttp($course['image']).'); background-size: cover;"></div>';
								}
								echo '<h2>'.$course['title'].'</h2>';
								echo '<p>'.$course['seo'].'</p>';
								echo '<p style="display: none;">'.@$course['keywords'].'</p>';
								echo'</a>';
								echo '</div>';
								if($counter%3 == 0) {
									echo '<br clear="all"/>';	
								}
							}
						?>
						<div id="message"><div>
					</div>
					
				</article>
			</div>
		</div>
	</div>
</div>