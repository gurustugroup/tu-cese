<div class="wrap enroll-view">


	<!-- Sidebar -->
	<div class="re3 sidebar enroll-s">
		<h3>You are currently registering for:</h3>
		<h2><?php echo $course['title']; ?></h2>
		<hr>
		<strong>Course Length:</strong><br/> <?php echo $course['days']; ?><br/><br/>
		<strong>Course Time:</strong><br/> <?php echo $course['time']; ?><br/><br/>
		<strong>Instructors:</strong> 
		<ul style="margin: 0px; opacity: 0.7; margin-bottom: 5px;">
			<?php get_instructors($course['instructors']); ?>
		</ul>
		<br/>
		<strong>Venue:</strong> 
		<ul style="margin: 0px; opacity: 0.7; margin-bottom: 5px;">
			<p style="margin: 0px;"><?php echo $venue['name']; ?></p>
			<p style="margin: 0px;"><?php echo $venue['address']; ?></p>
			<p style="margin: 0px;"><?php echo $venue['city']; ?>, <?php echo $venue['state']; ?> <?php echo $venue['zip']; ?></p>
			<p style="margin: 0px;"><?php echo $venue['phone']; ?></p>
		</ul>
		<br/>
		<?php if($online == 0) { ?>
		<strong>Course Information:</strong><br/>
		<div style="margin: 5px 0px; opacity: 0.7; margin-bottom: 5px;">
			<p style="margin: 0px;"><b>Start Date</b>: <?php echo date("M j, Y", strtotime($class['start_date'])); ?></p>	
			<p style="margin: 0px;"><b>End Date</b>: <?php echo date("M j, Y", strtotime($class['end_date'])); ?></p>	
		</div>
		<?php } else { ?>
		<strong>Online Course:</strong><br/>
		<div style="margin: 5px 0px; opacity: 0.7; margin-bottom: 5px;">
			<p style="margin: 0px;"><?php echo $course['seo']?></p>		
		</div>
		<?php } ?>
		<br/><br/>
		<a href="<?php echo $baseurl; ?>courses/cese-courses">Select a different course?</a>
		<br/><br/>
		<?php public_assets('tu-cese-logo-enroll-page.png'); ?>
	</div>

	<!--- Content -->
	<div class="re6 mainbody">
		<div class="mainbody-inner">
			<article>
				<form action="<?php $baseurl; ?>confirmation" method="post" id="registration_form" onsubmit="$('#registration_form').submit();">
					<input type="hidden" name="venue" value="<?php echo $venue_id; ?>">	
					<input type="hidden" name="course" value="<?php echo $course_id; ?>">
					<input type="hidden" name="v_name" value="<?php echo $venue['name']; ?>">	
					<input type="hidden" name="v_address" value="<?php echo $venue['address']; ?>">	
					<input type="hidden" name="v_city" value="<?php echo $venue['city']; ?>,  <?php echo $venue['state']; ?> <?php echo $venue['zip']; ?>">	
					<input type="hidden" name="v_phone" value="<?php echo $venue['phone']; ?>">	
					<input type="hidden" name="start_date" value="<?php echo date("M j, Y", strtotime($class['start_date'])); ?>">	
					<input type="hidden" name="end_date" value="<?php echo date("M j, Y", strtotime($class['end_date'])); ?>">	
					<input type="hidden" name="per" value="<?php echo $class['rate']; ?>">
					<h1 class="head-participants">Company</h1>
					<fieldset>
						<legend>Company Information</legend>
						<?php 
							$array = array(
								'company' => '',
								'address' => 'required',
								'city' => 'required', 
								'state' => 'required',
								'zip' => 'required',
								'country' => '',
							);
							foreach ($array as $key => $value) {
								echo '<div class="field">';
								echo '<label>'.ucfirst($key).'</label>';
								echo '<input type="text" name="'.$key.'" '.$value.'>';
								echo '</div>';
							}	
						?>
					</fieldset>
					
					<fieldset>
						<legend>Company Contact Information</legend>
						<?php 
							$array = array(
								'First Name' => 'required', 
								'Last Name' => 'required', 
								'title' => '',
								'department' => '', 
								'phone' => 'required',  
								'fax' => '',
								'email' => 'required', 
							);
							foreach ($array as $key => $value) {
								echo '<div class="field">';
								echo '<label>'.ucfirst($key).'</label>';
								echo '<input type="text" name="companycontact['.$key.']" '.$value.'>';
								echo '</div>';
							}	
						?>
					</fieldset>
					
					<h1 class="head-participants">Participants</h1>
					<div class="participants-list">
						<?php 
							for ($x = 0; $x < $class['registrants']; $x++) {
								echo '<fieldset class="parti"><legend>Participant Information</legend>';
								$array = array(
									'First Name' => 'required', 
									'Last Name' => 'required', 
									'name on badge' => '',
									'title' => '',
									'department' => '', 
									'phone' => 'required',  
									'fax' => '',
									'email' => 'required', 
								);
								foreach ($array as $key => $value) {
									$name = str_replace(' ', '', $key);
									echo '<div class="field">';
									echo '<label>'.ucfirst($key).'</label>';
									echo '<input type="text" name="'.$name.'['.$x.']" '.$value.'>';
									echo '</div>';
								}	
								if($online == 1) { 
									echo '<div class="field">';
										echo '<label>Pick Start Date</label>';
										echo '<input type="date" name="online['.$x.']" placeholder="01/01/2017" style="border: 1px solid #000" required>';
									echo '</div>';
								}
								echo '</fieldset>';
							}
						?>
					</div>
					<script>reloop('<?php echo $qty-$class['registrants']; ?>');</script>
					<fieldset>
						<legend>Questions / Comments</legend>
						<div class="field">
							<textarea name="questionscomments" id="questions-comments" cols="30" rows="10"></textarea>
						</div>
					</fieldset>
					<button class="enroll-btn" type="submit" name="registration_form">Continue Registration</button>
			</article>
		</div>
	</div>
	
	<div class="re3">
		<fieldset style="margin-top: 75px;">
			<legend>Total</legend>
			<div class="field">
				<label style="width: 161px; text-align: right; margin: 0px;">Per Participants <span style="padding-left: 10px;">$</span></label>
				<input type="text" class="ff" name="eker" style="width: 85px; border: 0 !important;text-align: left; padding-left: 0px" value="<?php echo $class['rate']; ?>" readonly>
			</div>
			<div class="field">
				<label style="width: 161px; text-align: right; margin: 0px;"># of Participants <span style="padding-left: 10px;">x</span></label>
				<input type="text" name="group" id="qq" style="width: 85px; border: 0 !important;text-align: left; padding-left: 0px" value="<?php echo @$qty; ?>" readonly>
			</div>
			<div class="field">
				<label style="width: 161px; text-align: right; margin: 0px;">Total <span style="padding-left: 10px;">$</span></label>
				<input type="text" id="tt" style="width: 85px; border: 0 !important;text-align: left; padding-left: 0px" value="<?php echo $class['rate']*@$qty; ?>" readonly>
			</div>
		</fieldset>	
		<?php 
				if($qty > 1 && $online == 0) {
					echo '<button class="add-registrants" type="button">Add Another Registrant</button>';		
				}
				
				if($online == 1 && $qty > 1) { 
					echo '<button class="add-registrants-online" type="button">Add Another Online Registrant</button>';	
				}
		?>
	</div>
	</form>
</div>