<?php 
	$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_SPECIAL_CHARS);
	$_GET = filter_input_array(INPUT_GET, FILTER_SANITIZE_SPECIAL_CHARS);
 ?>
<div class="wrap enroll-view">
	<!--- Content -->
	<div class="re12 ">
		<div class="mainbody-inner">
			<article class="confirmation-p">
				
				<br/>

				<img src="https://cese.s3.amazonaws.com/1425241453.png" width="200px" height="auto" style="margin-bottom: 20px"/>

				<form action="<?php echo $touchnet['post_url']; ?>" method="post" id="registration_form">

					<input type="hidden" name="AMT" value="<?php echo $total; ?>">	
					<input type="hidden" name="UPAY_SITE_ID" value="3">

					<input type="hidden" name="SUCCESS_LINK" value="<?php echo $touchnet['success_link']; ?>?confirmation=<?php echo $confirmation; ?>">		
					<input type="hidden" name="ERROR_LINK" value="<?php echo $touchnet['error_link']; ?>">
					<input type="hidden" name="CANCEL_LINK" value="<?php echo $touchnet['cancel_link']; ?>">
					
					<input type="hidden" name="BILL_NAME" value="<?php echo $_POST['FirstName'][0].' '.$_POST['LastName'][0]; ?>">	
					<input type="hidden" name="BILL_EMAIL_ADDRESS" value="<?php echo $_POST['email'][0]; ?>">		
					<input type="hidden" name="BILL_STREET1" value="<?php echo $_POST['address']; ?>">		
					<input type="hidden" name="BILL_STREET2" value="">
					<input type="hidden" name="BILL_CITY" value="<?php echo $_POST['city']; ?>">		
					<input type="hidden" name="BILL_STATE" value="<?php echo $_POST['state']; ?>">
					<input type="hidden" name="BILL_POSTAL_CODE" value="<?php echo $_POST['zip']; ?>">	
					<input type="hidden" name="BILL_COUNTRY" value="1">	
					<input type="hidden" name="VALIDATION_KEY" value="">
					<input type="hidden" name="EXT_TRANS_ID" value="">		
					<input type="hidden" name="EXT_TRANS_ID_LABEL" value="">
					<input type="hidden" name="SSV" value="">	

					<button class="enroll-btn" type="submit">Pay Now</button>
					<button class="enroll-btn" type="button" onClick="window.print()">Print Confirmation</button>
					
					<fieldset style="margin-top: 50px;">
						<legend>Company Information</legend>
						<?php 
							$array = array('company', 'address', 'city', 'state', 'zip', 'country');
							foreach ($array as $item) {
								echo '<div class="field">';
								echo '<label>'.ucfirst($item).'</label>';
								echo '<input type="disable" value="'.$_POST[$item].'" readonly>';
								echo '</div>';
							}	
						?>
					</fieldset>
					
					<?php if(isset($_POST['course'])) { ?>
						<fieldset>
							<legend>Company Contact Information</legend>
							<?php 
								$array = array(
									'FirstName' => 'First Name', 
									'LastName' => 'Last Name', 
									'title' => 'title', 
									'department' => 'department', 
									'phone' => 'phone',
									'fax' => 'fax'
								);
								foreach ($array as $key => $value) {
									echo '<div class="field">';
									echo '<label>'.ucfirst($value).'</label>';
									echo '<input type="disable" value="'.$_POST['companycontact'][$value].'" readonly>';
									echo '</div>';
								}	
							?>
						</fieldset>
					<?php } ?>
					
					<?php $i = -1; foreach($_POST['FirstName'] as $key) { $i++ ?>
					<fieldset>
						<legend>Participant Information</legend>
						<?php 
							$array = array(
								'FirstName' => 'First Name', 
								'LastName' => 'Last Name', 
								'title' => 'title', 
								'department' => 'department', 
								'phone' => 'phone',
								'fax' => 'fax'
							);
							foreach ($array as $key => $value) {
								echo '<div class="field">';
								echo '<label>'.ucfirst($value).'</label>';
								echo '<input type="disable" value="'.$_POST[$key][$i].'" readonly>';
								echo '</div>';
							}	
							if(isset($_POST['online'])) { 
								echo '<div class="field">';
									echo '<label>Start Date</label>';
									echo '<input type="disable" value="'.date('M j, Y', strtotime($_POST['online'][$i])).'" readonly>';
								echo '</div>';
							}
						?>
					</fieldset>
					<?php } ?>
					
					<fieldset>
						<legend>Questions / Comments</legend>
						<div class="field">
							<textarea id="questions-comments" cols="30" rows="10" readonly><?php echo $_POST['questionscomments'] ?></textarea>
						</div>
					</fieldset>
					
					<?php if(isset($_POST['course'])) { ?>
						<fieldset>
							<legend>Course Information</legend>
							<?php 
								echo '<div class="field">';
									echo '<label>Title</label>';
									echo '<input type="disable" value="'.$the_title.'" readonly>';
								echo '</div>';
								echo '<div class="field">';
									echo '<label>Length</label>';
									echo '<input type="disable" value="'.$the_days.'" readonly>';
								echo '</div>';
								echo '<div class="field">';
									echo '<label>Time</label>';
									echo '<input type="disable" value="'.$the_time.'" readonly>';
								echo '</div>';
								if(!isset($_POST['online'])) { 
									echo '<div class="field">';
										echo '<label>Venue</label>';
										echo '<input type="disable" value="'.$_POST['v_name'].'" readonly>';
									echo '</div>';
									echo '<div class="field">';
										echo '<label>Address</label>';
										echo '<input type="disable" value="'.$_POST['v_address'].'" readonly>';
									echo '</div>';
									echo '<div class="field">';
										echo '<label></label>';
										echo '<input type="disable" value="'.$_POST['v_city'].'" readonly>';
									echo '</div>';
									echo '<div class="field">';
										echo '<label>Start Date</label>';
										echo '<input type="disable" value="'.$_POST['start_date'].'" readonly>';
									echo '</div>';
									echo '<div class="field">';
										echo '<label>End Date</label>';
										echo '<input type="disable" value="'.$_POST['end_date'].'" readonly>';
									echo '</div>';
								}
							?>
						</fieldset>
					<?php } ?>
					
					<h1 class="alt">Confirmation # <?php echo $confirmation; ?></h1>
					<?php if(isset($_POST['course']) && $registrations > 1) {  ?>
						<br/>
						<?php $registrations = $registrations-1; ?>
						<h2 style="font-size: 16px; margin-bottom: -20px; color: #555;">Per Participant $<?php echo number_format($_POST['per'], 2); ?></h2>
					<?php } ?>
					<h2>Total $<?php echo number_format($_POST['eker'], 2); ?></h2>
					<button class="enroll-btn" type="submit">Pay Now</button>
					<button class="enroll-btn" type="button" onClick="window.print()">Print Confirmation</button>
				</form>
			</article>
		</div>
	</div>
	
</div>