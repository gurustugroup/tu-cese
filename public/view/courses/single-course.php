<br/>
<div class="wrap c-print">
	<?php include DIR_TMPL.'/_course-sidebar.php'; ?>
	<div class="re9 mainbody single-course">
		<div class="mainbody-inner">
			
			<!-- Main Content -->
			<article>
				<div class="re6 c-img">
					<div class="image" style="background: url('<?php if($course['image'] == '') { echo image('thumb-u5436-fr.png'); } else { echo replaceHttp($course['image']); } ?>'); background-size: cover;"></div>
				</div>
				<div class="re6 c-info">
					<h2><?php echo @$course['title']; ?></h2>
					<hr>
					<p><strong>Instructor(s): </strong></p>
					<ul>
						<?php if($course['instructors']) { get_instructors($course['instructors']); }?>
					</ul>
					<p><strong>Length: </strong><span style="opacity: 0.7;"><?php echo @$course['days']; ?></span></p>
					<p><strong>Time: </strong><span style="opacity: 0.7;"><?php echo @$course['time']; ?></span></p>
					<br/>
					
					<?php if($course['online'] == 'on') { ?>
					<button class="enroll-btn" id="show-table">Enroll Online</button>
					<?php } else { if(@$course['class']['classes']) { ?>
					<button class="enroll-btn" id="show-table">Date, Location, Enroll</button>
					<?php } else {
						echo '<button onclick="alert(\'There are no dates scheduled at this time.\')" class="enroll-btn" id="show-table">Date, Location, Enroll</button>';
						// $get = $db->meta->find(array('what' => 'coursessettings'));
						// $result = iterator_to_array($get, false);
						// $result = $result[0];
					?>
						<!-- <p class="expiremessage"> <?php echo $result['expiremessage']; ?></p> -->
					<?php } }?>
				</div>
				<br clear="all"/>
				<?php include DIR_TMPL.'/_fees.php'; ?>
				<hr>
				
				<div class="c-body">
					<?php if(@$course['description']) {?>
					<h3>Course Description:</h3>
					<div class="text">
						<?php echo $course['description']; ?>
					</div>
					<?php } ?>
					<?php 
						$loop =	array('one', 'two', 'three', 'four', 'five'); 
						foreach($loop as $item) {
							if($course[$item]['content'] == '') {
								
							}
							else {
								echo '<h3>'.trim($course[$item]['name']).'</h3>';
								echo '<div class="text">'.trim($course[$item]['content']).'</div>';
							}
						}
					?>
				</div>
				
				<br/>
				<?php if($course['incompany']) {?>
					<a href="<?php base(); ?>in-company/<?php echo $course['incompany']; ?>">
						<button class="enroll-btn" style="width: 100%;" id="show-table">Available In-Company</button>
					</a>
				<?php } ?>
				<br>
			</article>
			
			<!-- Related Courses -->
			<div class="courses pure-g">
				<?php 
					$count = 1;
					if(@$course['categories']) {
						if($count > 1) {
							echo '<br/><strong>You may also be interested in:</strong><hr class="divider">';
						}
						$get_courses = $classes->find(array('categories' => $course['categories'][0]));
						foreach( $get_courses as $relate ){ if($course['id'] == $relate['_id']) { } else {
							echo '<div class="course-block course re4 ';
							if(@$course['categories']) {
								foreach($relate['categories'] as $cat) { 
									echo str_replace(" ","-",$cat);
								}
							}
							echo '">';
							echo '<a href="'.BASE_URL.'courses/'.$relate['slug'].'">';
							if($relate['image'] == '') { 
								echo '<div class="image" style="background: url('.BASE_URL.'assets/_images/thumb-u5436-fr.png); background-size: cover;"></div>';
							}
							else {
								echo '<div class="image" style="background: url('.replaceHttp($relate['image']).'); background-size: cover;"></div>';
							}
							echo '<h2>'.$relate['title'].'</h2>';
							echo '<p>'.substr(@$relate['seo'],0,100).'...</p>';
							echo'</a>';
							echo '</div>';
						}
					}
					}
				?>
			</div>
			
		</div>
	</div>
</div>