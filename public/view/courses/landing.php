<div id="homepage-slide">
	<ul>
		<li>
			<img src="<?php echo replaceHttp($get->image); ?>" alt="" style="position: absolute">
			<div class="wrap">
				<div class="caption">
					<div class="caption-inner">
						<h2><?php echo $get->slider_message ?></h2>
					</div>
				</div>
			</div>
		<li>
    </ul>
</div>

<!-- Page -->

<div class="wrap">
	<div class="re12">
		<?php include DIR_TMPL.'/_sidebar.php'; ?>
		<div class="re9 mainbody">
			<div class="mainbody-inner">
				<article>
                	<h2 class="subhead"><?php echo $get->mission_statement; ?></h2>
					<hr class="divider">
					<p><?php echo $get->mission_statement_explained; ?></p>
				</article>
			</div>
		</div>
	</div>
</div>