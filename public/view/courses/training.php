<div class="row wrap">
	<?php include DIR_TMPL.'/_sidebar.php'; ?>
	<div class="re9 mainbody">
		<div class="mainbody-inner">
						
			<article>
				<ul class="tabs">
					<li class="tab-1 active">
						<a onclick="$('.tab-content-1').show(); $('.tab-content-2').hide(); $('.tab-1').addClass('active'); $('.tab-2').removeClass('active');">
							<h2>Calendar View</h2>
						</a>
					</li>
					<li class="tab-2">
						<a onclick="$('.tab-content-2').show(); $('.tab-content-1').hide(); $('.tab-2').addClass('active'); $('.tab-1').removeClass('active');">
							<h2>List View</h2>
						</a>
					</li>
				</ul>
				<div class="tab-content-1" style="height: 1100px">
					<div id="calendar" style="width: 100%;"></div> 
					<br clear="all" />	
				</div>						
				<div class="tab-content-2" style="display: none; width: 100%; height: 1000px;">
					<br clear="all" />	
					<div class="calendar-list">
					<?php 

						foreach($classes as $class) {
							if(isset($class['class']['classes'])) {
								echo '<a style="text-decoration:none;" href="'.BASE_URL.'courses/'.$class['slug'].'">';
								echo '<h3>'.$class['title'].'</h3>';
								echo '<strong>Available dates:</strong>';
								echo '<ul style="margin-top:0;">';
								foreach( $class['class']['classes'] as $class_date ){
									echo '<li><span> '.date("F j, Y", strtotime($class_date['start_date'])).' to '.date("F j, Y", strtotime($class_date['end_date'])).'</span></li>';
								}
								echo '</ul>';
								echo '<p>'.$class['seo'].'</p>';
								echo '<p><strong>Length:</strong> '.$class['days'].'<br/> <strong>Time:</strong> '.$class['time'].'</p>';
								echo '</a><br clear="all"/>';
							}
						}
					?>
					</div>
				</div>
			</article>	
		</div>
	</div>
</div>

<script>
	$(document).ready(function() { 
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,basicWeek,basicDay'
			},
			defaultDate: '<?php echo date("Y-m-d"); ?>',
			editable: false,
			eventLimit: true, 
			height: 850,
			events: <?php echo $json_string; ?>
		});
		
	});
</script>