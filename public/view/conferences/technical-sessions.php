<div class="wrap">
	<h1 class="co-header"><?php echo $options['headline']; ?></h1>
	<?php include DIR_TMPL.'/_conferences-sidebar.php'; ?>
	<div class="re9 mainbody">
		<div class="mainbody-inner">
        	<article>
				<ul class="tabs">

					<?php for ($i = 0; $i < 4; $i++) { if($results['title'][$i] != '') {?>
					<li class="tabc tab-<?php echo $i;  if($i == '0') { echo ' active'; } ?>">
						<a onclick="
							$('.tabb').hide();
							$('.tab-content-<?php echo $i; ?> ').show();
							$('.tabc').removeClass('active');
							$('.tab-<?php echo $i; ?> ').addClass('active'); ">
							<h2><?php echo $results['title'][$i]; ?></h2>
						</a>
					</li>
					<?php } } ?>
				</ul>

				<?php for ($i = 0; $i < 4; $i++) {  ?>
				<div class="tab-content-<?php echo $i; ?> re12 tabb">

					<?php for ($x = 0; $x < 18; $x++) { if(@$results['session'][$i][$x] != '') { ?>
						<h2><?php echo @$results['session'][$i][$x]; ?></h2>
						<div><?php echo @$results['message'][$i][$x]; ?></div>
						<br/>
						<table class="data-table">
							<thead>
								<tr>
									<th width="230px" style="text-align: left">Time</th>
									<th width="350px" style="text-align: left">Event</th>
									<th style="text-align: left;">Location</th>
								</tr>
							</thead>
							<tbody>
								<?php $c = count(@$results['start'][$i]); for ($y = 0; $y < $c; $y++) { if(@$results['start'][$i][$x][$y] != '' || @$results['end'][$i][$x][$y] != '') {?>
									<tr class="block-<?php echo $i.$x; ?>-<?php echo @$results['sort_order'][$i][$x][$y]; ?>">
										<td width="230px">
											<p style="padding: 2px 0px;"><strong><?php echo @$results['start'][$i][$x][$y]; ?></strong> to <strong><?php echo @$results['end'][$i][$x][$y]; ?></strong></p>
										</td>
										<td width="350px">
											<p style="padding: 2px 0px;"><strong><?php echo @$results['event'][$i][$x][$y]; ?></strong></p>
										</td>
										<td>
											<p style="padding: 2px 0px;"><strong style="padding-left: 20px;"><?php echo @$results['location'][$i][$x][$y]; ?></strong></p>
										</td>
									</tr>
								<?php } } ?>
							</tbody>
						</table>

						<script>
						    $('.block-<?php echo $i.$x; ?>-2').insertAfter('.block-<?php echo $i.$x; ?>-1');
							$('.block-<?php echo $i.$x; ?>-3').insertAfter('.block-<?php echo $i.$x; ?>-2');
							$('.block-<?php echo $i.$x; ?>-4').insertAfter('.block-<?php echo $i.$x; ?>-3');
							$('.block-<?php echo $i.$x; ?>-5').insertAfter('.block-<?php echo $i.$x; ?>-4');
							$('.block-<?php echo $i.$x; ?>-6').insertAfter('.block-<?php echo $i.$x; ?>-5');
							$('.block-<?php echo $i.$x; ?>-7').insertAfter('.block-<?php echo $i.$x; ?>-6');
							$('.block-<?php echo $i.$x; ?>-8').insertAfter('.block-<?php echo $i.$x; ?>-7');
							$('.block-<?php echo $i.$x; ?>-9').insertAfter('.block-<?php echo $i.$x; ?>-8');
							$('.block-<?php echo $i.$x; ?>-10').insertAfter('.block-<?php echo $i.$x; ?>-9');
							$('.block-<?php echo $i.$x; ?>-11').insertAfter('.block-<?php echo $i.$x; ?>-10');
						</script>
					<?php } } ?>

				</div>
				<?php } ?>

			</article>

		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		$('.tabb').hide();
		$('.tab-content-0').show();
		$('.tab-0 ').addClass('active');
	});
</script>
