<script>console.log('registration.php')</script>
<div class="wrap">
	<h1 class="co-header"><?php echo $options['headline']; ?></h1>
	<?php include DIR_TMPL.'/_conferences-sidebar.php'; ?>
	<div class="re9 mainbody">
		<div class="mainbody-inner">
			<article>
            	<h2 class="subhead"><?php echo $results['content'][0]; ?></h2>
            	<h2 class="subhead" style="font-weight: normal; color: #000; font-size: 20px;"><?php echo $results['content'][1]; ?></h2>
				<hr class="divider">
				<div class="re6">
					<div class="re-buttons" style="padding-top: 100px">
						<?php
							for ($x = 0; $x <= 2; $x++) {
								if($results['title'][$x] !== '') {					
						?>
							<a href="<?php echo $results['link'][$x]; ?>">
								<button class="enroll-btn" style="text-align: left; font-size: 20px; font-weight: bold;"><?php echo $results['title'][$x]; ?></button>
							</a><br/><br/>
						<?php } } ?>
					</div>
					<?php 

						if( isset($results['files']) && count($results['files']) >= 3 ) :

					?>

					<br clear="all"><br>
					<strong style="color: #124E8A; font-size: 16px;">Download PDF Registration Forms</strong><br/>
					<?php
						
						foreach($results['files'] as $file) {
							if( isset( $file['file'] ) ){

								if( !empty($file['name']) && !empty($file['file']) ){

									$query = iterator_to_array( $files->find(array('name' => $file['file'])), false);
									if( $query ){
										echo '<a href="'.$query[0]["url"].'" style="color: #000">'.$file["name"].'</a><br>';
									}

								}

							}
						}
					?>	
					<br><br>
					
					<?php endif; //isset($results['files']) ?>

				</div>
				<div class="re6">
					<img src="<?php echo replaceHttp($options['image']); ?>" width="70%" style="margin-top: 50px"/>
				</div>
				
				<br clear="all"/><br/>
				
            	<h2 class="subhead">Registration Policies</h2>
				<hr class="divider">
				<p><?php echo $results['content'][2]; ?></p>
			</article>
		</div>
	</div>
</div>