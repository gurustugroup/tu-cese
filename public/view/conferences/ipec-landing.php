<div class="wrap">
	<h1 class="co-header"><?php echo $options['headline']; ?></h1>
	<?php include DIR_TMPL.'/_conferences-sidebar.php'; ?>
	<div class="re9 mainbody">
		<div class="mainbody-inner">
			<article class="ipec-section">
				<?php echo $results['content'][0]; ?>
				<br clear="all"/><br/>
				<hr>
				<div class="re no-p">
					<?php $x = -1; while($x <= 2) { $x++; ?>
						<div class="re6">
							<div class="ipec-box" style="position: relative; z-index: 999;">
									<div class="img" onclick="location.href='<?php echo $results['link'][$x]; ?>'" style="background: url('<?php echo replaceHttp($results['image'][$x]); ?>'); background-repeat: no-repeat; background-position: center center; background-size: cover; cursor: pointer;">
								</div>
								<div class="blue">
									<a href="<?php echo $results['link'][$x]; ?>">
										<h4><?php echo $results['title'][$x]; ?></h4>
									</a>
									<p><?php echo $results['summary'][$x]; ?></p>
									<div class="buttons">
										<?php if($results['one_title'][$x]) { ?>
										 	<a href="<?php echo $results['one_link'][$x]; ?>"><button><?php echo $results['one_title'][$x]; ?></button></a>
										<?php } ?>
										<?php if($results['two_title'][$x]) { ?>
										 	<a href="<?php echo $results['two_link'][$x]; ?>"><button><?php echo $results['two_title'][$x]; ?></button></a>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>
					<div class="re">
						<p><?php echo $results['content'][1]; ?></p>
					</div>
					<br/>
					<?php
						for ($x = 0; $x <= 3; $x++) {
							if($results['btitle'][$x] !== '') {
					?>
						<div class="re6">
							<a href="<?php echo $results['blink'][$x]; ?>">
								<button class="enroll-btn"><?php echo $results['btitle'][$x]; ?></button>
							</a>
						</div>
					<?php } } ?>
				</div>
			</article>
		</div>
	</div>
</div>
