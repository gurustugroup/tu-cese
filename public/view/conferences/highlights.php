<div class="wrap">
	<h1 class="co-header"><?php echo $options['headline']; ?></h1>
	<?php include DIR_TMPL.'/_conferences-sidebar.php'; ?>
	<div class="re9 mainbody">
		<div class="mainbody-inner">
			<article>
            	<h2 class="subhead">Highlights</h2>
				<hr class="divider">
				<p><?php echo $results['highlights']; ?></p>
				<br/><br/>
            	<h2 class="subhead">Events</h2>
				<hr class="divider">
				<p><?php echo $results['events']; ?></p>
			</article>
		</div>
	</div>
</div>