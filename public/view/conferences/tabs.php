<div class="wrap">
	<div class="row">
		<h1 class="co-header"><?php echo $options['headline']; ?></h1>
		<?php include DIR_TMPL.'/_conferences-sidebar.php'; ?>
		<div class="re9 mainbody">
			<div class="mainbody-inner">

				<article>
					<ul class="tabs">
						<li class="tab-1 active">
							<a onclick="
								$('.tab-content-1').show();
								$('.tab-content-2').hide();
								$('.tab-content-3').hide();
								$('.tab-1').addClass('active');
								$('.tab-2').removeClass('active');
								$('.tab-3').removeClass('active');"
							>
								<h2><?php echo $results['one_title']; ?></h2>
							</a>
						</li>
						<li class="tab-2">
							<a onclick="
								$('.tab-content-1').hide();
								$('.tab-content-2').show();
								$('.tab-content-3').hide();
								$('.tab-1').removeClass('active');
								$('.tab-2').addClass('active');
								$('.tab-3').removeClass('active');"
							>
								<h2><?php echo $results['two_title']; ?></h2>
							</a>
						</li>
						<li class="tab-3">
							<a onclick="
								$('.tab-content-1').hide();
								$('.tab-content-2').hide();
								$('.tab-content-3').show();
								$('.tab-1').removeClass('active');
								$('.tab-2').removeClass('active');
								$('.tab-3').addClass('active');"
							>
								<h2><?php echo $results['three_title']; ?></h2>
							</a>
						</li>
					</ul>
					
					<div class="tab-content-1">
						<?php echo $results['one']; ?>
						<br clear="all" />
					</div>
					<div class="tab-content-2" style="display: none;">
						<?php echo $results['two']; ?>
						<br clear="all" />
					</div>
					<div class="tab-content-3" style="display: none;">
						<?php echo $results['three']; ?>
						<br clear="all" />
					</div>
				</article>

				<div class="re">
					<p><?php echo $results['promo']; ?></p>
				</div>
				<br/>
				<div class="tab-buttons">
					<?php
						for ($x = 0; $x <= 3; $x++) {
							if($results['title'][$x] !== '') {
					?>
						<a href="<?php if(strpos($results['link'][$x], '@')) { echo 'mailto:'; }  echo $results['link'][$x]; ?>">
							<button class="enroll-btn"><?php echo $results['title'][$x]; ?></button>
						</a>
					<?php } } ?>
				</div>

			</div>
		</div>
	</div>
</div>
