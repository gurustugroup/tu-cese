<div class="wrap">
	<h1 class="co-header"><?php echo $options['headline']; ?></h1>
	<?php include DIR_TMPL.'/_conferences-sidebar.php'; ?>
	<div class="re9 mainbody">
		<div class="mainbody-inner">
			<form action="<?php echo $baseurl; ?>conferences-confirmation" method="post" id="registration_form" onsubmit="$('#registration_form').submit();">
				<input type="hidden" name="conferences" value=""/>
				<input type="hidden" name="type" value="presenter"/>
				<input type="hidden" name="pagetitle" value="Presenter Registration"/>
				<article>
	            	<h2 class="subhead">Presenter Conference Registration</h2>
					<hr class="divider">
					<h1 class="head-participants" style="font-size: 16px;">Fee Schedule:</h1>
					<p>(Fees are in Net U.S. Dollars and are per person.)</p>
					<table style="opacity: 0.9;">
						<?php $c = count($results['fee']); for ($x = 0; $x < $c; $x++) { if($results['fee'][$x] == '') { break; }?>
						<tr>
							<td style="padding: 3px 5px"><input type="radio" name="eker" onclick="$('#tt').html('<?php echo $results['fee'][$x]; ?>')" value="<?php echo str_replace(',', '', $results['fee'][$x]); ?>"/></td>
							<td style="padding: 3px 5px" width="100px">$<?php echo $results['fee'][$x]; ?></td>
							<td style="padding: 3px 5px"><?php echo $results['title'][$x]; ?></td>
						</tr>
						<?php } ?>
					</table>
					<p><strong>Total:</strong> $<span id="tt">0.00</span></p>
					<br/>
					<?php include DIR_TMPL.'/_registration.php'; ?>
				</article>
			</form>
		</div>
	</div>
</div>
