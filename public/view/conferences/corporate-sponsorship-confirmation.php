<?php 
	$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_SPECIAL_CHARS);
	$_GET = filter_input_array(INPUT_GET, FILTER_SANITIZE_SPECIAL_CHARS);
 ?>
<div class="wrap">
	<h1 class="co-header"><?php echo $options['headline']; ?></h1>
	<?php include DIR_TMPL.'/_conferences-sidebar.php'; ?>
	<div class="re9 mainbody">
		<div class="mainbody-inner">
			<?php


				$hide_form = '';
				if( isset( $_POST['sponsor_submission'] ) ){
					$hide_form = 'hidden';
					// echo '<pre>';
					// @print_r( $_POST );
					// echo '</pre>';
					// 

						

					?>
						<div class="re">
							<div class="re no-p">
								<h2 class="subhead">Thank you for requesting a sponsorship reservation.</h2>
							</div>
						</div>
					<?php
				} else { ?> 
		        	<h2 class="subhead">Corporate Sponsorship Reservation Form</h2>
					<hr class="divider">

			 <?php } ?>
			<br>
			<form action="" method="post" id="registration_form">
				<article>
			
			
					<div class="re">
						<div class="re8 no-p">
							<fieldset <?php echo $hide_form; ?>>
								<legend>Information</legend>
								<?php 
									$array = array(
										'Company / Affiliation' => 'required',
										'Address' => 'required',
										'City' => 'required',
										'State' => 'required',
										'Zip' => 'required',
										'Country' => '',
										'Company Phone' => '',
										'Company Website' => '',
										'Company Contact Name' => 'required',
										'Contact\'s Phone Number' => 'required',
										'Contact\'s Email' => 'required',
									);
									foreach ($array as $key => $value) {
										echo '<div class="field">';
										echo '<label style="width: 160px">'.ucfirst($key).'</label>';
										echo '<input type="text" name="'.$key.'" '.$value.'>';
										echo '</div>';
									}	
								?>
							</fieldset>
							<input type="hidden" name="sponsor_submission">
							<!-- <fieldset> -->
								<!-- <legend>Electronic Signature</legend> -->
								<?php 
									// $array = array(
									// 	'Electronic Signature' => 'required'
									// );
									// foreach ($array as $key => $value) {
									// 	echo '<div class="field">';
									// 	echo '<label style="width: 160px">'.ucfirst($key).'</label>';
									// 	echo '<input type="text" name="'.$key.'" '.$value.'>';
									// 	echo '</div>';
									// }	
								?>
							<!-- </fieldset> -->
							<button <?php echo $hide_form; ?> class="enroll-btn" type="submit" name="registration_form" style="margin: 10px auto; width: 100%;">Submit</button>
							<br clear="all"/>
							<h1 class="head-participants">Contact</h1>
							<p>University of Tulsa<br/>
								Continuing Education for Science & Engineering<br/>
								918-631-3088<br/>
								<a href="mailto:cese@utulsa.edu">cese@utulsa.edu</a><br/>
								800 S. Tucker Drive<br/>
								Tulsa, OK 74104<br/></p>
					
						</div>
						<div class="re4">
							<center>
								<img src="https://cese.s3.amazonaws.com/1448466329.png" width="70%" style="margin-top: 50px"/>
							</center>
								<h1 class="co-header"><?php echo $options['headline']; ?></h1>
						</div>
					</div>

				</article>
			</form>
		</div>
	</div>
</div>