<div class="wrap">
	<?php include DIR_TMPL.'/_conferences-sidebar.php'; ?>
	<div class="re9 mainbody single-course">
		<div class="mainbody-inner">
			<article>
				<div class="re6 c-img">
					<div class="image" style="background: url('<?php if($results['image'] == '') { echo image('thumb-u5436-fr.png'); } else { echo replaceHttp($results['image']); } ?>'); background-size: cover;"></div>
				</div>
				<div class="re6 c-info">
					<h2><?php echo @$results['content'][0]; ?></h2>
					<hr>
					<?php if($results['content'][1] !== '') { ?>
						<p><strong>Booth Number: </strong><span style="opacity: 0.7;"><?php echo @$results['content'][1]; ?></span></p>
					<?php } ?>
					
					<?php if($results['content'][2] !== '') { ?>
						<p><strong>Date: </strong><span style="opacity: 0.7;"><?php echo @$results['content'][2]; ?></span></p>
					<?php } ?>
					
					<?php if($results['content'][3] !== '') { ?>
						<p><strong>Location: </strong><span style="opacity: 0.7;"><?php echo @$results['content'][3]; ?></span></p>
					<?php } ?>
					
					<?php if($results['content'][4] !== '') { ?>
						<p><strong>Venue: </strong><span style="opacity: 0.7;"><?php echo @$results['content'][4]; ?></span></p>
					<?php } ?>
					<br/>
				</div>
				<br clear="all"/><br/>
				<?php echo @$results['content'][5]; ?>
			</article>
		</div>
	</div>
</div>