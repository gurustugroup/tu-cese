<div class="wrap">
	<h1 class="co-header"><?php echo $options['headline']; ?></h1>
	<?php include DIR_TMPL.'/_conferences-sidebar.php'; ?>
	<div class="re9 mainbody">
		<div class="mainbody-inner">
			<article>
            	<h2 class="subhead"><?php echo $results['content'][0]; ?></h2>
				<hr class="divider">
				<p><?php echo $results['content'][4]; ?></p>
				<p><?php echo $results['content'][3]; ?></p>
				<br/>
				<?php if($results['content'][2]) { ?>
				<a href="<?php echo $results['content'][2]; ?>">
				<button class="enroll-btn"><?php echo $results['content'][1]; ?></button>
				</a>
				<?php } ?>
			</article>
		</div>
	</div>
</div>