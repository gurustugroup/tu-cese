<div class="wrap">
	<h1 class="co-header"><?php echo $options['headline']; ?></h1>
	<?php include DIR_TMPL.'/_conferences-sidebar.php'; ?>
	<div class="re9 mainbody">
		<div class="mainbody-inner">
			<form action="<?php echo $baseurl; ?>conferences-confirmation" method="post" id="registration_form" onsubmit="$('#registration_form').submit();">
				<input type="hidden" name="conferences" value=""/>
				<input type="hidden" name="type" value="exhibitor"/>
				<input type="hidden" name="pagetitle" value="Exhibitor Registration"/>
				<article>
	            	<h2 class="subhead">Exhibitor Conference Registration</h2>
					<hr class="divider">
					<h1 class="head-participants" style="font-size: 16px;">Fee Schedule:</h1>
					<p>(Fees are in Net U.S. Dollars and are per person.)</p>
					<p><b>Exhibitor Conference Representative:</b></p>
					<table style="opacity: 0.9;">
						<?php $c = count($results['fee1']); for ($x = 0; $x < $c; $x++) { if($results['fee1'][$x] !== '') { ?>
						<tr>
							<td style="padding: 3px 5px"><input type="radio" name="eker" onclick="addup();" value="<?php echo str_replace(',', '', $results['fee1'][$x]); ?>"/></td>
							<td style="padding: 3px 5px" width="100px">$<?php echo $results['fee1'][$x]; ?></td>
							<td style="padding: 3px 5px"><?php echo $results['title1'][$x]; ?></td>
						</tr>
						<?php } } ?>
					</table>
					<p><b>Optional Events:</b></p>
					<table style="opacity: 0.9;">
						<tr>
							<td style="padding: 3px 5px"><input type="radio" name="ekerr" value="0" onclick="addup();" checked/></td>
							<td style="padding: 3px 5px" width="100px" colspan="2">No Optional Fees</td>
						</tr>
						<?php $c = count($results['fee2']); for ($x = 0; $x < $c; $x++) { if($results['fee2'][$x] !== '') { ?>
						<tr>
							<td style="padding: 3px 5px"><input type="radio" name="ekerr" onclick="addup();"  value="<?php echo str_replace(',', '', $results['fee2'][$x]); ?>"/></td>
							<td style="padding: 3px 5px" width="100px">$<?php echo $results['fee2'][$x]; ?></td>
							<td style="padding: 3px 5px"><?php echo $results['title2'][$x]; ?></td>
						</tr>
						<?php } } ?>
					</table>
					<p><strong>Total:</strong> $<span id="tt">0.00</span></p
					<br/>
					<?php include DIR_TMPL.'/_registration.php'; ?>
				</article>
			</form>
		</div>
	</div>
</div>

<script>
	function addup() {
		var one = Number($('[name="eker"]:checked').val());
		var two = Number($('[name="ekerr"]:checked').val());
		$('#tt').html(one+two);
	}
</script>
