<div class="wrap">
	<h1 class="co-header"><?php echo $options['headline']; ?></h1>
	<?php include DIR_TMPL.'/_conferences-sidebar.php'; ?>
	<div class="re9 mainbody">
		<div class="mainbody-inner">
			<article>
            	<h2 class="subhead">Venue Information</h2>
				<hr class="divider">
				<p><?php echo $results['info']; ?></p>
				<br clear="all"/><br/>
				<?php echo $results['map']; ?>
				<br clear="all"/><br/>
				<p><?php echo $results['other']; ?></p>
			</article>
		</div>
	</div>
</div>