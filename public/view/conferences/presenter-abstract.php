<div class="wrap">
	<h1 class="co-header"><?php echo $options['headline']; ?></h1>
	<?php include DIR_TMPL.'/_conferences-sidebar.php'; ?>
	<div class="re9 mainbody">
		<div class="mainbody-inner">
			<form action="?" method="post" id="registration_form" onsubmit="alert('Thanks for your Abstract Submission')">
				<input type="hidden" name="conferences" value=""/>
				<input type="hidden" name="type" value="attendee"/>
				<article>
	            	<h2 class="subhead">IPEC Abstract Submission Form</h2>
					<hr class="divider">

					<div class="re">
						<div class="re8 no-p">
							<fieldset>
								<legend>Title of Power Point/Oral Presentation</legend>
								<?php
									$array = array(
										'title' => 'required',
									);
									foreach ($array as $key => $value) {
										echo '<div class="field">';
										echo '<label style="width: 100px">'.ucfirst($key).'</label>';
										echo '<input type="text" name="'.$key.'" '.$value.'>';
										echo '</div>';
									}
								?>
							</fieldset>
							<p>Please select the one session in the menu below that you feel best fits the subject matter of the abstract you are submitting.</p>
							<p>Possible sessions for this year's conference may include but are not limited to:</p>
							<fieldset>
								<legend>Subject</legend>
								<select name="subject" style="width: 100%; padding: 10px;" required>
									<option> -- Select Subject -- </option>
									<?php
										$array = $conferences->find(array('what' => 'abstract'));
										$array = iterator_to_array($array, false);
										$array = $array[0]['subject'];
										foreach($array as $a) {
											if($a != '') {
												echo '<option value="'.$a.'">'.$a.'</option>';
											}
										}
									?>
								</select>
							</fieldset>
							<fieldset>
								<legend>Author</legend>
								<?php
									$array = array(
										'Name of Primary Author' => 'required',
										'Company / Affiliation' => 'required',
										'Address' => 'required',
										'City' => 'required',
										'State' => 'required',
										'Zip' => 'required',
										'Country' => 'required',
										'Phone' => 'required',
										'Email Address' => 'required',
										'Presenter\'s Name' => 'required',
										'Presenter\'s Email' => 'required',
									);
									foreach ($array as $key => $value) {
										echo '<div class="field">';
										echo '<label style="width: 160px">'.ucfirst($key).'</label>';
										echo '<input type="text" name="'.$key.'" '.$value.'>';
										echo '</div>';
									}
								?>
							</fieldset>
							<fieldset>
								<legend>First Co-Author</legend>
								<?php
									$array = array(
										'First Co-Author' => '',
										'First Co-Author Company / Affiliation' => '',
										'First Co-Author Address' => '',
										'First Co-Author City' => '',
										'First Co-Author State' => '',
										'First Co-Author Zip' => '',
										'First Co-Author Country' => ''
									);
									foreach ($array as $key => $value) {
										echo '<div class="field">';
										echo '<label style="width: 160px">'.ucfirst($key).'</label>';
										echo '<input type="text" name="'.$key.'" '.$value.'>';
										echo '</div>';
									}
								?>
							</fieldset>
							<fieldset>
								<legend>Second Co-Author</legend>
								<?php
									$array = array(
										'Second Co-Author' => '',
										'Second Co-Author Company / Affiliation' => '',
										'Second Co-Author Address' => '',
										'Second Co-Author City' => '',
										'Second Co-Author State' => '',
										'Second Co-Author Zip' => '',
										'Second Co-Author Country' => ''
									);
									foreach ($array as $key => $value) {
										echo '<div class="field">';
										echo '<label style="width: 160px">'.ucfirst($key).'</label>';
										echo '<input type="text" name="'.$key.'" '.$value.'>';
										echo '</div>';
									}
								?>
							</fieldset>
							<fieldset>
								<legend>Third Co-Author</legend>
								<?php
									$array = array(
										'Third Co-Author' => '',
										'Third Co-Author Company / Affiliation' => '',
										'Third Co-Author Address' => '',
										'Third Co-Author City' => '',
										'Third Co-Author State' => '',
										'Third Co-Author Zip' => '',
										'Third Co-Author Country' => ''
									);
									foreach ($array as $key => $value) {
										echo '<div class="field">';
										echo '<label style="width: 160px">'.ucfirst($key).'</label>';
										echo '<input type="text" name="'.$key.'" '.$value.'>';
										echo '</div>';
									}
								?>
							</fieldset>
							<fieldset>
								<legend>Other Co-Authors: <br>(including Company/Affiliation and City, State)</legend>
								<?php
									$array = array(
										'other' => '',
									);
									foreach ($array as $key => $value) {
										echo '<div class="field">';
										echo '<textarea rows="10" name="'.$key.'" '.$value.' placeholder="Company / Affiliation and City, State"></textarea>';
										echo '</div>';
									}
								?>
							</fieldset>
							<fieldset>
								<legend>Abstract (up to 200 words)</legend>
								<?php
									$array = array(
										'abstract' => '',
									);
									foreach ($array as $key => $value) {
										echo '<div class="field">';
										echo '<textarea rows="10" name="'.$key.'" '.$value.' placeholder="Abstract (up to 200 words)" required></textarea>';
										echo '</div>';
									}
								?>
							</fieldset>
							<button class="enroll-btn" type="submit" name="registration_form" style="margin: 10px auto; width: 100%; display: block">Submit</button>
							<br clear="all"/>
							<h1 class="head-participants">Contact</h1>
							<p>University of Tulsa<br/>
								Continuing Education for Science & Engineering<br/>
								918-631-3088<br/>
								<a href="mailto:cese@utulsa.edu">cese@utulsa.edu</a><br/>
								800 S. Tucker Drive<br/>
								Tulsa, OK 74104<br/></p>

						</div>
						<div class="re4">
							<center>
								<img src="<?php echo $options['image']; ?>" width="70%" style="margin-top: 50px"/>
							</center>
								<h1 class="co-header"><?php echo $options['headline']; ?></h1>
						</div>
					</div>

				</article>
			</form>
		</div>
	</div>
</div>
