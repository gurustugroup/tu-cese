<div class="wrap">
	<h1 class="co-header"><?php echo $options['headline']; ?></h1>
	<?php include DIR_TMPL.'/_conferences-sidebar.php'; ?>
	<div class="re9 mainbody">
		<div class="mainbody-inner">
			
			<?php 

				// echo '<pre>';
				// print_r( $results );
				// echo '</pre>';

			 ?>

        	<article>
				<ul class="tabs">
					<?php for ($i = 0; $i < 4; $i++) { if($results['title'][$i] != '') {?>
					<li class="tabc tab-<?php echo $i; if($i == '0') { echo ' active'; }?>">
						<a onclick="
							$('.tabb').hide();
							$('.tab-content-<?php echo $i; ?>').show();
							$('.tabc').removeClass('active');
							$('.tab-<?php echo $i; ?>').addClass('active'); ">
							<h2><?php echo $results['title'][$i]; ?></h2>
						</a>
					</li>
					<?php } } ?>
				</ul>

				<?php for ($i = 0; $i < 4; $i++) { if($results['title'][$i] != '') { ?>

				<div class="tab-content-<?php echo $i; ?> re12 tabb" <?php  if($i != '0') { echo ' style="display: none;"'; } ?>>
					<table class="data-table">
						<thead>
							<tr>
								<th>Time</th>
								<th>Event</th>
								<th style="padding-left: 20px;">Location</th>
							</tr>
						</thead>
						<tbody>
							<?php 

								$c = @count($results['start'][$i]); 
								
								if( !$c ){
									echo '<tr><td><p style="padding:2px 0;">No event data at this time.</p></td><tr>';
								}

								for ($x = 0; $x < $c; $x++) : ?>

									<tr class="block-<?php echo $i; ?>-<?php echo @$results['sort_order'][$i][$x]; ?>">
										<td width="230px">
											<p style="padding: 2px 0px;"><strong><?php echo @$results['start'][$i][$x]; ?></strong> to <strong><?php echo @$results['end'][$i][$x]; ?></strong></p>
										</td>
										<td width="350px">
											<p style="padding: 2px 0px;"><strong><?php echo @$results['event'][$i][$x]; ?></strong></p>
										</td>
										<td>
											<p style="padding: 2px 0px;"><strong style="padding-left: 20px;"><?php echo @$results['location'][$i][$x]; ?></strong></p>
										</td>
									</tr>

							<?php endfor; ?>
						</tbody>
					</table>
				</div>

				<script>
				    $('.block-<?php echo $i; ?>-2').insertAfter('.block-<?php echo $i; ?>-1');
					$('.block-<?php echo $i; ?>-3').insertAfter('.block-<?php echo $i; ?>-2');
					$('.block-<?php echo $i; ?>-4').insertAfter('.block-<?php echo $i; ?>-3');
					$('.block-<?php echo $i; ?>-5').insertAfter('.block-<?php echo $i; ?>-4');
					$('.block-<?php echo $i; ?>-6').insertAfter('.block-<?php echo $i; ?>-5');
					$('.block-<?php echo $i; ?>-7').insertAfter('.block-<?php echo $i; ?>-6');
					$('.block-<?php echo $i; ?>-8').insertAfter('.block-<?php echo $i; ?>-7');
					$('.block-<?php echo $i; ?>-9').insertAfter('.block-<?php echo $i; ?>-8');
					$('.block-<?php echo $i; ?>-10').insertAfter('.block-<?php echo $i; ?>-9');
					$('.block-<?php echo $i; ?>-11').insertAfter('.block-<?php echo $i; ?>-10');
				</script>
				<?php } } ?>

			</article>

			<style>
				td {
					vertical-align: top !important;
				}
			</style>

		</div>
	</div>
</div>
