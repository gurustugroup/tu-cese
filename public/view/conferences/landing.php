<div id="homepage-slide" style="height: 590px;">
	<ul>
		<li style="max-height: 100%">
			<img src="<?php echo replaceHttp($results['image']); ?>" alt="" style="position: absolute">
			<div class="wrap">
				<div class="caption" style="width: 500px; position: static; margin-top: 50px; height: auto;">
					<div class="re">
						<div class="re6" style="padding: 10px">
							<img src="<?php echo replaceHttp($options['image']); ?>" width="100%"/>
						</div>
						<div class="re6" style="padding: 20px 10px;">
							<h2><?php echo $results['title']; ?></h2>
							<h3><?php echo $results['subtitle']; ?></h3>
							<div class="cl-content"><?php echo $results['content']; ?></div>
						</div>
					</div>
					<div class="re">
						<hr>
						<div class="cl-content" style="padding: 20px;"><?php echo $results['promo']; ?></div>
					</div>
					<button class="cl-button" onclick="location.href='<?php echo BASE_URL; ?>conferences/ipec'">More Info...</button>
				</div>
			</div>
		<li>
    </ul>
</div>

<div class="wrap sections-container">
	
	<h2 style="font-size: 36px; font-weight: bold; padding: 14px;">Visit TU-CESE at:</h2>
  	<?php
		$sections = $db->featuredconferences->find()->limit(3);
		$sections = idToString(iterator_to_array($sections, false));
		foreach( $sections as $section ) {
    ?>
		<div class="re4">
			<div class="section-inner">
				<a href="<?php echo BASE_URL; ?>featured-conf/<?php echo $section['slug']; ?>">
					<?php if($section['image']) { ?>
						<div class="fe-image" style="background: #fff url(<?php echo replaceHttp($section['image']); ?> ); background-size: cover; background-position: center center;"></div>
					<?php } else { ?>
						<div class="fe-image" style="background: #fff url(<?php echo BASE_URL.'assets/_images/thumb-u5436-fr.png'; ?> ); background-size: cover;"></div>
					<?php } ?>
				</a>
				<h2>
					<a href="<?php echo BASE_URL; ?>featured-conf/<?php echo $section['slug']; ?>"><?php echo $section['content'][0]; ?></a>
				</h2>
				<hr class="divider"/><br/>
			     <b style="color: #1f4d7a;">Booth Number:</b> <?php echo $section['content'][1]; ?><br/>
			     <b style="color: #1f4d7a;">Date:</b> <?php echo $section['content'][2]; ?><br/>
			     <b style="color: #1f4d7a;">Location:</b> <?php echo $section['content'][3]; ?><br/>
			     <b style="color: #1f4d7a;">Venue:</b> <?php echo $section['content'][4]; ?><br/>
			     <br/><br/>
			     <a href="<?php echo BASE_URL; ?>featured-conf/<?php echo $section['slug']; ?>"><b>More Info</b></a>
			</div>
		</div>
	<?php }?>
	
	<?php 
		$c = $db->featuredconferences->find()->count();
		if($c > 4) { 
	?>
	<h2 style="font-size: 36px; font-weight: bold; padding: 14px;">Additional Upcoming Conferences:</h2>
	<hr>
  	<?php
		$sections = $db->featuredconferences->find()->skip(3);
		$sections = idToString(iterator_to_array($sections, false));
		foreach( $sections as $section ) {
    ?>
		<div class="re6">
				<a href="<?php echo BASE_URL; ?>featured-conf/<?php echo $section['slug']; ?>">
					<b>
						<?php echo $section['content'][0]; ?>: <?php echo $section['content'][2]; ?>, <?php echo $section['content'][3]; ?>, Booth #<?php echo $section['content'][1]; ?>
					</b>
				</a><br/>
		</div>
	<?php } } ?>
</div>