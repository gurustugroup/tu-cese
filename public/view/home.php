
<div id="homepage-slider" class="re12 no-p">
  <?php 
	$slides = query(array('model' => 'sliders', 'public' => 'on'));
	if( count($slides) > 0 ) {
		echo '<ul>';
		foreach( $slides as $slide ) {
			echo '<li>';
			if($slide['slug'] !== '') {
				echo '<a href="https://'. $slide['slug'] .'">';
			}
			echo '<img src="'.replaceHttp($slide['image']).'" alt="" style="position: absolute">';
			echo '<div class="wrap"><div class="caption re12"><div class="caption-inner"><h2>'. $slide['name'] .'</h2>';
			echo '<p>'. $slide['profile'] .'</p>';
			echo '</div></div></div>';
			if($slide['slug'] !== '') {
				echo '</a>';
			}
			
		}
		echo '</ul>';
    }
   ?>
</div>
<br clear="all"/>

<div class="wrap-outer home-cta">
	<div class="wrap">
		<div class="re8">
			<h2><?php echo $get->cta->content ?></h2>
		</div>
		<div class="re4">
			<a href="http://<?php echo $get->cta->slug ?>" class="btn cta-btn"><?php echo $get->cta->title ?></a>
		</div>
	</div>
</div>


<div class="wrap sections-container">
  	<?php
		$sections = $db->sections->find(array('public'=>'on'))->sort(array('ts'=>-1))->limit(3);
		$sections = idToString(iterator_to_array($sections, false));
		foreach( $sections as $section ) {
    ?>
		<div class="re4">
			<div class="section-inner">
				<a href="http://<?php echo $section['slug']; ?>">
					<?php if($section['image']) { ?>
						<div class="fe-image" style="background: #fff url(<?php echo replaceHttp($section['image']); ?> ); background-size: cover; background-position: center center;"></div>
					<?php } else { ?>
						<div class="fe-image" style="background: #fff url(<?php echo BASE_URL.'assets/_images/thumb-u5436-fr.png'; ?> ); background-size: cover;"></div>
					<?php } ?>
				</a>
				<h2>
					<a href="http://<?php echo $section['slug']; ?>"><?php echo $section['name']; ?></a>
				</h2>
				<hr class="divider"/>
				<p style="color: #444;"><?php echo $section['content']; ?></p>
			</div>
		</div>
	<?php }?>
</div>

<div class="wrap-outer">
	<div class="wrap sections-container-alt">
	  	<?php
			$sections = $db->blog->find(array('public'=>'on'))->sort(array('_id'=>-1))->limit(3);
			$sections = idToString(iterator_to_array($sections, false));
			foreach( $sections as $section ) {
	    ?>
			<div class="re4">
				<div class="section-inner">
					<a href="<?php echo BASE_URL.'blog/'.$section['slug']; ?>">
						<?php if($section['image']) { ?>
							<div class="fe-image" style="background: #fff url(<?php echo replaceHttp($section['image']); ?> ); background-size: cover; background-position: center center;"></div>
						<?php } else { ?>
							<div class="fe-image" style="background: #fff url(<?php echo BASE_URL.'assets/_images/thumb-u5436-fr.png'; ?> ); background-size: cover;"></div>
						<?php } ?>
					</a>
					<h2>
						<a href="<?php echo  BASE_URL.'blog/'.$section['slug']; ?>"><?php echo $section['title']; ?></a>
					</h2>
				</div>
			</div>
		<?php }?>
	</div>
</div>
<?php if( isset($_GET['signedup']) ) : ?>
 <script>
 	alert("Thank you for joining our newsletter!");
 	var newsletter_redirect = window.location.protocol + '//' + window.location.host + window.location.pathname;
 	window.location.href = newsletter_redirect;
 </script>
 <?php endif; ?>