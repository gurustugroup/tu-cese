<div class="wrap">
	<?php include DIR_TMPL.'/_sidebar.php'; ?>
	<div class="re9 mainbody">
		<div class="mainbody-inner incompanylist">
			<article>
				<img src="<?php echo $get->image; ?>">
				<br clear="all"/><br/>
				<h1 style="font-weight: 700; font-size: 32px;">CESE Around the World</h1>
				<hr>
				<p><?php echo $get->world; ?></p>
				<br clear="all"/><br/>
				<h1 style="font-weight: 700; font-size: 32px;">Testimonials</h1>
				<hr>
				<p><?php echo $get->testimonials; ?></p>
			</article>
		</div>
	</div>
</div>