<div id="homepage-slide">
	<ul>
		<li>
			<img src="<?php echo replaceHttp($get->image); ?>" alt="" style="position: absolute">
			<div class="wrap">
				<div class="caption">
					<div class="caption-inner">
						<h2><?php echo $get->slider_message ?></h2>
					</div>
				</div>
			</div>
		<li>
    </ul>
</div>

<br/>
<div class="wrap">
	<div class="row">
		<?php include DIR_TMPL.'/_sidebar.php'; ?>
		<div class="re9 mainbody">
			<div class="mainbody-inner">
				<article>
					
                	<h2 class="subhead"><?php echo $get->mission_statement; ?></h2>
					<hr class="divider">
					<p><?php echo $get->mission_statement_explained; ?></p>
					<br/>
					
                	<h2 class="subhead">Maximize Your Training Dollars</h2>
					<hr class="divider">
					<p><?php echo $get->training_dollars; ?></p>
					<br/>
					
                	<h2 class="subhead">Advantages of In-Company Programs</h2>
					<hr class="divider">
					<p><?php echo $get->advantages; ?></p>
					
				</article>
			</div>
		</div>
	</div>
</div>