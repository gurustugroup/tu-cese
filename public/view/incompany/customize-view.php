<div class="wrap">
	<div class="row">
		
		<!-- Sidebar -->
		<div class="re3 sidebar" >
			<?php public_assets('tu-cese-logo-enroll-page.png'); ?>
		</div>
	
		<!--- Content -->
		<div class="re9 mainbody">
			<div class="mainbody-inner">
				
				<?php if((strpos($thispage,'?thanks') == true)): ?>
					<div class="status">
						Your Request was sent
					</div>
				<?php endif; ?>
				
				<article>
					<h2>Customize a Course</h2>
					<form action="" method="post" id="registration_form">
						<fieldset>
							<legend>Company Information</legend>
							<div class="field">
								<label>Company</label>
								<input type="text" name="company">												
							</div>
							<div class="field">
								<label>Address</label>
								<input type="text" name="address">												
							</div>
							<div class="field">
								<label>City</label>
								<input type="text" name="city">												
							</div>
							<div class="field">
								<label>State</label>
								<input type="text" name="state">												
							</div>
							<div class="field">
								<label>Zip</label>
								<input type="text" name="zip">												
							</div>
							<div class="field">
								<label>Country</label>
								<input type="text" name="country">												
							</div>
						</fieldset>
						<fieldset>
							<legend>Your Information</legend>
							
							<div class="field">
								<label>First and Last Name</label>
								<input type="text" name="name" required>												
							</div>
							<div class="field">
								<label>Job Title</label>
								<input type="text" name="title">												
							</div>
							<div class="field">
								<label>Phone</label>
								<input type="text" name="phone" required>												
							</div>
							<div class="field">
								<label>Email</label>
								<input type="text" name="email" required>												
							</div>
						</fieldset>
						<fieldset>
							<legend>The Class</legend>
							<div class="field">
								<label>Name</label>
								<select name="class">
									<option> -- Select course or leave blank -- </option>
								 	<?php foreach ($all_incompany as $course) { ?>
								 		<option value="<?php echo $course['title']; ?>"><?php echo substr($course['title'],0,70); ?></option>
								 	<?php } ?>
								</select>											
							</div>
							<div class="field">
								<label>Participants</label>
								<input type="text" name="participants">												
							</div>
							<div class="field">
								<label>Preferred Dates</label>
								<input type="text" name="dates">												
							</div>
						</fieldset>
						<fieldset>
							<legend>Type of class</legend>
							<div class="field">
								<textarea name="type" id="questions-comments" cols="30" rows="10"></textarea>
							</div>
						</fieldset>
						<fieldset>
							<legend>Class Objective / Overall Goal</legend>
							<div class="field">
								<textarea name="objective" id="questions-comments" cols="30" rows="10"></textarea>
							</div>
						</fieldset>
						<fieldset>
							<legend>Comments</legend>
							<div class="field">
								<textarea name="comments" id="questions-comments" cols="30" rows="10"></textarea>
							</div>
						</fieldset>
						<button class="enroll-btn" type="submit" name="contact">Submit</button>
					</form>
				</article>
			</div>
		</div>
		
	</div>
</div>