<div class="wrap">
	<div class="row">
		<?php include DIR_TMPL.'/_incompany-sidebar.php'; ?>
		<div class="re9 mainbody">
			<div class="mainbody-inner">
				<article class="incompany">
					<h1 class="subhead"><?php echo $get->title; ?></h1>
					<hr class="divider">
					<a href="<?php echo BASE_URL; ?>request?class=<?php echo $get->title; ?>"><button class="enroll-btn">Request a Quote</button></a>
					<?php if(@$get->public_c) {?>
						<a href="<?php echo BASE_URL.'courses/'.$get->public_c?>"><button class="enroll-btn">View Public Course</button></a>
					<?php } ?>
					<?php if(@$get->instructors) {?>
					<h3>Instructor(s):</h3>
					<div class="text">
					<?php
						foreach($get->instructors as $item) {
							$findname = $db->instructors->find(array('_id' => new MongoId($item)));
							foreach($findname as $name) {
								echo $name['fname'].' '.$name['lname'].'<br/>';
							}
						}
					?>
					</div>
					<?php } ?>
					<?php if(@$get->content) {?>
					<h3>Course Description:</h3>
					<div class="text">
						<?php place($get->content); ?>
					</div>
					<?php } ?>
					<?php if(@$get->audience) {?>
					<h3>Who Should Attend?:</h3>
					<div class="text">
						<?php place(@$get->audience); ?>
					</div>
					<?php } ?>
					<?php if(@$get->length) {?>
					<h3>Length:</h3>
					<div class="text">
						<?php place($get->length); ?>
					</div>
					<?php } ?>
					<?php if(@$get->accreditation) {?>
					<h3>Accreditation:</h3>
					<div class="text">
						<?php place($get->accreditation); ?>
					</div>
					<?php } ?>
					<?php if(@$get->objectives) {?>
					<h3>Objectives / Benefits:</h3>
					<div class="text">
						<?php place($get->objectives); ?>
					</div>
					<?php } ?>
					<?php if(@$get->objectives) {?>
					<h3>Special Features:</h3>
					<div class="text">
						<?php place($get->special); ?>
					</div>
					<?php } ?>
					<?php if(@$get->comments) {?>
					<h3>Participants Comments:</h3>
					<div class="text">
						<?php place($get->comments); ?>
					</div>
					<?php } ?>
					<br/>
					<a href="<?php echo BASE_URL; ?>request?class=<?php echo $get->title; ?>"><button class="enroll-btn">Request a Quote</button></a>
					<?php if(@$get->public_c) {?>
						<a href="<?php echo BASE_URL.'courses/'.$get->public_c?>"><button class="enroll-btn">View Public Course</button></a>
					<?php } ?>
				</article>
			</div>
		</div>
	</div>
</div>