<div class="wrap">
	<?php include DIR_TMPL.'/_sidebar.php'; ?>
	<div class="re9 mainbody">
		<div class="mainbody-inner">
			<article>
				<?php echo $get->content; ?> 
				<?php 
					foreach($all_instructors as $ppl):
				?>	
					<div class="row">
						<?php if($ppl['profile']) { if($ppl['image']){ ?>
							<div class="re12 no-p" id="<?php echo $ppl['_id'];?>">
								<div class="blog-preview ppl">
		                        	<h2 class="subhead"><?php echo $ppl['fname'];?> <?php echo $ppl['lname'];?></h2>
									<hr class="divider">
									<p>
										<div class="profile-pic" style="background: url('<?php echo replaceHttp($ppl['image']); ?>'); background-size: cover;"></div>
										<?php echo $ppl['profile']; ?>
									</p>
		                        </div>
							</div>
						<?php } else { ?>	
							<div class="re12 no-p" id="<?php echo $ppl['_id'];?>">
								<div class="blog-preview ppl">
		                        	<h2 class="subhead"><?php echo $ppl['fname'];?> <?php echo $ppl['lname'];?></h2>
									<hr class="divider">
									<p><?php echo $ppl['profile']; ?></p>
		                        </div>
							</div>										
						<?php } } ?>
					</div>
					<br clear="all"/><br/>
					
				<?php endforeach ?>
			</article>
		</div>
	</div>
</div>