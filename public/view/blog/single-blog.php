<div class="wrap">
	<div class="row">
		<?php include DIR_TMPL.'/_blog-sidebar.php'; ?>
		<div class="re9 mainbody">
			<div class="mainbody-inner">
				<article>

					<?php if( isset( $get->title ) && isset($get->content) ) : ?>
						<h1 style="font-size: 50px; font-weight: bold"><?php echo $get->title; ?></h1>
						<br/>
						<h2 style="margin-top: -20px;"><?php echo $get->time; ?></h2>
						<hr>
						<p>
						<?php if($get->image): ?>
							<img src="<?php echo replaceHttp($get->image); ?>" style="max-width: 300px; margin-right: 10px;" align="left"/>
						<?php endif; ?>
						<?php echo $get->content; ?></p>
						<br clear="all"/><br/>
						<div class="fb-comments" data-href="<?php echo $thispage; ?>" data-numposts="5" data-colorscheme="light" width="100%"></div>
					<?php endif; ?>
				</article>
			</div>
		</div>
	</div>
</div>