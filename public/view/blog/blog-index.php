<?php 
	$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_SPECIAL_CHARS);
	$_GET = filter_input_array(INPUT_GET, FILTER_SANITIZE_SPECIAL_CHARS);
 ?>
<div class="wrap">
	<?php include DIR_TMPL.'/_blog-sidebar.php'; ?>
	<div class="re9 mainbody">
		<div class="mainbody-inner theblog">
			<article>
				<?php 
					if(isset($_GET['cat'])) {
						$cats = $db->blog->find(array("categories" => array('$exists' => true))); 
						foreach($cats as $post) {
							if($post['categories']) {
								foreach($post['categories'] as $c) {
									if($c == $_GET['cat']) {
										echo '<a href="blog/'.$post['slug'].'">
											<div class="pure-g">
												<div class="re12">
													<div class="blog-preview">
								                    	<h2 class="subhead">'.$post['title'].'</h2>
														<hr class="divider">
														<p>'.$post['content'].'</p>
								                    </div>
												</div>
											</div>
										</a>
										<br clear="all"/><br/>';
									} 
								} 
							}
						}
					}
					else {
						foreach($all_posts as $post) {
							echo '<a href="blog/'.$post['slug'].'">
								<div class="pure-g">
									<div class="re12">
										<div class="blog-preview">
					                    	<h2 class="subhead">'.$post['title'].'</h2>
											<hr class="divider">
											<p>'.$post['content'].'</p>
					                    </div>
									</div>
								</div>
							</a>
							<br clear="all"/><br/>';
						} 
					} 
				?>
			</article>
		</div>
	</div>
</div>