<div class="wrap">
	<div class="row">
	<?php include DIR_TMPL.'/_glossary-sidebar.php'; ?>
		<div class="re9 mainbody">
			<div class="mainbody-inner terms">
				
				<article>
					<div class="glossary term-a">
						<h2 class="subhead">
							<?php if(isset($_GET['sp'])): ?>
								<?php echo $_GET['sp']; ?>
							<?php else: ?>
								A
							<?php endif; ?>
						</h2>
						<hr class="divider">
						<?php 
							foreach($all_terms as $term) {
								if(isset($_GET['sp'])) {
									if(substr( $term['sort'], 0, 1 ) == strtolower($_GET['sp'])) {
										echo '<dl>
											<dt><strong>'.$term['term'].'</strong></dt>
											<dd>'.$term['definition'].'</dd><br>
										</dl>';
									}
								}
							} 
						?>
					</div>
				</article>
				
			</div>
		</div>
	</div>
</div>

<?php if(isset($_GET['sp'])): ?>
	<script>
		$(document).ready(function() {
			$('.letter-<?php echo $_GET['sp']; ?>').addClass('active');
		})
	</script>
<?php endif; ?>
