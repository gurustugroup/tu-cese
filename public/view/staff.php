<div class="wrap">
	<?php include DIR_TMPL.'/_sidebar.php'; ?>
	<div class="re9 mainbody">
		<div class="mainbody-inner">
			<article>
				<?php echo $get->content; ?> 
				<?php 
					$all_staff = $db->staff->find()->sort(array('lname' => 1));
					foreach($all_staff as $ppl):
				?>	
					<div class="row">
						<?php if($ppl['profile']) { if($ppl['image']){ ?>
							<div class="re6 no-p">
								<div class="blog-preview ppl">
		                        	<h2 class="subhead"><?php echo $ppl['fname'];?> <?php echo $ppl['lname'];?></h2>
									<hr class="divider">
									<p><img src="<?php echo replaceHttp($ppl['image']); ?>" class="blog-img" width="100px" /><?php echo $ppl['profile']; ?></p>
									<br/>
									<a href="<?php echo BASE_URL.'contact/'.$ppl['email'];?>">
										<button class="enroll-btn" id="show-table">Contact <?php echo $ppl['fname'];?> <?php echo $ppl['lname'];?> </button>
									</a>
		                        </div>
							</div>
						<?php } else { ?>	
							<div class="re12 no-p">
								<div class="blog-preview ppl">
		                        	<h2 class="subhead"><?php echo $ppl['fname'];?> <?php echo $ppl['lname'];?></h2>
									<hr class="divider">
									<p><?php echo $ppl['profile']; ?></p>
									<?php if(isset($ppl['email'])): ?>
										<br/>
										<a href="<?php echo BASE_URL.'contact/'.$ppl['email'];?>">
											<button class="enroll-btn" id="show-table">Contact <?php echo $ppl['fname'];?> <?php echo $ppl['lname'];?> </button>
										</a>
									<?php endif; ?>
		                        </div>
							</div>										
						<?php } } ?>
					</div>
					<br clear="all"/><br/>
				<?php endforeach ?>
			</article>
		</div>
	</div>
</div>