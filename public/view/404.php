<div class="wrap">
	<div class="re">
		<div class="re mainbody">
			<div>
				<article>
					<p>The page you are looking for was not found. Here are some common pages.</p>
					<ul>
						<?php 
	
							$pages = query(array('model'=>'pages'));
							foreach( $pages as $page ){
								echo '<li><a href="about/'.$page['slug'].'">'.$page['title'].'</a></li>';
							}
						 ?>
					</ul>
				</article>
			</div>
		</div>
			
	</div>
</div>