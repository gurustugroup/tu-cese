<?php 

	$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_SPECIAL_CHARS);
	$_GET = filter_input_array(INPUT_GET, FILTER_SANITIZE_SPECIAL_CHARS);

?>
<div class="wrap">
	<?php if(isset($_GET['term'])) { include DIR_TMPL.'/_sidebar.php'; } else { include DIR_TMPL.'/_glossary-sidebar.php'; } ?>
	<div class="re9 mainbody">
		<div class="mainbody-inner search">

			<article>
				<?php if(isset($_GET['term'])) { ?>
				<h1 style="font-size: 60px; padding: 0px; margin: 0px;">Searching for "<?php echo htmlspecialchars($_GET['term']); ?>"</h1>
					<?php
						$page_t = '<br/><h2 style="font-size: 50px; margin: 0px; color: #000">Pages</h2><hr>';
						echo $page_t;
						foreach($all_pages as $page) {
							$co = $page['title'].' '.$page['content'];
							if(strpos($co, $_GET['term']) !== false) {
								if($page['pagelink']) {
									echo '<h3 style="margin: 8px 0px;"><a href="'.BASE_URL.$page["pagelink"].'">'.$page["title"].'</a></h3>';
								}
								else {
									echo '<h3 style="margin: 8px 0px;"><a href="'.BASE_URL.$page["base"].'/'.$page["slug"].'">'.$page["title"].'</a></h3>';
								}
							}
							else {
								$page_t = '';
							}
						}
					?>
					<?php
						$class = '<br/><h2 style="font-size: 50px; margin: 0px; color: #000">Courses</h2><hr>';
						echo $class;
						$classes = $db->classes->find();
						foreach($classes as $page) {
							$co = $page['title'].' '.$page['description']. ' '.$page['keywords'];
							if(strpos($co, $_GET['term']) !== false) {
								echo '<h3 style="margin: 8px 0px;"><a href="'.BASE_URL.'courses/'.$page["slug"].'">'.$page["title"].'</a></h3>';
							}
							else {
								$class = '';
							}
						}
					?>
					<?php
						$class = '<br/><h2 style="font-size: 50px; margin: 0px; color: #000">In-Company Employee Development</h2><hr>';
						echo $class;
						$classes = $db->company->find();
						foreach($classes as $page) {
							$co = $page['title'].' '.$page['content'];
							if(strpos($co, $_GET['term']) !== false) {
								echo '<h3 style="margin: 8px 0px;"><a href="'.BASE_URL.'courses/'.$page["slug"].'">'.$page["title"].'</a></h3>';
							}
							else {
								$class = '';
							}
						}
					?>
					<?php
						$class = '<br/><h2 style="font-size: 50px; margin: 0px; color: #000">Conferences</h2><hr>';
						echo $class;
						$classes = $db->conferences->find();
						foreach($classes as $page) {
							$co = $page['what'];
							if(strpos($co, $_GET['term']) !== false) {
								echo '<h3 style="margin: 8px 0px;"><a href="'.BASE_URL.'courses/'.$page["slug"].'">'.$page["title"].'</a></h3>';
							}
							else {
								$class = '';
							}
						}
					?>
				<?php  }  elseif(isset($_GET['glossary'])) { ?>
					<h1 style="font-size: 60px; padding: 0px; margin: 0px;">Searching for "<?php echo $_GET['glossary']; ?>"</h1>
					<div class="glossary terms">
						<?php
							$classes = $db->glossary->find();
							foreach($classes as $page) {
								$co = $page['term'];
								if(strpos($co, $_GET['glossary']) !== false) { ?>
									<dl>
										<br/>
										<dt><strong><?php echo $page['term']; ?></strong></dt>
										<dd><?php echo $page['definition']; ?> </dd><br>
									</dl>
								<?php }
							}
						?>
					</div>
				<?php }  else { ?>
					<h1 style="font-size: 60px; padding: 0px; margin: 0px;">Searching for "<?php echo htmlspecialchars($_GET['blog']); ?>"</h1>
					<?php
						$classes = $db->blog->find();
						foreach($classes as $page) {
							$co = $page['title'];
							if(strpos($co, $_GET['blog']) !== false) {
								echo '<h3 style="margin: 8px 0px;"><a href="'.BASE_URL.'blog/'.$page["slug"].'">'.$page["title"].'</a></h3>';
							}
						}
					}
				?>
			</article>

		</div>
	</div>
</div>
