<div class="wrap">
	<div class="row">
		<?php include DIR_TMPL.'/_sidebar.php'; ?>
		<div class="re9 mainbody">
			<div class="mainbody-inner">
				
				<article>
					<ul class="tabs">
						<li class="tab-1 active">
							<a onclick="$('.tab-content-1').show(); $('.tab-content-2').hide(); $('.tab-1').addClass('active'); $('.tab-2').removeClass('active');">
								<h2>Frequently Asked Questions</h2>
							</a>
						</li>
						<li class="tab-2">
							<a onclick="$('.tab-content-2').show(); $('.tab-content-1').hide(); $('.tab-2').addClass('active'); $('.tab-1').removeClass('active');">
								<h2>Policies</h2>
							</a>
						</li>
					</ul>
					<div class="tab-content-1 re12">
						<?php echo $get->faq; ?>	
						<br clear="all" />						
					</div>
					<div class="tab-content-2 re12" style="display: none;">
						<?php echo $get->policies; ?>
						<br clear="all" />	
					</div>
				</article>
				
			</div>
		</div>
	</div>
</div>