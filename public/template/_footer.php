<!--  footer -->
</div>
<?php if(isset($_GET['p']) && $_GET['p'] == 'conferences' && isset($_GET['sp'])) { ?>
<br clear="all"/><br/>
<h1 style="font-size: 24px; font-weight: bold; text-align: center;">Thank you to the IPEC Corporate Sponsors:</h1>
<br/>
<div class="re logos">
	<center>
		<?php
			$logos = iterator_to_array($conferences->find(array("what" => "options"))->sort(array('logo_order' => 1)), false);
			$logos = $logos[0];
			$c = 0;
			$newarray = array();

			foreach($logos['logos'] as $item) {
				if($item !== '') {
					$newarray[$c]['link'] = $logos['link'][$c];
					$newarray[$c]['logo'] = $item;
					$newarray[$c]['order'] = $logos['logo_order'][$c];
					$c++;
					// echo '<a href="'.$logos['link'][$c].'"><img src="'.$item.'" style="padding-right: 10px"/></a>';
					// if($c == 8) {
					// 	echo '<br/>';
					// }
				}
			}

			function sort_order($a, $b) {
			    return ($b['order'] - $a['order']);
			}
			usort($newarray, 'sort_order');
			foreach($newarray as $item) {
				echo '<a href="'.$item['link'].'"><img src="'.$item['logo'].'" style="padding-right: 10px"/></a>';
				if($c == 8) {
					echo '<br/>';
				}
			}
		?>
	</center>
</div>
<?php } ?>
<div id="showfoot"></div>
<div class="footer">
	<div class="wrap">
		<div class="re">
			<div class="re6">
				<div class="re12">
					<div class="re3">
						<p class="hd">About</p>
						<ul class="navv">
							<?php get_nav("about"); ?>
						</ul>
					</div>
					<div class="re3">
						<p class="hd">Courses</p>
						<ul class="navv">
							<?php get_nav("courses"); ?>
						</ul>
					</div>
					<div class="re3">
						<p class="hd">Conferences</p>
						<ul class="navv">
							<?php
								$sub = $conferences->find()->sort(array('order' => 1));
								foreach($sub as $item) {
									if(!empty($item["page_title"])) {
										echo '<li><a href="'.BASE_URL.'conferences/'.$item['what'].'">'.$item['page_title'].'</a></li>';
									}
								}
							?>
						</ul>
					</div>
					<div class="re3">
						<p class="hd">In-Company</p>
						<ul class="navv">
							<?php get_nav("in-company-employee-development"); ?>
						</ul>
					</div>
				</div>
			</div>
			<div class="re3">
				<div class="re12">
				<p class="hd alt">Sign Up for Our Mailing List</p>
					<div class="footer-nav">
						<form action="?" method="post">
							<input type="email" name="vEmail" placeholder="Email Address">
							<input type="submit" name="newsletter" value="Join Our Mailing List">
						</form>
					</div>
				</div>
			</div>
			<div class="re3">
				<div class="re12">
					<p class="hd alt">Contact CESE</p>
					<ul class="navv">
						<li><strong>Phone:</strong>  (918) 631-3088</li>
						<li><strong>Fax:</strong> (918) 631-2154</li>
						<li><strong>Email:</strong> <a href="mailto:cese@utulsa.edu">cese@utulsa.edu</a></li>
						<li><strong>Address:</strong> 800 S. Tucker Drive <br/> Tulsa, OK 74104</li>
						<li><a href="http://www.utulsa.edu" target="_blank"><strong>The University of Tulsa</strong></a></li>
						<li><a href="http://engineering.utulsa.edu/" target="_blank"><strong>The University of Tulsa, College of Engineering & Natural Sciences</strong> </a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="copyright">
	<div class="wrap">
		<a href="http://engineering.utulsa.edu/">
			<p style="float: left;">Copyright &copy; <?php echo date('Y'); ?> The University of Tulsa CESE</p>
		</a>
		<p style="float: right;">Site by <a href="http://gurustu.co" target="_blank">GuRuStu</a></p>
	</div>
</div>

</body>
</html>
