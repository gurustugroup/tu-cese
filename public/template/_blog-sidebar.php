<div class="re3 sidebar">
	<nav class="sidebar-nav">
		<h3 class="sidebar-header">Recent Blog Post</h3>
		<ul>
			<?php recent_posts(); ?>							
		</ul>
	</nav>
	
	<br>
	<nav class="sidebar-nav">
		<h3 class="sidebar-header">Blog Categories</h3>
		<ul>
			<?php blog_categories(); ?>							
		</ul>
	</nav>
	<?php 
		
		// CALL TO ACTION
		$sidebar = new stdClass;
		$home = $cta->find(array("name" => "sidebar"));
		foreach($home as $doc) {
			$sidebar->cta = new stdClass;
			$sidebar->cta->content = $doc['content'];
			$sidebar->cta->slug = $doc['url'];
			$sidebar->cta->title = $doc['title'];
		}
		
		if(@$get->sidebar->title) { ?>
		<aside class="sidebar-cta">
			<div class="sidebar-cta-inner"><?php echo $get->sidebar->content; ?> </div>
			<div class="sidebar-cta-lower">
				<a href="<?php get_link($get->sidebar->page); ?>"><?php echo get_title($get->sidebar->page); ?></a>
			</div>
		</aside>
		<?php } else { ?>
		<aside class="sidebar-cta">
			<div class="sidebar-cta-inner"><?php echo $sidebar->cta->content; ?> </div>
			<div class="sidebar-cta-lower">
				<a href="<?php echo $sidebar->cta->slug; ?>"><?php echo $sidebar->cta->title; ?></a>
			</div>
		</aside>
	<?php } ?>		
	
	<br/>
	<nav class="sidebar-nav blog-menu">
		<h3 class="sidebar-header">Blog Search</h3>
		<form action="<?php base(); ?>search" method="get">
			<input name="blog" class="search" placeholder="Search The Blog">
			<input type="submit" value="Search">
		</form>
	</nav>	
</div>