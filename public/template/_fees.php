<?php
	
	// $single_course = sort_single_class_dates( $course );
	@$course = sort_single_class_dates( $course );

	if(@$course['class']['classes']) {
		echo '<form class="the-table" action="'.$baseurl.'enroll" method="post">';
		echo '<input type="hidden" value="'.$course['id'].'" name="id">';
		echo '<table width="100%">';

		# IFNOT / Online
		if($course['online'] !== 'on') {
			echo '<tr>';
				echo '<th>Date</th>';
				echo '<th>Location</th>';
				echo '<th>Venue</th>';
			echo '</tr>';
		}
		# Classes
		foreach($course['class']['classes'] as $class) {
			if(date("Ymd") < date("Ymd", strtotime($class['end_date'])) or $course['online'] == 'on') {

				if($course['online'] !== 'on') {


					# Top Row
					echo '<tr class="head">';

						# Dates
						echo '<td valign="middle">';
							echo '<p>';
								echo '<b>Starts: </b>'.date("M j, Y", strtotime($class['start_date'])).'<span style="display: block; margin-top: -9px; ">';
								echo '<b>Ends: </b>'. date("M j, Y", strtotime($class['end_date'])).'</span>';
							echo '</p>';
						echo '</td>';

						# Venue Info
						$search = $venues->find(array('_id' => new MongoID($class['venue'])));
						foreach($search as $doc) {
							echo '<td><p>'.$doc['city'].', '.$doc['state'].'<span style="display: block; margin-top: -8px;">'.$doc['country'].'</span></p></td>';
							echo '<td><p>';
								echo $doc['name'];
								echo '<span style="display: block; margin-top: -9px; ">'.$doc['address'].' '.$doc['zip'].' </span>';
								echo '<span style="display: block; margin-top: -9px; ">'.$doc['phone'].' </span>';
							echo '</p></td>';
						}

					echo '</tr>';
					## End Top Row
				}
				else {

					# IF ONLINE
					echo '<tr class="head">';
						echo '<td valign="middle">';
							echo '<p>';
								echo '<b>Online Course</b>';
							echo '</p>';
						echo '</td>';
					echo '</tr>';

				 }

				# Fees
				echo '<tr>';
					echo '<td colspan="4" class="fees">';

						$count = -1;
						$feetable = array();
						foreach($class['fee'] as $fee) {
							$count++;
							# IF title
							if(@$fee['rate']) {
								$feetable[$count]['title'] = $fee['title'];
								$feetable[$count]['rate_u'] = $fee['rate'];
								if(@$fee['m'] == 'on' || $fee['registrants'] > 0) {
									$feetable[$count]['multiple'] = true;
								}
								$feetable[$count]['rate_u'] = $fee['rate'];
								$feetable[$count]['rate'] = '$'.number_format($fee['rate']);
								$feetable[$count]['venue'] = $class['venue'];
								$feetable[$count]['start_date'] = date("mdy", strtotime($class['start_date']));
								$feetable[$count]['end_date'] = date("mdy", strtotime($class['end_date']));
								if($fee['expire']) {
									$e = date("mdy", strtotime($fee['expire']));
									$t = date("mdy");
									if($t > $e) {
										$feetable[$count]['expired_date'] = 'Expires '.date("M j, Y", strtotime($fee['expire']));
										$feetable[$count]['expired'] = true;
									}
									else {

									}
								}
							}

							if($count > count($class['fee'])) {
								break;
							}
						}

						# Add Notes
						$feetable['notes'] = tinymce($class['notes']);

						# Load Mustache
						echo $mustache->render('fee_table',$feetable);

					echo '</td>';
				echo '</tr>';
			# Enroll Button
			echo '<tr>';
				echo '<td colspan="99" class="fees">';
					echo '<div class="enroll">';
						echo '<label id="fee-qty-label" class="fee-qty-label" style="padding-right: 10px">Number of Individuals</label> ';
						echo '<input type="number" id="fee-qty" value="1" name="fee_qty" class="fee_qty" style="display: none">';
						if($course['online'] !== 'on') {
							echo '<button type="submit" class="enroll-btn">Enroll</button>';
						}
						else {
							echo '<button type="submit" name="online" class="enroll-btn">Enroll</button>';
						}
					echo '</div>';
				echo '</td>';
			echo '</tr>';

			}

		}

	# THE END
	echo '</table>';
	echo '</form>';
}
