<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $page->title; ?> | The University of Tulsa CESE</title>
	<meta charset="utf-8" content="text/html">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSS -->
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:400,700" rel="stylesheet">
	<?php public_assets("public.min.css"); ?>
	<link rel="stylesheet" href="<?php echo BASE_URL.'assets/_public/builds/print.min.css' ?>" type="text/css" media="print" />

	<!-- scripts  -->
	<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<?php public_assets("fittext.js"); ?>
	<?php public_assets("responsiveslides.js"); ?>
	<?php public_assets("jquery.hideseek.min.js"); ?>
	<?php public_assets("application.js"); ?>
	<?php public_assets("moment.min.js"); ?>
	<?php public_assets("fullcalendar.min.js"); ?>

	<!-- SEO -->
	<meta name="application-name" content="CESE" />
	<meta name="description" content="<?php echo @$page->meta; ?>">
	
	<!-- Google Analytics Tracking Code -->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-71411911-1', 'auto');
	  ga('send', 'pageview');

	</script>

</head>
<body>


<!-- Facebook -->
<div id="fb-root"></div>
<script>
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>

<!-- Desktop Header -->
<div class="desktop-header">
	<div class="header_wrap">
		<div class="nav_bar" >
			<div class="wrap">
				<div class="logo re3">
					<a href="<?php base(); ?>">
						<img src="<?php image("logo_.png"); ?>" class="pure-img desktop-logo" id="desktop-logo">
					</a>
				</div>
				<div class="upper-nav">
					<a href="<?php base(); ?>courses/cese-courses" class="header-btn register-btn">Register Now</a>
					<a href="<?php echo $gobal->facebook; ?>" target="_blank" class="header-btn ss facebook"><i class="fa fa-facebook"></i></a>
					<a href="<?php echo $gobal->linkedin; ?>" target="_blank" class="header-btn ss twitter"><i class="fa fa-linkedin"></i></a>
					<span class="search_wrapper">
						<form action="<?php base(); ?>search" method="get">
							<input name="term" class="search" placeholder="Search This Site">
							<input type="submit" value="Go">
						</form>
					</span>
					<a href="<?php echo BASE_URL; ?>glossary" class="header-btn">Petroleum Glossary</a>
				</div>
			</div>
		</div>
	</div>
	<?php include DIR_TMPL."/_nav.php";  ?>
</div>


<!-- Mobile -->
<div class="mobile-header">
	<div class="wrap">
		<div class="logo">
			<a href="<?php base(); ?>">
				<img src="<?php image("logo_sticky_nav.png"); ?>">
			</a>
			<a href="#showfoot">
				<button class="menu">Menu</button>
			</a>
		</div>
	</div>
</div>


<br clear="all"/>
	<?php if( $page->control != "home" ) : ?>
		<!-- Header -->
		<div class="wrap-outer page-header">
			<div class="wrap">
				<div class="re12">
					<?php if($_GET['p'] == 'glossary') {?>
						<h1 class="page-title">Petroleum Industry Glossary</h1>
						<br/><br/>
						<h2 class="page-sub-title" style="color: #fff; margin-top: -30px;">By: Norman J. Hyne PhD</h2>
					<?php } else { ?>
						<h1 class="page-title"><?php echo $page->title; ?></h1>
					<?php } ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
