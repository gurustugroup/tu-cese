<div class="re3 sidebar">
	<nav class="sidebar-nav">
		<h3 class="sidebar-header"><?php get_parent(); ?></h3>
		<ul>
			<?php get_subpages(); ?>							
		</ul>
	</nav>
	
	<?php if($_GET['p'] == 'in-company-employee-development') { ?>
		
		<br/><br/>
		<nav class="sidebar-nav">
			<a href="<?php echo BASE_URL; ?>request"><h3 class="sidebar-header">Request a Quote</h3></a>
		</nav>	
	
		<aside class="sidebar-cta">
			<div class="sidebar-cta-inner">Customize a CESE In-Company Course</div>
			<div class="sidebar-cta-lower">
				<a href="<?php echo BASE_URL; ?>customize">Customize a Course</a>
			</div>
		</aside>	
	
	<?php } 
		
		// CALL TO ACTION
		$sidebar = new stdClass;
		$home = $cta->find(array("name" => "sidebar"));
		foreach($home as $doc) {
			$sidebar->cta = new stdClass;
			$sidebar->cta->content = $doc['content'];
			$sidebar->cta->slug = $doc['url'];
			$sidebar->cta->title = $doc['title'];
		}
		
		if(@$get->sidebar->title && @$get->sidebar->slug) { ?>
		<aside class="sidebar-cta">
			<div class="sidebar-cta-inner"><?php echo $get->sidebar->content; ?> </div>
			<div class="sidebar-cta-lower">
				<a href="<?php get_link($get->sidebar->slug); ?>"><?php echo $get->sidebar->title; ?></a>
			</div>
		</aside>
		<?php } else { ?>
		<aside class="sidebar-cta">
			<div class="sidebar-cta-inner"><?php echo $sidebar->cta->content; ?> </div>
			<div class="sidebar-cta-lower">
				<a href="https://<?php echo $sidebar->cta->slug; ?>"><?php echo $sidebar->cta->title; ?></a>
			</div>
		</aside>
	<?php } ?>			
</div>