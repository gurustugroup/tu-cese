<div class="row nav primary_nav">
	<div class="wrap">
		<div class="re12 no-p logo_pad">
			<div class="primary_nav">
				<?php

					# BASES
					$bases = iterator_to_array( $db->base->find(), false);

					# CONF.
					$c = 0;
					$sub = $conferences->find()->sort(array('order' => 1));
					foreach($sub as $doc) {
						if(!empty($doc["page_title"])) {
							$c++;
							$bases[2]['ipec'][$c]['slug'] = $doc["what"];
							$bases[2]['ipec'][$c]['title'] = $doc["page_title"];
						}
					}

					echo '<ul class="menu top-level">';
					foreach($bases as $b) {
						if($b['slug'] == 'conferences') {
							echo '<li><a href="'.BASE_URL.''.$b['slug'].'">'.$b['name'].'</a>';
							echo '<ul class="sub-menu">';
								echo '<li><a href="'.BASE_URL.'conferences/ipec">IPEC Conference</a>';
								echo '<ul class="sub-sub-menu">';
									foreach($b['ipec'] as $item) {
										echo '<li><a href="'.BASE_URL.'conferences/'.$item['slug'].'">'.$item['title'].'</a></li>';
									}
								echo '</ul></li>';
								echo '<li><a href="'.BASE_URL.'conferences">Exhibit Events</a>';
								echo '<ul class="sub-sub-menu">';
									$sub = $featuredconferences->find();
									foreach($sub as $item) {
										echo '<li><a href="'.BASE_URL.'featured-conf/'.$item['slug'].'">'.$item['content'][0].'</a></li>';
									}
								echo '</ul></li>';
							echo '</ul>';
							echo '</li>';

						}
						else {
							echo '<li><a href="'.BASE_URL.''.$b['slug'].'">'.$b['name'].'</a>';
							echo '<ul class="sub-menu">';
								get_nav($b['slug']);
							echo '</ul>';
							echo '</li>';
						}
					}
					echo '<ul>';
				?>
		</div>
		</div>
	</div>
</div>
