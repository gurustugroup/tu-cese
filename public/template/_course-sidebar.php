<div class="re3 sidebar" >
	<nav class="sidebar-nav">
		<h3 class="sidebar-header"><?php get_parent(); ?></h3>
		<ul>
			<?php get_subpages(); ?>							
		</ul>
	</nav>	
	<?php if(@$course['ctitle'] == '') { 
		$sidebar = new stdClass;
		$home = $cta->find(array("name" => "sidebar"));
		foreach($home as $doc) {
			$sidebar->cta = new stdClass;
			$sidebar->cta->content = $doc['content'];
			$sidebar->cta->slug = $doc['url'];
			$sidebar->cta->title = $doc['title'];
		}
	?>
		<aside class="sidebar-cta">
			<div class="sidebar-cta-inner"><?php echo $sidebar->cta->content; ?> </div>
			<div class="sidebar-cta-lower">
				<a href="https://<?php echo $sidebar->cta->slug; ?>"><?php echo $sidebar->cta->title; ?></a>
			</div>
		</aside>

	<?php } else { ?>
		<aside class="sidebar-cta">
			<div class="sidebar-cta-inner"><?php echo $course['cmessage']; ?> </div>
			<div class="sidebar-cta-lower">
				<a href="<?php get_link($course['cpage']); ?>"><?php echo $course['ctitle']; ?></a>
			</div>
		</aside>
	<?php } ?>	
	
	<br/>
	<?php if(@$course['files']) { ?>
		<nav class="sidebar-nav">
			<h3 class="sidebar-header">Downloads</h3>
			<ul>
				<?php 
					foreach($course['files'] as $list) { 
						$search = $files->find(array('name' => $list['file']));
						foreach ($search as $file) {
							echo '<li><a href="'.$file['url'].'" target="_blank">'.$list['name'].'</a><li>';
						}	
					}
				?>
									
			</ul>
		</nav>	
	<?php } ?>
	
	<br/>
	<nav class="sidebar-nav">
		<a href="mailto:?Subject=<?php echo @$course['title']; ?> - TU CESE Course&body=<?php echo @$course['title']; ?> <?php echo BASE_URL.$course['slug']; ?>"><h3 class="sidebar-header">Share with Colleague</h3></a>
	</nav>	
	
	<br/>	
	<nav class="sidebar-nav">
		<a href="#"  onClick="window.print()"><h3 class="sidebar-header">Print this page</h3></a>
	</nav>	
</div>