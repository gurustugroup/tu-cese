<div class="the-table" action="<?php echo $baseurl.'enroll'; ?>" method="post">
	<input type="hidden" value="<?php echo $course['title']; ?>" name="title">
	<input type="hidden" value="<?php echo $course['id']; ?>" name="id">
	<table width="100%">
		<tr>
			<th>Date</th>
			<th>Location</th>
			<th>Venue</th>
		</tr>
		<?php 
			foreach($course['class']['classes'] as $class) { ?>
				<tr class="head">
					<td><p><span class="step" style="font-size: 12px;">Step 1</span><?php echo date("F j, Y", strtotime($class['start_date'])); ?></p></td>
					<?php 
						
						$search = $venues->find(array('_id' => new MongoID($class['venue']))); 
						foreach($search as $doc) {
							echo '<td><p>'.$doc['address'].', '.$doc['city'].', '.$doc['state'].' '.$doc['zip'].'</p></td>';
							echo '<td><p>';
								echo $doc['name'];
							if($doc['website']) {
								echo ' <a href="'.$doc['website'].'" style="color: #fff;" target="blank">(Go to Website)</a>';
							}
							echo '</p></td>';
						}
						
					?>						
				</tr>											
				<tr>
					<td colspan="4" class="fees">
						<span class="step" style="margin-left: 10px; font-size: 12px;">Step 2</span><br/>
						<?php 
							$i = 1;
							foreach($class['fee'] as $fee) { if($fee['rate']) { $i++ ?>
							<input type="radio" name="fee" value="<?php echo $fee['rate']; ?>"><b> $<?php echo number_format($fee['rate']); ?></b> 
							<input type="hidden" name="venue" value="<?php echo $class['venue']; ?>">
							<?php echo $fee['title']; ?><br/>
						<?php } } ?>
					</td>
				</tr>
		<?php }  ?>
	</table>
</div>
