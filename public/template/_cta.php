<?php 
		
	// CALL TO ACTION
	$sidebar = new stdClass;
	$home = $cta->find(array("name" => "sidebar"));
	foreach($home as $doc) {
		$sidebar->cta = new stdClass;
		$sidebar->cta->content = $doc['content'];
		$sidebar->cta->slug = $doc['url'];
		$sidebar->cta->title = $doc['title'];
	}
	
	if(@$get->sidebar->title) { ?>
	<aside class="sidebar-cta">
		<div class="sidebar-cta-inner"><?php echo $get->sidebar->content; ?> </div>
		<div class="sidebar-cta-lower">
			<a href="<?php get_link($get->sidebar->page); ?>"><?php echo $get->sidebar->page; ?></a>
		</div>
	</aside>
	<?php } else { ?>
	<aside class="sidebar-cta">
		<div class="sidebar-cta-inner"><?php echo $sidebar->cta->content; ?> </div>
		<div class="sidebar-cta-lower">
			<a href="https://<?php get_link($sidebar->cta->slug); ?>"><?php echo $sidebar->cta->title; ?></a>
		</div>
	</aside>
<?php } ?>	