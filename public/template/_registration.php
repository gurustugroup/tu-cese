<div class="re">
	<div class="re7 no-p">
		<fieldset>
			<legend>Company Information</legend>
			<?php
				$array = array(
					'company' => 'required',
					'address' => 'required',
					'city' => 'required',
					'state' => 'required',
					'zip' => 'required',
					'country' => '',
					'myemail' => 'required',
				);
				foreach ($array as $key => $value) {
					echo '<div class="field">';
					echo '<label style="width: 100px">'.ucfirst($key).'</label>';
					echo '<input type="text" name="'.$key.'" '.$value.'>';
					echo '</div>';
				}
			?>
		</fieldset>
		<h1 class="head-participants">Participants</h1>
		<div class="participants-list">
			<?php
				for ($x = 0; $x < 1; $x++) {
					echo '<fieldset class="parti"><legend>Participant Information</legend>';
					$array = array(
						'First Name' => 'required',
						'Last Name' => 'required',
						'name on badge' => '',
						'title' => '',
						'department' => '',
						'phone' => 'required',
						'fax' => '',
						'email' => 'required',
					);
					foreach ($array as $key => $value) {
						$name = str_replace(' ', '', $key);
						echo '<div class="field">';
						echo '<label style="width: 100px">'.ucfirst($key).'</label>';
						echo '<input type="text" name="'.str_replace(' ', '', $name).'['.$x.']" '.$value.'>';
						echo '</div>';
					}
					echo '</fieldset>';
				}
			?>
		</div>
		<fieldset>
			<legend>Questions / Comments</legend>
			<div class="field">
				<textarea name="questionscomments" id="questions-comments" cols="30" rows="10"></textarea>
			</div>
		</fieldset>
		<button class="add-registrants" type="button" style="margin-bottom: 0px; width: 100%">Add Another Registrant</button>
		<button class="enroll-btn" type="submit" name="registration_form" style="margin: 10px auto; width: 100%; display: block">Enroll</button>
		<br clear="all"/>
		<h1 class="head-participants">Contact</h1>
		<p>University of Tulsa<br/>
			Continuing Education for Science & Engineering<br/>
			918-631-3088<br/>
			<a href="mailto:cese@utulsa.edu">cese@utulsa.edu</a><br/>
			800 S. Tucker Drive<br/>
			Tulsa, OK 74104<br/></p>

	</div>
	<div class="re5">
		<center>
			<img src="<?php echo replaceHttp($options['image']); ?>" width="70%" style="margin-top: 50px"/>
		</center>
			<h1 class="co-header"><?php echo $options['headline']; ?></h1>
	</div>
</div>
