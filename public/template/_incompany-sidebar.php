<div class="re3 sidebar" >
	<nav class="sidebar-nav">
		<h3 class="sidebar-header"><?php get_parent(); ?></h3>
		<ul>
			<?php get_subpages(); ?>							
		</ul>
	</nav>	
	
	<br/>
	<nav class="sidebar-nav">
		<a href="<?php echo BASE_URL; ?>request"><h3 class="sidebar-header">Request a Quote</h3></a>
	</nav>	
	
	<aside class="sidebar-cta">
		<div class="sidebar-cta-inner">Customize a CESE In-Company Course</div>
		<div class="sidebar-cta-lower">
			<a href="<?php echo BASE_URL; ?>customize">Customize a Course</a>
		</div>
	</aside>	
	
	<br/>
	
	<br/>
	<nav class="sidebar-nav">
		<a href="mailto:?Subject=<?php echo uncleanurl(@$_GET['sp']); ?> - TU CESE Course"><h3 class="sidebar-header">Share with Colleague!</h3></a>
	</nav>	
</div>