<div class="re3 sidebar" >
	<nav class="sidebar-nav">
		<h3 class="sidebar-header"><?php get_parent(); ?></h3>
		<ul>
			<?php
				$sub = $conferences->find()->sort(array('order' => 1));
				foreach($sub as $doc) {
					if(!empty($doc["page_title"])) {
						echo '<li><a href="'.$baseurl.'conferences/'.$doc["what"].'">'.$doc["page_title"].'</a></li>';
					}
				}
			?>							
		</ul>
	</nav>	
	
	<br/><br/>
	
	<nav class="sidebar-nav">
		<h3 class="sidebar-header">Conference Brochures</h3>
		<ul>
			<?php
				$sub = iterator_to_array( $conferences->find(array('what' => 'brochures')), false);
				$sub = $sub[0];
				foreach($sub['files'] as $doc) {
					$query = iterator_to_array( $files->find(array('name' => $doc['file'])), false);
					$query = $query[0];
					echo '<li><a href="'.$query["url"].'">'.$doc["name"].'</a></li>';
				}
			?>							
		</ul>
	</nav>
	
	
	<?php
		$cursor = $conferences->find(array('what' => 'options'));
		$cta = iterator_to_array($cursor, false);
		$cta = $cta[0];
	?>
	<aside class="sidebar-cta">
		<div class="sidebar-cta-inner"><?php echo $cta['cmessage']; ?></div>
		<div class="sidebar-cta-lower">
			<a href="<?php echo $cta['cpage']; ?>"><?php echo $cta['ctitle']; ?></a>
		</div>
	</aside>
	
</div>
<br/>