<div class="re3 sidebar" >
	<nav class="sidebar-nav blog-menu">
		<h3 class="sidebar-header">Glossary Index</h3>
		<a href="<?php base(); ?>glossary">
			<br/>
			<p>Overview</p>
			<br/>
		</a>
		<?php $abcs = range('A', 'Z'); foreach($abcs as $letter) { ?>
			<span class="letter">
				<a class="letter-<?php echo $letter; ?>" href="<?php base(); ?>glossary/<?php echo $letter; ?>"><?php echo $letter; ?></a>
			</span>
		<?php } ?>
		<br/>
	</nav>
	<br/>
	<nav class="sidebar-nav blog-menu">
		<h3 class="sidebar-header">Glossary Search</h3>
		<form action="<?php base(); ?>search" method="get">
			<input name="glossary" class="search" placeholder="Search The Glossary">
			<input type="submit" value="Go">
		</form>
	</nav>
	<br/>
</div>
