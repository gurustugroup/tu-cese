$(document).ready(function(){

	//(2/12/15)
	//===================================================================
	//======= The Basic
	//===================================================================

	// Enable Date Picker
 	$( ".date" ).datepicker();

 	// Table Search
    $('#filter').keyup(function () {
        var rex = new RegExp($(this).val(), 'i');
        $('.searchable tr').hide();
        $('.searchable tr').filter(function () {
            return rex.test($(this).text());
        }).show();
    });

    // Create SLUG
    $(document).on('keyup', 'input[name=title]', function(){
		var slug = $(this).val().replace(/\s+/g, '-').toLowerCase()
		slug = slug.replace(/[\[\]!$%^&*()_+|~=`{}:":;<>?',.\\/]/g, '');
		slug = slug.replace(/--/, '-');
		$('input[name=slug]').val( slug+'p' );
		$('span.permaslug').text(slug);
	});

    $('a').each(function(index, el) {
        var href = $(this).attr('href');
    });

	$(document).on('keyup', '#makeslug', function(){
		var slug = $(this).val().replace(/\s+/g, '-').toLowerCase()
		slug = slug.replace(/[\[\]!$%^&*()_+|~=`{}:":;<>?',.\\/]/g, '');
		slug = slug.replace(/--/, '-');
		$('input[name=slug]').val( slug+'p' );
		$('span.permaslug').text(slug);
	});

	// Create Sort Friendly DB term
	$(document).on('keyup', 'input[name=term]', function(){
		var slug = $(this).val().replace(/\s+/g, '-').toLowerCase()
		slug = slug.replace(/[\[\]!$%^&*()_+|~=`{}:":;<>?',.\\/]/g, '');
		slug = slug.replace(/--/, '-');
		$('input[name=sortterm]').val( slug );
	});

	// Disable Permalink ?? CC: @richard
	$(document).on('click', '#permalink', function(){
		$(this).removeAttr('disabled');
	});

	// Update Password
	$(document).on( 'click', '[id^=update_p_]', function(event){

		// console.log( guru.courses.ajaxurl );
		// return;

		var id = event.target.id.slice(9);
		var newpassword = $('#password_'+id).val();
		alert('Password Changed');
		location.href = '?';
		$.post( guru.courses.ajaxurl, { account: id, password: newpassword } );
	});

	$(document).on( 'click', '.remove-clone', function() {
		$(this).parent().parent().remove();
	});

	//(2/12/15)
	//===================================================================
	//======= TinyMCE
	//===================================================================


	// PDF Button
	tinymce.PluginManager.add('pdf', function(editor, url) {
	    editor.addButton('pdf', {
	        text: 'PDF',
	        icon: false,
	        onclick: function() {
	            $('.pdf_insert').show();
	        }
	    });
	});

	// Start TinyMCE
	tinymce.init({
    	menubar : true,
    	forced_root_block : false,
	    force_p_newlines : false,
	    remove_linebreaks : false,
	    force_br_newlines : true,
	    remove_trailing_nbsp : false,
	    verify_html : false,
	    forced_root_block : "",
   		plugins: "autoresize, textcolor, insertdatetime, moxiemanager, link, email, image, pdf, paste, code, print, searchreplace, insertdatetime, autolink",
        theme_advanced_buttons3_add : "pastetext,pasteword,selectall",
   		paste_as_text: true,
    	autoresize_min_height: 50,
    	insertdatetime_formats: ["%m / %d / %Y", "%H:%M"],
		toolbar: [
			"forecolor | paste pasteword bold italic underline styleselect | alignleft aligncenter alignright | bullist numlist outdent indent | link email insertdatetime | moxiemanager image insertimage pdf | code | print searchreplace",
		],
		selector: ".editor",
	});



	//(2/12/15)
	//===================================================================
	//======= Courses
	//===================================================================

	// Add Classes
	$(document).on( 'click', '#add_class', function(event){
		var classes = $('.class-templ').length;
		var v = $('.class-templ').length;
		var data  = {'classes': classes };
		$('#class_template').tmpl(data).appendTo('#class_box');
		$( ".date" ).datepicker();
		tinymce.init({
	    	menubar : false,
	   		plugins: "autoresize, textcolor, insertdatetime, link, email, image",
	    	autoresize_min_height: 50,
			toolbar: [
				"forecolor | bold italic underline styleselect | alignleft aligncenter alignright | bullist numlist outdent indent | link email | image insertimage",
			],
			selector: ".editor",
		});
	});
	$(document).on( 'click', '#remove_class', function(e){
		var above = $(this).closest( ".class-templ" );
		above.remove();
	});


	// Add Category
	$(document).on( 'click', '#add_category', function(event){
		$( "#cat_clone" ).clone().appendTo( ".category_box" );
	});
	$(document).on( 'click', '.delete_category', function(e){
		var above = $(this).closest( ".category_select");
		above.remove();
	});

	// Add Blogs Category
	$(document).on( 'click', '#add_blogcategories', function(event){
		$( "#blogcategory_clone" ).clone().appendTo( ".blogcategories_box" );
	});
	$(document).on( 'click', '.delete_blogcategories', function(e){
		var above = $(this).closest( ".blogcategories_select");
		above.remove();
	});

	// Add Tags
	$(document).on( 'click', '#add_tag', function(event){
		$( "#tag_clone" ).clone().appendTo( ".tag_box" );
	});
	$(document).on( 'click', '.delete_tag', function(e){
		var above = $(this).closest( ".tag_select");
		above.remove();
	});

	// Add Instructor
	$(document).on( 'click', '#add_instructor', function(event){
		$( "#instructor_clone" ).clone().appendTo( ".instructor_box" );
	});
	$(document).on( 'click', '.delete_instructor', function(e){
		var above = $(this).closest( ".instructor_select");
		above.remove();
	});

	var url = window.location.href;
	if (!url.indexOf("admin/pageorder/") >= 0) {
		try {
			// Public Check
			if($('.public')[0].checked == true) {
				$('.hidepublic').hide();
			}
			else {
				$('.hidepublic').show();
			}

			// Online Check
			if($('.online')[0].checked == true) {
				$('.hideonline').hide();
				$('.showonline').show();
			}
			else {
				$('.hideonline').show();
				$('.showonline').hide();
			}
		}
		catch(e) {

		}
	}


	$(document).on( 'click', '.online', function(e){
		if($('.online')[0].checked == true) {
			$('.hideonline').hide();
			$('.showonline').show();
		}
		else {
			$('.hideonline').show();
			$('.showonline').hide();
		}
	});

	$(document).on( 'click', '.public', function(e){
		if($('.public')[0].checked == true) {
			$('.hidepublic').hide();
		}
		else {
			$('.hidepublic').show();
		}
	});


});

//(2/12/15)
//===================================================================
//======= Modals
//===================================================================

// Add Instuctors
function addinstructor() {
	var first = $('#in-first').val();
	var last = $('#in-last').val();
	var bio = $('#in-bio').val();
	if(first == '') {

	}
	else {
		$.post( ajaxurl, { instructor: first, lname: last, profile: bio }, function(data) {
			var id = data;
			$('.ins_select').append('<option id="'+id+'" value="'+id+'">'+first+' '+last+'</option>');
			ajaxselect();
		});
	}

	// Refresh Page
	var url = window.location.href;
	if (url.indexOf("admin/instructors/") >= 0) {
		location.reload();
	}
}


// Add Category
function addcategory() {
	var catename = $('#category-n').val();
	if(catename == '') {

	}
	else {
		$.post( ajaxurl, { categoryname: catename }, function(data) {
			var id = data;
			$('.cat_select').append('<option id="'+id+'" value="'+id+'">'+catename+'</option>');
		});
	}
}

// Add Blog Category
function addblogcategory() {
	var catename = $('#blog-n').val();
	if(catename == '') {

	}
	else {
		$.post( ajaxurl, { blogcat: catename }, function(data) {
			var id = data;
			$('.blogcategories_selection').append('<option id="'+id+'" value="'+id+'">'+catename+'</option>');
		});
	}
}

function addtags() {
	var catename = $('#tag-n').val();
	if(catename == '') {

	}
	else {
		$.post( ajaxurl, { tagname: catename }, function(data) {
			var id = data;
			$('.tags_select').append('<option id="'+id+'" value="'+id+'">'+catename+'</option>');
		});
	}
}


// Add Venue
function addvenue() {
	var vname = $('input[name="name"]').val();
	var vaddress = $('input[name="address"]').val();
	var vcity = $('input[name="city"]').val();
	var vstate = $('#state option:selected').text();
	var vzip = $('input[name="zip"]').val();
	var vcountry = $('#country option:selected').text();
	var vphone = $('input[name="phone"]').val();
	var vrr = $('input[name="rr"]').val();
	var vwebsite = $('input[name="website"]').val();
	var vcomments = $('input[name="comments"]').val();
	if(vname == '') {

	}
	else {
		$.post( ajaxurl, { venue: vname, name: vname, address: vaddress, city: vcity, state: vstate, zip: vzip, country: vcountry, phone: vphone, rr: vrr, website: vwebsite, comments: vcomments  }, function(data) {
			var id = data;
			select(id);
		});

		function select(id) {
			$(document).find(".venue-select").each(function(index) {
				$(this).append('<option id="'+id+'" value="'+id+'">'+vname+' ('+vcity+', '+vstate+')</option>');
				ajaxselect();
			});
		}
	}

	// Refresh Page
	var url = window.location.href;
	if (url.indexOf("venues") >= 0) {
		location.reload();
	}
}


function ask() {
      var confirmed = confirm("Are you sure? This will remove this entry.");
      return confirmed;
}

function clone(num) {
    var html = $('#clone-'+num).html();
    $('.machine-'+num).append(html);
}
