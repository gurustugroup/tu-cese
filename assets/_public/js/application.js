$(document).ready(function(){


	// Required Fields
	$('*').each(function() {
		if ($(this).attr('required')) {
			$(this).after('<b style="color: red"> *</b>');
			// console.log('required');
		}
	});

	//Courses Search
	$('#courses-search').hideseek({
		highlight: true,
		nodata: 'No results found'
	});

	//(12/12/14)
	//===================================================================
	//======= Category Sort
	//===================================================================
	$('#categories').on('change', function() {
		var numItems = $('.'+this.value).length;
		// console.log(this.value);

		$('.ccb').hide();
		$('#message').text('');

		// Courses Found
		if(numItems > 0) {
			$('.'+this.value).show();
		}
		// Courses Not Found
		else {
			$('#message').text('No classes in this category.');
		}

		// All
		if(this.value == 'all') {
			$('.ccb').show();
			$('#message').text('');
		}

	});

	// Fee Table
	$('.the-table').first().hide();
	$('#show-table').click(function() {
		$('.the-table').first().slideToggle();
	});
	
	$('.fee-qty-label').hide();
	$('.fee-qty').hide();

	$('.m-registrants').click(function() {
		$('.fee-qty-label').show();
		$('.fee_qty').show();
		$('.fee_qty').val('0');
	});
	$('.s-registrants').click(function() {
		$('.fee-qty-label').hide();
		$('.fee_qty').hide();
		$('.fee_qty').val('0');
	});

	// Slider
	$(function(){
		$("#homepage-slider ul").responsiveSlides({
			auto: true,             // Boolean: Animate automatically, true or false
			speed: 900,            // Integer: Speed of the transition, in milliseconds
			timeout: 4000,          // Integer: Time between slide transitions, in milliseconds
			pager: true,           // Boolean: Show pager, true or false
			nav: true,             // Boolean: Show navigation, true or false
			random: false,          // Boolean: Randomize the order of the slides, true or false
			pause: false,           // Boolean: Pause on hover, true or false
			pauseControls: true,    // Boolean: Pause when hovering controls, true or false
			prevText: "<",   // String: Text for the "previous" button
			nextText: ">",       // String: Text for the "next" button
			maxwidth: "",           // Integer: Max-width of the slideshow, in pixels
			navContainer: ".caption",       // Selector: Where controls should be appended to, default is after the 'ul'
			manualControls: ".controls",     // Selector: Declare custom pager navigation
			namespace: "rslides",   // String: Change the default namespace used
			before: function(){},   // Function: Before callback
			after: function(){}     // Function: After callback
		});
	});



	$('.add-registrants-online').on('click', function() {
		var og = Number($('.ff').val());
		var qty = Number($('#qq').val());
		var total = Number($('#tt').val());
		$('#qq').val(qty+1);
		$('#tt').val(total+og);
		var count = $('.parti').length;
		count+1;
		$('.participants-list').append('<fieldset class="parti"><legend>Participant Information</legend><div class="field"><label>First Name</label><input type="text" name="FirstName['+count+']" required><b style="color: red"> *</b></div><div class="field"><label>Last Name</label><input type="text" name="LastName['+count+']" required=""><b style="color: red"> *</b></div><div class="field"><label>Name on badge</label><input type="text" name="nameonbadge['+count+']"></div><div class="field"><label>Title</label><input type="text" name="title['+count+']"></div><div class="field"><label>Department</label><input type="text" name="department['+count+']"></div><div class="field"><label>Phone</label><input type="text" name="phone['+count+']" required=""></div><div class="field"><label>Fax</label><input type="text" name="fax['+count+']"></div><div class="field"><label>Email</label><input type="text" name="email['+count+']" required=""><b style="color: red"> *</b></div><div class="field"><label>Pick Start Date</label><input type="date" name="online['+count+']" placeholder="01/01/2017" style="border: 1px solid #000" required=""><b style="color: red"> *</b></div><button type="button" class="rmove-registrants">Remove</button></fieldset>');
	});

	$('.add-registrants').on('click', function() {
		var og = Number($('.ff').val());
		var qty = Number($('#qq').val());
		var total = Number($('#tt').val());
		$('#qq').val(qty+1);
		$('#tt').val(total+og);
		var count = $('.parti').length;
		count+1;
		$('.participants-list').append('<fieldset class="parti"><legend>Participant Information</legend><div class="field"><label>First Name</label><input type="text" name="FirstName['+count+']" required=""><b style="color: red"> *</b></div><div class="field"><label>Last Name</label><input type="text" name="LastName['+count+']" required=""><b style="color: red"> *</b></div><div class="field"><label>Name on badge</label><input type="text" name="nameonbadge['+count+']"></div><div class="field"><label>Title</label><input type="text" name="title['+count+']"></div><div class="field"><label>Department</label><input type="text" name="department['+count+']"></div><div class="field"><label>Phone</label><input type="text" name="phone['+count+']" required=""><b style="color: red"> *</b></div><div class="field"><label>Fax</label><input type="text" name="fax['+count+']"></div><div class="field"><label>Email</label><input type="text" name="email['+count+']" required=""><b style="color: red"> *</b></div><button type="button" class="rmove-registrants">Remove</button></fieldset>');
	});

	$(document).on('click', '.rmove-registrants', function() {
		var og = Number($('.ff').val());
		var qty = Number($('#qq').val());
		var total = Number($('#tt').val());
		var count = $('.parti').length;
		$('#qq').val(qty-1);
		$('#tt').val(total-og);
		$(this).parent().remove();
	});


});



	function reloop(num) {
		for (i = 0; i < num; i++) {
			var count = $('.parti').length;
			count+1;
			$('.participants-list').append('<fieldset class="parti"><legend>Participant Information</legend><div class="field"><label>First Name</label><input type="text" name="FirstName['+count+']" required=""></div><div class="field"><label>Last Name</label><input type="text" name="LastName['+count+']" required=""></div><div class="field"><label>Name on badge</label><input type="text" name="nameonbadge['+count+']"></div><div class="field"><label>Title</label><input type="text" name="title['+count+']"></div><div class="field"><label>Department</label><input type="text" name="department['+count+']"></div><div class="field"><label>Phone</label><input type="text" name="phone['+count+']" required=""></div><div class="field"><label>Fax</label><input type="text" name="fax['+count+']"></div><div class="field"><label>Email</label><input type="text" name="email['+count+']" required=""></div><button type="button" class="rmove-registrants">Remove</button></fieldset>');
		}
	}
